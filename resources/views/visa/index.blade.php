@extends('layouts.app')

@section('title', 'Second Visa Form')

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <h2>Confirm Employee Details</h2>
            <form method="post" action="display/<?php echo $id; ?>" class="form-horizontal" target="outputForm">
                <input type="hidden" name="page" value="sendmail" />
                <input type="hidden" name="uid" value="<?php echo $contactUID; ?>" />
                <p>Please confirm the employee's name and email address:</p>
                <div class="form-group">
                    <label for="emp_id" class="control-label">Employee Number:</label>
                    <input type="text" class="form-control" name="emp_id" id="emp_id" value="<?php echo $empl_id; ?>" />
                </div>
                <div class="form-group">
                    <label for="emp_name" class="control-label">Employee Name:</label>
                    <input type="text" class="form-control"  name="emp_name" id="emp_name" value="<?php echo $contactName; ?>" />
                </div>
                <div class="form-group">
                    <label for="emp_mail" class="control-label">Employee Mail:</label>
                    <input type="text" class="form-control"  name="emp_mail" id="emp_mail" value="<?php echo $contactEmail; ?>" />
                </div>
                <div class="form-group">
                    <label for="start" class="control-label">Start:</label>
                    <input type="date" class="form-control"  name="start" id="start" value="<?php echo $startdate; ?>"/>
                </div>
                <div class="form-group">
                    <label for="finish" class="control-label">Finish:</label>
                    <input type="date" class="form-control"  name="finish" id="finish" value="<?php echo $enddate; ?>"/>
                </div>
                <div class="form-group">
                    <label for="total_days" class="control-label">Days:</label>
                    <input type="text" class="form-control"  name="total_days" id="total_days" value="<?php echo $totalDays; ?>"/>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit" id="updateForm">Update</button>
                    <button class="btn btn-warning" type="button" id="sendform">Send Form</button>
                </div>
            </form>
        </div>
        <div class="col-sm-8">
            <iframe id="outputForm" name="outputForm" width="100%" height="700px"></iframe>
        </div>
    </div>
@endsection