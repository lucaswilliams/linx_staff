@extends('layouts.app')
<?php
    $user = Auth::user();
    if(str_contains(url('/'), 'prototype.ddns.info')) {
        $reportpath = 'http://prototype.ddns.info/linx/staff/public/reports/';
    } else {
        $reportpath = 'http://reports.linxemployment.com.au/';
    }
?>

@section('title', 'Dashboard')

@section('header')
    <h1>Reports List</h1>
    <p>This is a listing of the reports that are available to you.</p>
@stop

@section('content')
    <div class="dashboard-tile">
        <table width="100%">
            <tr>
                <th width="35%">Name</th>
                <th width="65%">Description</th>
            </tr>
            <?php
                foreach($reports as $report) {
                	if($report->report_url != null) {
                		$link = $report->report_url;
					} else {
						$fileparts = explode(DIRECTORY_SEPARATOR, $report->report_filename);
						$filebits = array_slice($fileparts, -2);
						$projectname = $filebits[0];
						$filename = $filebits[1];
						$link = $reportpath.'run.php?execute_mode=PREPARE&project='.$projectname.'&xmlin='.$filename.'&project_password=d6ee2MqUHu&user_id='.$user->id;
                    }
            ?>
            <tr>
                <td><a href="<?php echo $link; ?>"><?php echo $report->report_name; ?></a></td>
                <td><?php echo $report->report_description; ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
@endsection