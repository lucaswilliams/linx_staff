@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Daily Report Parameters</h1>
@stop

@section('content')
    <form method="GET">
        <div class="dashboard-tile">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Start Date:</label>
                    <div class="col-sm-4">
                        <input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                    <label class="col-sm-2 control-label">Job:</label>
                    <div class="col-sm-4">
                        <select name="job" class="form-control">
                            <option value=""></option>
                            <?php foreach($jobs as $jobs) { ?>
                            <option value="<?php echo $jobs->id; ?>"><?php echo $jobs->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-tile text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Run Report</button>
        </div>
    </form>
@stop