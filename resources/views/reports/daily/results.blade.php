@extends('layouts.print')

@section('title', 'Dashboard')

@section('content')
    <?php
        if($recs != null) {
            foreach($recs as $client => $jobs) {
                echo '<div class="page">';
                echo '<h2>'.$client.'</h2>
                    <p>'.date('d/m/Y', strtotime($_GET['start_date'])).' - '.date('d/m/Y', strtotime($_GET['start_date'].' + 6 days')).'</p>';
                foreach($jobs as $job => $workers) {
                    echo '<table width="100%" cellpadding="0" cellspacing="0">
                            <tr><th colspan="4" align="left"><h3>'.$job.'</h3></th></tr>
                            <tr>
                                <th align="left" width="25%">Worker</th>
                                <th align="left" width="25%">Task</th>
                                <th width="25%">Worker Amounts</th>
                                <th width="25%">Supervisor Amounts</th>
                            </tr>';
                    $total_units = array();
                    foreach($workers as $worker) {
                        $workerD = $worker['details'];
                        if(isset($worker['shifts'])) {
                            foreach($worker['shifts'] as $rate => $shifts) {
                                if(!isset($total_units[$rate])) { $total_units[$rate] = array('user' => 0, 'super' => 0); }
                                echo '<tr>';
                                echo '<td>'.$workerD['given_name'].' '.$workerD['surname'].' ('.$workerD['external_id'] .')</td>';
                                echo '<td>'.$rate.'</td>';
                                /*if($shifts['payment_type'] == 0) {
                                    echo '<td align="right">
                                        <table width="100%">
                                            <tr>
                                                <td>'.substr($shifts['user']['start'], 11).'</td>
                                                <td>'.substr($shifts['user']['end'], 11).'</td>
                                                <td>'.$shifts['user']['break'].'</td>
                                            </tr>
                                        </table>
                                    </td>';
                                    echo '<td align="right">
                                        <table width="100%">
                                            <tr>
                                                <td>'.substr($shifts['super']['start'], 11).'</td>
                                                <td>'.substr($shifts['super']['end'], 11).'</td>
                                                <td>'.$shifts['super']['break'].'</td>
                                            </tr>
                                        </table>
                                    </td>';
                                } else {*/
                                    echo '<td align="right">'.$shifts['user']['units'].'</td>';
                                    echo '<td align="right">'.$shifts['super']['units'].'</td>';
                                    $total_units[$rate]['user'] += $shifts['user']['units'];
                                    $total_units[$rate]['super'] += $shifts['super']['units'];
                                //}
                                echo '</tr>';
                            }
                        }
                    }
                    echo '</table>';

                    echo '<table width="100%" cellpadding="0" cellspacing="0">';
                    echo '<tr><th colspan="3" align="left"><h4>Summary</h4></th></tr>';

                    echo '<tr>
                        <th align="left" width="50%">Rate</th>
                        <th align="right" width="25%">Worker</th>
                        <th align="right" width="25%">Supervisor</th>
                    </tr>';
                    foreach($total_units as $rate => $totals) {
                        echo '<tr>
                            <td>'.$rate.'</td>
                            <td align="right">'.$totals['user'].'</td>
                            <td align="right">'.$totals['super'].'</td>
                        </tr>';
                    }
                    echo '</table>';
                echo '</div>';
                }
            }
        } else {
            echo '<p>There were no timesheets entered for '.date('d/m/Y', strtotime($_GET['start_date'])).'.</p><p>Please go back and try a different start date</p>';
        }
    ?>
@stop