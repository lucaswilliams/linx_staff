@extends('layouts.app')

@section('title', 'Reports List')

@section('header')
    <h1>Reports List</h1>
    <p>This is a listing of the reports that are available in the system, and that have security assigned to them.</p>
    <p>Before attempting to assign permissions, please check that the report file has been configured in Reportico (check with Lucas if you don't know what that means).</p>
@endsection

@section('content')
    <div class="dashboard-tile text-right">
        <a href="http://reports.linxemployment.com.au/run.php?project=admin&execute_mode=ADMIN&clear_session=1&user_id={{$user->id}}" class="btn btn-primary"><i class="fa fa-cogs"></i> Report builder</a>
        <a href="{{ url('/settings/reporting/add') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
    </div>
    <div class="dashboard-tile">
        <table width="100%">
            <tr>
                <th width="200px">Name</th>
                <th>Description</th>
                <th width="120px">Actions</th>
            </tr>
            <?php foreach($reports as $report) { ?>
            <tr>
                <td><?php echo $report->report_name; ?></td>
                <td><?php echo $report->report_description; ?></td>
                <td class="text-right">
                    <a class="btn btn-warning" href="{{ url('/settings/reporting/'.$report->report_id) }}">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a class="btn btn-danger" href="{{ url('/settings/reporting/delete/'.$report->report_id) }}">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </table>
    </div>
@endsection