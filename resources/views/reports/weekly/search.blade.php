@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Weekly Report Parameters</h1>
@stop

@section('content')
    <form method="GET">
        <div class="dashboard-tile">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Week Commencing:</label>
                    <div class="col-sm-4">
                        <input type="date" name="start_date" class="form-control week_start" value="<?php echo date('Y-m-d', strtotime('last Monday')); ?>">
                    </div>

                    <label class="col-sm-2 control-label">Job:</label>
                    <div class="col-sm-4">
                        <select name="job" class="form-control">
                            <option value=""></option>
                            <?php foreach($jobs as $jobs) { ?>
                            <option value="<?php echo $jobs->id; ?>"><?php echo $jobs->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="dashboard-tile text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Run Report</button>
        </div>
    </form>
@stop