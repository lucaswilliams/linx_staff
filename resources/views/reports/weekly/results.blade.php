@extends('layouts.printls')

@section('title', 'Dashboard')

@section('content')
    <table class="table table-striped">
		<tr>
			<th>Worker</th>
			<?php for($i = 0; $i < 7; $i++) {
				$time = strtotime($_GET['start_date'].' +'.$i.' days');
				echo '<th>'.date('l', $time).'<br />'.date('d/m/Y', $time).'</th>';
			} ?>
			<th>Total</th>
		</tr>
		<?php foreach($data as $row) {
			echo '<tr><td>'.$row['header'];
			$total = 0;
			for($i = 0; $i < 7; $i++) {
				$time = date('Y-m-d', strtotime($_GET['start_date'].' +'.$i.' days'));
				echo '<td>';
				if(isset($row[$time])) {
					echo number_format($row[$time], 2);
					$total += $row[$time];
				}
				echo '</td>';
			}
			echo '<td>'.number_format($total, 2).'</td>';
		} ?>
	</table>
@stop