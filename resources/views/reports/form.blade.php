<?php
    if($report != null) {
        $filename = $report->report_filename;
    } else {
        $filename = old('report_filename');
    }
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1><?php echo (isset($report->report_id)) ? 'Edit' : 'Add' ; ?> Report</h1>
    <p>Set the options, and applicable users for this report.</p>
@endsection

@section('content')
<form method="POST" action="{{ url('/settings/reporting') }}">
    <div class="dashboard-tile">
        <h2>Report Details</h2>
        @include('input.hidden', ['name' => 'report_id', 'data' => $report])

        @include('input.text', ['name' => 'report_name', 'data' => $report, 'required' => true, 'label' => 'Report Name'])

        @include('input.textarea', ['name' => 'report_description', 'data' => $report, 'required' => true, 'label' => 'Report description'])

        <div class="form-group {{ $errors->has('report_filename') ? ' has-error' : '' }}">
            <label class="control-label" for="report_filename">Report filename<span class="required">*</span>:</label>
            <select name="report_filename" id="report_filename" class="form-control">
                <?php
                    $selected = '';
                    if(isset($reports) && $reports != null){
                foreach($reports as $project => $files) { ?>
                    <optgroup label="<?php echo $project; ?>">
                        <?php if(isset($files) && $files != null) { foreach($files as $file) {
                            $selected = '';
                            if(strcmp($file['filename'], $filename) == 0) { $selected = ' selected'; }
                        ?>
                            <option value="<?php echo $file['filename']; ?>"<?php echo $selected; ?>><?php echo $file['report']; ?></option>
                        <?php }} ?>
                    </optgroup>
                <?php }} ?>
            </select>
            @if ($errors->has('report_filename'))
                <div class="alert alert-danger alert-inline">
                    {{ $errors->first('report_filename') }}
                </div>
            @endif
        </div>

		@include('input.text', ['name' => 'report_url', 'data' => $report, 'required' => false, 'label' => 'Report URL'])
    </div>

    <div class="dashboard-tile">
        <h2>User Details</h2>
        <table width="100%">
            <tr>
                <th width="80%">User</th>
                <th width="20%">Access</th>
            </tr>
            <?php foreach($users as $user) {
                $tick = false;
                foreach($report_users as $ruser) {
                    if($ruser->user_id == $user->id) {
                        $tick = true;
                    }
                }
                if($user->level == 3) {
                    $tick = true;
                }
                ?>
                <tr>
                    <td><?php echo $user->given_name.' '.$user->surname; ?></td>
                    <td><input type="checkbox" name="users[]" <?php echo ($tick) ? 'checked ' : ''; ?>value="<?php echo $user->id; ?>" <?php echo ($user->level == 3 ? 'disabled' : '') ?>></td>
                </tr>
            <?php } ?>
        </table>
    </div>

    <div class="dashboard-tile text-center">
        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Details</button>
    </div>
</form>
@endsection