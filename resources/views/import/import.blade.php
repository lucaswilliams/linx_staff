@extends('layouts.app')

@section('title', 'Timesheet Import')

@section('header')
    <h1>Timesheet Importer</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <p>Spreadsheet upload completed.</p>
            <p><a href="/timesheets/import/{{$batch}}">Please click here to import the records</a></p>              </div>
    </div>
@stop
