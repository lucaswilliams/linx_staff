@extends('layouts.app')

@section('title', 'Import Success')

@section('header')
    <h1>Success</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <p>The batch has been imported successfully.</p>
            <?php if(is_array($importerrors)) {
                 echo '<p>There were some errors though: </p><ul>';
                 foreach($importerrors as $error) {
                     echo '<li>'.$error.'</li>';
                 }
                 echo '</ul>';
            }?>
        </div>
    </div>
@stop
