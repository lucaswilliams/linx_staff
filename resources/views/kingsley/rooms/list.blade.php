@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Rooms</h1>
            <p>Here's a list of all the rooms in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="rooms/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Capacity</th>
                        <th colspan="2">Actions</th>
                    </tr>
                    <?php foreach($rooms as $room) { ?>
                    <tr>
                        <td>
                            <a href="rooms/<?php echo $room->room_id; ?>"><?php echo $room->room_name; ?></a>
                        </td>
                        <td><?php echo $room->roomtype_name; ?></td>
                        <td><?php echo $room->room_capacity; ?></td>
                        <td>
                            <a href="rooms/<?php echo $room->room_id; ?>"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                        <td>
                            <a href="rooms/delete/<?php echo $room->room_id; ?>"><i class="fa fa-trash-o"></i> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop