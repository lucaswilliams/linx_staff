<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $room->room_name;
}

if(old('roomtype_id') > 0) {
    $roomtype_id = old('roomtype_id');
} else {
    $roomtype_id = $room->roomtype_id;
}

if(strlen(old('capacity')) > 0) {
    $capacity = old('capacity');
} else {
    $capacity = $room->room_capacity;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            if(strlen($room->room_name) > 0) {
                echo $room->room_name;
            } else {
                echo 'Add new room';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('kingsley/rooms') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="room_id" value="<?php echo $room->room_id; ?>">

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="roomtype_id">Room Type:</label>
                        <select name="roomtype_id" class="form-control auto-width custom-select select-s_external_id select2-offscreen" id="provider_id">
                            <?php foreach($roomtypes as $roomtype) { ?>
                            <option value="<?php echo $roomtype->roomtype_id; ?>" <?php if($roomtype->roomtype_id == $roomtype_id) { echo 'selected'; } ?>><?php echo $roomtype->roomtype_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group" id="bed_name_container">
                        <button type="button" class="btn btn-success" id="add_bed"><i class="fa fa-plus"></i> Add new</button>
                        <label for="bed_names">Bed Names:</label>
                        <?php if(count($beds) > 0) {
                            foreach($beds as $bed) { ?>
                                <input type="hidden" name="bed_ids[]" value="<?php echo $bed->beds_id; ?>" />
                                <input type="text" name="bed_names[]" id="bed_names" value="<?php echo $bed->beds_name; ?>" class="form-control">
                            <?php }
                        } ?>
                        <?php
                        foreach ($errors->get('capacity') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop