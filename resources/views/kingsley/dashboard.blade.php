@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Kingsley Accomodation Dashboard</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-book"></i> <a href="{{ URL::to('kingsley/bookings') }}">Bookings</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-wrench"></i> <a href="{{ URL::to('kingsley/roomtypes') }}">Room Types</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-bed"></i> <a href="{{ URL::to('kingsley/rooms') }}">Rooms</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-credit-card"></i> <a href="{{ URL::to('kingsley/payments') }}">Payments</a></h1>
            </div>
        </div>
    </div>
@stop