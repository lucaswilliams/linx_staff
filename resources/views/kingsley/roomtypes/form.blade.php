<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $roomtype->roomtype_name;
}

if(strlen(old('image')) > 0) {
    $image = old('image');
} else {
    $image = $roomtype->roomtype_image;
}

if(strlen(old('layout_image')) > 0) {
    $layout_image = old('layout_image');
} else {
    $layout_image = $roomtype->roomtype_position_image;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            if(strlen($roomtype->roomtype_name) > 0) {
                echo $roomtype->roomtype_name;
            } else {
                echo 'Add new room type';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('kingsley/roomtypes') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="roomtype_id" value="<?php echo $roomtype->roomtype_id; ?>">

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="image">Image:</label>
                        <input type="text" name="image" id="image" value="<?php echo $image; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('image') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="layout_image">Layout Image:</label>
                        <input type="text" name="layout_image" id="layout_image" value="<?php echo $image; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('layout_image') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop