@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Room Types</h1>
            <p>Here's a list of all the room types in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="roomtypes/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th colspan="2">Actions</th>
                    </tr>
                    <?php foreach($roomtypes as $roomtype) { ?>
                    <tr>
                        <td>
                            <a href="roomtypes/<?php echo $roomtype->roomtype_id; ?>"><?php echo $roomtype->roomtype_name; ?></a>
                        </td>
                        <td>
                            <a href="roomtypes/<?php echo $roomtype->roomtype_id; ?>"><i class="fa fa-pencil"></i> Edit</a>
                        </td>
                        <td>
                            <a href="roomtypes/delete/<?php echo $roomtype->roomtype_id; ?>"><i class="fa fa-trash-o"></i> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop