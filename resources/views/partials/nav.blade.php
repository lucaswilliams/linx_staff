<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('') }}"><img src="{{ asset('/images/sm-logo.png') }}" alt="Linx Employment"></a>
        </div>

        <li class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="{{ (Request::is('/') ? 'active' : '') }}">
                    <a href="{{ URL::to('') }}" data-toggle="tooltip" data-placement="bottom" title="Home"><i class="fa fa-2 fa-home"></i><span class="visible-xs-inline"> Home</span></a>
                </li>
                <?php if(Auth::check()) { ?>
                <?php if(Auth::user()->confirmed >= 1) { ?>
                <li class="{{ (Request::is('profile') ? 'active' : '') }}">
                    <a href="{{ URL::to('profile') }}" data-toggle="tooltip" data-placement="bottom" title="My Details"><i class="fa fa-2 fa-user"></i><span class="visible-xs-inline"> My Details</span></a>
                </li>
                <?php if(Auth::user()->level > 0) {  /* Can't see timesheets if you aren't staff. */?>
                <li class="{{ (Request::is('timesheets') ? 'active' : '') }}">
                    <a href="{{ URL::to('timesheets') }}" data-toggle="tooltip" data-placement="bottom" title="My Timesheets" ><i class="fa fa-2 fa-clock-o"></i><span class="visible-xs-inline"> My Timesheets</span></a>
                </li>
                <?php } ?>
                <?php if(Auth::user()->level > 1) { ?>
					<li class="{{ (Request::is('staff') ? 'active' : '') }}">
						<a href="{{ URL::to('staff') }}"><i class="fa fa-2 fa-users"></i><span class="visible-xs-inline"> Staff List</span></a>
					</li>

					<li class="{{ (Request::is('client') ? 'active' : '') }}">
						<a href="{{ URL::to('client') }}" data-toggle="tooltip" data-placement="bottom" title="Client List"><i class="fa fa-2 fa-building"></i><span class="visible-xs-inline"> Client List</span></a>
					</li>
					<li class="{{ (Request::is('jobs') ? 'active' : '') }}">
						<a href="{{ URL::to('jobs') }}" data-toggle="tooltip" data-placement="bottom" title="Jobs List"><i class="fa fa-2 fa-briefcase"></i><span class="visible-xs-inline"> Jobs List</span></a>
					</li>
					<li class="{{ (Request::is('timesheets/manage') ? 'active' : '') }}">
						<a href="{{ URL::to('timesheets/manage') }}" data-toggle="tooltip" data-placement="bottom" title="Manage timesheets"><i class="fa fa-hourglass-half"></i><span class="visible-xs-inline"> Manage timesheets</span></a>
					</li>
                <?php } ?>
                    <li class="{{ Request::is('payslips') ? 'active' : '' }}">
                        <a href="{{ URL::to('payslips') }}" data-toggle="tooltip" data-placement="bottom" title="Payslips"><i class="fa fa-money"></i><span class="visible-xs-inline"> Payslips</span></a>
                    </li>
                <?php if((Auth::user()->is_supervisor == 1) || (Auth::user()->level > 1)) { ?>
                <li class="{{ Request::is('invoices') ? 'active' : '' }}">
                    <a href="{{ URL::to('invoices') }}" data-toggle="tooltip" data-placement="bottom" title="Invoices"><i class="fa fa-usd"></i><span class="visible-xs-inline"> Invoices</span></a>
                </li>
                <li class="{{ (Request::is('reporting') ? 'active' : '') }}">
                    <a href="{{ URL::to('reporting') }}" data-toggle="tooltip" data-placement="bottom" title="Reporting"><i class="fa fa-2 fa-line-chart"></i><span class="visible-xs-inline"> Reporting</span></a>
                </li>
                <?php } ?>
                <?php if(Auth::user()->level > 1) { ?>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-placement="bottom" title="Accommodation"><i class="fa fa-2 fa-bed"></i><span class="visible-xs-inline"> Accommodation</span><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ URL::to('kingsley/bookings') }}"><i class="fa fa-book"></i> Bookings</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('kingsley/bookings/calendar') }}"><i class="fa fa-calendar"></i> Bookings</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('kingsley/roomtypes') }}"><i class="fa fa-wrench"></i> Room Types</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('kingsley/rooms') }}"><i class="fa fa-bed"></i> Rooms</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('kingsley/payments') }}"><i class="fa fa-credit-card"></i> Payments</a>
                        </li>
                    </ul>
                </li>-->
                <?php } ?>

                <?php if(Auth::user()->level == 3) { ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" data-placement="bottom" title="Settings"><i class="fa fa-2 fa-cogs"></i><span class="visible-xs-inline"> Settings</span><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ URL::to('settings/credentials') }}"><i class="fa fa-file-text-o"></i> Credentials</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('settings/payrates') }}"><i class="fa fa-usd"></i> Pay Rates</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('settings/super') }}"><i class="fa fa-balance-scale"></i> Super</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('settings/myob') }}"><i class="fa fa-money"></i> Integration</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('settings/inductions') }}"><i class="fa fa-external-link-square fa-rotate-180"></i> Inductions</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('settings/reporting') }}"><i class="fa fa-line-chart"></i> Report Settings</a>
                        </li>
                        <li>
                            <a href="{{ URL::to('settings/myob/import') }}"><i class="fa fa-download"></i> Import employees</a>
                        </li>
                    </ul>
                </li>
                <?php }
                }
                }?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ URL::to('help') }}" data-toggle="modal" data-target="#helpModal"><i class="fa fa-question-circle"></i> Help</a>
                </li>
                <?php if(Auth::check()) { ?>
                    <li>
                        <a href="{{ URL::to('changepw') }}"><i class="fa fa-key"></i> Change password</a>
                    </li>
                    <?php if(Auth::user()->isImpersonating()) { ?>
                    <li>
                        <a href="{{ URL::to('impersonate/stop') }}"><i class="fa fa-user-secret"></i> Stop being <?php echo Auth::user()->given_name; ?></a>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="{{ URL::to('logout') }}"><i class="fa fa-sign-out"></i> Log out (<?php echo Auth::user()->given_name; ?>)</a>
                    </li>
                <?php } ?>
                    <li>
                        <a id="notification-button" href="#" title="Notifications"><i class="fa fa-flag-o"></i><span id="notification-number"></span></a>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="{{ URL::to('login') }}"><i class="fa fa-sign-in"></i> Log in</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('register') }}"><i class="fa fa-user"></i> Register</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>
<div id="notification-panel" class="col-xs-12 col-sm-6 col-md-4">
    <div class="notification-header text-right">
        <a id="notification-read" class="btn btn-default">Mark as read</a>
    </div>
</div>