<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="{{ asset('images/ssl.png') }}" style="width: 150px; margin: 5px 0;">
                <p><a href="{{ asset('docs/linx_privacy.pdf') }}">Privacy Policy</a></p>
            </div>
            <div class="col-sm-6 text-right">
                &copy; 2015 - <?php if((int)date('Y') > 2015) { echo date('Y'); } ?>
                <a href="http://linxemployment.com.au">Linx Employment Tas Pty Ltd</a>
            </div>
        </div>
    </div>
</footer>
