@extends('layouts.app')

@section('title', 'Client List')

@section('header')
    <h1>Contact List for <?php echo $client->name; ?></h1>
@endsection

@section('content')
	<input type="hidden" id="client_id" value="<?php echo $client->id; ?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a data-toggle="modal" data-target="#contactModal" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="contact-table">
                    <tr>
                        <th>Name</th>
                        <th>Level</th>
                        <th width="110px">Actions</th>
                    </tr>
                    <?php
                        foreach($contacts as $contact) {
                    ?>
                    <tr>
                        <td><?php echo $contact->given_name; ?> <?php echo $contact->surname; ?></td>
                        <td><?php echo ($contact->level == 0 ? 'Supervisor' : 'Manager'); ?></td>
                        <td>
                            <a href="<?php echo url('client/contact/'.$contact->id.'/delete'); ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
							<?php if($contact->level == 1) { ?>
							<a href="<?php echo url('client/contact/'.$contact->id.'/supervisor'); ?>" class="btn btn-warning"><i class="fa fa-arrow-down"></i></a>
							<?php } else { ?>
                            <a href="<?php echo url('client/contact/'.$contact->id.'/manager'); ?>" class="btn btn-warning"><i class="fa fa-arrow-up"></i></a>
							<?php } ?>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

	@include('modals.contact')
@stop