<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $client->name;
}

if(strlen(old('abn')) > 0) {
    $abn = old('abn');
} else {
    $abn = $client->abn;
}

if(strlen(old('address')) > 0) {
    $address = old('address');
} else {
    $address = $client->address;
}

if(strlen(old('city')) > 0) {
    $city = old('city');
} else {
    $city = $client->city;
}

if(strlen(old('state')) > 0) {
    $state = old('state');
} else {
    $state = $client->state;
}

if(strlen(old('postcode')) > 0) {
    $postcode = old('postcode');
} else {
    $postcode = $client->postcode;
}

if(strlen(old('telephone')) > 0) {
    $telephone = old('telephone');
} else {
    $telephone = $client->telephone;
}

if(strlen(old('status')) > 0) {
    $status = old('status');
} else {
    $status = $client->status;
}

if(strlen(old('external_id')) > 0) {
    $external_id = old('external_id');
} else {
    $external_id = $client->external_id;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            if(strlen($client->name) > 0) {
                echo $client->name;
            } else {
                echo 'Add new client';
            }
        ?>
    </h1>
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).on('blur', '#name', function(e) {
            if($('#external_id').val() == '') {
                _parts = $('#name').val().split(' ');
                if(_parts.length > 1) {
                    _str = _parts[0].substr(0, 3) + _parts[1].substr(0,4);
                    $('#external_id').val(_str.toUpperCase());
                }
            }
        });

        $(document).on('click', '.add_email', function(e) {
            $('#clientemails').append('<div class="row">' +
                '<div class="col-xs-10 col-sm-11">' +
                '<input type="text" name="email[]" class="form-control">' +
                '</div>' +
                '<div class="col-xs-2 col-sm-1" style="padding: 0">' +
                '<a class="btn btn-danger del_email"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</div>');
        });

        $(document).on('click', '.del_email', function(e) {
            $(this).closest('.row').remove();
        });
    </script>
@endsection

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('client') }}">
            <div class="col-xs-12">
                <ul class="nav nav-tabs" id="profileTabs">
                    <li role="presentation" class="active">
                        <a href="#details" id="details-tab">Details</a>
                    </li>
                    <li role="presentation">
                        <a href="#jobs" id="jobs-tab">Jobs</a>
                    </li>
                </ul>
                <div role="tabpanel" class="tab-pane fade active in" id="details">
                    <div class="dashboard-tile">
                    {!! csrf_field() !!}
                        <input type="hidden" name="client_id" value="<?php echo $client->id; ?>">

                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('name') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="abn">ABN:</label>
                            <input type="text" name="abn" id="abn" value="<?php echo $abn; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('abn') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="address">Address:</label>
                            <input type="text" name="address" id="address" value="<?php echo $address; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('address') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="city">City:</label>
                            <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('city') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="state">State:</label>
                            <select name="state" id="state" class="form-control">
                                <option value=""></option>
                                <option value="ACT" <?php if(strtoupper($state) == 'ACT') { echo 'selected'; } ?>>Australian Capital Territory</option>
                                <option value="NSW" <?php if(strtoupper($state) == 'NSW') { echo 'selected'; } ?>>New South Wales</option>
                                <option value="NT" <?php if(strtoupper($state) == 'NT') { echo 'selected'; } ?>>Northern Territory</option>
                                <option value="QLD" <?php if(strtoupper($state) == 'QLD') { echo 'selected'; } ?>>Queensland</option>
                                <option value="SA" <?php if(strtoupper($state) == 'SA') { echo 'selected'; } ?>>South Australia</option>
                                <option value="TAS" <?php if(strtoupper($state) == 'TAS') { echo 'selected'; } ?>>Tasmania</option>
                                <option value="VIC" <?php if(strtoupper($state) == 'VIC') { echo 'selected'; } ?>>Victoria</option>
                                <option value="WA" <?php if(strtoupper($state) == 'WA') { echo 'selected'; } ?>>Western Australia</option>
                            </select>
                            <?php
                            foreach ($errors->get('state') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="postcode">Postcode:</label>
                            <input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('postcode') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-10 col-sm-11">
                                    <label for="email">Email address(es):</label>
                                </div>
                                <div class="col-xs-2 col-sm-1" style="padding: 0">
                                    <a class="btn btn-success add_email"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <div id="clientemails">
                            <?php
                                if(isset($emails)) {
                                //echo '<pre>'; var_dump($emails); echo '</pre>'; die();
                                foreach($emails as $email) {
                            ?>
                            <div class="row">
                                <div class="col-xs-10 col-sm-11">
                                    <input type="text" name="email[]" value="<?php echo $email->emai_address; ?>" class="form-control">
                                </div>
                                <div class="col-xs-2 col-sm-1" style="padding: 0">
                                    <a class="btn btn-danger del_email"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </div>
                            <?php
                                }}
                            ?>
                            </div>
                            <?php
                            foreach ($errors->get('email') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="telephone">Phone:</label>
                            <input type="text" name="telephone" id="telephone" value="<?php echo $telephone; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('telephone') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <?php if(Auth::user()->level > 1) { ?>
                        <div class="form-group">
                            <label for="external_id">MYOB Number:</label>
                            <input type="text" name="external_id" id="external_id" value="<?php echo $external_id; ?>" class="form-control">
                            <?php
                            foreach ($errors->get('external_id') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="status">Active</label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" <?php if($status == 1) { echo 'selected'; } ?>>Active</option>
                                <option value="0" <?php if($status == 0) { echo 'selected'; } ?>>Inactive</option>
                            </select>
                            <?php
                            foreach ($errors->get('status') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>
                        <?php } ?>

                        <input type="hidden" name="myob_uid" id="myob_uid" value="<?php if(isset($client->myob_uid)) { echo $client->myob_uid; } ?>">

                        <div class="form-group">
                            <label for="income_account">Income account for Invoices:</label>
                            <select name="income_account" id="income_account" class="search">
                                <option value="">(None selected)</option>
                                <?php
                                if(isset($accounts)) {
                                    foreach($accounts as $account) {
                                        $selected = '';
                                        if(strcmp($account->UID, $client->income_account) == 0) {
                                            $selected = ' selected';
                                        }
                                        echo '<option value="'.$account->UID.'"'.$selected.'>'.$account->Name.' ('.$account->DisplayID.')</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade active in" id="jobs">
                    <div class="dashboard-tile">
                        <a href="<?php echo url('/jobs/add'); ?>" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add new</a>
                        @include('jobs.listcontent')
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop