@extends('layouts.app')

@section('title', 'Client List')

@section('header')
    <h1>Client List</h1>
    <p>Here's a list of all the clients in the system, from which you can add, edit or delete them.</p>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="client/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Client Number</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                        foreach($clients as $client) {
                            if($client->status == 1) {
                                $active = 'Active';
                            } else {
                                $active = 'Inactive';
                            }
                    ?>
                    <tr>
                        <td>
                            <a href="client/<?php echo $client->id; ?>"><?php echo $client->name ?></a>
                        </td>
                        <td><?php echo $client->external_id; ?></td>
                        <td><?php echo $client->emai_address; ?></td>
                        <td><?php echo $client->telephone; ?></td>
                        <td><?php echo $active; ?></td>
                        <td width="150px">
                            <a href="client/<?php echo $client->id; ?>/contacts" class="btn btn-default"><i class="fa fa-user-circle-o"></i></a>
                            <a href="client/<?php echo $client->id; ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                            <a href="client/delete/<?php echo $client->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop