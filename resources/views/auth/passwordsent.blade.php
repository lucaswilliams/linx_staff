@extends('layouts.app')

@section('title', 'Forgotten Password')

@section('header')
    <h1>Forgotten Password</h1>
    <p>Your password reset email has been sent.  Please check your email (including spam and junk folders) and follow the instructions contained within to reset your password.</p>
@stop

@section('content')

@stop