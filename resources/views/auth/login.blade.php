@extends('layouts.app')

@section('title', 'Login')

@section('header')
<h1>Linx Employment<br />Timesheet Management System</h1>
<p>Please log in with your details to enter the times that you have worked.</p>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="login" class="dashboard-tile col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" id="username" value="{{ old('username') }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>

            <div class="form-group">
                <label>
                    <input type="checkbox" name="remember"> Remember Me
                </label>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i> Login</button>
                <p><a href="{{ URL::to('password') }}">Forgot your details?</a></p>
            </div>
        </form>
    </div>
@stop