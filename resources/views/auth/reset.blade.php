@extends('layouts.app')

@section('title', 'Forgotten Password')

@section('header')
    <h1>Reset Password</h1>
    <p>Please fill out the following form to finalise the reset of your password.  Once completed, you will be logged in.  Take note of your username here, as this might not be your email address; you will need this for logging in.</p>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ url('/password/reset') }}" class="dashboard-tile col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>

            <div class="form-group">
                <label for="password_confirmation">Password (again):</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-succcess">Reset Password</button>
            </div>
        </form>
    </div>
@stop