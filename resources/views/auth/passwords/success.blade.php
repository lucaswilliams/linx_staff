@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">

				<h1>Reset Password</h1>

				<div class="dashboard-tile">
					<div class="success">
						<h2>Success!</h2>
						<p>Your password has been successfully reset.</p>
						<p>Your username is <strong><?php echo $user['username']; ?></strong></p>
						<p>Your password is <strong><?php echo $user['password']; ?></strong></p>
						<p><a href="<?php echo url('/login'); ?>">Click here to log in</a>.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection