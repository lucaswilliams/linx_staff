@extends('emails.layout.hero')

@section('content')
    <table class="row">
        <tr>
            <td class="wrapper last">

                <table class="twelve columns">
                    <tr>
                        <td>
                            <p>To reset your password on the Linx Employment Timesheet Management System, please click here: {{ url('password/reset/'.$token) }}</p>
                        </td>
                        <td class="expander"></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    @include('emails.layout.contactus', ['email' => false])
@stop