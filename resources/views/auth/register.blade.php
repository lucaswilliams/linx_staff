<?php
$yesno = array (
        (object)array('key' => '1', 'value' => 'Yes'),
        (object)array('key' => '0', 'value' => 'No')
);
?>

@extends('layouts.app')

@section('title', 'Register')

@section('header')
    <h1>Registration</h1>
    <p>Please enter your details below to sign up for work with Linx Employment.  Through further pages, we will be asking you more questions to see what you're suited for.</p>
@stop

@section('content')
<div class="row">
    <form method="POST" action="register" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <div class="col-xs-12">
            <div class="dashboard-tile">
                <h2>Your details</h2>
                <div class="row">
                    @include('input.text', ['data' => null, 'label' => 'First/given name', 'name' => 'given_name', 'required' => true, 'class' => 'col-sm-6'])

                    @include('input.text', ['data' => null, 'label' => 'Surname', 'name' => 'surname', 'required' => true, 'class' => 'col-sm-6'])
                    @include('input.text', ['data' => null, 'label' => 'Preferred name', 'name' => 'preferred', 'required' => false, 'class' => 'col-sm-6'])

                    @include('input.dob', ['data' => null, 'label' => 'Date of Birth', 'required' => true, 'name' => 'date_of_birth', 'class' => 'col-sm-6'])

                    <?php $genders = array (
                            (object)array('key' => '0', 'value' => 'Not specified'),
                            (object)array('key' => '1', 'value' => 'Male'),
                            (object)array('key' => '2', 'value' => 'Female')
                    );
                    ?>
                    @include('input.select', ['data' => null, 'label' => 'Gender', 'name' => 'gender', 'source' => $genders, 'required' => true, 'class' => 'col-sm-6'])
                    <div class="form-group col-sm-6">
                        <label for="title">Title:</label>
                        <select name="title" id="title" class="form-control">
                            <option value=""></option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Miss">Miss</option>
                            <option value="Ms">Ms</option>
                        </select>
                    </div>

                    @include('input.email', ['data' => null, 'label' => 'Email', 'name' => 'email', 'required' => true, 'class' => 'col-sm-12'])

                    @include('input.text', ['data' => null, 'label' => 'Mobile Phone', 'name' => 'mobilephone', 'required' => false, 'class' => 'col-sm-6'])

                    @include('input.text', ['data' => null, 'label' => 'Landline', 'name' => 'telephone', 'required' => false, 'class' => 'col-sm-6'])
                </div>
            </div>

            <div class="dashboard-tile">
                <h2>Address Details</h2>
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label for="no_fixed_address">
                            <input type="checkbox" id="no_fixed_address" name="no_address" value="1"> I don't have an Australian address
                        </label>
                    </div>

                    @include('input.text', ['data' => null, 'label' => 'Address', 'name' => 'address', 'required' => true, 'class' => 'col-xs-12'])

                    @include('input.text', ['data' => null, 'label' => 'City', 'name' => 'city', 'required' => true, 'class' => 'col-sm-6'])

                    <?php
                    $states = array(
                        (object)array('key' => 'ACT', 'value' => 'Australian Capital Territory'),
                        (object)array('key' => 'NSW', 'value' => 'New South Wales'),
                        (object)array('key' => 'NT', 'value' => 'Northern Territory'),
                        (object)array('key' => 'QLD', 'value' => 'Queensland'),
                        (object)array('key' => 'SA', 'value' => 'South Australia'),
                        (object)array('key' => 'TAS', 'value' => 'Tasmania'),
                        (object)array('key' => 'VIC', 'value' => 'Victoria'),
                        (object)array('key' => 'WA', 'value' => 'Western Australia'),
                        (object)array('key' => 'OS', 'value' => 'Other')
                    );
                    ?>
                    @include('input.select', ['data' => null, 'label' => 'State', 'name' => 'state', 'required' => true, 'source' => $states, 'class' => 'col-sm-3'])

                    @include('input.text', ['data' => null, 'label' => 'Postcode', 'name' => 'postcode', 'required' => true, 'class' => 'col-sm-3'])
                </div>
            </div>

            <div class="dashboard-tile">
                <h2>Login Details</h2>
                <p>Please enter a username and password.  You will need to remember these details in order to complete your registration, and fill in future timesheets.</p>

                @include('input.text', ['data' => null, 'required' => true, 'label' => 'Username', 'name' => 'username'])
                @include('input.password', ['data' => null, 'required' => true, 'label' => 'Password', 'name' => 'password'])
                @include('input.password', ['data' => null, 'required' => true, 'label' => 'Confirm Password', 'name' => 'password_confirmation'])
            </div>
        </div>

		<?php if(count($campaign) > 0) { ?>
		<div class="col-xs-12">
			<div class="dashboard-tile">
				<h2><?php echo $campaign['name']; ?></h2>
                <p><?php echo $campaign['desc']; ?></p>
				<input type="hidden" name="campaign_id" value="<?php echo $campaign['id']; ?>">
				<?php foreach($campaign['questions'] as $camp) {
				    echo '<div class="form-group campaign';
                    if($camp->caqu_conditional == 1) {
                        echo ' conditional" data-field="caqu_'.$camp->caqu_conditional_question.'" data-value="'.$camp->caqu_conditional_value.'';
                    }
                    echo '">';
					switch($camp->caqu_type) {
						case 2: //radio

							break;
						case 3: //select
                            echo '<label for="caqu_'.$camp->caqu_id.'">'.$camp->caqu_question.'</label>';
							echo '<select class="form-control" name="caqu['.$camp->caqu_id.']" id="caqu_'.$camp->caqu_id.'">';
							echo '<option value="0"></option>';
							foreach($camp->options as $option) {
								echo '<option value="'.$option->cqop_id.'">'.$option->cqop_value.'</option>';
							}
							echo '</select>';
							break;
						case 4: //multi-select
							echo '<label for="caqu_'.$camp->caqu_id.'">'.$camp->caqu_question.'</label>';
							echo '<select class="form-control search" name="caqu['.$camp->caqu_id.']" id="caqu_'.$camp->caqu_id.'" multiple>';
							echo '<option value="0"></option>';
							foreach($camp->options as $option) {
								echo '<option value="'.$option->cqop_id.'">'.$option->cqop_value.'</option>';
							}
							echo '</select>';
							break;
						case 1: //input type=text
						default: //input type=text
							echo '<label for="caqu_'.$camp->caqu_id.'">'.$camp->caqu_question.'</label>';
							echo '<input class="form-control" type="text" name="caqu['.$camp->caqu_id.']" id="caqu_'.$camp->caqu_id.'">';
							break;
					}
					echo '</div>';
				} ?>

                <?php if(isset($_GET['camp']) && $_GET['camp'] == 'REID2019') { ?>
                <div id="reid-q">
                    <div class="form-group" id="reid-q-1">
                        <label for="reid-1">Have you worked for Reid Fruits before?</label>
                        <select name="reid-1" id="reid-1" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-1-1">
                        <label for="reid-1-1">What role/position(s) did you perform?</label>
                        <select name="reid-1-1" id="reid-1-1" class="form-control" multiple="multiple">
                            <option>Picker</option>
                            <option>Supervisor</option>
                            <option>Quality Control</option>
                            <option>Tractor/Forklift</option>
                            <option>Packer</option>
                            <option>Other</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-1-2">
                        <label for="reid-1-2">Please enter other roles/positions</label>
                        <input type="text" name="reid-1-2" id="reid-1-2" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-1-3">
                        <label for="reid-1-3">What is the most recent year you worked for Reid Fruits?</label>
                        <input type="text" name="reid-1-3" id="reid-1-3" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-2">
                        <label for="reid-2">Do you have prior experience working during a cherry harvest?</label>
                        <select name="reid-2" id="reid-2" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-2-1">
                        <label for="reid-2-1">What role/position(s) did you perform?</label>
                        <select name="reid-2-1" id="reid-2-1" class="form-control" multiple="multiple">
                            <option>Picker</option>
                            <option>Supervisor</option>
                            <option>Quality Control</option>
                            <option>Tractor/Forklift</option>
                            <option>Packer</option>
                            <option>Other</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-2-2">
                        <label for="reid-2-2">Please enter other roles/positions</label>
                        <input type="text" name="reid-2-2" id="reid-2-2" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-2-3">
                        <label for="reid-2-3">Where did you work?</label>
                        <input type="text" name="reid-2-3" id="reid-2-3" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-3">
                        <label for="reid-3">Have you picked other fruits?</label>
                        <select name="reid-3" id="reid-3" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-3-1">
                        <label for="reid-3-1">Which fruits have you picked?</label>
                        <select name="reid-3-1" id="reid-3-1" class="form-control" multiple="multiple">
                            <option>Berries</option>
                            <option>Apples</option>
                            <option>Citrus</option>
                            <option>Mango</option>
                            <option>Grapes</option>
                            <option>Other</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-3-2">
                        <label for="reid-3-2">Which other fruits have you picked?</label>
                        <input type="text" name="reid-3-2" id="reid-3-2" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-4">
                        <label for="reid-4">Have you other experience working physically on farms/outdoors?</label>
                        <select name="reid-4" id="reid-4" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-4-1">
                        <label for="reid-4-1">Where?</label>
                        <input type="text" name="reid-4-1" id="reid-4-1" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-5">
                        <label for="reid-5">What is your preferred location?</label>
                        <select name="reid-5" id="reid-5" class="form-control">
                            <option>No preference</option>
                            <option>Plenty</option>
                            <option>Castle Forbes Bay</option>
                        </select>
                    </div>
                </div>
                <?php } ?>

                <?php if(isset($_GET['camp']) && ($_GET['camp'] == 'REID2021' || $_GET['camp'] == 'REID2022')) { ?>
                <div id="reid-q">
                    <div class="form-group" id="reid-q-1">
                        <label for="reid-1">Have you worked for Reid Fruits before?</label>
                        <select name="reid-1" id="reid-1" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-1-1">
                        <label for="reid-1-1">What role/position(s) did you perform?</label>
                        <select name="reid-1-1" id="reid-1-1" class="form-control" multiple="multiple">
                            <option>Picker</option>
                            <option>Supervisor</option>
                            <option>Quality Control</option>
                            <option>Tractor/Forklift</option>
                            <option>Packer</option>
                            <option>Other</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-1-2">
                        <label for="reid-1-2">Please enter other roles/positions</label>
                        <input type="text" name="reid-1-2" id="reid-1-2" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-1-3">
                        <label for="reid-1-3">What is the most recent year you worked for Reid Fruits?</label>
                        <input type="text" name="reid-1-3" id="reid-1-3" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-2">
                        <label for="reid-2">Have you picked cherries before, or worked during a cherry harvest?</label>
                        <select name="reid-2" id="reid-2" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-2-1">
                        <label for="reid-2-1">What role/position(s) did you perform?</label>
                        <select name="reid-2-1" id="reid-2-1" class="form-control" multiple="multiple">
                            <option>Picker</option>
                            <option>Supervisor</option>
                            <option>Quality Control</option>
                            <option>Tractor/Forklift</option>
                            <option>Packer</option>
                            <option>Other</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-2-2">
                        <label for="reid-2-2">Please enter other roles/positions</label>
                        <input type="text" name="reid-2-2" id="reid-2-2" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-2-3">
                        <label for="reid-2-3">Where did you work?</label>
                        <input type="text" name="reid-2-3" id="reid-2-3" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-3">
                        <label for="reid-3">Have you picked other fruits?</label>
                        <select name="reid-3" id="reid-3" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-3-1">
                        <label for="reid-3-1">Which fruits have you picked?</label>
                        <select name="reid-3-1" id="reid-3-1" class="form-control" multiple="multiple">
                            <option>Berries</option>
                            <option>Apples</option>
                            <option>Citrus</option>
                            <option>Mango</option>
                            <option>Grapes</option>
                            <option>Other</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-3-2">
                        <label for="reid-3-2">Which other fruits have you picked?</label>
                        <input type="text" name="reid-3-2" id="reid-3-2" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-4">
                        <label for="reid-4">Have you other experience working physically on farms/outdoors?</label>
                        <select name="reid-4" id="reid-4" class="form-control">
                            <option value="">(please select)</option>
                            <option value="1">Yes</option>
                            <option value="2">No</option>
                        </select>
                    </div>

                    <div class="form-group" id="reid-q-4-1">
                        <label for="reid-4-1">Where?</label>
                        <input type="text" name="reid-4-1" id="reid-4-1" class="form-control">
                    </div>

                    <div class="form-group" id="reid-q-5">
                        <label for="reid-5">What is your preferred location?</label>
                        <select name="reid-5" id="reid-5" class="form-control">
                            <option>No preference</option>
                            <option>Derwent Valley (42km North of Hobart)</option>
                            <option>Huon Valley (53km South of Hobart)</option>
                        </select>
                    </div>

                        @include('input.select', ['data' => null, 'label' => 'Where are you currently located?', 'name' => 'reid-6', 'required' => true, 'source' => $states, 'class' => ''])
                </div>
                <?php } ?>
            </div>
		</div>
		<?php } ?>

        <div class="col-xs-12 text-center">
            <div class="dashboard-tile">
                <button type="submit" class="btn btn-success"><i class="fa fa-arrow-right"></i> Register Now</button>
            </div>
        </div>
    </form>
</div>
@endsection

<?php if(isset($_GET['camp']) && ($_GET['camp'] == 'REID2019' || $_GET['camp'] == 'REID2020')) { ?>
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#reid-1-1').select2();
            $('#reid-2-1').select2();
            $('#reid-3-1').select2();
        });

        $('#reid-q select').on('change', function(e) {
            _id = $(this).attr('id');
            _val = $(this).val();

            console.log(_id + ':' + _val);

            switch(_id) {
                case 'reid-1':
                    if(_val == 1) {
                        $('#reid-q-1-1').show();
                        $('#reid-q-1-3').show();
                        $('#reid-q-2').hide();
                        $('#reid-q-3').hide();
                        $('#reid-q-4').hide();
                    } else {
                        $('#reid-q-1-1').hide();
                        $('#reid-q-1-2').hide();
                        $('#reid-q-1-3').hide();
                        $('#reid-q-2').show();
                    }
                    break;

                case 'reid-1-1':
                    if(_val.indexOf('Other') > -1) {
                        $('#reid-q-1-2').show();
                    } else {
                        $('#reid-q-1-2').hide();
                    }
                    break;

                case 'reid-2':
                    if(_val == 1) {
                        $('#reid-q-2-1').show();
                        $('#reid-q-2-3').show();
                        $('#reid-q-3').hide();
                        $('#reid-q-4').hide();
                    } else {
                        $('#reid-q-2-1').hide();
                        $('#reid-q-2-2').hide();
                        $('#reid-q-2-3').hide();
                        $('#reid-q-3').show();
                    }
                    break;

                case 'reid-2-1':
                    if(_val.indexOf('Other') > -1) {
                        $('#reid-q-2-2').show();
                    } else {
                        $('#reid-q-2-2').hide();
                    }
                    break;

                case 'reid-3':
                    if(_val == 1) {
                        $('#reid-q-3-1').show();
                        $('#reid-q-4').hide();
                    } else {
                        $('#reid-q-3-1').hide();
                        $('#reid-q-3-1').hide();
                        $('#reid-q-4').show();
                    }
                    break;

                case 'reid-3-1':
                    if(_val.indexOf('Other') > -1) {
                        $('#reid-q-3-2').show();
                    } else {
                        $('#reid-q-3-2').hide();
                    }
                    break;

                case 'reid-4':
                    if(_val == 1) {
                        $('#reid-q-4-1').show();
                    } else {
                        $('#reid-q-4-1').hide();
                    }
                    break;
            }
        });
    </script>
@endsection

@section('styles')
    <style type="text/css">
        #reid-q-1-1, #reid-q-1-2, #reid-q-1-3, #reid-q-2, #reid-q-2-1, #reid-q-2-2, #reid-q-2-3,
        #reid-q-3, #reid-q-3-1, #reid-q-3-2, #reid-q-4, #reid-q-4-1 {
            display: none;
        }

        #reid-q .select2 {
            width: 100% !important;
        }
    </style>
@endsection
<?php } ?>