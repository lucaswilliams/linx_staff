@extends('layouts.app')

@section('title', 'Forgotten Password')

@section('header')
    <h1>Forgotten Password</h1>
    <p>Please enter your registered email address to be sent a password reset confirmation.</p>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="password" class="dashboard-tile col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Send Link</button>
            </div>
        </form>
    </div>
@stop