@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Hi Admin,</p>
                        <p><?php echo $user->given_name; ?> <?php echo $user->surname; ?> has rejected their job offer for <?php echo $user->name; ?>.</p>
                        <p>You may wish to review their application, or assign alternate users to this job..</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@include('emails.layout.contactus')
@stop

