@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Thank you for your registration on the Linx Employment Timesheet Management System.  The details are provided below</p>
                        <ul>
                            <li><strong>Given Name: </strong><?php echo $user->given_name; ?></li>
                            <li><strong>Surname: </strong><?php echo $user->surname; ?></li>
                            <li><strong>Email: </strong><?php echo $user->email; ?></li>
                            <li><strong>Username: </strong><?php echo $user->username; ?></li>
                            <li><strong>Resume: </strong><?php echo ($user->has_resume == 1 ? 'uploaded' : 'skipped'); ?></li>
                            <li><strong>Cards: </strong><?php echo ($user->has_credentials == 1 ? 'completed' : 'skipped'); ?></li>
                            <li><strong>Medical: </strong><?php echo ($user->has_medical == 1 ? 'completed' : 'skipped'); ?></li>
                        </ul>
                        <p>We will be in contact with you shortly if there are any additional details that we require.</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@stop

