<?php if (!isset($email)) { $email = true; } ?>

<table class="row footer">
    <tr>
        <td colspan="2">
            <h2>Contact Us:</h2>
        </td>
    </tr>

    <tr>
        <td class="wrapper">

            <table class="six columns">
                <tr>
                    <td class="left-text-pad" style="vertical-align: top;">

                        <p style="margin-bottom: 0"><strong>Visit our websites</strong></p>
                        <p style="height: 24px;"><a href="http://linxemployment.com.au"><img src="{{URL::to('images/www-linx.png')}}" /></a></p>
                        <p><a href="http://kingsleyhouse.com.au"><img src="{{URL::to('images/www-kingsley.png')}}" /></a></p>

                        <p style="clear: left; margin-bottom: 0; padding-top: 10px;"><strong>Like us on Facebook</strong></p>
                        <p style="height: 24px"><a href="http://facebook.com/LinxEmployment"><img src="{{URL::to('images/fb-linx.png')}}" /></a></p>
                        <p><a href="http://facebook.com/KingsleyHouseBackpackers"><img src="{{URL::to('images/fb-kingsley.png')}}" /></a></p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
        <td class="wrapper last">

            <table class="six columns">
                <tr>
                    <td class="left-text-pad" style="vertical-align: top;">
                        <p><strong>Phone:</strong><br>03 6330 2471</p>
                        <p><strong>Address:</strong><br>2/16 Freshwater Point Road<br>Legana TAS 7277</p>
                        <p><strong>Postal Address:</strong><br>PO Box 41<br>Legana TAS 7277</p>
                        <?php if($email) { echo '<p><strong>Payroll and VISA enquiries:</strong><br>admin@linxemployment.com.au</p>'; } ?>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>