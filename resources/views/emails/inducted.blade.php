@extends('emails.layout.hero')

@section('content')
    <table class="row">
        <tr>
            <td class="wrapper last">

                <table class="twelve columns">
                    <tr>
                        <td>
                            <p>Hi <?php echo $user->given_name; ?>,</p>
                            <p>Thank you for completing your induction on the Linx Employment Timesheet Management System.  A Linx representative will be in contact with you soon.</p>
                        </td>
                        <td class="expander"></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    @include('emails.layout.contactus')
@stop

