@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Hi <?php echo $user->given_name; ?>,</p>
                        <p>You have been offered a job at <?php echo $user->name; ?>, through Linx Employment's timesheet management system.</p>
                        <p>Please log into the <a href="https://staff.linxemployment.com.au">timesheet management system</a> to accept this offer, complete your induction, and record timesheets for this position.</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@include('emails.layout.contactus')
@stop

