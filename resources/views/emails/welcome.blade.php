@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Hi <?php echo $user['given_name']; ?>,</p>
                        <p>Welcome to the Linx Employment Timesheet Management System.  For your records, a copy of the details that you have provided are shown below.</p>
                        <p>We will review your application, and provide more information via email.</p>
                        <ul>
                            <li><strong>Given Name: </strong><?php echo $user['given_name']; ?></li>
                            <li><strong>Surname: </strong><?php echo $user['surname']; ?></li>
                            <li><strong>Preferred Name: </strong><?php echo $user['preferred']; ?></li>
                            <li><strong>Email: </strong><?php echo $user['email']; ?></li>
                            <li><strong>Username: </strong><?php echo $user['username']; ?></li>
                            <li><strong>Referred by: </strong><?php echo $user['referral']; ?></li>
                            <li><strong>Current Location: </strong><?php echo $user['current_location']; ?></li>
                            <?php if(strlen($user['loca_arrive']) > 0 || strlen($user['date_arrive']) > 0) { ?>
                                <li><strong>Arriving in Tasmania: </strong><?php echo $user['loca_arrive']; ?> on <?php echo $user['date_arrive']; ?></li>
                            <?php } ?>
                            <li><strong>Are you working for a second visa? </strong><?php echo $user['visa_88_days'] == 1 ? 'Yes' : 'No'; ?></li>
                            <li><strong>Length of stay: </strong><?php echo $user['stay_length']; ?> months</li>
                            <li><strong>Country of Origin: </strong><?php echo $user['country']; ?></li>
                        </ul>
                        <p>You can use your username and chosen password to gain access to the system at any time.  If you forget your password, you can reset it from the <a href="http://staff.linxemployment.com.au/login">Login</a> page of the Timesheet Management System.</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@include('emails.layout.contactus', ['email' => false])
@stop

