@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Hi <?php echo $client->name; ?>,</p>
                        <p>Attached is your invoice.  Please contact us immediately via return email if you are unable to open this attachment.</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@include('emails.layout.contactus')
@stop

