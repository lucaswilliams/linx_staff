@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Hi Linx,</p>
                        <p><?php echo $user->given_name; ?> <?php echo $user->surname; ?> has accepted their job offer for <?php echo $user->name; ?>.</p>
                        <p>You can see that they will be available on the workers list for this job from now.</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@include('emails.layout.contactus')
@stop

