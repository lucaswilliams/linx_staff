@extends('emails.layout.hero')

@section('content')
<table class="row">
    <tr>
        <td class="wrapper last">

            <table class="twelve columns">
                <tr>
                    <td>
                        <p>Hi <?php echo $user->given_name; ?>,</p>
                        <p>Thank you for confirming your acceptance of the job offer at <?php echo $user->name; ?>.</p>
                        <p>You have been assigned <strong><?php echo $user->external_id; ?></strong> as your employee number.  Please retain a copy of this number for your records.</p>
                        <p>Once you have started your work on this job, please log into the Linx Employment Timesheet Management System to start recording your timesheets, and to keep your information up to date.</p>
                    </td>
                    <td class="expander"></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
@include('emails.layout.contactus')
@stop

