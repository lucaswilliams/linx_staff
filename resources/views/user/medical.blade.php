@extends('layouts.app')

@section('title', 'Medical Questionnaire')

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/medical.js') }}"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form method="POST" id="medical-form">
                {{csrf_field()}}
                <div class="dashboard-tile">
                    <h1>Pre-Employment Medical Questionnaire</h1>
                    <p>The aim of the Pre-Employment Medical (PEM) Questionnaire is to ensure that applicants physical and other related abilities are matched to the medical and fitness standards for the particular duties of a job.</p>
                    <p>Pre-Employment Medical Questionnaires (PEM’s) are necessary to determine that:</p>
                    <ul>
                        <li>There is not risk of aggravating a pre-existing medical condition.</li>
                        <li>The applicant is able to productively carry out the duties of the position safely.</li>
                        <li>The applicant should not, because of a medical condition, increase risk to other workers, equipment, products or the general public.</li>
                    </ul>
					<p><strong>Please scroll down and complete the entire form below.  Without doing this, we may be unable to provide you with a job.</strong></p>
                    <h2>CONFIDENTIALITY</h2>
                    <p>The Pre-Employment Questionnaire is treated as a confidential document and access is limited to a ‘need to know’ basis.  In the event of you being employed, Linx Employment will retain this form on a confidential file and reserve the right to refer to the information in the event of an accident, sickness, injury or claim for worker’s compensation.  The information may also be used for other purposes, if so required by law.</p>
                    <h2>IMPORTANT NOTICE</h2>
                    <p>To assist Linx Employment in assessing your medical fitness for employment, you must answer the questions contained in this questionnaire truthfully and to the best of your knowledge.</p>
                    <p><strong>Failure to disclose any relevant matter relating to your health may result in your not being employed by the employer and, if already employed by the employer, your employment may be affected and rights to workplace compensation compromised.</strong></p>
                    <p>The following documents are available for your reference</p>
                    <ul>
                        <li><a href="{{ asset('docs/workers-compensation.pdf') }}" target="_blank">Workers' Rehabilitation and Compensation Act 1988</a> <i class="fa fa-external-link"></i></li>
                        <li><a href="{{ asset('docs/linx_fair_work.pdf') }}" target="_blank">Fair Work Information Statement</a> <i class="fa fa-external-link"></i></li>
                        <li><a href="{{ asset('docs/on-hire-employees.pdf') }}" target="_blank">A guide to on-hire businesses and host organisations</a> <i class="fa fa-external-link"></i></li>
						<li><a href="{{ asset('docs/linx_privacy.pdf') }}">Linx Employment Privacy Policy</a></li>
                    </ul>
                    <h2>Your Details</h2>
                    <table>
                        <tr>
                            <th>Surname:</th>
                            <td><?php echo $user->surname; ?></td>
                        </tr>
                        <tr>
                            <th>Given Name:</th>
                            <td><?php echo $user->given_name; ?></td>
                        </tr>
                        <tr>
                            <th>Address:</th>
                            <td><?php echo $user->address; ?><br /><?php echo $user->city; ?></td>
                        </tr>
                        <tr>
                            <th>Postcode:</th>
                            <td><?php echo $user->postcode; ?></td>
                        </tr>
                        <tr>
                            <th>State:</th>
                            <td><?php echo $user->state; ?></td>
                        </tr>
                        <tr>
                            <th>Phone:</th>
                            <td><?php echo $user->mobilephone; ?><br /><?php echo $user->telephone; ?></td>
                        </tr>
                        <tr>
                            <th>Age:</th>
                            <td>
                                <?php
                                $diff = abs(time() - strtotime($user->date_of_birth));

                                echo floor($diff / (365*60*60*24));
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Date of Birth:</th>
                            <td><?php echo date('d/m/Y', strtotime($user->date_of_birth)); ?></td>
                        </tr>
                        <tr>
                            <th>Gender:</th>
                            <td>
                                <?php
                                switch($user->gender) {
                                    case 1 : echo 'Male'; break;
                                    case 2 : echo 'Female'; break;
                                    default: echo 'Not specified'; break;
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dashboard-tile" id="questions">
                    <h1>Questions</h1>
                    <p id="questions-message" class="text-danger"></p>
                    <div class="col-sm-6 col-xs-12">
                        @include('input.text', ['data' => $usermedical, 'required' => true, 'name' => 'height', 'label' => 'Height (in cm)'])
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        @include('input.text', ['data' => $usermedical, 'required' => true, 'name' => 'weight', 'label' => 'Weight (in whole kg)'])
                    </div>

                    <table class="table table-striped">
                        <tr>
                            <th width="50%">Question</th>
                            <th width="5%">Yes</th>
                            <th width="5%">No</th>
                            <th width="40%">Details</th>
                        </tr>
                        <tr id="lost_time">
                            <td>Have you lost any time from work in the last 12 months due to illness or injury?</td>
                            <td>
                                <input type="radio" name="lost_time_radio" data-id="lost_time" value="1" <?php echo ($usermedical->lost_time == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="lost_time_radio" data-id="lost_time" value="0" <?php echo ($usermedical->lost_time == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="lost_time" class="form-control lost_time" <?php echo ($usermedical->lost_time == null ? 'disabled' : ''); ?>><?php echo $usermedical->lost_time; ?></textarea>
                            </td>
                        </tr>

                        <tr id="medication">
                            <td>Are you currently taking, or in the past two years have taken any medication?</td>
                            <td>
                                <input type="radio" name="medication_radio" data-id="medication" value="1" <?php echo ($usermedical->medication == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="medication_radio" data-id="medication" value="0" <?php echo ($usermedical->medication == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="medication" class="form-control medication" <?php echo ($usermedical->medication == null ? 'disabled' : ''); ?>><?php echo $usermedical->medication; ?></textarea>
                            </td>
                        </tr>

                        <tr id="allergies">
                            <td>Do you have any known allergies?</td>
                            <td>
                                <input type="radio" name="allergies_radio" data-id="allergies" value="1" <?php echo ($usermedical->allergies == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="allergies_radio" data-id="allergies" value="0" <?php echo ($usermedical->allergies == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="allergies" class="form-control allergies" <?php echo ($usermedical->allergies == null ? 'disabled' : ''); ?>><?php echo $usermedical->allergies; ?></textarea>
                            </td>
                        </tr>

                        <tr id="allergies_medication">
                            <td>Are you taking, or do you need to take, any medication for the known allergies?</td>
                            <td>
                                <input type="radio" name="allergies_medication_radio" data-id="allergies_medication" value="1" <?php echo ($usermedical->allergies_medication == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="allergies_medication_radio" data-id="allergies_medication" value="0" <?php echo ($usermedical->allergies_medication == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="allergies_medication" class="form-control allergies_medication" <?php echo ($usermedical->allergies_medication == null ? 'disabled' : ''); ?>><?php echo $usermedical->allergies_medication; ?></textarea>
                            </td>
                        </tr>

                        <tr id="allergies_additional">
                            <td>Is there any additional information relating to these allergies?</td>
                            <td>
                                <input type="radio" name="allergies_additional_radio" data-id="allergies_additional" value="1" <?php echo ($usermedical->allergies_additional == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="allergies_additional_radio" data-id="allergies_additional" value="0" <?php echo ($usermedical->allergies_additional == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="allergies_additional" class="form-control allergies_additional" <?php echo ($usermedical->allergies_additional == null ? 'disabled' : ''); ?>><?php echo $usermedical->allergies_additional; ?></textarea>
                            </td>
                        </tr>

                        <tr id="sport_hobbies">
                            <td>Are you involved in any sporting activities or hobbies?</td>
                            <td>
                                <input type="radio" name="sport_hobbies_radio" data-id="sport_hobbies" value="1" <?php echo ($usermedical->sport_hobbies == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="sport_hobbies_radio" data-id="sport_hobbies" value="0" <?php echo ($usermedical->sport_hobbies == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="sport_hobbies" class="form-control sport_hobbies" <?php echo ($usermedical->sport_hobbies == null ? 'disabled' : ''); ?>><?php echo $usermedical->sport_hobbies; ?></textarea>
                            </td>
                        </tr>

                        <tr id="existing_condition">
                            <td>Do you have any condition or problem that may impact upon your ability to perform your job?</td>
                            <td>
                                <input type="radio" name="existing_condition_radio" data-id="existing_condition" value="1" <?php echo ($usermedical->existing_condition == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="existing_condition_radio" data-id="existing_condition" value="0" <?php echo ($usermedical->existing_condition == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="existing_condition" class="form-control existing_condition" <?php echo ($usermedical->existing_condition == null ? 'disabled' : ''); ?>><?php echo $usermedical->existing_condition; ?></textarea>
                            </td>
                        </tr>
                        <tr id="stand">
                            <td>How many hours at a time can you stand and/or walk for at a time?</td>
                            <td colspan="3">
                                <select name="stand">
                                    <option value="0"></option>
                                    <option value="1" <?php echo $usermedical->stand == 1 ? 'selected' : ''; ?>>Less than 1 hour at a time</option>
                                    <option value="2" <?php echo $usermedical->stand == 2 ? 'selected' : ''; ?>>1 – 2 hours</option>
                                    <option value="3" <?php echo $usermedical->stand == 3 ? 'selected' : ''; ?>>2 - 4 hours</option>
                                    <option value="4" <?php echo $usermedical->stand == 4 ? 'selected' : ''; ?>>4 – 6 hours</option>
                                    <option value="5" <?php echo $usermedical->stand == 5 ? 'selected' : ''; ?>>6 – 8 hours</option>
                                    <option value="6" <?php echo $usermedical->stand == 6 ? 'selected' : ''; ?>>more than 8 hours at a time</option>
                                </select>
                            </td>
                        </tr>
                    </table>

                    <h2>Medical conditions</h2>
                    <p>Do you have, or have you had, any of the following conditions?  If "yes" for any question, you will need to provide further details in the fields provided.</p>
                    <table class="table table-striped" id="med-questions">
                        <tr>
                            <th width="30%">Question</th>
                            <th width="5%">Yes</th>
                            <th width="5%">No</th>
                            <th width="30%">Duration and Dates</th>
                            <th width="30%">Current status</th>
                        </tr>
                        <?php foreach($medical as $ques) { ?>
                        <tr>
                            <td><?php echo $ques->ques_text; ?></td>
                            <td>
                                <input type="radio" name="ques[<?php echo $ques->ques_id; ?>]" data-id="ques_<?php echo $ques->ques_id; ?>" value="1" <?php echo ($ques->duration == null ? '' : 'checked'); ?>>
                            </td>
                            <td>
                                <input type="radio" name="ques[<?php echo $ques->ques_id; ?>]" data-id="ques_<?php echo $ques->ques_id; ?>" value="0" <?php echo ($ques->duration == null ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <textarea name="duration[<?php echo $ques->ques_id; ?>]" class="form-control ques_<?php echo $ques->ques_id; ?>"  <?php echo ($ques->duration == null ? 'disabled' : ''); ?>><?php echo $ques->duration; ?></textarea>
                            </td>
                            <td>
                                <textarea name="status[<?php echo $ques->ques_id; ?>]" class="form-control ques_<?php echo $ques->ques_id; ?>"  <?php echo ($ques->duration == null ? 'disabled' : ''); ?>><?php echo $ques->status; ?></textarea>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>

                    <h2>Lifestyle habits</h2>
                    <div class="form-group">
                        <p>Are you a current smoker?</p>
                        <table class="table table-striped">
                            <tr id="current_smoker">
                                <td colspan="2">
                                    <label class="radio-inline">
                                        <input type="radio" name="current_smoker" id="current_smoker_yes" data-id="current_smoker" value="1" <?php echo ($usermedical->current_smoker == 1 ? 'checked' : ''); ?>> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="current_smoker" id="current_smoker_used" data-id="current_smoker" value="2" <?php echo ($usermedical->current_smoker == 2 ? 'checked' : ''); ?>> No, but I used to
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="current_smoker" id="current_smoker_no" data-id="current_smoker" value="0" <?php echo ($usermedical->current_smoker === 0 ? 'checked' : ''); ?>> No, never have been
                                    </label>
                                </td>
                            </tr>
                            <tr id="smokes_daily_cigarettes">
                                <td>Average Cigarettes Per Day:</td>
                                <td><input type="text" name="smokes_daily_cigarettes" class="form-control current_smoker" <?php echo ($usermedical->current_smoker === 0 ? 'disabled' : 'value="'.$usermedical->smokes_daily_cigarettes.'"'); ?>></td>
                            </tr>
                            <tr id="smokes_daily_cigars">
                                <td>Average Cigars Per Day:</td>
                                <td><input type="text" name="smokes_daily_cigars" class="form-control current_smoker" <?php echo ($usermedical->current_smoker === 0 ? 'disabled' : 'value="'.$usermedical->smokes_daily_cigars.'"'); ?>></td>
                            </tr>
                            <tr id="smokes_daily_pipes">
                                <td>Average Pipe Per Day:</td>
                                <td><input type="text" name="smokes_daily_pipes" class="form-control current_smoker" <?php echo ($usermedical->current_smoker === 0 ? 'disabled' : 'value="'.$usermedical->smokes_daily_pipes.'"'); ?>></td>
                            </tr>
                            <tr id="smoker_quit_year">
                                <td>When did you quit (year):</td>
                                <td><input type="text" name="smoker_quit_year" class="form-control old_smoker"  <?php echo ($usermedical->smoker_quit_year == 2 ? 'value="'.$usermedical->alcohol_per_week.'"' : 'disabled'); ?>></td>
                            </tr>
                            <tr id="smoker_duration">
                                <td>How long did you smoke for (years):</td>
                                <td><input type="text" name="smoker_duration" class="form-control old_smoker"  <?php echo ($usermedical->smoker_duration == 2 ? 'value="'.$usermedical->alcohol_per_week.'"' : 'disabled'); ?>></td>
                            </tr>
                        </table>
                    </div>

                    <div class="form-group">
                        <p>Do you drink alcohol?</p>
                        <table class="table table-striped">
                            <tr id="drink_alcohol">
                                <td colspan="2">
                                    <label class="radio-inline">
                                        <input type="radio" name="drink_alcohol" id="drink_alcohol_yes" data-id="drink_alcohol" value="1" <?php echo ($usermedical->drink_alcohol == 1 ? 'checked' : ''); ?>> Yes
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="drink_alcohol" id="drink_alcohol_used" data-id="drink_alcohol" value="0" <?php echo ($usermedical->drink_alcohol === 0 ? 'checked' : ''); ?>> No
                                    </label>
                                </td>
                            </tr>
                            <tr id="alcohol_per_week">
                                <td>Average Per Week (glasses):</td>
                                <td><input type="text" name="alcohol_per_week" class="form-control drink_alcohol" <?php echo ($usermedical->drink_alcohol == 1 ? 'value="'.$usermedical->alcohol_per_week.'"' : 'disabled'); ?>></td>
                            </tr>
                            <tr id="alcohol_type">
                                <td>What type(s)?</td>
                                <td><input type="text" name="alcohol_type" class="form-control drink_alcohol"  <?php echo ($usermedical->drink_alcohol == 1 ? 'value="'.$usermedical->alcohol_type.'"' : 'disabled'); ?>></td>
                            </tr>
                        </table>
                    </div>

                    <h2>Vaccination Information</h2>
                    <p>Have you been vaccinated against the following?</p>
                    <table class="table table-striped">
                        <tr>
                            <th width="70%">&nbsp;</th>
                            <th width="15%">Yes</th>
                            <th width="15%">No</th>
                        </tr>
                        <tr id="vaccinate_hep_a">
                            <td>Hepatitis A</td>
                            <td>
                                <input type="radio" name="vaccinate_hep_a" value="1" <?php echo ($usermedical->vaccinate_hep_a == 1 ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <input type="radio" name="vaccinate_hep_a" value="0" <?php echo ($usermedical->vaccinate_hep_a === 0 ? 'checked' : ''); ?>>
                            </td>
                        </tr>
                        <tr id="vaccinate_hep_b">
                            <td>Hepatitis B</td>
                            <td>
                                <input type="radio" name="vaccinate_hep_b" value="1" <?php echo ($usermedical->vaccinate_hep_b == 1 ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <input type="radio" name="vaccinate_hep_b" value="0" <?php echo ($usermedical->vaccinate_hep_b === 0 ? 'checked' : ''); ?>>
                            </td>
                        </tr>
                        <tr id="vaccinate_tetanus">
                            <td>Tetanus</td>
                            <td>
                                <input type="radio" name="vaccinate_tetanus" value="1" <?php echo ($usermedical->vaccinate_tetanus == 1 ? 'checked' : ''); ?>>
                            </td>
                            <td>
                                <input type="radio" name="vaccinate_tetanus" value="0" <?php echo ($usermedical->vaccinate_tetanus === 0 ? 'checked' : ''); ?>>
                            </td>
                        </tr>
                    </table>

                    <h2>Medical Practitioner Information</h2>
                    <div class="form-group">
                        <label for="doctor_1_name">Doctor's Name:</label>
                        <input type="text" name="doctor_1_name" id="doctor_1_name" class="form-control" value="<?php echo $usermedical->doctor_1_name; ?>">
                    </div>

                    <div class="form-group">
                        <label for="doctor_1_address">Clinic Name and Address:</label>
                        <textarea name="doctor_1_address" id="doctor_1_address" class="form-control"><?php echo $usermedical->doctor_1_address; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="doctor_1_phone">Phone:</label>
                        <input type="tel" name="doctor_1_phone" id="doctor_1_phone" class="form-control" value="<?php echo $usermedical->doctor_1_phone; ?>">
                    </div>

                    <h2>Emergency Contact Information</h2>
                    <div class="form-group">
                        <label for="emergency_name">Contact Name:</label>
                        <input type="text" name="emergency_name" id="emergency_name" class="form-control" value="<?php echo $user->emergency_name; ?>">
                    </div>

                    <div class="form-group">
                        <label for="emergency_number">Contact Number:</label>
                        <input type="text" name="emergency_number" id="emergency_number" class="form-control" value="<?php echo $user->emergency_number ?>">
                    </div>
                </div>
                <div class="dashboard-tile">
                    <h1>Declaration</h1>
                    <p>By submitting this online form, I hereby declare that:</p>
                    <ul>
                        <li>I have read and understood the conditions of this form.
                        I understand that the information I provide will be retained on my employee file and that the employer reserves the right to access and use the information, in the event of any accident, injury, sickness or claim for workers compensation or for any other reasonable purposes, if so required by law.</li>
                        <li>I consent to Linx Employment and its medical representatives obtaining or exchanging further medical information from my treating doctors or other health practitioners, if required for the purposes of this assessment.</li>
                        <li>My answers relating to my medical and employment history are true and complete to the best of my knowledge.  Furthermore there is nothing else regarding my health, well being or ability to carry out the potential role which Linx Employment or its medical advisers may need to know to assess me for the position(s) I have applied.</li>
                        <li>I am fully aware that if I fail to disclose any relevant matter relating to my health, which renders me incapable of properly fulfilling the duties of the position, the employer may not employ me and if already employed by the employer, my employment may be summarily terminated.</li>
						<li>I have read and understood the fair work information statement and Linx privacy policy.</li>
                        <li>I understand and agree that this report and any related health information provided may be supplied to Linx Employment and its medical advisors.</li>
                    </ul>
                </div>
                <div class="dashboard-tile text-center">
                    <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-floppy-o"></i> Save Details</button>
                    <button type="submit" class="btn btn-default" name="skip"><i class="fa fa-share"></i> Skip this step</button>
                </div>
            </form>
        </div>
    </div>
@stop