@extends('layouts.app')

@section('title', 'Upload a Resume')

@section('header')
    <h2>Certificate and Licence Details</h2>
    <p>Please provide any details about certificates or licences that you currently hold.  Examples of things you might have are a Driver's Licence, Food Handling Certificate, or First Aid Certificate.</p>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form method="POST" id="resume-form" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="dashboard-tile">
                    <table width="100%">
                        <tr>
                            <th>Name</th>
                            <th>Use</th>
                            <th>Expiry Date</th>
                            <th>Description</th>
                        </tr>
                        <?php
                            foreach($creds as $cred) { //echo '<pre>'; var_dump($cred); echo '</pre>';
                        ?>
                        <tr>
                            <td>
                                <?php echo $cred->name; ?>
                            </td>
                            <td>
                                <input type="checkbox" name="cred_id[<?php echo $cred->cred_id; ?>]" <?php if($cred->id) { echo 'checked'; } ?> />
                            </td>
                            <td>
                                <?php if($cred->has_expiry) { ?>
                                <input type="date" name="expiry[<?php echo $cred->cred_id; ?>]" value="<?php echo $cred->expiry_date; ?>">
                                <?php } else { ?>
                                <input type="hidden" name="expiry[<?php echo $cred->cred_id; ?>]" value="<?php echo $cred->expiry_date; ?>">
                                <?php } ?>
                            </td>
                            <td>
                                <?php if($cred->has_description) { ?>
                                <textarea name="description[<?php echo $cred->cred_id; ?>]"><?php echo $cred->description; ?></textarea>
                                <?php } else { ?>
                                <input type="hidden" name="description[<?php echo $cred->cred_id; ?>]" value="<?php echo $cred->description; ?>">
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>

                <div class="dashboard-tile text-center">
                    <button type="submit" class="btn btn-success" name="upload"><i class="fa fa-floppy-o"></i> Save Details</button>
                    <button type="submit" class="btn btn-default" name="skip"><i class="fa fa-share"></i> Skip this step</button>
                </div>
            </form>
        </div>
    </div>
@stop