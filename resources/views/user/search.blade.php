<div class="row">
    <div class="col-sm-6">
        <label for="search-name">Name contains:</label>
        <input type="text" id="search-name" name="search-name" class="form-control">

        <label for="search-job">Part of job:</label>
        <select id="search-job" name="search-job" class="search" multiple>
            <option value=""></option>
            <?php
                foreach($jobs as $job) {
                    echo '<option value="'.$job->id.'">'.$job->name.'('.$job->client_name.'; '.date('d/m/Y', strtotime($job->start_date)).' - '.(strtotime($job->end_date) > 0 ? date('d/m/Y', strtotime($job->end_date)) : '???').')</option>';
                }
            ?>
        </select>
        <label for="search-email">Email contains:</label>
        <input type="text" id="search-email" name="search-email" class="form-control">
    </div>
    <div class="col-sm-6">
        <label for="search-external">Employee Number:</label>
        <input type="text" id="search-external" name="search-external" class="form-control">

        <label for="search-status">Status:</label>
        <select id="search-status" name="search-name" multiple>
            <option value=""></option>
            <option value="4">Active</option>
            <option value="3">Induction Pending</option>
            <option value="2">Accepted Offer</option>
            <option value="1">Offered</option>
            <option value="0">Applicant</option>
            <option value="-1">Inactive</option>
        </select>
        <label for="search-phone">Phone:</label>
        <input type="text" id="search-phone" name="search-phone" class="form-control">
    </div>
    <div class="col-xs-12 text-right">
        <button id="search-btn-staff" class="btn btn-success btn-save"><i class="fa fa-search"></i> Search</button>
    </div>
</div>