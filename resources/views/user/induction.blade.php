@extends('layouts.app')

@section('title', 'Induction')

@section('header')
    <h1><?php echo $induction->ind_name; ?></h1>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <form method="POST" action="{{ URL::to('induct') }}" class="induction">
            <div class="dashboard-tile">
                {!! csrf_field() !!}

                <input type="hidden" name="usin_id" value="<?php echo $induction->usin_id; ?>">
                <input type="hidden" name="ind_id" value="<?php echo $induction->ind_id; ?>">

                <?php
                    echo $induction->ind_content;
                    foreach($questions as $question) {
                        echo '<div class="question">';
                        echo $question->ques_text;
                        echo '<div class="answers">';
                        foreach($question->answers as $answer) {
                            echo '<label class="radio-inline">
                                <input type="radio" name="opt_id['.$question->ques_id.']" value="'.$answer->opt_id.'">'.
                                $answer->opt_text.
                            '</label>';
                        }
                        echo '</div>';
                        echo '</div>';
                    }
                ?>
            </div>
            <div class="dashboard-tile text-center">
                <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Submit Responses</button>
            </div>
        </form>
    </div>
</div>
@stop