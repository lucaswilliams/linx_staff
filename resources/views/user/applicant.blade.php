@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Dashboard</h1>
            <p>Thank you for your registration with Linx Employment.  We will be in touch with you soon regarding the information that you have provided.</p>
        </div>
    </div>
@stop