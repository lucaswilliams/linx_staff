<?php
$yesno = array (
    (object)array('key' => '1', 'value' => 'Yes'),
    (object)array('key' => '0', 'value' => 'No')
);

if(strlen(old('date_of_birth')) > 0) {
    $date_of_birth = old('date_of_birth');
} else {
    $date_of_birth = $user->date_of_birth;
}

if(strlen(old('gender')) > 0) {
    $gender = old('gender');
} else {
    $gender = $user->gender;
}

if(strlen(old('title')) > 0) {
    $title = old('title');
} else {
    $title = $user->title;
}

if(strlen(old('email')) > 0) {
    $email = old('email');
} else {
    $email = $user->email;
}

if(strlen(old('no_address')) > 0) {
    $no_address = old('no_address');
} else {
    $no_address = $user->no_address;
}

if(strlen(old('no_bank_account')) > 0) {
    $no_bank_account = old('no_bank_account');
} else {
    $no_bank_account = $user->no_bank_account;
}

if(strlen(old('no_super')) > 0) {
    $no_super = old('no_super');
} else {
    $no_super = $user->no_super;
}

if(strlen(old('address')) > 0) {
    $address = old('address');
} else {
    $address = $user->address;
}

if(strlen(old('city')) > 0) {
    $city = old('city');
} else {
    $city = $user->city;
}

if(strlen(old('state')) > 0) {
    $state = old('state');
} else {
    $state = $user->state;
}

if(strlen(old('postcode')) > 0) {
    $postcode = old('postcode');
} else {
    $postcode = $user->postcode;
}

if(strlen(old('telephone')) > 0) {
    $telephone = old('telephone');
} else {
    $telephone = $user->telephone;
}

if(strlen(old('mobilephone')) > 0) {
    $mobilephone = old('mobilephone');
} else {
    $mobilephone = $user->mobilephone;
}

if(strlen(old('emergency_name')) > 0) {
    $emergency_name = old('emergency_name');
} else {
    $emergency_name = $user->emergency_name;
}

if(strlen(old('emergency_number')) > 0) {
    $emergency_number = old('emergency_number');
} else {
    $emergency_number = $user->emergency_number;
}

if(strlen(old('aus_resident')) > 0) {
    $aus_resident = old('aus_resident');
} else {
    $aus_resident = $user->aus_resident;
}

if(strlen(old('passport_number')) > 0) {
    $passport_number = old('passport_number');
} else {
    $passport_number = $user->passport_number;
}

if(strlen(old('country')) > 0) {
    $country = old('country');
} else {
    $country = $user->country;
}

if(strlen(old('visa_expiry')) > 0) {
    $visa_expiry = old('visa_expiry');
} else {
    $visa_expiry = $user->visa_expiry;
}

if(strlen(old('tfn')) > 0) {
    $tfn = old('tfn');
} else {
    $tfn = $user->tfn;
}

if(strlen(old('tax_resident')) > 0) {
    $tax_resident = old('tax_resident');
} else {
    $tax_resident = $user->tax_resident;
}

if(strlen(old('tax_free_threshold')) > 0) {
    $tax_free_threshold = old('tax_free_threshold');
} else {
    $tax_free_threshold = $user->tax_free_threshold;
}

if(strlen(old('senior_tax_offset')) > 0) {
    $senior_tax_offset = old('senior_tax_offset');
} else {
    $senior_tax_offset = $user->senior_tax_offset;
}

if(strlen(old('help_debt')) > 0) {
    $help_debt = old('help_debt');
} else {
    $help_debt = $user->help_debt;
}
if(strlen(old('account_bsb')) > 0) {
    $account_bsb = old('account_bsb');
} else {
    $account_bsb = $user->account_bsb;
}

if(strlen(old('account_number')) > 0) {
    $account_number = old('account_number');
} else {
    $account_number = $user->account_number;
}

if(strlen(old('account_name')) > 0) {
    $account_name = old('account_name');
} else {
    $account_name = $user->account_name;
}

if(strlen(old('provider_id')) > 0) {
    $provider_id = old('provider_id');
} else {
    $provider_id = $user->provider_id;
}

if(strlen(old('super_number')) > 0) {
    $super_number = old('super_number');
} else {
    $super_number = $user->super_number;
}

if(strlen(old('usi_id')) > 0) {
    $usi_id = old('usi_id');
} else {
    $usi_id = $user->usi_id;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Complete Registration</h1>
            <h2><?php echo $user->given_name.' '.$user->surname; ?></h2>
            <p>Please finalise filling out your details in the form below.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <form method="POST" action="{{ URL::to('profile') }}" class="row">
                {!! csrf_field() !!}
                <input type="hidden" name="register_accomm" value="<?php echo $user->accommodation; ?>" />
                <input type="hidden" name="user_id" value="<?php echo $user->id; ?>" />
                <input type="hidden" name="register" value="1">
                <input type="hidden" name="profile_complete" value="1">
                <div class="dashboard-tile">
                    <h2>Personal Details</h2>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <label>Given Name</label>
                                <input type="text" name="given_name" value="<?php echo $user->given_name; ?>" class="form-control">
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <label>Middle Name(s)</label>
                                <input type="text" name="middle_name" value="<?php echo $user->middle_name; ?>" class="form-control">
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <label>Surname</label>
                                <input type="text" name="surname" value="<?php echo $user->surname; ?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php
                            $year = substr($date_of_birth, 0, 4);
                            $month = substr($date_of_birth, 5, 2);
                            $day = substr($date_of_birth, 8, 2);
                        ?>
                        <label for="date_of_birth_month">Date of Birth<span class="required">*</span>:</label>
                        <div style="height: 34px; margin-bottom: 15px;">
                            <select name="date_of_birth_month" id="date_of_birth_month" class="noselect form-control" style="width: 50%; float:left;">
                                <option></option>
                                <option value="01" <?php if($month == '01') echo 'selected'; ?>>January</option>
                                <option value="02" <?php if($month == '02') echo 'selected'; ?>>February</option>
                                <option value="03" <?php if($month == '03') echo 'selected'; ?>>March</option>
                                <option value="04" <?php if($month == '04') echo 'selected'; ?>>April</option>
                                <option value="05" <?php if($month == '05') echo 'selected'; ?>>May</option>
                                <option value="06" <?php if($month == '06') echo 'selected'; ?>>June</option>
                                <option value="07" <?php if($month == '07') echo 'selected'; ?>>July</option>
                                <option value="08" <?php if($month == '08') echo 'selected'; ?>>August</option>
                                <option value="09" <?php if($month == '09') echo 'selected'; ?>>September</option>
                                <option value="10" <?php if($month == '10') echo 'selected'; ?>>October</option>
                                <option value="11" <?php if($month == '11') echo 'selected'; ?>>November</option>
                                <option value="12" <?php if($month == '12') echo 'selected'; ?>>December</option>
                            </select>
                            <input type="text" name="date_of_birth_day" id="date_of_birth_day" placeholder="Day" class="form-control" style="width: 25%; float:left;" value="<?php echo $day; ?>">
                            <input type="text" name="date_of_birth_year" id="date_of_birth_year" placeholder="Year" class="form-control" style="width: 25%; float:left;" value="<?php echo $year; ?>">
                        </div>
                            <?php
                            foreach ($errors->get('date_of_birth_month') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            foreach ($errors->get('date_of_birth_day') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            foreach ($errors->get('date_of_birth_year') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <label for="title">Title:</label>
                                <select name="title" id="title" class="form-control">
                                    <option value=""></option>
                                    <option value="Mr"<?php echo ($title == "Mr" ? ' selected' : ''); ?>>Mr</option>
                                    <option value="Mrs"<?php echo ($title == "Mrs" ? ' selected' : ''); ?>>Mrs</option>
                                    <option value="Miss"<?php echo ($title == "Miss" ? ' selected' : ''); ?>>Miss</option>
                                    <option value="Ms"<?php echo ($title == "Ms" ? ' selected' : ''); ?>>Ms</option>
                                </select>
                                <?php
                                foreach ($errors->get('title') as $error) {
                                    echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                                }
                                ?>
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <label for="gender">Gender:</label>
                                <select name="gender" id="gender" class="form-control">
                                    <option value="0">Not specified</option>
                                    <option value="1" <?php if($gender == 1) { echo 'selected'; } ?>>Male</option>
                                    <option value="2" <?php if($gender == 2) { echo 'selected'; } ?>>Female</option>
                                </select>
                                <?php
                                foreach ($errors->get('gender') as $error) {
                                    echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-tile">
                    <h2>Contact Details</h2>
                    <div class="form-group">
                        <label for="no_fixed_address">
                            <input type="checkbox" id="no_fixed_address" name="no_address" value="1" <?php echo ($no_address == 1 ? 'checked' : '');?>> I don't have an Australian address
                        </label>

                        <label for="address">Address:</label>
                        <input type="text" name="address" id="address" value="<?php echo $address; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('address') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="city">City:</label>
                        <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('city') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="state">State:</label>
                        <select name="state" id="state" class="form-control">
                            <option value=""></option>
                            <option value="ACT" <?php if($state == 'ACT') { echo 'selected'; } ?>>Australian Capital Territory</option>
                            <option value="NSW" <?php if($state == 'NSW') { echo 'selected'; } ?>>New South Wales</option>
                            <option value="NT" <?php if($state == 'NT') { echo 'selected'; } ?>>Northern Territory</option>
                            <option value="QLD" <?php if($state == 'QLD') { echo 'selected'; } ?>>Queensland</option>
                            <option value="SA" <?php if($state == 'SA') { echo 'selected'; } ?>>South Australia</option>
                            <option value="TAS" <?php if($state == 'TAS') { echo 'selected'; } ?>>Tasmania</option>
                            <option value="VIC" <?php if($state == 'VIC') { echo 'selected'; } ?>>Victoria</option>
                            <option value="WA" <?php if($state == 'WA') { echo 'selected'; } ?>>Western Australia</option>
                        </select>
                        <?php
                        foreach ($errors->get('state') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="postcode">Postcode:</label>
                        <input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('postcode') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="telephone">Home Phone:</label>
                        <input type="text" name="telephone" id="telephone" value="<?php echo $telephone; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('telephone') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="mobilephone">Mobile Phone:</label>
                        <input type="text" name="mobilephone" id="mobilephone" value="<?php echo $mobilephone; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('mobilephone') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="email">Email<span class="required">*</span>:</label>
                        <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('email') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="emergency_name">Emergency Contact:</label>
                        <input type="text" name="emergency_name" id="emergency_name" value="<?php echo $emergency_name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('emergency_name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="emergency_number">Emergency Phone:</label>
                        <input type="text" name="emergency_number" id="emergency_number" value="<?php echo $emergency_number; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('emergency_number') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>
                </div>
                <div class="dashboard-tile">
                    <h2>A bit about working</h2>
                    @include('input.select', ['data' => $user, 'name' => 'job_network_tick', 'label' => 'Are you registered with a Job Network', 'required' => false, 'source' => $yesno])

                    <div class="form-group" style="display: <?php echo (old('job_network_tick') == 0 ? 'none' : 'block'); ?>">
                        <label for="job_network">Job Network name:</label>
                        <select name="job_network" id="job_network" class="form-control">
                            <option value=""></option>
                            <?php
                            $jobnets = array(
                                'APM', 'Mission Australia', 'ORS', 'CVGT', 'Employment Plus', 'Max Employment', 'My Pathway', 'Other'
                            );
                            foreach($jobnets as $jobnet) { ?>
                            <option value="<?php echo $jobnet; ?>" <?php if(strcmp(old('job_network'), $jobnet) == 0) { echo 'selected'; } ?>><?php echo $jobnet; ?></option>
                            <?php } ?>
                        </select>
                        <?php
                        foreach ($errors->get('job_network') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="current_location">Where are you currently located<span class="required">*</span>:</label>
                        <select name="current_location" id="current_location" class="form-control">
                            <option value=""></option>
                            <optgroup label="Tasmania">
                                <option value="Launceston"<?php echo (strcmp(old('current_location'), 'Launceston') == 0 ? ' selected' : ''); ?>>Launceston</option>
                                <option value="Hobart"<?php echo (strcmp(old('current_location'), 'Hobart') == 0 ? ' selected' : ''); ?>>Hobart</option>
                                <option value="Burnie/Devonport"<?php echo (strcmp(old('current_location'), 'Burnie/Devonport') == 0 ? ' selected' : ''); ?>>Burnie/Devonport</option>
                                <option value="Other Tasmania"<?php echo (strcmp(old('current_location'), 'Other Tasmania') == 0 ? ' selected' : ''); ?>>Other Tasmania</option>
                            </optgroup>
                            <optgroup label="Outside Tasmania">
                                <option value="Mainland Australia"<?php echo (strcmp(old('current_location'), 'Mainland Australia') == 0 ? ' selected' : ''); ?>>Mainland Australia</option>
                                <option value="Outside Australia"<?php echo (strcmp(old('current_location'), 'Outside Australia') == 0 ? ' selected' : ''); ?>>Outside Australia</option>
                            </optgroup>
                        </select>
                        <?php
                        foreach ($errors->get('current_location') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div id="outta_tassie">
                        @include('input.date', ['data' => $user, 'required' => false, 'name' => 'date_arrive', 'label' => 'Date arriving in Tasmania'])

                        <div class="form-group">
                            <label for="loca_arrive">Where are you arriving:</label>
                            <select name="loca_arrive" id="loca_arrive" class="form-control">
                                <option value=""></option>
                                <option value="Launceston"<?php echo (strcmp(old('loca_arrive'), 'Launceston') == 0 ? ' selected' : ''); ?>>Launceston</option>
                                <option value="Hobart"<?php echo (strcmp(old('loca_arrive'), 'Hobart') == 0 ? ' selected' : ''); ?>>Hobart</option>
                                <option value="Burnie/Devonport"<?php echo (strcmp(old('loca_arrive'), 'Burnie/Devonport') == 0 ? ' selected' : ''); ?>>Burnie/Devonport</option>
                                <option value="Other Tasmania"<?php echo (strcmp(old('loca_arrive'), 'Other Tasmania') == 0 ? ' selected' : ''); ?>>Other Tasmania</option>
                            </select>
                        </div>
                        <?php
                        foreach ($errors->get('loca_arrive') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>

                        <?php
                        $staying = array (
                            (object)array ('key' => 1, 'value' => '1 month'),
                            (object)array ('key' => 2, 'value' => '2 months'),
                            (object)array ('key' => 3, 'value' => '3 months'),
                            (object)array ('key' => 6, 'value' => '6 months'),
                            (object)array ('key' => 12, 'value' => '12 or more months')
                        );
                        ?>
                        @include('input.select', ['data' => $user, 'required' => false, 'name' => 'stay_length', 'label' => 'How long are you staying in Tasmania', 'source' => $staying ])
                    </div>

                    <?php
                    $cc = new App\Http\Controllers\CountriesController();
                    $countries = $cc->getCountries();
                    ?>

                    @include('input.select', ['data' => $user, 'required' => true, 'name' => 'country', 'label' => 'Nationality', 'source' => $countries ])

                    <?php
                    $visa = array(
                        (object)array('key' => 0, 'value' => 'No'),
                        (object)array('key' => 1, 'value' => 'Working'),
                        (object)array('key' => 2, 'value' => 'Student'),
                        (object)array('key' => 3, 'value' => 'My partner is a Student')
                    );
                    ?>
                    @include('input.select', ['data' => $user, 'name' => 'visa_88_days', 'label' => 'Are you on a working or student visa?', 'required' => false, 'source' => $visa])

                    <div id="internationalDetails">
                        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'passport_number', 'label' => 'Passport Number'])
                        @include('input.date', ['data' => $user, 'required' => false, 'name' => 'visa_expiry', 'label' => 'VISA expiry date'])
                    </div>

                    @include('input.select', ['data' => $user, 'name' => 'has_transport', 'label' => 'Do you have access to transport', 'required' => false, 'source' => $yesno])

                    @include('input.text', ['data' => $user, 'label' => 'If you\'re registering with a group, how many people are in your group?', 'name' => 'num_in_group', 'required' => false])

                    @include('input.select', ['data' => $user, 'name' => 'share_details', 'label' => 'Are you happy to carpool with other workers, starting at the same time as you', 'required' => false, 'source' => $yesno])

                    @include('input.text', ['data' => $user, 'label' => 'If someone referred you to us, who was it?', 'name' => 'referral', 'required' => false])

                    <?php if($user->campaign_id == null) { ?>
                    @include('input.select', ['data' => $user, 'name' => 'accommodation', 'label' => 'Are you interested in information regarding our accommodation service at Kingsley House in Longford, Northern Tasmania (http://www.kingsleyhouse.com.au)?', 'required' => false, 'source' => $yesno])
                    <?php } ?>
                </div>

                <div class="dashboard-tile">
                    <h2>Payment and Tax details</h2>
                    <div class="form-group">
                        <label for="bsb_none">
                            <input type="checkbox" name="no_bank_account" value="1" id="bsb_none"<?php echo ($no_bank_account == 1 ? 'checked' : ''); ?>> I don't have an Australian Bank Account
                        </label>
                    </div>
                    <div id="bank-details">
                    @include('input.text', ['data' => $user, 'label' => 'BSB', 'name' => 'account_bsb', 'required' => true])
                    @include('input.text', ['data' => $user, 'label' => 'Account Number', 'name' => 'account_number', 'required' => true])
                    @include('input.text', ['data' => $user, 'label' => 'Account Name', 'name' => 'account_name', 'required' => true])
                    </div>

                    <div class="form-group {{ $errors->has('tfn') ? ' has-error' : '' }}">
                        <label class="control-label" for="tfn">Tax File Number<span class="required">*</span>:</label>
                        <input type="text" class="form-control" name="tfn" id="tfn" value="{{ old('tfn') }}" <?php if(strcmp(old('tfn'), 'xxx xxx xxx') == 0) { echo ' readonly="readonly"'; } ?>>
                        @if ($errors->has('tfn'))
                            <div class="alert alert-danger alert-inline">
                                {{ $errors->first('tfn') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="tfn_none">
                            <input type="checkbox" id="tfn_none"<?php if(strcmp(old('tfn'), '000 000 000') == 0) { echo ' checked'; } ?>> I don't have a Tax File Number
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="tfn_applied">
                            <input type="checkbox" id="tfn_applied"<?php if(strcmp(old('tfn'), '111 111 111') == 0) { echo ' checked'; } ?>> I have applied for a Tax File Number
                        </label>
                    </div>

                    <div class="form-group">
                        <label><input type="checkbox" name="previous_names" value="1" <?php if($user->previous_names == 1 ? 'checked' : ''); ?>> I have dealt with the Australian Taxation Office using another name in the past.</label>
                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <label>Previous Given Name</label>
                                <input type="text" name="previous_given_name" value="<?php echo $user->previous_given_name; ?>" class="form-control">
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <label>Previous Middle Name(s)</label>
                                <input type="text" name="previous_middle_name" value="<?php echo $user->previous_middle_name; ?>" class="form-control">
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <label>Previous Surname</label>
                                <input type="text" name="previous_surname" value="<?php echo $user->previous_surname; ?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    @include('input.select', ['data' => $user, 'name' => 'tax_resident', 'label' => 'Are you an Australian Resident for taxation purposes', 'required' => true, 'source' => $yesno])
                    @include('input.select', ['data' => $user, 'name' => 'tax_free_threshold', 'label' => 'Do you want to claim the tax free threshold', 'required' => true, 'source' => $yesno])
                    @include('input.select', ['data' => $user, 'name' => 'senior_tax_offset', 'label' => 'Do you want to claim the Senior Australian Tax offset', 'required' => true, 'source' => $yesno])
                    @include('input.select', ['data' => $user, 'name' => 'help_debt', 'label' => 'Do you have a Higher Education Loan Program (HELP) debt', 'required' => true, 'source' => $yesno])
                    @include('input.select', ['data' => $user, 'name' => 'fs_debt', 'label' => 'Do you have a Financial Supplement debt', 'required' => true, 'source' => $yesno])
                </div>
                <div class="dashboard-tile">
                    <h2>Super Details</h2>
                    <div class="form-group">
                        <label for="super_none">
                            <input type="checkbox" name="no_super" value="1" id="super_none"<?php echo ($no_super == 1 ? 'checked' : ''); ?>> I don't have a Super Provider
                        </label>
                    </div>
                    <div id="super_details" style="display: <?php echo(strcmp(old('super_number'), 'xxx') == 0 ? 'none' : 'block'); ?>">
                        <div class="form-group">
                            <label for="provider_id">Super Fund<span class="required">*</span>:</label>
                            <?php
                            require base_path('app/Http/Controllers/Settings/SuperController.php');
                            $sc = new App\Http\Controllers\Settings\SuperController();
                            $super = $sc->getSuperProviders();
                            ?>

                            <select name="provider_id" class="form-control search" id="provider_id">
                                <option value="">Please Select</option>
                                <?php
                                foreach($super as $sup) {
                                ?>
                                <option value="<?php echo $sup->super_name; ?>" <?php if(strcmp(old('provider_id'), $sup->super_name) == 0) { echo 'selected'; } ?>><?php echo $sup->super_name; ?></option>
                                <?php } ?>
                            </select>
                            <?php
                            foreach ($errors->get('provider_id') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="super_not_listed">
                                <input type="checkbox" id="super_not_listed"<?php echo (strcmp(old('provider_id_other'), '') == 0 ? '' : ' checked'); ?> value="1"> My provider is not listed:
                            </label>
                        </div>
                        <div class="form-group" style="display: <?php echo (strcmp(old('provider_id_other'), '') == 0 ? 'none' : 'block'); ?>">
                            <label for="provider_id_other">Super fund name:</label>
                            <input type="text" name="provider_id_other" id="provider_id_other" value="{{ old('provider_id_other') }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="super_number">Member Number<span class="required">*</span>:</label>
                            <input type="text" name="super_number" id="super_number" value="<?php echo old('super_number'); ?>" class="form-control">
                            <?php
                            foreach ($errors->get('super_number') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="usi_id">Super USI<span class="required">*</span>:</label>
                            <?php $usis = $sc->getSuperUSIs(); ?>
                            <select name="usi_id" class="form-control search" id="usi_id">
                                <option value="">Please Select</option>
                                <?php
                                foreach($usis as $usi) {
                                ?>
                                <option value="<?php echo $usi->usi_id; ?>" <?php if(old('usi_id') == $usi->usi_id) { echo 'selected'; } ?>><?php echo $usi->usi_number; ?></option>
                                <?php } ?>
                            </select>
                            <?php
                            foreach ($errors->get('usi_id') as $error) {
                                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="usi_not_listed">
                                <input type="checkbox" id="usi_not_listed"<?php echo (strcmp(old('usi_id_other'), '') == 0 ? '' : ' checked'); ?> value="1"> My USI is not listed:
                            </label>
                        </div>
                        <div class="form-group" style="display: <?php echo (strcmp(old('provider_id_other'), '') == 0 ? 'none' : 'block'); ?>">
                            <label for="usi_id_other">Super USI:</label>
                            <input type="text" name="usi_id_other" id="usi_id_other" value="{{ old('usi_id_other') }}" class="form-control">
                        </div>
                    </div>
                </div>

                <?php
                //if(isset($payments) && is_array($payments)) {
                if(false) {
                ?>
                <div class="dashboard-tile" id="payment-detail">
                    <h2>Payment</h2>
                    <p>Before coming to work for us, we require a AU$50.00 surety payment, which will be refunded to you with your first payslip.  Below are details of any outstanding payments you are required to make to us.</p>
                    <?php
                    foreach($payments as $payment){
                    ?>
                    <ul>
                        <li><strong>Reference:</strong> LNX<?php echo sprintf('%05d', $payment->payment_id); ?></li>
                        <li><strong>Amount:</strong> $<?php echo $payment->payment_amount; ?></li>
                    </ul>
                    <p>Please choose a payment method below to complete this process:<br />
                    <input type="radio" name="payment_method[<?php echo $payment->payment_id; ?>]" data-type="card" data-id="<?php echo $payment->payment_id; ?>" value="2" />
                    <label>Credit Card</label>

                    <input type="radio" name="payment_method[<?php echo $payment->payment_id; ?>]" data-type="eft" data-id="<?php echo $payment->payment_id; ?>" value="1" />
                    <label>Bank Transfer</label></p>
                    <div class="payment-info"  data-type="card" data-id="<?php echo $payment->payment_id; ?>">
                        <p>Stay tuned, our credit card gateway is coming soon.</p>
                    </div>

                    <div class="payment-info"  data-type="eft" data-id="<?php echo $payment->payment_id; ?>">
                        <p>Please make a payment to the following details:</p>
                        <ul>
                            <li><strong>BSB:</strong> 123 456</li>
                            <li><strong>Account Number:</strong> 1234 5678</li>
                            <li><strong>Account Name:</strong> Linx Employment</li>
                            <li><strong>Reference:</strong> LNX<?php echo sprintf('%05d', $payment->payment_id); ?></li>
                            <li><strong>Amount:</strong> $<?php echo $payment->payment_amount; ?></li>
                        </ul>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <?php
                }
                ?>

                <div class="dashboard-tile text-center">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </form>
        </div>
    </div>
@stop