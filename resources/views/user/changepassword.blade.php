@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Change password</h1>
            <p>Enter your current password, and your new one to change your password.</p>
            <div class="dashboard-tile">
                <form method="POST">
                    {!! csrf_field() !!}
                    @include('input.password', ['data' => null, 'required' => true, 'label' => 'Old Password', 'name' => 'old_password'])
                    @include('input.password', ['data' => null, 'required' => true, 'label' => 'New Password', 'name' => 'password'])
                    @include('input.password', ['data' => null, 'required' => true, 'label' => 'Confirm Password', 'name' => 'confirm_password'])
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-check"></i> Change password</button>
                </form>
            </div>
        </div>
    </div>
@stop