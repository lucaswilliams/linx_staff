<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>International Details</h2>
        <div class="form-group">
            <?php
                if(strlen(old('current_location')) > 0) {
                    $current_location = old('current_location');
                } elseif(isset($user->current_location)) {
                    $current_location = $user->current_location;
                } else {
                    $current_location = '';
                }
            ?>
            <label for="current_location">Where are you currently located<span class="required">*</span>:</label>
            <select name="current_location" id="current_location" class="form-control">
                <option value=""></option>
                <optgroup label="Tasmania">
                    <option value="Launceston"<?php echo (strcmp($current_location, 'Launceston') == 0 ? ' selected' : ''); ?>>Launceston</option>
                    <option value="Hobart"<?php echo (strcmp($current_location, 'Hobart') == 0 ? ' selected' : ''); ?>>Hobart</option>
                    <option value="Burnie/Devonport"<?php echo (strcmp($current_location, 'Burnie/Devonport') == 0 ? ' selected' : ''); ?>>Burnie/Devonport</option>
                    <option value="Other Tasmania"<?php echo (strcmp($current_location, 'Other Tasmania') == 0 ? ' selected' : ''); ?>>Other Tasmania</option>
                </optgroup>
                <optgroup label="Outside Tasmania">
                    <option value="Mainland Australia"<?php echo (strcmp($current_location, 'Mainland Australia') == 0 ? ' selected' : ''); ?>>Mainland Australia</option>
                    <option value="Outside Australia"<?php echo (strcmp($current_location, 'Outside Australia') == 0 ? ' selected' : ''); ?>>Outside Australia</option>
                </optgroup>
            </select>
            <?php
            foreach ($errors->get('current_location') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div id="outta_tassie" style="display: <?php echo (strcmp($current_location, 'Mainland Australia') == 0 || (strcmp($current_location, 'Outside Australia') == 0) ? 'block' : 'none'); ?>">
            @include('input.date', ['data' => $user, 'required' => false, 'name' => 'date_arrive', 'label' => 'Date arriving in Tasmania'])

            <div class="form-group">
                <?php
                    if(strlen(old('loca_arrive')) > 0) {
                        $loca_arrive = old('loca_arrive');
                    } else {
                        $loca_arrive = $user->loca_arrive;
                    }
                ?>
                <label for="loca_arrive">Where are you arriving:</label>
                <select name="loca_arrive" id="loca_arrive" class="form-control">
                    <option value=""></option>
                    <option value="Launceston"<?php echo (strcmp($loca_arrive, 'Launceston') == 0 ? ' selected' : ''); ?>>Launceston</option>
                    <option value="Hobart"<?php echo (strcmp($loca_arrive, 'Hobart') == 0 ? ' selected' : ''); ?>>Hobart</option>
                    <option value="Burnie/Devonport"<?php echo (strcmp($loca_arrive, 'Burnie/Devonport') == 0 ? ' selected' : ''); ?>>Burnie/Devonport</option>
                    <option value="Other Tasmania"<?php echo (strcmp($loca_arrive, 'Other Tasmania') == 0 ? ' selected' : ''); ?>>Other Tasmania</option>
                </select>
            </div>
            <?php
            foreach ($errors->get('loca_arrive') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>

            <?php
            $staying = array (
                    (object)array ('key' => 1, 'value' => '1 month'),
                    (object)array ('key' => 2, 'value' => '2 months'),
                    (object)array ('key' => 3, 'value' => '3 months'),
                    (object)array ('key' => 6, 'value' => '6 months'),
                    (object)array ('key' => 12, 'value' => '12 or more months')
            );
            ?>
            @include('input.select', ['data' => $user, 'required' => false, 'name' => 'stay_length', 'label' => 'How long are you staying in Tasmania', 'source' => $staying ])
        </div>

        <?php
        $cc = new App\Http\Controllers\CountriesController();
        $countries = $cc->getCountries();

        $yesno = array(
            (object)array('key' => 0, 'value' => 'No'),
            (object)array('key' => 1, 'value' => 'Yes')
        );
        ?>

        @include('input.select', ['data' => $user, 'required' => true, 'name' => 'country', 'label' => 'Nationality', 'source' => $countries ])

        <?php
            $visa = array(
                (object)array('key' => 0, 'value' => 'No'),
                (object)array('key' => 1, 'value' => 'Working'),
                (object)array('key' => 2, 'value' => 'Student'),
                (object)array('key' => 3, 'value' => 'My partner is a Student')
            );
        ?>
        @include('input.select', ['data' => $user, 'name' => 'visa_88_days', 'label' => 'Are you on a working or student visa?', 'required' => false, 'source' => $visa])

        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'passport_number', 'label' => 'Passport Number'])
        @include('input.date', ['data' => $user, 'required' => false, 'name' => 'visa_expiry', 'label' => 'VISA expiry date'])
        
        @include('input.select', ['data' => $user, 'name' => 'has_transport', 'label' => 'Do you have access to transport', 'required' => false, 'source' => $yesno])

        @include('input.text', ['data' => $user, 'label' => 'If you\'re registering with a group, how many people are in your group?', 'name' => 'num_in_group', 'required' => false])

        @include('input.select', ['data' => $user, 'name' => 'share_details', 'label' => 'Are you happy to carpool with other workers, starting at the same time as you', 'required' => false, 'source' => $yesno])

        @include('input.text', ['data' => $user, 'label' => 'If someone referred you to us, who was it?', 'name' => 'referral', 'required' => false])

        @include('input.select', ['data' => $user, 'name' => 'accommodation', 'label' => 'Are you interested in information regarding our accommodation service at Kingsley House in Longford, Northern Tasmania (http://www.kingsleyhouse.com.au)?', 'required' => false, 'source' => $yesno])
    </div>
    </div>
</div>
