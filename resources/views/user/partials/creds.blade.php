<?php
/*if(strlen(old('username')) > 0) {
    $username = old('username');
} else {
    $username = $user->username;
}*/
?>

<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Credential Details</h2>
        <table width="100%">
            <tr>
                <th>Name</th>
                <th>Use</th>
                <th>Expiry Date</th>
                <th>Description</th>
            </tr>
            <?php
                foreach($creds as $cred) { //echo '<pre>'; var_dump($cred); echo '</pre>';
            ?>
            <tr>
                <td>
                    <?php echo $cred->name; ?>
                </td>
                <td>
                    <input type="checkbox" name="cred_id[<?php echo $cred->cred_id; ?>]" <?php if($cred->id) { echo 'checked'; } ?> />
                </td>
                <td>
                    <?php if($cred->has_expiry) { ?>
                    <input type="date" name="expiry[<?php echo $cred->cred_id; ?>]" value="<?php echo $cred->expiry_date; ?>">
                    <?php } else { ?>
                    <input type="hidden" name="expiry[<?php echo $cred->cred_id; ?>]" value="<?php echo $cred->expiry_date; ?>">
                    <?php } ?>
                </td>
                <td>
                    <?php if($cred->has_description) { ?>
                    <textarea name="description[<?php echo $cred->cred_id; ?>]"><?php echo $cred->description; ?></textarea>
                    <?php } else { ?>
                    <input type="hidden" name="description[<?php echo $cred->cred_id; ?>]" value="<?php echo $cred->description; ?>">
                    <?php } ?>
                </td>
            </tr>
            <?php
                }
            ?>
        </table>
    </div>
</div>
