<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Personal Details</h2>
        @include('input.text', ['data' => $user, 'label' => 'First/given name', 'name' => 'given_name', 'required' => true])

        @include('input.text', ['data' => $user, 'label' => 'Surname', 'name' => 'surname', 'required' => true])
        @include('input.text', ['data' => $user, 'label' => 'Preferred name', 'name' => 'preferred', 'required' => false])

        @include('input.dob', ['data' => $user, 'label' => 'Date of Birth', 'required' => true, 'name' => 'date_of_birth'])

        <?php $genders = array (
                (object)array('key' => '0', 'value' => 'Not specified'),
                (object)array('key' => '1', 'value' => 'Male'),
                (object)array('key' => '2', 'value' => 'Female')
        );
        ?>
        <div class="row">
            @include('input.select', ['data' => $user, 'label' => 'Gender', 'name' => 'gender', 'source' => $genders, 'class' => 'col-xs-12 col-md-6'])
            <div class="form-group col-md-6">
                <label for="title">Title:</label>
                <select name="title" id="title" class="form-control">
                    <option value=""></option>
                    <option value="Mr"<?php echo ($user->title == "Mr" ? ' selected' : ''); ?>>Mr</option>
                    <option value="Mrs"<?php echo ($user->title == "Mrs" ? ' selected' : ''); ?>>Mrs</option>
                    <option value="Miss"<?php echo ($user->title == "Miss" ? ' selected' : ''); ?>>Miss</option>
                    <option value="Ms"<?php echo ($user->title == "Ms" ? ' selected' : ''); ?>>Ms</option>
                </select>
            </div>
        </div>
    </div>
</div>