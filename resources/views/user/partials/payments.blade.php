<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Payment Details</h2>

        <table width="100%">
            <tr>
                <th>Reference</th>
                <th>Amount</th>
                <th>Payment Method</th>
                <th>Date Made</th>
                <th>Date Received</th>
            </tr>
            <?php
            foreach($payments as $payment){
            ?>
            <tr>
                <td>
                    LNX<?php echo sprintf('%05d', $payment->payment_id); ?>
                </td>
                <td>
                    $<?php echo $payment->payment_amount; ?>
                </td>
                <td>
                    <?php echo $payment->provider_name; ?>
                </td>
                <td>
                    <?php echo $payment->payment_made; ?>
                </td>
                <td>
                    <?php
                    if($payment->payment_received > '2015-01-01') {
                        echo $payment->payment_received;
                    } else {
                        echo '<input type="text" class="date" name="payment_received['.$payment->payment_id.']" />';
                    }
                    ?>
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
    </div>
</div>
