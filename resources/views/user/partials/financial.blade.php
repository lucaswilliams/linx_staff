<?php
if(strlen(old('account_bsb')) > 0) {
    $account_bsb = old('account_bsb');
} else {
    $account_bsb = $user->account_bsb;
}

if(strlen(old('account_number')) > 0) {
    $account_number = old('account_number');
} else {
    $account_number = $user->account_number;
}

if(strlen(old('account_name')) > 0) {
    $account_name = old('account_name');
} else {
    $account_name = $user->account_name;
}
?>

<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Bank Details</h2>
        <div class="form-group">
            <label for="account_bsb">BSB<span class="required">*</span>:</label>
            <input type="text" name="account_bsb" id="account_bsb" value="<?php echo $account_bsb; ?>" class="form-control">
            <?php
            foreach ($errors->get('account_bsb') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="account_number">Account Number<span class="required">*</span>:</label>
            <input type="text" name="account_number" id="account_number" value="<?php echo $account_number; ?>" class="form-control">
            <?php
            foreach ($errors->get('account_number') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="account_name">Account Name<span class="required">*</span>:</label>
            <input type="text" name="account_name" id="account_name" value="<?php echo $account_name; ?>" class="form-control" maxlength="32">
            <?php
            foreach ($errors->get('account_name') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
    </div>
</div>