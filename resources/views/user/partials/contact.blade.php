<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Contact Details</h2>
        <?php
            $states = array();
            $states[] = (object)array('key' => 'ACT', 'value' => 'Australian Capital Territory');
            $states[] = (object)array('key' => 'NSW', 'value' => 'New South Wales');
            $states[] = (object)array('key' => 'NT', 'value' => 'Northern Territory');
            $states[] = (object)array('key' => 'QLD', 'value' => 'Queensland');
            $states[] = (object)array('key' => 'SA', 'value' => 'South Australia');
            $states[] = (object)array('key' => 'TAS', 'value' => 'Tasmania');
            $states[] = (object)array('key' => 'VIC', 'value' => 'Victoria');
            $states[] = (object)array('key' => 'WA', 'value' => 'Western Australia');
            $states[] = (object)array('key' => 'OS', 'value' => 'Overseas');
        ?>

        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'address', 'label' => 'Address'])
        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'city', 'label' => 'City'])
        @include('input.select', ['data' => $user, 'required' => false, 'name' => 'state', 'label' => 'State', 'source' => $states])
        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'postcode', 'label' => 'Postcode'])
        @include('input.tel', ['data' => $user, 'required' => false, 'name' => 'mobilephone', 'label' => 'Mobile Phone'])
        @include('input.tel', ['data' => $user, 'required' => false, 'name' => 'telephone', 'label' => 'Landline'])
        @include('input.email', ['data' => $user, 'required' => true, 'name' => 'email', 'label' => 'Email'])
        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'emergency_name', 'label' => 'Emergency Contact'])
        @include('input.text', ['data' => $user, 'required' => false, 'name' => 'emergency_number', 'label' => 'Emergency Phone'])
    </div>
</div>
