<?php
/*if(strlen(old('username')) > 0) {
    $username = old('username');
} else {
    $username = $user->username;
}*/
?>

<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Notes</h2>
        <div class="row">
            <div class="col-xs-12 text-right">
                <a class="btn btn-success pull-right user-notes" data-id="<?php echo $user->id; ?>" data-toggle="modal" data-target="#notesModal"><i class="fa fa-plus"></i> Add new note</a>
            </div>
        </div>
        <?php
        if($notes != null) {
            foreach($notes as $note) {
                echo '<div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            '.$note->given_name.' '.$note->surname.' at '.date('d/m/Y H:i', strtotime($note->note_stamp)).'
                        </div>
                        <div class="panel-body">
                            '.$note->note_content.'
                        </div>
                    </div>
                </div>
            </div>';
            }
        }
        ?>
    </div>
</div>
