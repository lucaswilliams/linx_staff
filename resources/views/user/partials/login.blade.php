<?php
if(strlen(old('username')) > 0) {
    $username = old('username');
} else {
    $username = $user->username;
}
?>

<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Login Details</h2>
        <div class="form-group">
            <label for="username">Username:</label>
            <input type="text" name="username" id="username" value="<?php echo $username; ?>" class="form-control">
            <?php
            foreach ($errors->get('username') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" name="password" id="password" value="" class="form-control">
            <?php
            foreach ($errors->get('password') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="password_confirmation">Password (again):</label>
            <input type="password" name="password_confirmation" id="password_confirmation" value="" class="form-control">
            <?php
            foreach ($errors->get('password_confirmation') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
    </div>
</div>
