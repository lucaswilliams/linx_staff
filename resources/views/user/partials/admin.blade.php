<?php
if(strlen(old('external_id')) > 0) {
    $external_id = old('external_id');
} else {
    $external_id = $user->external_id;
}

if(strlen(old('passport_number_validated')) > 0) {
    $passport_number_validated = old('passport_number_validated');
} else {
    $passport_number_validated = $user->passport_number_validated;
}

if(strlen(old('visa_expiry_validated')) > 0) {
    $visa_expiry_validated = old('visa_expiry_validated');
} else {
    $visa_expiry_validated = $user->visa_expiry_validated;
}

if(strlen(old('vivo_validated')) > 0) {
    $vivo_validated = old('vivo_validated');
} else {
    $vivo_validated = $user->vivo_validated;
}

if(strlen(old('level')) > 0) {
    $level = old('level');
} else {
    $level = $user->level;
}

if(strlen(old('is_supervisor')) > 0) {
    $is_supervisor = old('is_supervisor');
} else {
    $is_supervisor = $user->is_supervisor;
}

if(strlen(old('job_network')) > 0) {
    $job_network = old('job_network');
} else {
    $job_network = $user->job_network;
}
if(strlen(old('job_network_tick')) > 0) {
    $job_network_tick = old('job_network_tick');
} else {
    $job_network_tick = $user->job_network_tick;
}

if(old('seasonal_worker') != null) {
	$seasonal_worker = old('seasonal_worker');
} else {
	$seasonal_worker = $user->seasonal_worker;
}
?>

<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Administration Details</h2>

        <div class="form-group">
            <label for="external_id">Employee Number:</label>
            <input type="text" name="external_id" id="external_id" value="<?php echo $external_id; ?>" class="form-control">
            <?php
            foreach ($errors->get('external_id') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

		<div class="form-group">
			<label for="seasonal_worker">Part of the Seasonal Workers Program:</label>
			<select name="seasonal_worker" id="seasonal_worker" class="form-control">
				<option value="1" <?php if($seasonal_worker == 1) { echo 'selected'; } ?>>Yes</option>
				<option value="0" <?php if($seasonal_worker == 0) { echo 'selected'; } ?>>No</option>
			</select>
			<?php
			foreach ($errors->get('seasonal_worker') as $error) {
				echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
			}
			?>
		</div>

        <input type="hidden" name="myob_uid" id="myob_uid" value="<?php echo $user->myob_uid; ?>">

        <?php if($user->id > 0) { ?>
        <div class="form-group" id="in_myob" style="<?php echo (strlen($user->myob_uid) == 0 ? 'display:none;' : '') ?>">
            <p>This staff member is in MYOB.</p>
        </div>

        <div id="myob-load">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
        </div>

        <?php if(strlen($user->myob_uid) == 0) { ?>
        <div class="form-group">
            <a class="btn btn-success" id="client-myob" data-id="<?php echo $user->uid; ?>"><i class="fa fa-cloud-upload"></i> Add to MYOB</a>
        </div>
        <?php } ?>
        <?php } ?>

        <div class="form-group">
            <label for="job_network_tick">Are you registered with a Job Network:</label>
            <select name="job_network_tick" id="job_network_tick" class="form-control">
                <option value="1" <?php if($job_network_tick == 1) { echo 'selected'; } ?>>Yes</option>
                <option value="0" <?php if($job_network_tick == 0) { echo 'selected'; } ?>>No</option>
            </select>
            <?php
            foreach ($errors->get('job_network_tick') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="job_network">Job Network name:</label>
            <select name="job_network" id="job_network" class="form-control">
                <option value=""></option>
                <?php
                $jobnets = array(
                        'APM', 'Mission Australia', 'ORS', 'CVGT', 'Employment Plus', 'Max Employment', 'My Pathway', 'Other'
                );
                foreach($jobnets as $jobnet) { ?>
                <option value="<?php echo $jobnet; ?>" <?php if(strcmp($job_network, $jobnet) == 0) { echo 'selected'; } ?>><?php echo $jobnet; ?></option>
                <?php } ?>
            </select>
        </div>

        <div class="form-group">
            <label for="level">User Level:</label>
            <select name="level" id="level" class="form-control">
                <option value="0" <?php if($user->id > 0 && $level == 0) { echo 'selected'; } ?>>Applicant</option>
                <option value="1" <?php if($user->id == 0 || $level == 1) { echo 'selected'; } ?>>Worker</option>
                <option value="2" <?php if($level == 2) { echo 'selected'; } ?>>Linx Staff</option>
                <option value="3" <?php if($level == 3) { echo 'selected'; } ?>>Administrator</option>
            </select>
            <?php
            foreach ($errors->get('level') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="confirmed">Status:</label>
            <select name="confirmed" id="confirmed" class="form-control">
                <option value="4" <?php echo ($user->id == 0 || $user->confirmed == 4 ? 'selected' : ''); ?>>Active</option>
                <option value="3" <?php echo ($user->confirmed == 3 ? 'selected' : ''); ?>>Needs Induction</option>
                <option value="2" <?php echo ($user->confirmed == 2 ? 'selected' : ''); ?>>Accepted Offer</option>
                <option value="1" <?php echo ($user->confirmed == 1 ? 'selected' : ''); ?>>Offered</option>
                <option value="0" <?php echo ($user->id > 0 && $user->confirmed == 0 ? 'selected' : ''); ?>>Applicant</option>
                <option value="-1" <?php echo ($user->confirmed == -1 ? 'selected' : ''); ?>>Inactive</option>
            </select>
            <?php
            foreach ($errors->get('confirmed') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="is_supervisor">Is this staff member a supervisor?</label>
            <select name="is_supervisor" id="is_supervisor" class="form-control">
                <option value="0" <?php if($is_supervisor == 0) { echo 'selected'; } ?>>No</option>
                <option value="1" <?php if($is_supervisor == 1) { echo 'selected'; } ?>>Yes</option>
            </select>
            <?php
            foreach ($errors->get('is_supervisor') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="vivo_validated">Validated against VIVO:</label>
            <select name="vivo_validated" id="vivo_validated" class="form-control">
                <option value="1" <?php if($vivo_validated == 1) { echo 'selected'; } ?>>Yes</option>
                <option value="0" <?php if($vivo_validated == 0) { echo 'selected'; } ?>>No</option>
            </select>
            <?php
            foreach ($errors->get('vivo_validated') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
    </div>
</div>
