<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Job history details</h2>
        <table class="staff-table">
            <tr>
                <?php if($clie) { ?><th>Client</th><?php } ?>
                <th>Campaign Name</th>
                <th colspan="2">Dates</th>
                <th width="64px">Actions</th>
            </tr>
            <?php
                //echo '<pre>'; var_dump($jobs); echo '</pre>';
                if(isset($jobs)){
                    foreach($jobs as $job) {
            ?>
            <tr>
                <?php if($clie) { ?><td><?php echo $job->client_name; ?></td><?php } ?>
                <td><?php echo $job->name; ?></td>
                <td><?php echo date('d/m/Y', strtotime($job->start_date)); ?></td>
                <td><?php echo (($job->end_date != null) ? date('d/m/Y', strtotime($job->end_date)) : ''); ?></td>
                <td class="text-right">
                    <a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>/staff" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View shifts"><i class="fa fa-list"></i></a>
                </td>
            </tr>
            <?php
                    }
                }
            ?>
        </table>
    </div>
</div>