<?php
if(strlen(old('provider_id')) > 0) {
    $provider_id = old('provider_id');
} else {
    $provider_id = $user->provider_id;
}

if(strlen(old('usi_id')) > 0) {
	$usi_id = old('usi_id');
} else {
	$usi_id = $user->usi_id;
}

if(strlen(old('super_number')) > 0) {
    $super_number = old('super_number');
} else {
    $super_number = $user->super_number;
}
?>
<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Super Details</h2>
        <div class="form-group">
            <label for="provider_id">Super Fund<span class="required">*</span>:</label>
            <select name="provider_id" class="form-control search" id="provider_id">
                <option value="">Please Select</option>
                <?php foreach($super as $sup) { ?>
                    <option value="<?php echo $sup->super_name; ?>" <?php if(strcmp($provider_id, $sup->super_name) == 0) { echo 'selected'; } ?>><?php echo $sup->super_name; ?></option>
                <?php } ?>
            </select>
            <?php
            foreach ($errors->get('provider_id') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
        <div class="form-group">
            <label for="super_not_listed">
                <input type="checkbox" id="super_not_listed"<?php echo (strcmp($user->provider_id_other, '') == 0 ? '' : ' checked'); ?> value="1"> My provider is not listed:
            </label>
        </div>
        <div class="form-group" style="display: <?php echo (strcmp($user->provider_id_other, '') == 0 ? 'none' : 'block'); ?>">
            <label for="provider_id_other">Super fund name:</label>
            <input type="text" name="provider_id_other" id="provider_id_other" value="{{ $user->provider_id_other }}" class="form-control">
        </div>
        <div class="form-group">
            <label for="super_number">Member Number<span class="required">*</span>:</label>
            <input type="text" name="super_number" id="super_number" value="<?php echo $super_number; ?>" class="form-control">
            <?php
            foreach ($errors->get('super_number') as $error) {
                echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
            }
            ?>
        </div>
		<div class="form-group">
			<label for="usi_id">Super USI<span class="required">*</span>:</label>
			<select name="usi_id" class="form-control search" id="usi_id">
				<option value="">Please Select</option>
				<?php
				foreach($usis as $usi) {
				?>
				<option value="<?php echo $usi->usi_id; ?>" <?php if($usi_id == $usi->usi_id) { echo 'selected'; } ?>><?php echo $usi->usi_number; ?></option>
				<?php } ?>
			</select>
			<?php
			foreach ($errors->get('usi_id') as $error) {
				echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
			}
			?>
		</div>
		<div class="form-group">
			<label for="usi_not_listed">
				<input type="checkbox" id="usi_not_listed"<?php echo (strcmp($user->usi_id_other, '') == 0 ? '' : ' checked'); ?> value="1"> My USI is not listed:
			</label>
		</div>
		<div class="form-group" style="display: <?php echo (strcmp($user->provider_id_other, '') == 0 ? 'none' : 'block'); ?>">
			<label for="usi_id_other">Super USI:</label>
			<input type="text" name="usi_id_other" id="usi_id_other" value="{{ $user->usi_id_other }}" class="form-control">
		</div>
    </div>
</div>