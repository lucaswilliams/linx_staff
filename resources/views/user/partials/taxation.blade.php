<div class="col-xs-12">
    <div class="dashboard-tile">
        <h2>Tax Details</h2>

        <?php
            if(strlen(old('tfn')) > 0) {
                $tfn = old('tfn');
            } elseif(isset($user->tfn)) {
                $tfn = $user->tfn;
            } else {
                $tfn = 'xxx xxx xxx';
            }
        ?>
        <div class="form-group {{ $errors->has('tfn') ? ' has-error' : '' }}">
            <label class="control-label" for="tfn">Tax File Number<span class="required">*</span>:</label>
            <input type="text" class="form-control" name="tfn" id="tfn" value="<?php echo $tfn; ?>" <?php if(strcmp($tfn, 'xxx xxx xxx') == 0) { echo ' readonly="readonly"'; } ?>>
            @if ($errors->has('tfn'))
                <div class="alert alert-danger alert-inline">
                    {{ $errors->first('tfn') }}
                </div>
            @endif
        </div>

        <div class="form-group">
            <label for="tfn_none">
                <input type="checkbox" id="tfn_none"<?php if(strcmp($tfn, 'xxx xxx xxx') == 0) { echo ' checked'; } ?>> I don't have a Tax File Number
            </label>
        </div>

        <?php
        $yesno = array (
                (object)array('key' => '1', 'value' => 'Yes'),
                (object)array('key' => '0', 'value' => 'No')
        );
        ?>

        @include('input.select', ['data' => $user, 'name' => 'tax_resident', 'label' => 'Are you an Australian Resident for taxation purposes', 'required' => true, 'source' => $yesno])
        @include('input.select', ['data' => $user, 'name' => 'tax_free_threshold', 'label' => 'Do you want to claim the tax free threshold', 'required' => true, 'source' => $yesno])
        @include('input.select', ['data' => $user, 'name' => 'senior_tax_offset', 'label' => 'Do you want to claim the Senior Australian Tax offset', 'required' => true, 'source' => $yesno])
        @include('input.select', ['data' => $user, 'name' => 'help_debt', 'label' => 'Do you have a Higher Education Loan Program (HELP) debt', 'required' => true, 'source' => $yesno])
        @include('input.select', ['data' => $user, 'name' => 'fs_debt', 'label' => 'Do you have a Financial Supplement debt', 'required' => true, 'source' => $yesno])
    </div>
</div>
