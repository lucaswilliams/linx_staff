@extends('layouts.app')

@section('title', 'Upload a Resume')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form method="POST" id="resume-form" enctype="multipart/form-data">
                {{csrf_field()}}
                <?php
                    if($user->has_resume) {
                ?>
                    <div class="dashboard-tile">
                        <h1>Your resume</h1>
                        <p>You have previously uploaded a resume, which can be accessed below:</p>
                        <p><a href="{{url('profile/'.$user->id.'/resume')}}">Download my resume</a></p>
                    </div>
                <?php
                    }
                ?>


                <div class="dashboard-tile">
                    <h1>Upload a new resume</h1>
                    <p>If you have prior work experience that you need to show us, please upload a PDF or Word copy of your resume so it can be reviewed.</p>

                    <div class="form-group">
                        <label class="control-label" for="resume_file">Select the file to upload:</label>
                        <input type="file" class="form-control" name="resume_file" id="resume_file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" />
                    </div>

                </div>

                <div class="dashboard-tile text-center">
                    <button type="submit" class="btn btn-success" name="upload"><i class="fa fa-upload"></i> Upload Resume</button>
                    <button type="submit" class="btn btn-default" name="skip"><i class="fa fa-share"></i> Skip this step</button>
                </div>
            </form>
        </div>
    </div>
@stop