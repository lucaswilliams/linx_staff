@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1><?php echo (isset($_GET['type']) && $_GET['type'] == 0) ? 'Applicant' : 'Staff'; ?> List</h1>
            <p>Here's a list of all the staff in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" id="search-user">
            <div class="dashboard-tile">
                <h2>Search Staff</h2>
                @include('user.search')
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile">
                <h2>Results</h2>
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-xs-12 text-right">
                        <a href="{{ URL::to('/staff/add') }}" class="btn btn-success">
                            <i class="fa fa-plus"></i> Add Staff Member
                        </a>
                    </div>
                </div>
                <table class="staff-table">
                    <tr>
                        <th width="250px">
                            <div class="row">
                                <div class="col-xs-6">
                                    Name
                                </div>
                                <div class="col-xs-6 text-right">
                                    <i class="fa fa-caret-up" id="order-name-asc"></i>
                                    <i class="fa fa-caret-down" id="order-name-desc"></i>
                                </div>
                            </div>
                        </th>
                        <th width="100px">Employee ID</th>
                        <th width="150px">Email</th>
                        <th width="136px">Mobile Phone</th>
                        <th width="200px">Notes</th>
                        <th width="136px">
                            <div class="row">
                                <div class="col-xs-6">
                                    Registered
                                </div>
                                <div class="col-xs-6 text-right">
                                    <i class="fa fa-caret-up" id="order-date-asc"></i>
                                    <i class="fa fa-caret-down" id="order-date-desc"></i>
                                </div>
                            </div>
                        </th>
                        <th width="97px">Status</th>
                        <th width="196px">Actions</th>
                    </tr>
                    <?php
                        foreach($users as $user) {
                            switch($user->confirmed) {
                                case 0:
                                    if($user->has_medical == 1) {
                                        $active = 'Applicant';
                                    } elseif($user->has_medical == 2) {
                                        $active = '<strong class="text-danger">Medical alert</strong>';
                                    } else {
                                        $active = '<strong class="text-danger">No Medical</strong>';
                                    }
                                    break;
                                case 1: $active = 'Offered';
                                    break;
                                case 2: $active = 'Accepted';
                                    break;
                                case 3: $active = 'Induction Pending';
                                    break;
                                case 4: $active = 'Active';
                                    break;
                                case -1 : $active = 'Inactive';
                                    break;
                            }
                    ?>
                    <tr class="staff-table-row">
                        <td>
                            <a href="profile/<?php echo $user->uid; ?>"><?php echo $user->surname.', '.$user->given_name ?><?php echo (strlen($user->preferred) > 0 ? ' ('.$user->preferred.')' : '') ?></a>
                        </td>
                        <td><?php echo $user->external_id; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo $user->mobilephone; ?></td>
                        <td><?php echo $user->notes; ?><a class="btn btn-primary pull-right user-notes" data-id="<?php echo $user->uid; ?>" data-toggle="modal" data-target="#notesModal"><i class="fa fa-sticky-note-o"></i></a></td>
                        <td>
                            <?php echo (strtotime($user->created_at) > 0 ? date('d/m/Y', strtotime($user->created_at)) : ''); ?>
                            <?php
                            if(strlen($user->campaign_name) > 0) {
                                echo '<br>('.$user->campaign_name.')';
                            }
                            ?>
                        </td>
                        <td><?php echo $active; ?></td>
                        <td class="text-right">
                            <?php if($user->confirmed == 0 && $user->has_medical > 0) { ?>
                                <a href="profile/<?php echo $user->uid; ?>/approve" class="btn btn-success approve-staff"><i class="fa fa-check"></i></a>

                            <?php }?>
                            <a href="profile/<?php echo $user->uid; ?>" class="btn btn-warning"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="bottom" title="Edit profile"></i></a>
                            <a href="profile/<?php echo $user->uid; ?>/print" target="_blank" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print details"><i class="fa fa-print"></i></a>
                            <?php if($user->has_resume > 0) { ?>
                                <a href="profile/<?php echo $user->uid; ?>/resume" class="btn btn-default"><i class="fa fa-paperclip" data-toggle="tooltip" data-placement="bottom" title="View resume"></i></a>
                            <?php }?>
                            <?php /*if($user->myob_uid != null) {*/ ?>
                                <a href="visa/<?php echo $user->uid; ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print Second Visa form"><i class="fa fa-file-text"></i></a>
                                <a href="profile/refresh/<?php echo $user->uid; ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Sync from MYOB"><i class="fa fa-cloud-download"></i></a>
                            <?php /*}*/ ?>
                            <a href="profile/delete/<?php echo $user->uid; ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete staff"><i class="fa fa-trash-o"></i></a>

                            <a href="impersonate/<?php echo $user->uid; ?>" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Log in as user"><i class="fa fa-user-secret"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-xs-4">
                        <a id="paginate-back" class="btn btn-default disabled">
                            <i class="fa fa-caret-left"></i> Previous
                        </a>
                        <input type="hidden" id="pagenum" value="1">
                        <input type="hidden" id="ordering" value="asc">
                    </div>
                    <div class="col-xs-4 text-center" id="recordcount">
                        Page 1 of <?php echo ceil($usercount / 50); ?>. (<?php echo $usercount; ?> records)
                    </div>
                    <div class="col-xs-4 text-right">
                        <a id="paginate-forward" class="btn btn-default <?php if($usercount < 51) { echo 'disabled'; } ?>">
                            Next <i class="fa fa-caret-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container">


    <div class="modal fade" id="notesModal" tabindex="-1" role="dialog" aria-labelledby="notesModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title">Notes for Worker</h2>
                </div>
                <div class="modal-body row">
                    <div class="col-xs-12" id="user-notes-div">

                    </div>
                    <div class="col-xs-12 text-center" id="user-notes-loading">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop