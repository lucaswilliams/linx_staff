<div class="row" style="margin-bottom: 15px;">
    <div class="col-xs-12">
        <form method="POST" action="{{ url('/profile/notes') }}" id="user-note-form">
            <div class="row">
                <div class="col-xs-12">
                    <textarea name="note_content" class="form-control"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
//echo '<pre>'; var_dump($notes); echo '</pre>';
    foreach($notes as $note) {
        echo '<div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            '.$note->given_name.' '.$note->surname.' at '.date('d/m/Y H:i', strtotime($note->note_stamp)).'
                        </div>
                        <div class="panel-body">
                            '.$note->note_content.'
                        </div>
                    </div>
                </div>
            </div>';
    }
?>