<?php $user = Auth::user(); ?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Dashboard</h1>
    <p>This is your dashboard.  From here, it is possible for you to edit your details, and to submit your timesheets.</p>
    <?php if($user->is_supervisor == 1) { ?>
        <p>As a supervisor, it is also possible for you to approve or alter the timesheets for your workers for any given day, or view a report of what they have entered.</p>
    <?php } ?>
    <?php if($user->level > 1) { ?>
        <p>As an administrator, it is also possible for you to do a great many things in the system, such as setting up new clients and jobs; approving applicants to become staff members; and processing their timesheets into MYOB.</p>
    <?php } ?>
@endsection

@section('content')
    <div class="row dashboard">
        <?php if($worker) { ?>
            <div class="col-xs-12">
                <div class="dashboard-tile large text-center text-xs-left">
                    <?php if($signin > 0) { ?>
                        <h1>
                            <i class="fa fa-sign-in"></i>
                            <a href="{{ URL::to('timesheets/signout') }}">Sign Out</a>
                        </h1>
                    <?php } else { ?>
                        <h1>
                            <i class="fa fa-sign-in"></i>
                            <a href="{{ URL::to('timesheets/signin') }}">Sign In</a>
                        </h1>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <?php if($super || $user->level > 2) { ?>
            <div class="col-xs-12">
                <div class="dashboard-tile large text-center text-xs-left">
                    <h1>
                        <i class="fa fa-users"></i>
                        <a href="{{ URL::to('timesheets/signinSuper') }}">Manage workers</a>
                    </h1>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="dashboard-tile large text-center text-xs-left">
                    <h1>
                        <span class="fa-stack">
                          <i class="fa fa-share fa-stack-1x"></i>
                          <i class="fa fa-users fa-stack-half text-danger"></i>
                        </span>
                        <a href="{{ URL::to('timesheets/workers') }}">Assign workers</a>
                    </h1>
                </div>
            </div>
        <?php } ?>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-user"></i> <a href="{{ URL::to('profile') }}">My details</a></h1>
            </div>
        </div>

        <?php if($user->level > 1) { ?>
            <div class="col-xs-12 col-sm-4">
                <div class="dashboard-tile">
                    <h1><i class="fa fa-users"></i> <a href="{{ URL::to('staff') }}">Staff List</a></h1>
                </div>
            </div>
        <?php } ?>

        <?php if($user->is_supervisor == 1 || $user->level > 2) {  /* Can't see timesheets if you aren't staff. */?>
        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-clock-o"></i> <a href="{{ URL::to('timesheets') }}">Timesheets</a></h1>
            </div>
        </div>

        <?php if($user->level > 1) { ?>
        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-hourglass-half"></i> <a href="{{ URL::to('timesheets/manage') }}?type=0">Time</a></h1>
            </div>
        </div>
        <?php } ?>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1>
                    <i class="fa fa-money"></i> <a href="{{ URL::to('payslips') }}">Payslips</a>
                </h1>
            </div>
        </div>
        <?php } ?>

        <?php if($user->level > 1) { ?>
        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><span class="fa-stack">
                        <i class="fa fa-file-o fa-stack-1x"></i>
                        <i class="fa fa-usd fa-stack-half"></i>
                    </span> <a href="{{ URL::to('invoices') }}">Invoices</a></h1>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-building"></i> <a href="{{ URL::to('client') }}">Client List</a></h1>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-briefcase"></i> <a href="{{ URL::to('jobs') }}">Jobs List</a></h1>
            </div>
        </div>

        <?php } ?>

        <?php if($report) { ?>
        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-line-chart"></i> <a href="{{ URL::to('reporting') }}">Reporting</a></h1>
            </div>
        </div>
        <?php } ?>

        <?php if($user->level > 2) { ?>
        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa fa-cogs"></i> <a href="{{ URL::to('settings') }}">Settings</a></h1>
            </div>
        </div>
        <?php } ?>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>Profile completion</h1>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa <?php echo ($user->has_resume != 1) ? 'fa-exclamation-triangle text-danger' : 'fa-check'; ?>"></i> <a href="<?php echo url('resume'); ?>">My Resume</a></h1>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa <?php echo ($user->has_credentials != 1) ? 'fa-exclamation-triangle text-danger' : 'fa-check'; ?>"></i> <a href="<?php echo url('certificates'); ?>">Cards</a></h1>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="dashboard-tile">
                <h1><i class="fa <?php echo ($user->has_medical != 1) ? 'fa-exclamation-triangle text-danger' : 'fa-check'; ?>"></i> <a href="<?php echo url('medical'); ?>">Medical</a></h1>
            </div>
        </div>
    </div>
@stop