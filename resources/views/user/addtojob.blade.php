@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Approve Staff: <?php echo $user->given_name.' '.$user->surname; ?></h1>
    <p>To approve a user, you will need to add them to a job; this will send them an email</p>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
         <form method="POST" action="<?php echo url('/staff/approve'); ?>">
             <div class="dashboard-tile">
                 <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}" />

                 <div class="form-group">
                    <label for="induction">Select an Induction:</label>
                    <select name="induction" id="induction" class="noselect form-control">
                        <?php
                        foreach($inductions as $induction) {
                            echo '<option value="'.$induction->ind_id.'">'.$induction->ind_name.'</option>';
                        }
                        ?>
                    </select>
                 </div>

                 <div class="form-group row">
                     <div class="col-xs-12">
                         <label for="external_id">Employee Number:</label><br />
                     </div>

                     <div class="col-sm-3">
                         <input type="text" class="form-control" name="external_id" id="external_id" value="{{ $emp_id }}">
                     </div>
                 </div>

                 <div id="approveUserDetails">
                     <div class="row add_to_job form-group">
                         <div class="col-xs-12">
                            <label for="approve_job">Select job to add them to:</label>
                         </div>

                         <div class="col-sm-9">
                            <select name="approve_job[]" class="noselect job form-control">
                                <option value=""></option>
                                <?php
                                foreach($modal_jobs as $job) {
                                    echo '<option value="'.$job->id.'">'.$job->name.'('.$job->client_name.')</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <input type="date" name="approve_job_date[]" class="form-control" placeholder="dd/mm/yyyy">
                        </div>
                     </div>
                 </div>
             </div>
             <div class="dashboard-tile text-center">
                 <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Approve User</button>
             </div>
        </form>
    </div>
</div>

<div style="display: none" id="approveUserTemplate">
    <div class="row add_to_job form-group">
        <div class="col-sm-9">
            <select name="approve_job[]" class="noselect job">
                <option value=""></option>
                <?php
                foreach($modal_jobs as $job) {
                    echo '<option value="'.$job->id.'">'.$job->name.'('.$job->client_name.')</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-sm-3">
            <input type="date" name="approve_job_date[]" class="form-control" placeholder="dd/mm/yyyy">
        </div>
    </div>
</div>
@stop