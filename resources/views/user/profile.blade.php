@extends('layouts.app')

@section('title', 'Worker Profile')

@section('header')
    <h1><?php echo $user->given_name.' '.$user->surname; ?></h1>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <form method="POST" action="{{ URL::to('profile') }}" class="row">
            {!! csrf_field() !!}

            <input type="hidden" name="user_id" value="<?php echo $user->uid; ?>">

            <ul class="nav nav-tabs" id="profileTabs">
                <li role="presentation" class="active">
                    <a href="#personal" id="personal-tab">Personal</a>
                </li>
                <li role="presentation">
                    <a href="#contact" id="contact-tab">Contact</a>
                </li>
                <li role="presentation">
                    <a href="#login" id="login-tab">Login</a>
                </li>
                <li role="presentation">
                    <a href="#bank" id="bank-tab">Financial</a>
                </li>
                <li role="presentation">
                    <a href="#tax" id="tax-tab">Taxation</a>
                </li>
                <li role="presentation">
                    <a href="#super" id="super-tab">Superannuation</a>
                </li>
                <li role="presentation">
                    <a href="#intl" id="intl-tab">International</a>
                </li>
                <li role="presentation">
                    <a href="#cred" id="cred-tab">Certificates</a>
                </li>
                <?php if(Auth::user()->level > 1) { ?>
                <li role="presentation">
                    <a href="#hiring" id="hiring-tab">Hiring</a>
                </li>
                <li role="presentation">
                    <a href="#reports" id="reports-tab">Reports</a>
                </li>
                <li role="presentation">
                    <a href="#notes" id="notes-tab">Notes</a>
                </li>
                <li role="presentation">
                    <a href="#admin" id="admin-tab">Administration</a>
                </li>
                <!--<li>
                    <a href="#payments" id="payments-tab">Payments</a>
                </li>-->
                <?php } ?>
            </ul>

            <div role="tabpanel" class="tab-pane fade active in" id="personal">
                @include('user.partials.personal')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="contact">
                @include('user.partials.contact')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="login">
                @include('user.partials.login')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="bank">
                @include('user.partials.financial')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="tax">
                @include('user.partials.taxation')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="super">
                @include('user.partials.super')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="intl">
                @include('user.partials.intl')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="cred">
                @include('user.partials.creds')
            </div>

            <?php if(Auth::user()->level > 1) { ?>
            <div role="tabpanel" class="tab-pane fade in" id="hiring">
                @include('user.partials.hiring')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="reports">
                @include('user.partials.reports')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="notes">
                @include('user.partials.notes')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="admin">
                @include('user.partials.admin')
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="payments">
                @include('user.partials.payments')
            </div>
            <?php } ?>

            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="notesModal" tabindex="-1" role="dialog" aria-labelledby="notesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Notes for Worker</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12" id="user-notes-div">

                </div>
                <div class="col-xs-12 text-center" id="user-notes-loading">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                </div>
            </div>
        </div>
    </div>
</div>
@stop