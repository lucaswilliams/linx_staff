@extends('layouts.print')

@section('title', 'Dashboard')

@section('content')
    <h1>
        <?php
            echo $user->given_name.' '.$user->surname;
            if(strlen($user->external_id) > 0) {
                echo ' ('.$user->external_id.')';
            }
        ?>
    </h1>
    <table width="100%" cellspacing="0" cellpadding="3">
        <tr>
            <td colspan="2" class="header">Personal Details</td>
        </tr>
        <tr>
            <td>Registered on:</td>
            <td><?php echo date('d/m/Y', strtotime($user->created_at)); ?></td>
        </tr>
        <?php if($user->campaign_name != null) { ?>
            <tr>
                <td>Campaign:</td>
                <td><?php echo $user->campaign_name; ?></td>
            </tr>
            <?php if($campaign != null) { ?>
                <?php foreach($campaign as $question) { ?>
                    <tr>
                        <td><?php echo $question->caqu_question; ?></td>
                        <td><?php echo ($question->cqop_value != null ? $question->cqop_value : ($question->response_string == '0' ? '' : $question->response_string)); ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="2" class="header">Personal Details</td>
        </tr>
        <tr>
            <td>Preferred Name:</td>
            <td><?php echo $user->preferred; ?></td>
        </tr>
        <tr>
            <td>Date of Birth:</td>
            <td><?php echo date('d/m/Y', strtotime($user->date_of_birth)); ?></td>
        </tr>
        <tr>
            <td>Gender:</td>
            <td><?php switch($user->gender) { case 1: echo 'Male'; break; case 2: echo 'Female'; break; default: echo 'Not stated'; break; } ?></td>
        </tr>

        <tr>
            <td colspan="2" class="header">Contact Details</td>
        </tr>
        <tr>
            <td>Address:</td>
            <td><?php echo $user->address; ?></td>
        </tr>
        <tr>
            <td>City:</td>
            <td><?php echo $user->city; ?></td>
        </tr>
        <tr>
            <td>State:</td>
            <td><?php echo $user->state; ?></td>
        </tr>
        <tr>
            <td>Postcode:</td>
            <td><?php echo $user->postcode; ?></td>
        </tr>
        <tr>
            <td>Home Phone:</td>
            <td><?php echo $user->telephone; ?></td>
        </tr>
        <tr>
            <td>Mobile Phone:</td>
            <td><?php echo $user->mobilephone; ?></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><?php echo $user->email; ?></td>
        </tr>
        <tr>
            <td>Emergency Contact:</td>
            <td><?php echo $user->emergency_name; ?></td>
        </tr>
        <tr>
            <td>Emergency Phone:</td>
            <td><?php echo $user->emergency_number; ?></td>
        </tr>

        <tr>
            <td colspan="2" class="header">Financial Details</td>
        </tr>
        <tr>
            <td>BSB:</td>
            <td><?php echo $user->account_bsb; ?></td>
        </tr>
        <tr>
            <td>Account Number:</td>
            <td><?php echo $user->account_number; ?></td>
        </tr>
        <tr>
            <td>Account Name:</td>
            <td><?php echo $user->account_name; ?></td>
        </tr>

        <tr>
            <td colspan="2" class="header">Taxation Details</td>
        </tr>
        <tr>
            <td>Tax File Number:</td>
            <td><?php echo $user->tfn; ?></td>
        </tr>
        <tr>
            <td>Australian Resident:</td>
            <td><?php echo ($user->tax_resident == 0) ? 'No' : 'Yes'; ?></td>
        </tr>
        <tr>
            <td>Tax Free Threshold:</td>
            <td><?php echo ($user->tax_free_threshold == 0) ? 'No' : 'Yes'; ?></td>
        </tr>
        <tr>
            <td>Senior Australian Tax Offset:</td>
            <td><?php echo ($user->senior_tax_offset == 0) ? 'No' : 'Yes'; ?></td>
        </tr>
        <tr>
            <td>HELP debt:</td>
            <td><?php echo ($user->help_debt == 0) ? 'No' : 'Yes'; ?></td>
        </tr>

        <tr>
            <td colspan="2" class="header">Superannuation Details</td>
        </tr>
        <tr>
            <td>Super Fund:</td>
            <td><?php echo (strlen($user->provider_id) > 0 ? $user->provider_id : $user->provider_id_other); ?></td>
        </tr>
        <tr>
            <td>Member Number:</td>
            <td><?php echo $user->super_number; ?></td>
        </tr>
		<tr>
			<td>Super USI:</td>
			<td><?php echo (strlen($user->usi_id) > 0 ? $user->usi_id : $user->usi_id_other); ?></td>
		</tr>
        <?php if($user->job_network_tick == 1) { ?>
        <tr>
            <td>Job Network:</td>
            <td><?php echo $user->job_network; ?></td>
        </tr>
        <?php } ?>

        <tr>
            <td colspan="2" class="header">Nationality Details</td>
        </tr>
        <tr>
            <td>Current Location:</td>
            <td><?php echo $user->current_location; ?></td>
        </tr>
        <tr>
            <td>Arriving at:</td>
            <td><?php echo $user->loca_arrive; ?></td>
        </tr>
        <tr>
            <td>Arriving on:</td>
            <td><?php echo (strtotime($user->date_arrive) > 0 ? date('d/m/Y', strtotime($user->date_arrive)) : ''); ?></td>
        </tr>
        <tr>
            <td>Nationality:</td>
            <td><?php echo $user->country; ?></td>
        </tr>
        <tr>
            <td>Working or Student Visa:</td>
            <td>
                <?php
                    switch($user->visa_88_days) {
                        case 0 : echo 'No'; break;
                        case 1 : echo 'Working'; break;
                        case 2 : echo 'Student'; break;
                        case 3 : echo 'My partner is a Student'; break;
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td>Passport Number:</td>
            <td><?php echo $user->passport_number; ?></td>
        </tr>
        <tr>
            <td>Visa Expiry:</td>
            <td><?php echo ((strtotime($user->visa_expiry)) > 0 ? date('d/m/Y', strtotime($user->visa_expiry)) : ''); ?></td>
        </tr>
        <tr>
            <td>Access to transport:</td>
            <td><?php echo ($user->has_transport == 1 ? 'Yes' : 'No'); ?></td>
        </tr>
        <tr>
            <td>Number in group:</td>
            <td><?php echo $user->num_in_group; ?></td>
        </tr>
        <tr>
            <td>Happy to carpool:</td>
            <td><?php echo ($user->share_details == 1 ? 'Yes' : 'No'); ?></td>
        </tr>
        <tr>
            <td>Referred by:</td>
            <td><?php echo $user->referral; ?></td>
        </tr>
        <tr>
            <td>Interested in Kingsley:</td>
            <td><?php echo ($user->accommodation == 1 ? 'Yes' : 'No'); ?></td>
        </tr>

        <?php if(count($creds) > 0) { ?>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2" class="header">Credentials Details</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" cellspacing="0" cellpadding="3">
                        <tr>
                            <td class="header">Name</td>
                            <td class="header">Expiry Date</td>
                            <td class="header">Description</td>
                        </tr>
                        <?php
                            foreach($creds as $cred) {
                                echo '<tr>
                                    <td>'.$cred->name.'</td>
                                    <td>'.$cred->expiry_date.'</td>
                                    <td>'.$cred->description.'</td>
                                </tr>';
                            }
                        ?>
                    </table>
                </td>
            </tr>
        <?php } ?>

        <?php if(count($jobs) > 0) { ?>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td colspan="2" class="header">Job Details</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" cellspacing="0" cellpadding="3">
                        <tr>
                            <td class="header">Job Name</td>
                            <td class="header">Client Name</td>
                            <td class="header">Start Date</td>
                        </tr>
                        <?php
                            foreach($jobs as $job) {
                                echo '<tr>';
                                    echo '<td>'.$job->job_name.'</td>';
                                    echo '<td>'.$job->client_name.'</td>';
                                    echo '<td>'.date('d/m/Y', strtotime($job->startdate)).'</td>';
                                echo '</tr>';
                            }
                        ?>
                    </table>
                </td>
            </tr>
        <?php } ?>

        <tr>
            <td colspan="2"><h3>Medical Questionnaire</h3></td>
        </tr>

        <?php if(count($med['medical']) > 0) { $medical_detail = $med['medical'][0];?>
        <tr>
            <td colspan="2">
                <table width="100%" cellspacing="0" cellpadding="3">
                    <tr class="header">
                        <td class="header" width="60%">Question</td>
                        <td class="header" width="40%"></td>
                    </tr>
                    <tr>
                        <td>Current height (in cm)</td>
                        <td><?php echo $medical_detail->height; ?></td>
                    </tr>
                    <tr>
                        <td>Current weight (in whole kg)</td>
                        <td><?php echo $medical_detail->weight; ?></td>
                    </tr>

                    <tr>
                        <td>Have you lost any time from work in the last 12 months due to illness or injury?</td>
                        <td><?php echo $medical_detail->lost_time; ?></td>
                    </tr>

                    <tr>
                        <td>Are you currently taking, or in the past two years have taken any medication?</td>
                        <td><?php echo $medical_detail->medication; ?></td>
                    </tr>

                    <tr>
                        <td>Do you have any known allergies?</td>
                        <td><?php echo $medical_detail->allergies; ?></td>
                    </tr>

                    <tr>
                        <td>Are you taking, or do you need to take, any medication for the known allergies?</td>
                        <td><?php echo $medical_detail->allergies_medication; ?></td>
                    </tr>

                    <tr>
                        <td>Is there any additional information relating to these allergies?</td>
                        <td><?php echo $medical_detail->allergies_additional; ?></td>
                    </tr>

                    <tr>
                        <td>Are you involved in any sporting activities or hobbies?</td>
                        <td><?php echo $medical_detail->sport_hobbies; ?></td>
                    </tr>

                    <tr>
                        <td>Do you have any condition or problem that may impact upon your ability to perform your job?</td>
                        <td><?php echo $medical_detail->existing_condition; ?></td>
                    </tr>

                    <td>How many hours at a time can you stand and/or walk for at a time?</td>
                    <td>
                        <?php
                            switch($medical_detail->stand) {
                                case 0: echo ''; break;
                                case 1: echo 'Less than 1 hour at a time'; break;
                                case 2: echo '1 – 2 hours'; break;
                                case 3: echo '2 - 4 hours'; break;
                                case 4: echo '4 – 6 hours'; break;
                                case 5: echo '6 – 8 hours'; break;
                                case 6: echo 'more than 8 hours at a time'; break;
                            }
                        ?>
                    </td>

                    <tr>
                        <td>Are you a current smoker?</td>
                        <td>
                            <?php switch($medical_detail->current_smoker) {
                                case 0 : echo 'No, never have been'; break;
                                case 1 : echo 'Yes'; break;
                                case 2 : echo 'No, but I used to be'; break;
                            }?>
                        </td>
                    </tr>
                    <tr>
                        <td>Average Cigarettes Per Day:</td>
                        <td><?php echo $medical_detail->smokes_daily_cigarettes; ?></td>
                    </tr>
                    <tr>
                        <td>When did you quit (year):</td>
                        <td><?php echo $medical_detail->smoker_quit_year; ?></td>
                    </tr>
                    <tr>
                        <td>How long did you smoke for (years):</td>
                        <td><?php echo $medical_detail->smoker_duration; ?></td>
                    </tr>

                    <tr>
                        <td>Do you drink alcohol?</td>
                        <td>
                            <?php switch($medical_detail->drink_alcohol) {
                                case 0 : echo 'No'; break;
                                case 1 : echo 'Yes'; break;
                            }?>
                        </td>
                    </tr>
                    <tr>
                        <td>Average Per Week (glasses):</td>
                        <td><?php echo $medical_detail->alcohol_per_week; ?></td>
                    </tr>
                    <tr>
                        <td>What type(s)?</td>
                        <td><?php echo $medical_detail->alcohol_type; ?></td>
                    </tr>

                    <tr>
                        <td>Have you been vaccinated against Hepatitis A?</td>
                        <td><?php echo ($medical_detail->vaccinate_hep_a) == 1 ? 'Yes' : 'No'; ?></td>
                    </tr>

                    <tr>
                        <td>Have you been vaccinated against Hepatitis B?</td>
                        <td><?php echo ($medical_detail->vaccinate_hep_b) == 1 ? 'Yes' : 'No'; ?></td>
                    </tr>

                    <tr>
                        <td>Have you been vaccinated against Tetanus?</td>
                        <td><?php echo ($medical_detail->vaccinate_tetanus) == 1 ? 'Yes' : 'No'; ?></td>
                    </tr>

                    <tr>
                        <td>Doctor's Name:</td>
                        <td><?php echo $medical_detail->doctor_1_name; ?></td>
                    </tr>

                    <tr>
                        <td>Clinic Name and Address:</td>
                        <td><?php echo $medical_detail->doctor_1_address; ?></td>
                    </tr>

                    <tr>
                        <td>Phone:</td>
                        <td><?php echo $medical_detail->doctor_1_phone; ?></td>
                    </tr>
                </table>


                <table width="100%" cellspacing="0" cellpadding="3">
                    <tr>
                        <td class="header">Question</td>
                        <td class="header">Duration</td>
                        <td class="header">Current Status</td>
                    </tr>
                    <?php foreach($med['questions'] as $question) {
                        if(strlen($question->duration) > 0 || strlen($question->status) > 0) {
                            echo '<tr>';
                            echo '<td width="40%">'.$question->ques_text.'</td>';
                            echo '<td width="30%">'.$question->duration.'</td>';
                            echo '<td width="30%">'.$question->status.'</td>';
                            echo '</tr>';
                        }
                    }?>
                </table>
            </td>
        </tr>
        <?php } else { ?>
            <tr>
                <td colspan="2">
                    There is no medical questionnaire on file for this staff member.
                </td>
            </tr>
        <?php }?>
    </table>
@stop