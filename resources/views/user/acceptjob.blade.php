@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Complete Registration</h1>
            <h2><?php echo $user->given_name.' '.$user->surname; ?></h2>
            <p>Congratulations, you have been allocated a position at the following job(s):</p>
            <ul>
                <?php foreach($jobs as $job) { ?>
                    <li><?php echo $job->jobs_name; ?> (<?php echo $job->client_name; ?>)
                        <?php /*echo date('d/m/Y', strtotime($job->start_date)); ?> - <?php echo date('d/m/Y', strtotime($job->end_date));*/ ?>
                    </li>
                <?php } ?>
            </ul>
            <p>Please confirm your acceptance of these positions with the button(s) below.</p>
        </div>
    </div>

    <div class="dashboard-tile text-center">
        <form method="POST" action="{{ URL::to('/profile/confirm') }}">
            {!! csrf_field() !!}
            <?php foreach($jobs as $job) { ?>
            <input name="jobs_id[]" type="hidden" value="<?php echo $job->id; ?>">
            <?php } ?>
            <input name="user_id" type="hidden" value="<?php echo $user->id; ?>">
            <button type="submit" class="btn btn-success btn-save"><i class="fa fa-check"></i> Accept offer</button>
        </form>
        <form method="POST" action="{{ URL::to('/profile/reject') }}">
            {!! csrf_field() !!}
            <?php foreach($jobs as $job) { ?>
            <input name="jobs_id[]" type="hidden" value="<?php echo $job->id; ?>">
            <?php } ?>
            <input name="user_id" type="hidden" value="<?php echo $user->id; ?>">
            <button type="submit" class="btn btn-danger btn-times"><i class="fa fa-times"></i> Reject offer</button>
        </form>
    </div>
@stop