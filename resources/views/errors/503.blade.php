<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Linx Timesheet Management System | @section('title') @show</title>
    @section('meta_keywords')
        <meta name="keywords" content="your, awesome, keywords, here"/>
    @show @section('meta_author')
        <meta name="author" content="Lucas Williams"/>
    @show @section('meta_description')
        <meta name="description"
              content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei."/>
    @show
    <link href='//fonts.googleapis.com/css?family=Orbitron:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->

    <?php
    $server = $_SERVER["SERVER_NAME"];
    /*if(stristr($server, 'prototype.ddns') !== false || stristr($server, 'localhost') !== false || stristr($server, 'kingsley') !== false) {
        echo '<link href="'.asset('css/kingsley.css').'" rel="stylesheet">';
    } elseif(stristr($server, 'test.lucas') !== false) {
        echo '<link href="'.asset('css/test.css').'" rel="stylesheet">';
    } else {*/
        echo '<link href="'.asset('css/linx.css').'" rel="stylesheet">';
    //}
    ?>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.structure.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-ui.theme.min.css') }}" rel="stylesheet">

    @yield('styles')

    <script src="//cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{{ asset('assets/site/ico/favicon.ico') }}">
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h1>Timesheet Management System</h1>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-center">
                <h1>Maintenance Mode</h1>
                <p>This website is currently undergoing maintenance, and will be down for the next 24 hours.</p>
                <p>If your query is urgent, please contact Linx Employment on (03) 6330 2471</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

@include('partials.footer')

<!-- Scripts -->
<script>var BASE_URL = '<?php echo $app['url']->to('/'); ?>';</script>
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="{{ asset('js/modernizr-custom.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
@yield('scripts')
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/site.js') }}"></script>

</body>
</html>