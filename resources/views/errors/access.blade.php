@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Permissions Error</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <p>Oops!  It looks like you don't have permission to be here.</p>
            <p>Please go <a onClick="history.go(-1);">back</a> and try again.  If the problem persists, please let <a href="mailto:admin@linxemployment.com.au">the administrator</a> know of your difficulties so it can be investigated.</p>
        </div>
    </div>
@stop
