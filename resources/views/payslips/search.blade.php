@extends('layouts.app')

@section('title', 'Payslips')

@section('header')
    <h1>Payslips</h1>
    <p>Search payslips by using the form below.</p>
@endsection

@section('scripts')
    <script src="{{ asset('js/payslip.js') }}"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="dashboard-tile">
            <div class="col-xs-12">Payslips dated between</div>
            <div class="col-sm-6">
                <input type="date" name="slip_start" id="slip_start" class="form-control" value="<?php echo date('Y-m-d', strtotime('4 weeks ago, last Monday')); ?>">
            </div>
            <div class="col-sm-6">
                <input type="date" name="slip_end" id="slip_end" class="form-control" value="<?php echo date('Y-m-d', strtotime('next Sunday')); ?>">
            </div>
            <?php if($user->level > 1) { ?>
                <div class="col-sm-6">
                    <label for="user_id">Select worker(s):</label>
                    <!-- other fields for linx admin -->
                    <select name="user_id" id="user_id" class="searchAjax form-control" multiple>

                    </select>
                </div>
                <div class="col-sm-6">
                    <label for="jobs">Select job(s):</label>
                    <select name="jobs" id="jobs" class="search form-control" multiple>
                        <?php foreach($jobs as $job) {
                            echo '<option value="'.$job->id.'">'.$job->name.'</option>';
                        } ?>
                    </select>
                </div>
                <div class="col-xs-12 text-center" style="margin-top: 10px;">
                    <a class="btn btn-primary" href="payslips/retrieve">
                        <i class="fa fa-cloud-download"></i> Retrieve payslips from MYOB
                    </a>
                </div>
            <?php } else { ?>
                <!-- worker fields -->
                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user->id; ?>">
                <input type="hidden" name="jobs" id="jobs" value="0">
            <?php } ?>
            <div class="row" style="padding-top: 15px;">
                <div class="col-xs-12 text-right">
                    <button class="btn btn-success" id="payslipSearch"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </div>
        <div class="dashboard-tile">
            <table class="table table-striped" id="slip_table">
                <tr>
                    <th>Worker</th>
                    <th>Date</th>
                    <th>Number</th>
                    <th>Gross</th>
                    <th>Net</th>
                    <th>Actions</th>
                </tr>
            </table>
        </div>
    </div>
</div>
@stop