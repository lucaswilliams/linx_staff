<page style="width: 18cm;" backtop="0mm" backbottom="0mm" backleft="0mm" backright="10mm">
    <table style="width: 100%">
        <tr>
            <td style="width: 100%">
                <h1>Linx Employment Pty Ltd</h1>
                <p><b>ABN:</b> 77 162 415 689</p>
            </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
    </table>

    <table style="width: 100%;">
        <tr>
            <td style="width: 65%">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Pay Slip For:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            <?php echo $client->given_name.' '.$client->surname; ?><br />
                            <?php echo $client->address; ?><br />
                            <?php echo $client->city.' '.$client->state.' '.$client->postcode; ?>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>

                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Pay Period:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            <?php echo date('d/m/Y', strtotime($payslip->slip_start)); ?> to <?php echo date('d/m/Y', strtotime($payslip->slip_end)); ?>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Superannuation Fund:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            <?php echo $client->super_name; ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 35%; vertical-align: top;">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Payment Date:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            <?php echo date('d/m/Y', strtotime($payslip->slip_date)); ?>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Employee #:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            <?php echo $client->external_id; ?>
                        </td>
                    </tr>

                    <tr><td>&nbsp;</td></tr>

                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Gross Pay:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            $<?php echo number_format($payslip->slip_gross, 2); ?>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 40%; vertical-align: top;">
                            Net Pay:
                        </td>
                        <td style="width: 60%; vertical-align: top;">
                            $<?php echo number_format($payslip->slip_net, 2); ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2"></td></tr>
        <tr><td colspan="2"></td></tr>
    </table>

    <table cellspacing="0" style="width: 100%; border: solid 1px black;">
        <tr>
            <th style="width: 25%; vertical-align: top; background: #CCCCCC; font-weight: bold; border-right: solid 1px black;">Description</th>
            <th style="width: 15%; vertical-align: top; background: #CCCCCC; font-weight: bold; border-right: solid 1px black;">Hours/Units</th>
            <th style="width: 15%; vertical-align: top; background: #CCCCCC; font-weight: bold; border-right: solid 1px black;">Calc Rate</th>
            <th style="width: 15%; vertical-align: top; background: #CCCCCC; font-weight: bold; border-right: solid 1px black;">Amount</th>
            <th style="width: 15%; vertical-align: top; background: #CCCCCC; font-weight: bold; border-right: solid 1px black;">YTD</th>
            <th style="width: 15%; vertical-align: top; background: #CCCCCC; font-weight: bold">Type</th>
        </tr>

        <?php
            foreach($lines as $line) {
                //echo '<pre>'; var_dump($line); echo '</pre>';
                if(isset($line['items']) && $line['slit_type'] == 'Wage') {
                    foreach($line['items'] as $item) {
                        echo '<tr>';
                        echo '<td>';
                        echo $item['client_name'].' - '.$item['jobs_name'];
                        echo '</td>';
                        echo '<td align="right">';
                        echo $item['tita_quantity'];
                        echo '</td>';
                        echo '<td align="right">$';
                        echo $item['shta_rate'];
                        echo '</td>';
                        echo '<td align="right">$';
                        echo number_format($item['tita_amount'], 2);
                        echo '</td>';
                        echo '<td align="right">$'.number_format($line['slit_ytd'], 2).'</td>';
                        echo '<td align="right">Wage</td>';
                        echo '</tr>';
                    }
                } else {
        ?>
            <tr>
                <td colspan="3"><?php echo $line['slit_name']; ?></td>
                <td align="right">$<?php echo number_format($line['slit_amount'], 2); ?></td>
                <td align="right">$<?php echo number_format($line['slit_ytd'], 2); ?></td>
                <td align="right"><?php echo $line['slit_type']; ?></td>
            </tr>
        <?php
                }
        ?>
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
        <?php
            }
        ?>

    </table>
</page>