@extends('layouts.app')

@section('title', 'Edit role')

@section('header')
    <h1>Edit role</h1>
@stop

@section('content')
    <div class="dashboard-tile">
        <form class="form-vertical" method="POST" action="<?php echo url('jobs/roles'); ?>">
            <div class="row">
                <div class="col-xs-12">
                    <input type="hidden" name="role_id" value="<?php echo $role->role_id; ?>">
                    <input type="hidden" name="jobs_id" value="<?php echo $role->jobs_id; ?>">

                    <label for="role_name">Role name:</label>
                    <input type="text" name="role_name" id="role_name" class="form-control" value="<?php echo $role->role_name; ?>">

                    <label for="rate_id">Payrate:</label>
                    <select name="rate_id" id="rate_id" class="form-control">
                        <option value="0">(none)</option>
                        <?php foreach($rates as $t) { ?>
                        <option value="<?php echo $t->id; ?>" <?php echo ($t->id == $role->rate_id ? 'selected' : '') ?>><?php echo $t->name; ?></option>
                        <?php } ?>
                    </select>

					<label for="role_default">Default?</label>
					<input name="role_default" type="checkbox" id="role_default" value="1" <?php echo ($role->role_default == 1 ? 'checked' : ''); ?>>

				</div>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done</button>
                </div>
            </div>
        </form>
    </div>
@stop