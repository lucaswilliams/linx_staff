@extends('layouts.app')

@section('title', 'Edit task')

@section('header')
    <h1>Edit task</h1>
@stop

@section('content')
    <div class="dashboard-tile">
        <form class="form-vertical" method="POST" action="#">
            <div class="row">
                <div class="col-xs-12">
                    <input type="hidden" name="task_id" value="<?php echo $task->task_id; ?>">
                    <input type="hidden" name="jobs_id" value="<?php echo $task->jobs_id; ?>">
                    <label for="task_parent_id">Parent task:</label>
                    <select name="task_parent_id" id="task_parent_id" class="form-control">
                        <option value="0">(none)</option>
                        <?php foreach($tasks as $t) { ?>
                        <option value="<?php echo $t->task_id; ?>" <?php echo ($t->task_id == $task->task_parent_id ? 'selected' : '') ?>><?php echo $t->task_name; ?></option>
                        <?php } ?>
                    </select>

                    <label for="task_name">Task name:</label>
                    <input type="text" name="task_name" id="task_name" class="form-control" value="<?php echo $task->task_name; ?>">

                    <label for="task_desc">Task description (optional):</label>
                    <textarea name="task_desc" id="task_desc" class="form-control" rows="4"><?php echo $task->task_desc; ?></textarea>

                    <!--<label for="rate_id_1">Payrates:</label>
					<table width="100%" class="table table-striped">
						<tr>
							<th>Role</th>
							<th>Pay rate</th>
						</tr>
					<?php foreach($taskroles as $role) { ?>
						<tr>
							<td><?php echo $role->role_name; ?></td>
							<td>
								<select name="rate_id[<?php echo $role->taro_id; ?>]" class="form-control">
									<option value="0">(none)</option>
									<?php foreach($rates as $rate) { ?>
									<option value="<?php echo $rate->id; ?>" <?php echo ($rate->id == $role->rate_id ? 'selected' : '') ?>><?php echo $rate->name; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
					<?php } ?>
					</table>-->
                </div>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done</button>
                </div>
            </div>
        </form>
    </div>
@stop