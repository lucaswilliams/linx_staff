@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Copy Shift</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('/shifts/copy') }}">
            <input type="hidden" value="1" name="form_type" />
            <input type="hidden" value="<?php echo $shift->id; ?>" name="shift_id" />
            <input type="hidden" value="<?php echo $shift->jobs_id; ?>" name="jobs_id" />
            {!! csrf_field() !!}
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul>
                                <li>
                                    <strong>Date:</strong> <?php echo date('d/m/Y', strtotime($shift->start_time)); ?>
                                </li>
                                <li>
                                    <strong>Start Time:</strong> <?php echo date('H:i', strtotime($shift->start_time)); ?>
                                </li>
                                <li>
                                    <strong>End Time:</strong> <?php echo date('H:i', strtotime($shift->end_time)); ?>
                                </li>
                                <li>
                                    <strong>Break duration:</strong> <?php echo $shift->break_duration; ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="dashboard-tile">
                    <table class="table table-striped">
                        <tr>
                            <td colspan="3">
                                <label>Copy shifts until:</label>
                                <input type="date">
                                <a class="btn btn-warning" id="copy_destination"><i class="fa fa-file-o"></i> Copy</a>
                            </td>
                            <td class="text-right"><a class="btn btn-success" id="add_shift"><i class="fa fa-plus"></i> Add</a></td>
                        </tr>
                        <tr>
                            <th>New Date</th>
                            <th>New Start</th>
                            <th>New End</th>
                            <th>New Break Duration</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>
                                <input type="date" name="shift_date[]" value="<?php echo ($shift->start_time != null ? date('Y-m-d', strtotime($shift->start_time.' + 1 day')) : ''); ?>" class="form-control">
                            </td>

                            <td>
                                <input type="time" name="start_time[]" value="<?php echo ($shift->start_time != null ? date('H:i', strtotime($shift->start_time)) : ''); ?>" class="form-control">
                            </td>

                            <td>
                                <input type="time" name="end_time[]" value="<?php echo ($shift->end_time != null ? date('H:i', strtotime($shift->end_time)) : ''); ?>" class="form-control">
                            </td>

                            <td>
                                <select name="break_duration[]" class="form-control">
                                    <option value="0" <?php echo ($shift->break_duration == 0 ? 'selected' : ''); ?>>0:00</option>
                                    <option value="15" <?php echo ($shift->break_duration == 15 ? 'selected' : ''); ?>>0:15</option>
                                    <option value="30" <?php echo ($shift->break_duration == 30 ? 'selected' : ''); ?>>0:30</option>
                                    <option value="45" <?php echo ($shift->break_duration == 45 ? 'selected' : ''); ?>>0:45</option>
                                    <option value="60" <?php echo ($shift->break_duration == 60 ? 'selected' : ''); ?>>1:00</option>
                                </select>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger delete-copy-shift"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop