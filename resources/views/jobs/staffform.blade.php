<?php $maxint = 2147483647; ?>
@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1><?php echo (($job->id > 0) ? 'Edit' : 'Add'); ?> Job workers</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('jobs') }}">
            {!! csrf_field() !!}
			<div class="col-xs-4" style="padding-right: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_details">Details</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_roles">Roles</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_tasks">Tasks</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_super">Supervisors</button>
			</div>
			<div class="col-xs-2" style="padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_staff">Staff</button>
			</div><input type="hidden" value="<?php echo $job->id; ?>" name="job_id" />
            <input type="hidden" value="2" name="form_type" />
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <table width="100%" id="job_worklist">
                        <tr>
                            <th>Name</th>
                            <th width="200px">Start Date</th>
                            <th width="200px">End Date</th>
                            <th width="100px">Status</th>
                            <th width="110px">Actions</th>
                        </tr>
                        <?php
                            foreach($workers as $work) {
                                $activeWorker = ($work->enddate != null);
                        ?>
                        <tr class="<?php echo ($activeWorker ? 'archived' : ''); ?>">
                            <td>
                                <input type="hidden" name="user_ids[]" value="<?php echo $work->id; ?>">
                                <input type="hidden" name="supervisor_ids[]" value="<?php echo $work->supervisor_id; ?>">
                                <?php echo $work->given_name.' '.$work->surname; ?> (<?php echo $work->external_id; ?>)</td>
                            <td>
                                <input type="date" class="form-control" name="w_start_date[]" value="<?php echo $work->startdate; ?>">
                            </td>
                            <td>
                                <input type="date" class="form-control enddate" name="w_end_date[]" <?php echo ($activeWorker ? '' : 'style=display:none;"'); ?> value="<?php echo date('Y-m-d', strtotime($work->enddate)); ?>">
                            </td>
                            <td>
                                <?php
                                    switch($work->confirmed) {
                                        case 0:
                                            if($work->has_medical == 1) {
                                                $active = 'Applicant';
                                            } elseif($work->has_medical == 2) {
                                                $active = '<strong class="text-danger">Medical alert</strong>';
                                            } else {
                                                $active = '<strong class="text-danger">No Medical</strong>';
                                            }
                                            break;
                                        case 1: $active = 'Offered';
                                            break;
                                        case 2: $active = 'Accepted';
                                            break;
                                        case 3: $active = 'Induction Pending';
                                            break;
                                        case 4: $active = 'Active';
                                            break;
                                        case -1: $active = 'Active';
                                            break;
                                    }

                                    echo $active;
                                ?>
                            </td>
                            <td class="text-right">
                                <a class="btn btn-primary" href="{{ url('/jobs/sendworker') }}/{{ $work->jw_id }}"><i class="fa fa-cloud-upload"></i></a>
                                <a class="btn btn-danger archive-worker"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr id="lastWorkerRow">
                            <td colspan="2">
                                <button type="button" id="job-all-workers" class="btn btn-default">
                                    <i class="fa fa-eye"></i> Show all workers
                                </button>
                            </td>
                            <td colspan="3" class="text-right">
                                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <input type="checkbox" name="send_email" id="send_email" value="1" checked><label for="send_email">Send notification to new workers?</label><br />
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>

        <table style="display: none">
            <tr id="job_worklist_template">
                <td>
                    <input type="hidden" name="new_user_ids[]">
                </td>
                <td>
                    <input type="date" name="n_start_date[]">
                </td>
                <td>
                    <input type="date" name="n_end_date[]" style="display: none;">
                </td>
                <td>Active</td>
                <td class="text-right">
                    <a href="#" class="btn btn-danger archive-worker"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        </table>
        @include('modals.jobsuser')
    </div>
@stop