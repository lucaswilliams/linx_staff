@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Jobs List</h1>
            <p>Here's a list of all the jobs in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
				<?php if($all) { ?>
					<a href="<?php echo url('jobs'); ?>" class="btn btn-default"><i class="fa fa-eye-slash"></i> Show active</a>
				<?php } else { ?>
					<a href="<?php echo url('jobs/all'); ?>" class="btn btn-default"><i class="fa fa-eye"></i> Show all</a>
				<?php } ?>

                <a href="jobs/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                @include('jobs.listcontent')
            </div>
        </div>
    </div>
@stop