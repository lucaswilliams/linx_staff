@extends('layouts.printls')

@section('title', 'Print Job Sheet')

@section('content')
    <div class="page">
        <h1>Job information</h1>
        <ul>
            <li><strong>Job Name:</strong> <?php echo $job->client_name; ?></li>
            <li><strong>Client:</strong> <?php echo $job->name; ?></li>
        </ul>
        <table width="100%">
            <tr>
                <th>Worker</th>
                <th>Number</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>
            <?php foreach($workers as $worker) { ?>
                <tr>
                    <td><?php echo $worker->given_name; ?> <?php echo $worker->surname; ?></td>
                    <td><?php echo $worker->external_id; ?></td>
                    <td><?php echo $worker->mobilephone; ?></td>
                    <td><?php echo $worker->email; ?></td>
                    <td><?php echo date('d/m/Y', strtotime($worker->startdate)); ?></td>
                    <td><?php echo ($worker->enddate == null ? '': date('d/m/Y', strtotime($worker->enddate))); ?></td>
                </tr>
            <?php } ?>
        </table>
        <p>Printed on <?php echo date('d/m/y H:i'); ?></p>
    </div>
@stop