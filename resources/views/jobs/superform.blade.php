<?php $maxint = 2147483647; ?>
@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1><?php echo (($job->id > 0) ? 'Edit' : 'Add'); ?> Job supervisors</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('jobs') }}">
            {!! csrf_field() !!}
			<div class="col-xs-4" style="padding-right: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_details">Details</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_roles">Roles</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_tasks">Tasks</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_super">Supervisors</button>
			</div>
			<div class="col-xs-2" style="padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_staff">Staff</button>
			</div><input type="hidden" value="<?php echo $job->id; ?>" name="job_id" />
            <input type="hidden" value="8" name="form_type" />
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <table width="100%" id="job_worklist">
                        <tr>
                            <th>Name</th>
                            <th>Default</th>
                            <th width="100px">Actions</th>
                        </tr>
                        <?php
                            foreach($workers as $work) {
                        ?>
                        <tr>
                            <td><input type="hidden" name="user_ids[]" value="<?php echo $work->id; ?>"><?php echo $work->given_name.' '.(((strlen($work->preferred) > 0) && (strcmp($work->preferred, $work->given_name) != 0)) ? '('.$work->preferred.') ' : '').$work->surname; ?> (<?php echo $work->external_id; ?>)</td>
                            <td><?php echo ($work->default ? 'Yes' : ''); ?></td>
                            <td class="text-right">
                                <?php if(!$work->default) { ?>
                                <a href="super/<?php echo $work->id; ?>" class="btn btn-warning"><i class="fa fa-check-circle"></i></a>
                                <?php } ?>
                                <a href="super/delete/<?php echo $work->id; ?>" class="btn btn-danger remove-worker"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr id="lastWorkerRow">
                            <td colspan="6" class="text-right">
                                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>

        <table style="display: none">
            <tr id="job_worklist_template">
                <td>
                    <input type="hidden" name="new_user_ids[]">
                </td>
                <td></td>
                <td class="text-right">
                    <a href="#" class="btn btn-danger remove-worker"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        </table>
        @include('modals.jobsuser')
    </div>
@stop