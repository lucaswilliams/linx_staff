@extends('layouts.app')

@section('title', 'Shift Payrates')

@section('header')
    <h1>Shift history</h1>
    <p><?php echo $user['given_name'].' '.$user['surname'].' ('.$user['external_id'].')'; ?></p>
    <p><?php echo $user['jobs_name'].' ('.$user['client_name'].')'; ?></p>
@endsection

@section('content')
    <div class="dashboard-tile">
        <table style="width: 100%">
            <tr>
                <th>Task</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
                <th>Sun</th>
            </tr>
        <?php
            if(count($shifts) == 0) {
                echo '<tr><td colspan="2">There are no shifts for this person</td></tr>';
            } else {
                foreach($shifts as $wc => $tasks) {
            ?>
                <tr>
                    <td class="week-commencing" colspan="8">Week Commencing <?php echo date('d/m/Y', strtotime($wc)); ?></td>
                </tr>
                <?php
                    foreach($tasks as $name => $shiftlist) {
                ?>
                <tr>
                    <td><?php echo $name; ?></td>
                    <td align="right">
                    <?php if(isset($shiftlist[1])) {
                        echo $shiftlist[1]->tita_quantity;
                    } ?>
                    </td>
                    <td align="right">
                        <?php if(isset($shiftlist[2])) {
                            echo $shiftlist[2]->tita_quantity;
                        } ?>
                    </td>
                    <td align="right">
                        <?php if(isset($shiftlist[3])) {
                            echo $shiftlist[3]->tita_quantity;
                        } ?>
                    </td>
                    <td align="right">
                        <?php if(isset($shiftlist[4])) {
                            echo $shiftlist[4]->tita_quantity;
                        } ?>
                    </td>
                    <td align="right">
                        <?php if(isset($shiftlist[5])) {
                            echo $shiftlist[5]->tita_quantity;
                        } ?>
                    </td>
                    <td align="right">
                        <?php if(isset($shiftlist[6])) {
                            echo $shiftlist[6]->tita_quantity;
                        } ?>
                    </td>
                    <td align="right">
                        <?php if(isset($shiftlist[7])) {
                            echo $shiftlist[7]->tita_quantity;
                        } ?>
                    </td>
                </tr>
                <?php
                    }
                }
            }
        ?>
        </table>
    </div>
@endsection