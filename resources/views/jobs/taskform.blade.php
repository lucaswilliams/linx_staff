<?php $maxint = 2147483647; ?>
@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1><?php echo (($job->id > 0) ? 'Edit' : 'Add'); ?> Job tasks</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('jobs') }}">
            {!! csrf_field() !!}
            <div class="col-xs-4" style="padding-right: 0">
                <button class="btn btn-default" style="width: 100%" type="submit" name="save_details">Details</button>
            </div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_roles">Roles</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_tasks">Tasks</button>
			</div>
            <div class="col-xs-2" style="padding-right: 0; padding-left: 0">
                <button class="btn btn-default" style="width: 100%" type="submit" name="save_super">Supervisors</button>
            </div>
            <div class="col-xs-2" style="padding-left: 0">
                <button class="btn btn-default" style="width: 100%" type="submit" name="save_staff">Staff</button>
            </div>
			<input type="hidden" value="<?php echo $job->id; ?>" name="job_id" />
            <input type="hidden" value="4" name="form_type" />
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <table width="100%" id="job_worklist">
                        <tr>
                            <th>Name</th>
                            <th width="100px">Actions</th>
                        </tr>
                        <?php
                            foreach($tasks as $task) {
                        ?>
                        <tr>
                            <td><input type="hidden" name="task_ids[]" value="<?php echo $task->task_id; ?>"><?php echo $task->task_name; ?></td>
                            <td class="text-right">
                                <a href="<?php echo url('/jobs/tasks/edit/'.$task->task_id); ?>" class="btn btn-warning edit-task"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo url('/jobs/tasks/delete/'.$task->task_id); ?>" class="btn btn-danger remove-task"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr id="lastWorkerRow">
                            <td colspan="6" class="text-right">
                                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
        @include('modals.jobtask')
    </div>
@stop