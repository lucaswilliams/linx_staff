@extends('layouts.app')

@section('title', 'Shift Payrates')

@section('content')
<form method="POST">
    {{ csrf_field() }}
    <h1>Configure Shift Payrates</h1>
    <div class="dashboard-tile">
        <p>job information goes here</p>
    </div>
<?php foreach($shifts as $shift) { ?>
<div class="dashboard-tile">
    <table width="100%" id="shift-list">
        <tr>
            <th width="100px">Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Break</th>
            <th width="300px">Role</th>
            <th width="379px">Pay Type</th>
            <!--<th>Copy</th>-->
        </tr>
        <tr class="shift-info" data-id="<?php echo $shift->id; ?>">
            <td><?php echo date('Y-m-d', strtotime($shift->start_time)); ?></td>
            <td><?php echo date('H:i', strtotime($shift->start_time)); ?></td>
            <td><?php echo date('H:i', strtotime($shift->end_time)); ?></td>
            <td><?php echo $shift->break_duration; ?></td>
            <td><?php echo $shift->role_name; ?></td>
            <td>Contract</td>
            <!--<td class="text-right">
                <a class="btn btn-warning copy-rates"><i class="fa fa-files-o"></i></a>
                <a class="btn btn-default checkbox"><i class="fa fa-square-o"></i></a>
            </td>-->
        </tr>
    </table>
    <table width="100%" class="payrateTable" data-shift="<?php echo $shift->id; ?>">
        <tr>
            <th>Rate Name</th>
            <th>Rate Value</th>
            <th>Action</th>
        </tr>
        <tr>
            <td colspan="3" class="text-right">
                <button type="button" class="btn btn-success addPayrateBtn">
                    <i class="fa fa-plus"></i> Add item
                </button>
            </td>
        </tr>
        <?php
        foreach($shift->rates as $rate) {
            echo '<tr>
                <td>
                <input type="hidden" name="shpa_id['.$shift->id.'][]" value="'.$rate->shpa_id.'" class="shpa_id">
                <select name="rate_id['.$shift->id.'][]" class="form-control rate_id" autocomplete="off">';
                foreach($payrates as $payrate) {
                    $selected = '';
                    if($payrate->id == $rate->rate_id) {
                        $selected = ' selected';
                    }
                    echo '<option value="'.$payrate->id.'"'.$selected.'>'.$payrate->name.'</option>';
                }
            echo'</select>
            </td>
                <td><input type="text" name="shpa_rate['.$shift->id.'][]" value="'.$rate->shpa_rate.'" class="form-control shpa_rate"></td>
                <td class="text-right">
                    <a class="btn btn-danger removePayrate">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>';
        }
        ?>
    </table>
    <table style="display: none">
        <tr class="payrateTemplate">
            <td>
                <input type="hidden" name="shpa_id[<?php echo $shift->id; ?>][]" class="shpa_id">
                <select name="rate_id[<?php echo $shift->id; ?>][]" class="form-control rate_id" autocomplete="off">
                    <option>(please select)</option>
                    <?php foreach($payrates as $payrate) {
                        echo '<option value="'.$payrate->id.'">'.$payrate->name.'</option>';
                    } ?>
                </select>
            </td>
            <td><input type="text" name="shpa_rate[<?php echo $shift->id; ?>][]" class="form-control shpa_rate"></td>
            <td class="text-right">
                <a type="button" class="btn btn-danger removePayrate">
                    <i class="fa fa-trash-o"></i>
                </a>
            </td>
        </tr>
    </table>
</div>
<?php } ?>

<div class="dashboard-tile text-center">
    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Details</button>
</div>
</form>
@stop