@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1><?php echo (($shift->id > 0) ? 'Edit' : 'Add'); ?> Shift</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('/shifts/edit') }}">
            <input type="hidden" value="1" name="form_type" />
            <input type="hidden" value="<?php echo $shift->id; ?>" name="shift_id" />
            <input type="hidden" value="<?php echo $shift->jobs_id; ?>" name="jobs_id" />
            {!! csrf_field() !!}
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Shift Times</h3>
                            <p>Please enter the times that workers are expected to be present for this shift.  This is an indication only.</p>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="shift_date">Date:</label>
                            <input type="date" name="shift_date" value="<?php echo ($shift->start_time != null ? date('Y-m-d', strtotime($shift->start_time)) : ''); ?>" id="shift_date" class="form-control">
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="start_time">Start Time:</label>
                            @include('input/timefields', ['name' => 'start_time', 'value' => ($shift->start_time != null ? date('H:i', strtotime($shift->start_time)) : '')])
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="end_time">End Time:</label>
                            @include('input/timefields', ['name' => 'end_time', 'value' => ($shift->end_time != null ? date('H:i', strtotime($shift->end_time)) : '')])
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="name">Break duration:</label>
                            <select name="break_duration" class="form-control">
                                <option value="0" <?php echo ($shift->break_duration == 0 ? 'selected' : ''); ?>>0:00</option>
                                <option value="15" <?php echo ($shift->break_duration == 15 ? 'selected' : ''); ?>>0:15</option>
                                <option value="30" <?php echo ($shift->break_duration == 30 ? 'selected' : ''); ?>>0:30</option>
                                <option value="45" <?php echo ($shift->break_duration == 45 ? 'selected' : ''); ?>>0:45</option>
                                <option value="60" <?php echo ($shift->break_duration == 60 ? 'selected' : ''); ?>>1:00</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Tasks for this shift</h3>
                            <p>This will determine which tasks and pay rates are available to workers on this day.</p>
                            <table class="table table-striped" id="role_table">
                                <tr>
                                    <td colspan="4" class="text-right"><a class="btn btn-success" id="add_role_btn"><i class="fa fa-plus"></i> Add new</a></td>
                                </tr>
                                <tr>
                                    <th width="50%">Task Name</th>
                                    <th width="25%">Payment Type</th>
                                    <th width="25%">Rate</th>
                                    <th width="50px">Remove</th>
                                </tr>
                                <?php if(count($shifttasks) > 0) {
                                foreach($shifttasks as $shifttask) { ?>
                                <tr>
                                    <td>
                                        <select name="tasks[]" class="form-control search tasks-list">
                                            <option value=""></option>
                                            <?php foreach($tasks as $task) {
                                                $selected = '';
                                                if($task->task_id == $shifttask->task_id) {
                                                    $selected = ' selected';
                                                    $ratetype = $task->rate_type;
                                                    $rateamt = $task->staff_rate;
                                                }
                                                echo '<option value="'.$task->task_id.'"'.$selected.' data-type="'.$task->rate_type.'" data-rate="'.$task->staff_rate.'">'.$task->role_name.' - '.$task->task_name.'</option>';
                                            } ?>
                                        </select>
                                    </td>
                                    <td class="task-payment-type">
                                        <?php echo ($ratetype == 0 ? 'Hourly' : 'Contract'); ?>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" name="shta_rate[]" class="form-control task-payment-amount" value="<?php echo $shifttask->shta_rate; ?>" <?php echo ($ratetype == 0 ? ' readonly' : ''); ?>>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger delete_shift_role"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <?php }
                                } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>

    <table style="display: none">
        <tr id="new_role_template">
            <td>
                <select name="tasks[]" class="form-control tasks-list">
                    <option value=""></option>
                    <?php foreach($tasks as $task) {
                        echo '<option value="'.$task->task_id.'" data-type="'.$task->rate_type.'" data-rate="'.$task->staff_rate.'">'.$task->role_name.' - '.$task->task_name.'</option>';
                    } ?>
                </select>
            </td>
            <td class="task-payment-type">
            </td>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" name="shta_rate[]" class="form-control task-payment-amount">
                </div>
            </td>
            <td>
                <a class="btn btn-danger delete_shift_role"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    </table>
@stop