@extends('layouts.app')

@section('title', 'Add Task to Shifts')

@section('header')
    <h1>Add Task to Shifts</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST">
            {!! csrf_field() !!}
            <input type="hidden" name="jobsID" value="{{$shift_id}}" />
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="taskID">Task:</label>
                            <select name="taskID" id="taskID" class="form-control">
                                <option value=""></option>
                                <?php foreach($tasks as $role => $rtask) { ?>
                                <optgroup label="<?php echo $role; ?>">
                                    <?php foreach($rtask as $task) { ?>
                                    <option value="<?php echo $task->task_id; ?>"><?php echo $task->task_name; ?></option>
                                    <?php } ?>
                                </optgroup>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label for="startDate">Start Date:</label>
                            <input type="date" id="startDate" name="startDate" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label for="endDate">End Date:</label>
                            <input type="date" id="endDate" name="endDate" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
            </div>
        </form>
    </div>
@stop