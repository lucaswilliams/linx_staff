@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1><?php echo (($job->id > 0) ? 'Edit' : 'Add'); ?> Job</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('jobs') }}">
			<div class="col-xs-4" style="padding-right: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_details">Details</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_roles">Roles</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_tasks">Tasks</button>
			</div>
			<div class="col-xs-2" style="padding-right: 0; padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_super">Supervisors</button>
			</div>
			<div class="col-xs-2" style="padding-left: 0">
				<button class="btn btn-default" style="width: 100%" type="submit" name="save_staff">Staff</button>
			</div><input type="hidden" value="<?php echo $job->id; ?>" name="job_id" />
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="room_id" value="">

                    <div class="form-group">
                        <label for="client">Client:</label>
                        <select name="client" class="form-control search" id="client">
                            <option></option>
                            <?php
                                foreach($clients as $clie) {
                                    $selected = '';
                                    if($clie->id == $job->client_id) {
                                        $selected = ' selected';
                                    }
                                    echo '<option value="'.$clie->id.'"'.$selected.'>'.$clie->name.'</option>';
                                }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" value="<?php echo $job->name; ?>" id="name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="job_start">Start date:</label>
                        <input type="date" name="job_start" value="<?php echo $job->start_date; ?>" id="job_start" class="form-control date">
                    </div>

                    <div class="form-group">
                        <label for="job_end"><input type="checkbox" id="job_end" <?php echo (strtotime($job->end_date) > 0 ? 'checked' : ''); ?>>Job has finished:</label>
                        <input type="date" name="job_end" value="<?php echo $job->end_date; ?>" id="job_end_date" class="form-control date" <?php echo (strtotime($job->end_date) > 0 ? '' : 'style="display: none;"'); ?>>
                    </div>

                    <div class="form-group">
                        <label for="job_uid">Job Code</label>
                        <select name="job_uid" id="job_uid" class="search">
                            <option value="">No job</option>
                            <?php
                            $jobuid = '';
                            if(isset($job->jobs_uid)) { $jobuid = $job->jobs_uid; }
                            if(isset($mjobs->Items)) {
                            foreach($mjobs->Items as $job) { ?>
                            <option value="<?php echo $job->UID; ?>" <?php if(strcmp($job->UID, $jobuid) == 0) { echo 'selected'; } ?>><?php echo $job->Name; ?> (<?php echo $job->Number; ?>)</option>
                            <?php }} ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>

        <table style="display: none">
            <tr class="shift-info" data-id="" id="shift-list-template">
                <input type="hidden" name="shift_id[]" />
                <td><input type="date" class="form-control date" name="start_date[]"></td>
                <td>@include('input/timefields', ['name' => 'start_time[]'])</td>
                <td>@include('input/timefields', ['name' => 'end_time[]'])</td>
                <td>
                    <select class="form-control" name="break_duration[]">
                        <option value="0">0</option>
                        <option value="15">15</option>
                        <option value="30">30</option>
                        <option value="45">45</option>
                        <option value="60">60</option>
                    </select>
                </td>
                <td width="300px">

                </td>
                <td>
                    <select name="payment_type[]" class="payment_type form-control">
                        <option value="0">Hourly</option>
                        <option value="1">Contract</option>
                    </select>
                </td>
                <td>
                    <select name="payrate[]" class="form-control">
                        <option value="0"></option>
                        <?php foreach($hrate as $rate) { ?>
                        <option value="<?php echo $rate->id; ?>"><?php echo $rate->name; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <a class="btn btn-warning copy-shift"><i class="fa fa-clipboard"></i></a>
                </td>
            </tr>

            <tr id="job_worklist_template">
                <td><input type="hidden" name="user_ids[]"></td>
                <td>
                    <select name="supervisor_ids[]" class="form-control">
                        <option>(unassigned)</option>
                        <?php foreach($supervisors as $super) {
                            echo '<option value="'.$super->id.'">'.$super->given_name.' '.$super->surname.'</option>';
                        } ?>
                    </select>
                </td>
                <td>
                    <input type="date" name="w_start_date[]">
                    <input type="date" name="w_end_date[]">
                </td>
                <td class="text-right"><a href="#" class="btn btn-danger remove-worker"><i class="fa fa-trash"></i></a></td>
            </tr>
        </table>
        @include('modals.jobsuser')
    </div>
@stop