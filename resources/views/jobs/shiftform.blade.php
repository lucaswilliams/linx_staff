@extends('layouts.app')

@section('title', 'Edit Shift Tasks')

@section('header')
    <h1><?php echo (($job->id > 0) ? 'Edit' : 'Add'); ?> Job Shifts</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('jobs') }}">
            <div class="col-xs-4" style="padding-right: 0">
                <button class="btn btn-default" style="width: 100%" type="submit" name="save_details">Details</button>
            </div>
            <div class="col-xs-4" style="padding-right: 0; padding-left: 0">
                <button class="btn btn-default" style="width: 100%" type="submit" name="save_shifts">Shifts</button>
            </div>
            <div class="col-xs-4" style="padding-left: 0">
                <button class="btn btn-default" style="width: 100%" type="submit" name="save_staff">Staff</button>
            </div>
            <input type="hidden" value="1" name="form_type" />
            <input type="hidden" value="<?php echo $job->id; ?>" name="job_id" />
            {!! csrf_field() !!}
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <div class="col-xs-12 text-right">
                        <a class="btn btn-warning" id="btn-add-tasks" href="shifts/tasks"><i class="fa fa-clipboard"></i> Add tasks</a>
                        <a class="btn btn-success" id="btn-add-shift" href="shifts/add"><i class="fa fa-plus"></i> Add shift</a>
                    </div>
                    <table width="100%" id="shift-list">
                        <tr>
                            <th width="10%">Date</th>
                            <th width="10%">Start Time</th>
                            <th width="10%">End Time</th>
                            <th width="10%">Break</th>
                            <th width="45%">Task(s)</th>
                            <th width="15%">Actions</th>
                        </tr>
                        <?php foreach($shifts as $shift) { ?>
                            <tr class="shift-info" data-id="<?php echo $shift->id; ?>">
                                <td>
                                    <?php echo date('d/m/Y', strtotime($shift->start_time)); ?>
                                </td>
                                <td>
                                    <?php echo date('H:i', strtotime($shift->start_time)); ?>
                                </td>
                                <td>
                                    <?php echo ($shift->end_time == null ? '' : date('H:i', strtotime($shift->end_time))); ?>
                                </td>
                                <td>
                                    <?php echo $shift->break_duration; ?>
                                </td>
                                <td>
                                    <?php echo implode(', ', $shift->tasks); ?>
                                </td>
                                <td>
                                    <a class="btn btn-default" href="{{ URL::to('/shifts/edit') }}/<?php echo $shift->id; ?>"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-warning" href="{{ URL::to('/shifts/copy') }}/<?php echo $shift->id; ?>"><i class="fa fa-clipboard"></i></a>
                                    <a class="btn btn-danger" href="{{ URL::to('/shifts/delete') }}/<?php echo $shift->id; ?>"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <a href="{{ url('/jobs') }}" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</a>
                </div>
            </div>
        </form>
    </div>
@stop