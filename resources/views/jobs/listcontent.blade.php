<table class="staff-table tablesort">
    <thead>
    <tr>
        <?php if($clie) { ?><th>Client</th><?php } ?>
        <th>Campaign Name</th>
        <th>Dates</th>
        <th width="185px">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
        if(isset($jobs)){
            /*if(!is_array($jobs)) {
                $jobs = array($jobs);
            }*/
            foreach($jobs as $job) { ?>
            <tr>
                <?php if($clie) { ?><td><?php echo $job->client_name; ?></td><?php } ?>
                <td><?php echo $job->name; ?></td>
                <td><?php echo date('d/m/Y', strtotime($job->start_date)); ?> - <?php echo (($job->end_date != null) ? date('d/m/Y', strtotime($job->end_date)) : ''); ?></td>
                <td class="text-right">
					<div class="btn-group" style="width:80px">
						<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-pencil"></i> Edit <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>">Details</a></li>
							<li><a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>/staff">Workers</a></li>
							<li><a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>/super">Supervisors</a></li>
							<li><a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>/roles">Roles</a></li>
							<li><a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>/tasks">Tasks</a></li>
						</ul>
					</div>
                    <a href="{{URL::to('/jobs/')}}/<?php echo $job->id; ?>/print" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print details"><i class="fa fa-print"></i></a>
					<?php if(!isset($user)) { ?>
					<a href="{{URL::to('/jobs/')}}/delete/<?php echo $job->id; ?>" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete job"><i class="fa fa-trash-o"></i></a>
					<?php } ?>
                </td>
            </tr>
    <?php
            }
        }
    ?>
    </tbody>
</table>