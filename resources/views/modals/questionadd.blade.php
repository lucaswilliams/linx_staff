<div class="modal fade" id="addQuestionModal" tabindex="-1" role="dialog" aria-labelledby="addQuestionModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Add Question</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <p>Enter the text for your question in the area provided below</p>
                    <form id="add_question">

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success save-modal" data-dismiss="modal"><i class="fa fa-check"></i> Done</button>
            </div>
        </div>
    </div>
</div>

<form id="addQuestionModalContent" style="display: none;">
    {!! csrf_field() !!}
    <input type="hidden" id="ques_id" name="ques_id" />
    <input type="hidden" id="ind_id" name="ind_id" />

    <label for="ques_text">Question Text:</label>
    <textarea name="ques_text" id="ques_text"></textarea>
</form>