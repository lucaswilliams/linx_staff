<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Search Staff</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    @include('user.search')
                </div>
                <div class="col-xs-12">
                    <table class="staff-table">
                        <tr>
                            <th>Name</th>
                            <th>Employee ID</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-check"></i> Done</button>
            </div>
        </div>
    </div>
</div>