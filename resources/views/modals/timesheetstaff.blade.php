<div class="modal fade" id="timesheetViewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Timesheet details</h2>
            </div>
            <form method="post" action="{{ URL::to('timesheets/update') }}">
                {{ csrf_field() }}
                <div class="modal-body row">
                    <div class="col-xs-12" id="tsView">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="staffmodalclose"><i class="fa fa-check"></i> Update</button>
                </div>
            </form>
        </div>
    </div>
</div>