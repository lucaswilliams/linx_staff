<div class="modal fade" id="showPayrateModal" tabindex="-1" role="dialog" aria-labelledby="showPayrateModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Pay rates</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <p>The amounts for today are as follows:</p>
                    <table width="100%" id="payrate-table">
                        <tr class="header">
                            <th>Item Name</th>
                            <th>Payment Type</th>
                            <th>Rate per item</th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <?php
                    echo '<form method="post" action="timesheets/signin">';
                ?>
                {{ csrf_field() }}
                <?php
                echo '<input type="hidden" name="user_id" id="user_id" />
                    <input type="hidden" name="shift_id" id="shift_id" />
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Accept</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger"><i class="fa fa-times"></i> Decline</button>
                </form>';
                ?>
            </div>
        </div>
    </div>
</div>