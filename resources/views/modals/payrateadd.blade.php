<div class="modal fade" id="addPayrateModal" tabindex="-1" role="dialog" aria-labelledby="addPayrateModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Add Pay rate</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <p>Add a new contract rate here.  The amount is set against the shift.</p>
                    <form id="payrate_add">
                        {{ csrf_field() }}
                        <label for="rate_name">Pay rate name:</label>
                        <input type="text" class="form-control" name="name" id="rate_name">
                        <input type="hidden" name="rate_guid">
                        <input type="hidden" name="rate_type" value="1">
                        <input type="hidden" name="staff_rate" value="0">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success save-modal" data-dismiss="modal"><i class="fa fa-check"></i> Done</button>
            </div>
        </div>
    </div>
</div>