<div class="modal fade" id="addAnswerModal" tabindex="-1" role="dialog" aria-labelledby="addAnswerModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Add Question options</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <p>Set up the details for one of your potential answers here</p>
                    <form id="add_answer">

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success save-modal" data-dismiss="modal"><i class="fa fa-check"></i> Done</button>
            </div>
        </div>
    </div>
</div>

<form id="addAnswerModalContent" style="display: none;">
    {!! csrf_field() !!}
    <input type="hidden" id="opt_id" name="opt_id" />
    <input type="hidden" id="ques_id" name="ques_id" />

    <label for="opt_text">Answer Text:</label>
    <input type="text" class="form-input" name="opt_text" id="opt_text">

    <label for="opt_correct">Correct Answer?</label>
    <input type="checkbox" value="1" name="opt_correct" id="opt_correct">
</form>