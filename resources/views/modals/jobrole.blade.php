<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Add a role</h2>
            </div>

            <form class="form-vertical" method="POST" action="<?php echo url('jobs/roles'); ?>">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <input type="hidden" name="jobs_id" value="<?php echo $job->id; ?>">
                        <label for="role_name">Role name:</label>
                        <input type="text" name="role_name" id="role_name" class="form-control">
					</div>
                    <div class="col-xs-12">
                        <label for="rate_id">Payrate:</label>
                        <select name="rate_id" id="rate_id" class="form-control">
                            <option value="0">(none)</option>
                            <?php foreach($rates as $rate) { ?>
                            <option value="<?php echo $rate->id; ?>"><?php echo $rate->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
					<div class="col-xs-12">
						<label for="role_default">Default?</label>
						<input name="role_default" type="checkbox" id="role_default" value="1">
					</div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done</button>
                </div>
            </form>
        </div>
    </div>
</div>