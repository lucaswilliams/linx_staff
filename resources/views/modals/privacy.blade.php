<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="privacyModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Privacy Statement</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <p>This following document sets forth the Privacy Policy for the Linx Employment Timesheet Management System website, staff.linxemployment.com.au.</p>
                    <p>Linx Employment is a business that provides employees to Host employers in peak periods,<br />
                        Rosevears Drive,<br />
                        PO Box 41 Legana Tas 7277<br />
                        <a href="mailto:admin@linxemployment.com.au">admin@linxemployment.com.au</a><br />
                        0499 007 500</p>
                    <p>Linx Employment is committed to providing you with the best possible customer service experience. Linx Employment is bound by the Privacy Act 1988 (Crh), which sets out a number of principles concerning the privacy of individuals.</p>
                    <ul>
                        <li>Collection</li>
                        <li>Use and disclosure</li>
                        <li>Data quality</li>
                        <li>Openness</li>
                        <li>Access and correction</li>
                        <li>Identifiers</li>
                        <li>Anonymity</li>
                        <li>Transborder data flows</li>
                        <li>Sensitive information</li>
                    </ul>
                    <p>Linx Employment collects personal information in a variety of ways including email, phone, text, documented registration forms, and online registrations.</p>
                    <p>We will only collect information if it is necessary for payroll, taxation office, Department of Immigration, and validating previous employment history.</p>
                    <p>Individuals will be notified of our intended use of the personal information at the time of its collection.</p>
                    <p>Linx Employment will store any personal information collected securely and it will not be disclosed to other parties without the individual's consent.</p>
                    <p>If you wish to view your personal information, please contact Kim Slater at the above phone number. If you have any concerns about your personal information, please contact Kim Slater in writing and if you are not happy with our response, your complaint will be sent to the Privacy Commissioner's Office for their attention.</p>
                </div>
            </div>
        </div>
    </div>
</div>