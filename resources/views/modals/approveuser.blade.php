<div class="modal fade" id="approveUserModal" tabindex="-1" role="dialog" aria-labelledby="approveUserModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Approve Staff</h2>
            </div>
            <div class="modal-body row">
                <div class="col-xs-12">
                    <p>To approve a user, you will need to add them to a job; this will send them an email</p>
                    <form id="approve_user">

                    </form>
                    <div class="text-right" style="margin-top: 20px; ">
                        <button class="btn btn-success add-job"><i class="fa fa-plus"></i> Add to another job</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success save-modal" data-dismiss="modal"><i class="fa fa-check"></i> Done</button>
            </div>
        </div>
    </div>
</div>

<form id="approveUserModalContent" style="display: none">
    <input type="hidden" id="user_id" name="user_id" />

    <label for="induction">Select an Induction:</label>
    <select name="induction" id="induction" class="noselect">
        <?php
        foreach($inductions as $induction) {
            echo '<option value="'.$induction->ind_id.'">'.$induction->ind_name.'</option>';
        }
        ?>
    </select>

    <!--<label for="external_id">Employee Number:</label><br />
    <input type="text" class="form-input" name="external_id" id="external_id"><br />-->

    <label for="approve_job">Select job(s) to add them to:</label>
    <div class="row add_to_job form-group">
        <div class="col-sm-9">
            <select name="approve_job[]" class="noselect job">
                <option value=""></option>
                <?php
                foreach($modal_jobs as $job) {
                    echo '<option value="'.$job->id.'">'.$job->name.'('.$job->client_name.')</option>';
                }
                ?>
            </select>
            </div>
        <div class="col-sm-3">
            <input type="date" name="approve_job_date[]" class="form-control" placeholder="dd/mm/yyyy">
        </div>
    </div>
</form>