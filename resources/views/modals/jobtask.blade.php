<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Add a task</h2>
            </div>

            <form class="form-vertical" method="POST" action="#">
                <div class="modal-body row">
                    <div class="col-xs-12">
                        <input type="hidden" name="jobs_id" value="<?php echo $job->id; ?>">
                        <label for="task_parent_id">Parent task:</label>
                        <select name="task_parent_id" id="task_parent_id" class="form-control">
                            <option value="0">(none)</option>
                            <?php foreach($tasks as $task) { ?>
                                <option value="<?php echo $task->task_id; ?>"><?php echo $task->task_name; ?></option>
                            <?php } ?>
                        </select>

                        <label for="task_name">Task name:</label>
                        <input type="text" name="task_name" id="task_name" class="form-control">

                        <label for="task_desc">Task description (optional):</label>
                        <textarea name="task_desc" id="task_desc" class="form-control" rows="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done</button>
                </div>
            </form>
        </div>
    </div>
</div>