@extends('layouts.app')

@section('title', 'Charge Rate')

@section('header')
    <h1><?php echo (isset($charge_rate->charge_id) ? 'Edit' : 'Add'); ?> Charge Rate</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form action="<?php echo url('/settings/charge_rates'); ?>" method="POST">
                <div class="dashboard-tile">
                    <input type="hidden" name="charge_id" value="<?php echo (isset($charge_rate->charge_id) ? $charge_rate->charge_id : 0); ?>" />
                    @include('input.text', ['data' => $charge_rate, 'label' => 'Name', 'name' => 'charge_name', 'required' => true])
                    @include('input.text', ['data' => $charge_rate, 'label' => 'Rate', 'name' => 'charge_rate', 'required' => true])
                    @include('input.select', ['data' => $charge_rate, 'label' => 'Client', 'name' => 'client_id', 'required' => false, 'source' => $clients])
                </div>

                <div class="dashboard-tile text-center">
                    <button class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Changes</button>
                </div>
            </form>
        </div>
    </div>
@endsection