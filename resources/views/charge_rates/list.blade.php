@extends('layouts.app')

@section('title', 'Charge Rate List')

@section('header')
    <h1>Charge Rates</h1>
    <p>This is a complete listing of the charge rates in the system, both generic and those for particular clients.<br />
    <strong>Generic</strong> charge rates are applied to all clients in the system.<br />
    <strong>Specific</strong> charge rates are applied to only one client in the system.</p>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="charge_rates/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>

            <div class="dashboard-tile">
                <table width="100%">
                    <tr>
                        <th>Name</th>
                        <th>Client</th>
                        <th>Rate</th>
                        <th width="100px">Actions</th>
                    </tr>
                    <?php if(count($charge_rates) > 0) {
                        foreach($charge_rates as $rate) {
                            echo '<tr>
                                <td>'.$rate->charge_name.'</td>
                                <td>'.$rate->name.'</td>
                                <td class="text-right">'.$rate->charge_rate.'%</td>
                                <td>
                                    <a href="charge_rates/'.$rate->charge_id.'" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                    <a href="charge_rates/delete/'.$rate->charge_id.'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </td>';
                        }
                    } ?>
                </table>
            </div>
        </div>
    </div>
@endsection