<h2>My Shifts</h2>
<div class="dashboard-tile">
    <table width="100%">
        <tr>
            <th>Job</th>
            <th>Start</th>
            <th>End</th>
            <th>Action</th>
        </tr>
    <?php
        $this_user = Auth::user();
        //echo '<pre>'; var_dump($shifts); echo '</pre>';

        foreach($shifts as $shift) {
            echo '<tr>
                <td>'.$shift->job_name.'</td>
                <td>'.date('d/m/Y H:i', strtotime($shift->start_time)).'</td>
                <td>'.date('d/m/Y H:i', strtotime($shift->end_time)).'</td>
                <td>';
            if($shift->time_id > 0) {
                if($shift->approved_super == 1 && $shift->approved_user == 0) {
                    //Approve button
                    echo '<a class="btn btn-default" href="'.url('/timesheets/approve').'/'.$shift->time_id.'">
                        <i class="fa fa-eye"></i> Approve
                    </a>';
                } elseif($shift->approved_user == -1) {
                    echo '<i class="fa fa-exclamation text-danger"></i> Disputed';
                } elseif($shift->approved_super == 1 && $shift->approved_user == 1) {
                    echo '<i class="fa fa-check text-success"></i> Submitted';
                } elseif($shift->approved_user == 1) {
                    echo '<i class="fa fa-check text-success"></i> Submitted';
                } else {
                    //Sign out
                    echo '<a class="btn btn-default signOutBtn" href="'.url('/timesheets/signOut').'/'.$shift->time_id.'">
                        <i class="fa fa-sign-out"></i> Sign out
                    </a>';
                }
            } else {
                //Sign in
                //Any contract rates?
                if($shift->rates > 0) {
                    echo '<a class="btn btn-default view-payrates" data-shift="'.$shift->shift_id.'" data-user="'.$shift->user_id.'" data-toggle="modal" data-target="#showPayrateModal"><i class="fa fa-sign-in"></i> Sign In</a>';
                } else {
                    echo '<form method="POST" action="'.url('timesheets/signin').'">
                        <input type="hidden" name="user_id" id="user_id" value="'.$this_user->id.'" />
                        <input type="hidden" name="shift_id" id="shift_id" value="'.$shift->shift_id.'" />
                        <button type="submit" class="btn btn-default view-payrates" data-shift="'.$shift->shift_id.'" data-user="'.$shift->user_id.'"><i class="fa fa-sign-in"></i> Sign In</button>
                    </form>';
                }
            }
            echo '</td></tr>';
        }
    ?>
    </table>
</div>