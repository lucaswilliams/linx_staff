<input type="hidden" name="time_id" value="<?php echo $shift->time_id; ?>">

<div class="col-xs-12">
    <table width="100%" class="nostyle">
        <tr>
            <td width="50%">
                Job Name
            </td>
            <td width="50%">
                <?php echo $shift->job_name; ?>
            </td>
        </tr>

        <tr>
            <td width="50%">
                Shift Start
            </td>
            <td width="50%">
                <?php echo $shift->start_time; ?>
            </td>
        </tr>

        <tr>
            <td width="50%">
                Shift Finish
            </td>
            <td width="50%">
                <?php echo $shift->end_time; ?>
            </td>
        </tr>

        <tr>
            <td width="50%">
                Scheduled break
            </td>
            <td width="50%">
                <?php echo $shift->break_duration; ?>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <p><strong>Tasks</strong></p>
                <table class="table table-striped" id="time_tasks">
                    <tr>
                        <th width="40%">Task</th>
                        <th width="15%">Start</th>
                        <th width="15%">Finish</th>
                        <th width="15%">Break</th>
                        <th width="15%">Units</th>
                    </tr>
                    <?php foreach($tasks as $task) {
                        echo '<tr>
                                <td rowspan="2">'.$task->task_name.'
                                    <input type="hidden" name="tita_id['.$task->tita_id.']" value="'.$task->tita_id.'">
                                </td>
                                <td>'.date('H:i', strtotime($task->tita_user_start)).'</td>
                                <td>'.date('H:i', strtotime($task->tita_user_finish)).'</td>
                                <td>'.$task->tita_user_break_duration.'</td>
                                <td>'.($task->rate_type == 1 ? $task->tita_user_quantity : '').'</td>
                                </tr><tr>
                                <td'.(strtotime($task->tita_user_start) != strtotime($task->tita_start) ? ' class="mismatch"' : '').'>';
                            ?>
                                    @include('input/timefields', ['name' => 'tita_start['.$task->tita_id.']', 'value' => ($task->tita_start != null ? date('H:i', strtotime($task->tita_start)) : date('H:i', strtotime($shift->start_time)))])
                            <?php
                                echo '</td>
                                <td'.(strtotime($task->tita_user_finish) != strtotime($task->tita_finish) ? ' class="mismatch"' : '').'>';
                            ?>
                                    @include('input/timefields', ['name' => 'tita_finish['.$task->tita_id.']', 'value' => ($task->tita_finish != null ? date('H:i', strtotime($task->tita_finish)) : date('H:i', strtotime($shift->end_time)))])
                            <?php
                                echo '</td>
                                <td'.($task->tita_user_break_duration == $task->tita_break_duration ? '' : ' class="mismatch"').'>
                                    <select name="tita_break_duration['.$task->tita_id.']" class="form-control">
                                        <option value="0"'.(($task->tita_break_duration == 0) ? ' selected' : '').'>0</option>
                                        <option value="15"'.(($task->tita_break_duration == 15) ? ' selected' : '').'>15</option>
                                        <option value="30"'.(($task->tita_break_duration == 30) ? ' selected' : '').'>30</option>
                                        <option value="45"'.(($task->tita_break_duration == 45) ? ' selected' : '').'>45</option>
                                        <option value="60"'.(($task->tita_break_duration == 60) ? ' selected' : '').'>60</option>
                                    </select>
                                </td>
                                <td'.($task->tita_user_quantity == $task->tita_quantity ? '' : ' class="mismatch"').'>
                                    '.($task->rate_type == 1 ? '<input type="text" name="tita_quantity['.$task->tita_id.']" value="'.$task->tita_quantity.'" class="form-control">' : '').'
                                </td>
                                </tr>';
                    } ?>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="2" id="notes_desc">
                Notes
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="hidden" name="old_notes" value="<?php echo $shift->time_notes; ?>">
                <p><?php echo $shift->time_notes; ?></p>
                <textarea name="notes" id="notes" style="width: 100%"></textarea>
            </td>
        </tr>
    </table>
</div>