<h2>Supervised</h2>
<div class="dashboard-tile">
    <?php foreach($super as $client => $clients) { ?>
        <h2><?php echo $client; ?></h2>
        <?php foreach($clients as $job => $jobs) { ?>
            <h3><?php echo $job; ?></h3>
            <table width="100%">
                <tr>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Status</th>
                    <th width="201px">Actions</th>
                </tr>
                <?php foreach($jobs as $shift) { ?>
                    <tr>
                        <td><?php echo $shift->start_time; ?></td>
                        <td><?php echo $shift->end_time; ?></td>
                        <td><?php echo $shift->status; ?></td>
                        <td>
                            <a href="<?php echo url('/timesheets/shift/'.$shift->shift_id); ?>" class="btn btn-default"><i class="fa fa-pencil-square-o"></i> Record</a>
                            <?php if($shift->canCancel) { ?>
                                <a href="<?php echo url('/timesheets/shift/'.$shift->shift_id.'/cancel'); ?>" class="btn btn-danger"><i class="fa fa-ban"></i> Cancel</a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        <?php } ?>
    <?php } ?>
</div>