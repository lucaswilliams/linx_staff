@extends('layouts.app')

@section('title', 'Timesheets')

@section('content')
    <h1>Shift Details</h1>
    <?php echo $shift->client_name; ?><br />
    <?php echo $shift->job_name; ?><br />
    <?php echo date('d/m/Y H:i', strtotime($shift->start_time)); ?> - <?php echo date('H:i', strtotime($shift->end_time)); ?>

    <form method="post" action="{{ URL::to('/timesheets/shift') }}" id="superSignOut">
        {{ csrf_field() }}
        <input type="hidden" name="shift" value="<?php echo $shift->shift_id; ?>">

        <input type="hidden" name="date_start" value="<?php echo substr($shift->start_time, 0, 10); ?>">
        <a href="<?php echo url('/timesheets/shift/'.$shift->shift_id.'/copy'); ?>" class="btn btn-warning pull-right"><i class="fa fa-clipboard"></i> Copy from yesterday</a>
        <?php if(count($signinshifts) > 0) { ?>
        <h2>Sign in Workers</h2>
        <div class="dashboard-tile">
            <table width="100%">
                <tr>
                    <th>Status</th>
                    <th>Payroll Number</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                <?php
                    $i = 0;
                foreach($signinshifts as $shift) {
                    echo '<tr>';

                    //Status of shift
                    echo '<td>';
                    echo '<i class="fa fa-ban fa-2x text-danger" data-toggle="tooltip" data-placement="bottom" title="Not entered"></i>';
                    echo '</td>';

                    echo '<td>'.$shift->external_id.'</td>
                <td>'.$shift->given_name.' '.$shift->surname.'</td>';

                    echo '<td class="text-right">
                        <input type="hidden" name="user_id['.$i.']" value="'.$shift->user_id.'" />
                        <input type="hidden" name="shift_id['.$i.']" value="'.$shift->shift_id.'" />
                        <input type="hidden" name="signin['.$i.'] value="0" class="signinflag">
                        <button type="button" class="btn btn-default signin">
                            <i class="fa fa-sign-in"></i> Sign In
                        </button>
                    </td>';
                    $i++;
                }
                echo '</tr>';
                ?>

                <tr>
                    <td colspan="7" class="text-right">
                        <a class="btn btn-success" id="shiftworkers-signin"><i class="fa fa-check"></i> Select All</a>
                    </td>
                </tr>
            </table>
        </div>

        <?php } ?>

        <?php if(count($shifts) > 0) { ?>
        <h2>Sign out workers</h2>
        <div class="dashboard-tile">
            <table width="100%">
                <tr>
                    <th width="80px">Status</th>
                    <th width="70px">Payroll Number</th>
                    <th width="120px">Name</th>
                    <th>Details</th>
                    <th width="100px">Action</th>
                </tr>

                <?php
                foreach($shifts as $shift) {
                    echo '<tr>';

                    //Status of shift
                    echo '<td>';
                    if($shift->approved_user == -1) {
                        echo '<i class="fa fa-exclamation fa-2x text-danger" data-toggle="tooltip" data-placement="bottom" title="User would like to dispute"></i>';
                    } else {
                        if($shift->approved_user == 0) {
                            echo '<i class="fa fa-exclamation-circle fa-2x text-warning" data-toggle="tooltip" data-placement="bottom" title="Not worker approved"></i>';
                        }

                        if($shift->approved_super == 0) {
                            echo '<i class="fa fa-exclamation-triangle fa-2x text-warning" data-toggle="tooltip" data-placement="bottom" title="Not supervisor approved"></i>';
                        }

                        if(($shift->approved_user == 1) && ($shift->approved_super == 1)) {
                            echo '<i class="fa fa-check fa-2x text-success" data-toggle="tooltip" data-placement="bottom" title="Ready for processing"></i>';
                        }
                    }
                    echo '</td>';

                    echo '<td>'.$shift->external_id.'</td>
                <td>'.$shift->given_name.' '.$shift->surname.'</td>';

                    shiftTimes($shift);
                    echo '</tr>';
                }
                ?>

                <tr>
                    <td colspan="7" class="text-right">
                        <a class="btn btn-success" id="shiftworkers-all"><i class="fa fa-check"></i> Select All</a>
                    </td>
                </tr>
            </table>
        </div>
        <?php } ?>

        <div class="dashboard-tile text-right">
            <button type="submit" class="btn btn-success">
                <i class="fa fa-floppy-o"></i> Save
            </button>
        </div>

        <?php
        function shiftTimes($shift) {
            $canEdit = (($shift->approved_super == 0));
            $user = Auth::user();
            if($user->level > 2) {
                $canEdit = true;
            }
            echo '<td>
            <table width="100%" class="tita_table">
                <tr>
                    <th width="30%" colspan="2">Task</th>
                    <th width="25%">Start</th>
                    <th width="25%">Finish</th>
                    <th width="15%">Breaks</th>
                    <th width="10%">Quantity</th>
                    <th width="5%"></th>
                </tr>';

            foreach($shift->tasks as $task) {
                //echo '<pre>'; var_dump($task); echo '</pre>';
                echo '<tr>
                    <td rowspan="2"><strong>'.$task->role_name.'</strong><br />'.$task->task_name.'<input type="hidden" name="task_id['.$shift->time_id.'][]" value="'.$task->task_id.'"></td>
                    <td>W</td>
                    <td>'.date('H:i', strtotime($task->tita_user_start));
                echo '<input type="hidden" name="tita_user_start['.$shift->time_id.']['.$task->tita_id.']" value="'.date('H:i', strtotime($task->tita_user_start)).'" class="form-control">';
                echo '</td>
                    <td>'.date('H:i', strtotime($task->tita_user_finish));
                echo '<input type="hidden" name="tita_user_finish['.$shift->time_id.']['.$task->tita_id.']" value="'.date('H:i', strtotime($task->tita_user_finish)).'" class="form-control">';
                echo '</td>
                    <td>'.$task->tita_user_break_duration;
                echo '<input type="hidden" name="tita_user_break_duration['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_user_break_duration.'" class="form-control">';
                echo '</td>
                    <td>'.($task->rate_type == 1 ? $task->tita_user_quantity : '');
                if ($task->rate_type == 1) {
                    echo '<input type="hidden" name="tita_user_quantity['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_user_quantity.'" class="form-control">';
                }
                echo '</td>
                </tr>
                <tr>
                    <td>S</td>
                    <td>
                        <input type="hidden" name="tita_id['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_id.'">';
                if($canEdit) {
                    echo view('input/timefields')->with('name', 'tita_start['.$shift->time_id.']['.$task->tita_id.']')->with('value', date('H:i', strtotime(($task->tita_start != null) ? $task->tita_start : $task->tita_user_start)));
                } else {
                    echo date('H:i', strtotime(($shift->signed_out > 0) ? $task->tita_start : $shift->shift_start)).'<input type="hidden" name="tita_start['.$shift->time_id.']['.$task->tita_id.']" value="'.date('H:i', strtotime(($shift->signed_out > 0) ? $task->tita_start : $shift->shift_start)).'">';
                }
                echo '</td>

                    <td>';
                    if($canEdit) {
        echo view('input/timefields')->with('name', 'tita_end['.$shift->time_id.']['.$task->tita_id.']')->with('value', date('H:i', strtotime(($task->tita_finish > 0) ? $task->tita_finish : $task->tita_user_finish)));
                    } else {
                        echo date('H:i', strtotime(($shift->signed_out > 0) ? $task->tita_finish : $shift->shift_end)).'<input type="hidden" name="tita_end['.$shift->time_id.']['.$task->tita_id.']" value="'.date('H:i', strtotime(($shift->signed_out > 0) ? $task->tita_finish : $shift->shift_end)).'">';
                    }
                echo '</td>

                    <td>';
                if($canEdit) {
                    $duration = ($task->tita_break_duration != null) ? $task->tita_break_duration : $task->tita_user_break_duration;
                    echo '<select name="tita_break_duration['.$shift->time_id.']['.$task->tita_id.']" class="form-control">
                            <option value="0"'.(($duration == 0) ? ' selected' : '').'>0</option>
                            <option value="15"'.(($duration == 15) ? ' selected' : '').'>15</option>
                            <option value="30"'.(($duration == 30) ? ' selected' : '').'>30</option>
                            <option value="45"'.(($duration == 45) ? ' selected' : '').'>45</option>
                            <option value="60"'.(($duration == 60) ? ' selected' : '').'>60</option>
                        </select>';
                } else {
                    echo $task->tita_break_duration.'<input type="hidden" name=tita_break_duration['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_break_duration.'">';
                }
                echo '</td>

                    <td>';
                if($task->rate_type == 1) {
                    if(!$canEdit) {
                        echo $task->tita_quantity.'<input type="hidden" name="tita_quantity['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_quantity.'">';
                    } else {
                        echo '<input type="text" name="tita_quantity['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_user_quantity.'" class="form-control">';
                    }
                } else {
                    echo $task->tita_quantity;
                    //echo '<input type="hidden" name="tita_quantity['.$shift->time_id.']['.$task->tita_id.']" value="'.$task->tita_quantity.'">';
                }
                echo '</td>
                    </tr>';
                if(strlen($shift->time_notes) > 0) {
                    echo '<tr class="notes">
                        <td colspan="6">
                            '.$shift->time_notes.'
                        </td>
                    </tr>';
                }
            }
            echo'</table>
        </td>';

            //Either way, we need the "include" box
            echo '<td class="text-right">';
            if($canEdit) {
                //show the add task button
                echo '<a class="btn btn-danger delete_timesheet" data-id="'.$shift->time_id.'"><i class="fa fa-trash-o"></i></a>';
                echo '<a class="btn btn-success add_time_task" data-id="'.$shift->time_id.'"><i class="fa fa-plus"></i></a>';
            }
            //Show the right thing here
            if($shift->approved_user == -1) {
                echo '<i class="fa fa-exclamation text-danger"></i> Disputed';
                echo '<input type="hidden" name="time_id_ticks['.$shift->time_id.']" value="0">
            <input type="hidden" name="time_id['.$shift->time_id.']" value="'.$shift->time_id.'">';
            } else if ($shift->approved_super == 1 && $user->level < 3) {
                echo '<i class="fa fa-check text-success"></i> Submitted';
                echo '<input type="hidden" name="time_id_ticks['.$shift->time_id.']" value="0">
            <input type="hidden" name="time_id['.$shift->time_id.']" value="'.$shift->time_id.'">';
            } else {
                echo '<a class="btn btn-default time_id"><i class="fa fa-check-square-o"></i></a>';
                echo '<input type="checkbox" style="display: none;" name="time_id_ticks['.$shift->time_id.']" value="1">
            <input type="hidden" name="time_id['.$shift->time_id.']" value="'.$shift->time_id.'">';
            }

            echo '</td>';
        }
        ?>
    </form>

    <table style="display:none">
        <tr id="task_template">
            <td colspan="2">
                <input type="hidden" name="tita_id[][]" class="form-control">
                <select name="task_id[][]" class="form-control task_box">
                    <option value=""></option>
                    <?php foreach($tasks as $role => $rtask) { ?>
                        <optgroup label="<?php echo $role; ?>">
                            <?php foreach($rtask as $task) { ?>
                                <option value="<?php echo $task->task_id; ?>" data-type="<?php echo $task->rate_type; ?>" data-rate="<?php echo $task->shta_rate; ?>"><?php echo $task->task_name; ?></option>
                            <?php } ?>
                        </optgroup>
                    <?php } ?>
                </select>
            </td>
            <td>
                @include('input/timefields', ['name' => 'tita_start[][]'])
                <input type="hidden" name="tita_user_start[][]" class="form-control">
            </td>
            <td>
                @include('input/timefields', ['name' => 'tita_end[][]'])
                <input type="hidden" name="tita_user_finish[][]" class="form-control">
            </td>
            <td>
                <input type="hidden" name="tita_user_break_duration[][]" class="form-control">
                <select name="tita_break_duration[][]" class="form-control">
                    <option value="0">0</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                    <option value="60">60</option>
                </select>
            </td>
            <td>
                <input type="text" name="tita_quantity[][]" class="form-control shta_contract">
                <input type="hidden" name="tita_user_quantity[][]" class="form-control">
            </td>
            <td>
                <a class="delete_time_task btn btn-danger"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    </table>

    @include('modals.timesheetstaff')
@stop