@extends('layouts.app')

@section('title', 'Manage Timesheets')

@section('header')
    <h1>Manage Timesheets</h1>
    <p>This is the management interface for setting up and processing timesheets.</p>
@endsection

@section('content')
    <div class="row dashboard">
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-usd"></i> <a href="{{ URL::to('settings/payrates') }}">Pay Rates</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-sign-out"></i> <a href="{{ URL::to('timesheets/process') }}">Process times</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-wrench"></i> <a href="{{ URL::to('timesheets/finishTimes') }}">Fix times</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-exclamation-triangle"></i> <a href="{{ URL::to('timesheets/errors') }}">Error List</a></h1>
            </div>
        </div>

        <!--<div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-credit-card"></i> <a href="{{ URL::to('settings/charge_rates') }}">Charge rates</a></h1>
            </div>
        </div>-->
    </div>
@stop