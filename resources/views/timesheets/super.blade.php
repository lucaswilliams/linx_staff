@extends('layouts.app')

@section('title', 'Timesheets')

@section('content')
    <h1>Enter timesheets</h1>
    <p>Select a shift from the list below to record or view times.</p>
    <p><?php echo $mode; ?></p>
    <?php if(count($shifts) > 0) { ?>
        @include('timesheets.partials.user')
    <?php } ?>

    @include('timesheets.partials.super')

    @include('modals.timesheetstaff')
    @include('modals.payrates')
@stop