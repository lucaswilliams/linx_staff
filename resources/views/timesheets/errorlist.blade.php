@extends('layouts.app')

@section('title', 'Pay Rates')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>MYOB Errors</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Worker</th>
                        <th>Start</th>
                        <th>Finish</th>
                        <th>Pay Rate</th>
						<th>Task</th>
                        <th>Message</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($times as $time) { ?>
                        <tr>
                            <td><?php echo $time->given_name; ?> <?php echo $time->surname; ?></td>
                            <td><?php echo $time->tita_start; ?></td>
                            <td><?php echo $time->tita_finish; ?></td>
                            <td><?php echo $time->rate_name; ?></td>
                            <td><?php echo $time->task_name; ?></td>
                            <td><?php echo $time->log_message; ?></td>
                            <td><a href="<?php echo url('/timesheets/errors/'.$time->myob_id); ?>" class="btn btn-warning"><i class="fa fa-check"></i></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop