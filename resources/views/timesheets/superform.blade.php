@extends('layouts.app')

@section('title', 'Timesheets')

@section('content')
    <h1>Manage workers<button type="button" class="btn btn-success pull-right" id="toggle-workers">Show all workers</button><a href="completed/<?php echo $_GET['id']; ?>" class="btn btn-warning pull-right" style="text-transform: none;">Completed workers</a></h1>
    <div class="dashboard-tile">
        <form method="POST" action="" id="super-form">
            <input type="hidden" id="super_id" value="<?php echo Auth::user()->id; ?>">
            <input type="hidden" id="jobs_id" name="jobs_id" value="<?php echo $jobs_id; ?>">
        <div class="row">
            <div class="col-sm-3">
                <h2>Select Task</h2>
                <div class="tasks" style="height: calc(100vh - 300px); overflow-y: scroll;">
					<label for="role_id">Role:</label>
					<select name="role_id" id="role_id" class="form-control">
						<?php foreach($roles as $role) {
							echo '<option value="'.$role->role_id.'" '.($role->role_default == 1 ? 'selected' : '').'>'.$role->role_name.'</option>';
						} ?>
					</select>
					<label for="task_id_1">Task:</label>
					<select name="task_id" id="task_id_1" class="form-control task-list">
						<option value="0">(none)</option>
						<?php foreach($tasks as $task) { ?>
							<option value="<?php echo $task->task_id; ?>" data-rate-id="<?php echo $task->rate_id; ?>"><?php echo $task->task_name; ?></option>
						<?php } ?>
					</select>
                </div>
            </div>
            <div class="col-sm-3" id="workerAvailable">
                <h2>Available</h2>
                <label for="start_time">Start Time</label>
                <div class="row">
                    <div class="col-xs-2">
                        <button type="button" class="btn btn-primary" id="checkAllAvail"><i class="fa fa-check-square-o"></i></button>
                    </div>
                    <div class="col-xs-10">
                        @include('input/timefields', ['name' => 'start_time'])
                    </div>
                </div>
                <div class="row">
                    <label class="col-xs-12"><input type="checkbox" name="public_holiday" id="public_holiday"> Public holiday</label>
                </div>

                <div style="height: calc(100vh - 360px); overflow-y: scroll;">
                    <?php foreach($available as $worker) { ?>
                        <div class="btn-group" data-supervisor="<?php echo $worker->supervisor_id; ?>">
                            <button type="button" class="btn btn-default first">
                                <label style="margin: 0;" for="worker-<?php echo $worker->id; ?>">
                                    <input type="checkbox" name="available[]" id="worker-<?php echo $worker->id; ?>" value="<?php echo $worker->current_signin; ?>" style="margin:0"> <?php echo (strlen($worker->nickname) > 0 ? $worker->nickname : $worker->given_name.' '.(((strlen($worker->preferred) > 0) && (strcmp($worker->preferred, $worker->given_name) != 0)) ? '('.$worker->preferred.') ' : '').$worker->surname); ?> (<?php echo $worker->external_id; ?>)
                                </label>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu btn-dropdown">
                                <li><a href="<?php echo url('timesheets/signoutWorker/'.$worker->id); ?>">Not present</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-3" id="workerWorking">
                <h2>Working</h2>
                <div class="row">
                    <label for="end_time" class="col-xs-12">End Time</label>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        <button type="button" class="btn btn-primary" id="checkAllWorking"><i class="fa fa-check-square-o"></i></button>
                    </div>
                    <div class="col-xs-10">
                        @include('input/timefields', ['name' => 'end_time'])
                    </div>
                </div>
                <div class="row">
                    <label class="col-xs-6"><input type="checkbox" name="more_work" id="more_work"> More work</label>
                    <label class="col-xs-6"><input type="checkbox" name="lunch_break" id="lunch_break"> No lunch</label>
                </div>
                <div style="height: calc(100vh - 360px); overflow-y: scroll;">
                    <?php foreach($working as $worker) { ?>
                        <div class="btn-group" data-supervisor="<?php echo $worker->supervisor_id; ?>">
                            <button type="button" class="btn btn-default first">
                                <label style="margin: 0;" for="worker-<?php echo $worker->id; ?>">
                                    <input type="checkbox" name="working[]" id="worker-<?php echo $worker->id; ?>" value="<?php echo $worker->tita_id; ?>" style="margin:0"> <?php echo (strlen($worker->nickname) > 0 ? $worker->nickname : $worker->given_name.' '.(((strlen($worker->preferred) > 0) && (strcmp($worker->preferred, $worker->given_name) != 0)) ? '('.$worker->preferred.') ' : '').$worker->surname); ?> (<?php echo $worker->external_id; ?>)
                                </label>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu btn-dropdown">
                                <li>
                                    <?php
                                        $tasks = new \App\Http\Controllers\TasksController();
                                        $tree = $tasks->getParentTree($worker->task_id);
                                        $finish_time = '';
									    if ($worker->tita_finish == null) {
									    	if($worker->tita_user_finish != null) {
									    		$finish_time = ' - '.date('H:i', strtotime($worker->tita_user_finish));
									    	}
									    } else {
											$finish_time = ' - '.date('H:i', strtotime($worker->tita_finish));
										}
                                        echo join(' -> ', array_reverse($tree)).' ('.date('H:i', strtotime($worker->tita_start)).$finish_time.')';
                                    ?>
                                </li>
                                <li><a href="deleteSuper/<?php echo $worker->tita_id; ?>">Cancel worker</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-3">
                <h2>Absent</h2>
                <div style="height: calc(100vh - 310px); overflow-y: scroll;">
                    <?php foreach($unsigned as $worker) { ?>
                        <!-- Split button -->
                        <div class="btn-group" data-supervisor="<?php echo $worker->supervisor_id; ?>">
                            <button type="button" class="btn btn-default first">
                                <label style="margin: 0;" for="worker-<?php echo $worker->id; ?>">
                                    <?php echo (strlen($worker->nickname) > 0 ? $worker->nickname : $worker->given_name.' '.(((strlen($worker->preferred) > 0) && (strcmp($worker->preferred, $worker->given_name) != 0)) ? '('.$worker->preferred.') ' : '').$worker->surname); ?> (<?php echo $worker->external_id; ?>)
                                </label>
                            </button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu btn-dropdown">
                                <li><a href="<?php echo url('timesheets/signinWorker/'.$worker->id); ?>">Sign in</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo url('timesheets/notrequiredWorker/'.$worker->id); ?>">Not required</a></li>
                                <li><a href="<?php echo url('timesheets/sickWorker/'.$worker->id); ?>">Sick</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
                <button type="submit" id="saveWorkers" class="form-control btn btn-success disabled"><i class="fa fa-floppy-o"></i> Save</button>
            </div>
        </div>
        </form>
    </div>
@stop