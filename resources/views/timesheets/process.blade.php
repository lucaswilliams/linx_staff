@extends('layouts.app')

@section('title', 'Timesheets')

@section('content')
    <form method="POST">
        {{ csrf_field() }}
        <h1>Process timesheets</h1>
        <?php
        if(count($errs) > 0) {
            echo '<div class="dashboard-tile error">
            <h2><i class="fa fa-exclamation-triangle"></i> UH-OH:</h2>
            <p>There are some issues with items to be processed.  You will need to resolve these before processing timesheets into MYOB.</p>
            <ul>';
            foreach($errs as $error) {
                echo '<li>'.$error.'</li>';
            }
            echo '</ul></div>';
        }
        ?>
        <div class="dashboard-tile">
            <p>Please select the shifts you would like to process from the list below.</p>
            <h2>Shift List</h2>
            <?php
                $client_name = '';
                $job_name = '';
                $shift_id = 0;
                echo '<table width="100%">';
                echo '<tr>
                        <th>Client - Job</th>
                        <th>Shift Start</th>
                        <th>Status</th>
                        <th>Worker</th>
                        <th>Actions</th>
                        <th>Include</th>
                </tr>';
                foreach($shifts as $shift) {
                    echo '<td>'.$shift->client_name.' - '.$shift->job_name.'</td>';
                    echo '<td>'.$shift->time_start.'</td>';
                    echo '<td class="text-center">';
                    if($shift->approved_user == -1) {
                        echo '<i class="fa fa-exclamation fa-2x text-danger" data-toggle="tooltip" data-placement="bottom" title="Worker would like to dispute"></i>';
                    } elseif(($shift->processed == -1)) {
                        echo '<i class="fa fa-share-square-o fa-2x" data-toggle="tooltip" data-placement="bottom" title="Queued"></i>';
                    } else if(($shift->approved_user == 1) && ($shift->approved_super == 1)) {
                        echo '<i class="fa fa-check fa-2x text-success" data-toggle="tooltip" data-placement="bottom" title="Ready for processing"></i>';
                    } else {
                        if($shift->approved_user == 0) {
                            echo '<i class="fa fa-exclamation-circle fa-2x text-warning" data-toggle="tooltip" data-placement="bottom" title="Not worker approved"></i>';
                        }

                        if($shift->approved_super == 0) {
                            echo '<i class="fa fa-exclamation-triangle fa-2x text-warning" data-toggle="tooltip" data-placement="bottom" title="Not supervisor approved"></i>';
                        }
                    }

                    echo '</td><td>'.$shift->given_name.' '.$shift->surname.' ('.$shift->external_id.')</td>
                    <td>';
                    /*echo '<a class="btn btn-danger" href="'.url('/timesheets/delete').'/'.$shift->time_id.'">
                            <i class="fa fa-trash-o"></i>
                        </a>';*/
                    echo '</td><td>';
                    //A "checkbox" style button
                    if($shift->myob_uid != null) {
                        echo '<a class="btn btn-default timesheetIncludeBtn"><i class="fa fa-check-square-o"></i></a>';
                    }
                    echo '<a class="btn btn-default timesheetDoneBtn"><i class="fa fa-pencil-square-o"></i></a>';

                    //Finally, the input to hold the time_id.  We cannot process a timesheet that doesn't exist.
                    echo '<input type="checkbox" name="time_id[]" value="'.$shift->time_id.'" style="display:none">';
                    echo '<input type="checkbox" name="done_id[]" value="'.$shift->time_id.'" style="display:none">';
                    echo '</td></tr>';
                }
            echo '<tr>
                <td colspan="6" class="text-right">
                    <button type="button" class="btn btn-success" id="selectAllBtn">
                        <i class="fa fa-check"></i> Select all
                    </button>
                </td>
            </tr>';
            echo '</table>';
            ?>
        </div>
        <div class="dashboard-tile text-center">
            <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Process</button>
        </div>
    </form>

    @include('modals.timesheetstaff')
@stop
