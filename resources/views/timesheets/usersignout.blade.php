@extends('layouts.app')

@section('title', 'Timesheets')

@section('header')
    <h1>Sign Out</h1>
    <p>Please confirm the details entered by your supervisor.</p>
@endsection

@section('content')
    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
        <div class="dashboard-tile">
            <table class="table table-striped">
                <tr>
                    <th>Task</th>
                    <th>Start</th>
                    <th>Finish</th>
                </tr>
                <?php foreach($tasks as $task) { ?>
                    <tr>
                        <td><?php echo $task->task_name; ?></td>
                        <td><?php echo date('d/m/Y H:i', strtotime($task->tita_start)); ?></td>
                        <td><?php echo ($task->tita_finish == null ? date('d/m/Y H:i') : date('d/m/Y H:i', strtotime($task->tita_finish))); ?></td>
                    </tr>
                <?php } ?>
            </table>

            <div class="text-center">
                <form method="POST" action="{{ url('/timesheets/signoutY') }}">
                    <input type="hidden" name="time_id" value="<?php echo $shift->time_id; ?>">
                    <input type="hidden" name="tita_finish" value="<?php echo date("Y-m-d H:i"); ?>">
                    <button class="btn btn-success"><i class="fa fa-check"></i> Accept</button>
                </form>

                <div style="clear:left;"></div>
            </div>
        </div>
    </div>
@endsection