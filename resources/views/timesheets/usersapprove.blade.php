@extends('layouts.app')

@section('title', 'Timesheets')

@section('header')
    <h1>Approve Changes</h1>
    <p>Your supervisor has made changes to the timesheet that you have submitted.  Please review them.</p>
@endsection

@section('content')
    <form method="POST" action="{{ url('/timesheets/accept') }}">
        {{ csrf_field() }}
        <input type="hidden" name="time_id" value="<?php echo $shift->time_id; ?>">

        <div class="col-xs-12">
            <div class="dashboard-tile">
                <table width="100%" class="nostyle">
                    <tr>
                        <td width="50%">
                            Job Name
                        </td>
                        <td width="50%">
                            <?php echo $shift->job_name; ?>
                        </td>
                    </tr>

                    <tr>
                        <td width="50%">
                            Shift Start
                        </td>
                        <td width="50%">
                            <?php echo $shift->start_time; ?>
                        </td>
                    </tr>

                    <tr>
                        <td width="50%">
                            Shift Finish
                        </td>
                        <td width="50%">
                            <?php echo $shift->end_time; ?>
                        </td>
                    </tr>

                    <tr>
                        <td width="50%">
                            Scheduled break
                        </td>
                        <td width="50%">
                            <?php echo $shift->break_duration; ?>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <p><strong>Tasks</strong></p>
                            <table class="table table-striped" id="time_tasks">
                                <tr>
                                    <td colspan="6" class="text-right"><a class="btn btn-success" id="add_time_task"><i class="fa fa-plus"></i> Add</a></td>
                                </tr>
                                <tr>
                                    <th width="40%">Task</th>
                                    <th width="15%">Start</th>
                                    <th width="15%">Finish</th>
                                    <th width="15%">Break</th>
                                    <th width="15%">Units</th>
                                </tr>
                                <?php foreach($tasks as $task) {
                                    echo '<tr>
                                    <td rowspan="2">'.$task->task_name.'</td>
                                    <td>'.($task->rate_type == 0 ? $task->tita_user_start : '').'</td>
                                    <td>'.($task->rate_type == 0 ? $task->tita_user_finish : '').'</td>
                                    <td>'.($task->rate_type == 0 ? $task->tita_user_break_duration : '').'</td>
                                    <td>'.($task->rate_type == 1 ? $task->tita_user_quantity : '').'</td>
                                    </tr><tr>
                                    <td'.($task->tita_user_start == $task->tita_start ? '' : ' class="mismatch"').'>'.($task->rate_type == 0 ? $task->tita_start : '').'</td>
                                    <td'.($task->tita_user_finish == $task->tita_finish ? '' : ' class="mismatch"').'>'.($task->rate_type == 0 ? $task->tita_finish : '').'</td>
                                    <td'.($task->tita_user_break_duration == $task->tita_break_duration ? '' : ' class="mismatch"').'>'.($task->rate_type == 0 ? $task->tita_break_duration : '').'</td>
                                    <td'.($task->tita_user_quantity == $task->tita_quantity ? '' : ' class="mismatch"').'>'.($task->rate_type == 1 ? $task->tita_quantity : '').'</td>
                                    </tr>';
                                } ?>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" id="notes_desc">
                            Notes
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="hidden" name="old_notes" value="<?php echo $shift->time_notes; ?>">
                            <p><?php echo $shift->time_notes; ?></p>
                            <textarea name="notes" id="notes" style="width: 100%"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dashboard-tile text-center">
                <button type="submit" class="btn btn-success" id="user-approve"><i class="fa fa-check"></i> Accept</button>
                <button type="submit" class="btn btn-danger" id="user-dispute"><i class="fa fa-times"></i> Dispute</button>
            </div>
        </div>
    </form>
@endsection