@extends('layouts.app')

@section('title', 'MYOB Settings')

@section('header')
	<h1>Job Selection</h1>
@stop

@section('content')
	<div class="dashboard-tile">
		<form method="GET">
			<div class="form-group">
				<label for="id">Select job:</label>
				<select name="id" id="id" class="form-control">
					<option value="">(None selected)</option>
					<?php
					foreach($jobs as $job) {
						echo '<option value="'.$job->id.'">'.$job->name.'</option>';
					}
					?>
				</select>
			</div>

			<p class="text-center">
				<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Select Job</button>
			</p>
		</form>
	</div>
@stop