@extends('layouts.app')

@section('title', 'MYOB Settings')

@section('header')
    <h1>Select timesheets</h1>
@stop

@section('content')
    <div class="dashboard-tile">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h2>View by worker <a class="btn btn-warning pull-right <?php echo (Auth::user()->level != 3 ? 'hidden' : ''); ?>" id="teViewAll" data-tooltip="Show all workers"><i class="fa fa-user-plus"></i></a></h2>
				<table width="100%" id="teWorkers">
					<?php
						foreach($workers as $worker) {
							echo '<tr '.($worker->enddate == null ? '' : 'class="hidden"').'><td>';
							echo '<a href="'.url('timesheets/'.$jobs_id.'/worker/'.$worker->id).'">'.$worker->given_name.' '.(((strlen($worker->preferred) > 0) && (strcmp($worker->preferred, $worker->given_name) != 0)) ? '('.$worker->preferred.') ' : '').$worker->surname.' ('.$worker->external_id.')</a>';
							echo '</td></tr>';
						}
					?>
				</table>
			</div>

			<div class="col-xs-12 col-sm-6">
				<h2>View by date</h2>
				<table width="100%">
					<?php
						foreach($days as $date) {
							echo '<tr><td>';
							echo '<a href="'.url('timesheets/'.$jobs_id.'/'.$date['value']).'">'.$date['display'].'</a>';
							echo '</td></tr>';
						}
					?>
				</table>
			</div>
		</div>
    </div>
@stop