@extends('layouts.app')

@section('title', 'MYOB Settings')

@section('header')
	<h1>Job Selection</h1>
@stop

@section('content')
	<div class="dashboard-tile">
		<table class="table table-striped">
			<tr>
				<th>Worker</th>
				<th>External Number</th>
				<th>Options</th>
			</tr>
			<?php foreach($users as $user) { ?>
				<tr>
					<td><?php echo (strlen($user->nickname) > 0 ? $user->nickname : $user->given_name.' '.(((strlen($user->preferred) > 0) && (strcmp($user->preferred, $user->given_name) != 0)) ? '('.$user->preferred.') ' : '').$user->surname); ?></td>
					<td><?php echo $user->external_id; ?></td>
					<td><a class="btn btn-primary" href="<?php echo url('/timesheets/uncomplete/'.$user->id); ?>"><i class="fa fa-user-times"></i></a></td>
				</tr>
			<?php } ?>
		</table>
	</div>
@stop