@extends('layouts.app')

@section('title', 'Assign Workers')

@section('content')
    <h1>Assign workers</h1>
    <p>If you prefer, you can simplify which workers are assigned to you to make selecting them easier.</p>
    <form method="POST">
        <input type="hidden" name="jobs_id" value="<?php echo $jobs_id; ?>" />
        <div class="dashboard-tile">
            <div class="row">
                <label for="super_id" class="col-xs-12 col-sm-3 control-label">Supervisor:</label>
                <div class="col-xs-12 col-sm-9">
                    <select id="super_id" name="super_id" class="form-control">
                        <option value="0">(none)</option>
                        <?php foreach($supers as $super) { ?>
                            <option value="<?php echo $super->id; ?>"><?php echo $super->given_name; ?> <?php echo $super->surname; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="dashboard-tile">
            <table class="table table-striped">
                <tr>
                    <th><input type="checkbox" id="select-all-workers"></th>
                    <th>Name</th>
                    <th>Employee Number</th>
					<th>Nickname</th>
                    <th>Current Supervisor</th>
                </tr>
                <?php foreach($workers as $worker) { ?>
                    <tr>
                        <td><input type="checkbox" name="worker_id[<?php echo $worker->worker_id; ?>]"></td>
                        <td><?php echo $worker->worker_given; ?> <?php echo (((strlen($worker->worker_preferred) > 0) && (strcmp($worker->worker_preferred, $worker->worker_given) != 0)) ? '('.$worker->worker_preferred.') ' : ''); echo $worker->worker_surname; ?></td>
                        <td><?php echo $worker->external_id; ?></td>
						<td><input type="text" name="nickname[<?php echo $worker->worker_id; ?>]" value="<?php echo $worker->worker_nickname; ?>"></td>
                        <td><?php echo $worker->super_given; ?> <?php echo $worker->super_surname; ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>

        <div class="dashboard-tile text-center">
            <button class="btn btn-success"><i class="fa fa-floppy-o"></i> Save changes</button>
        </div>
    </form>
@stop