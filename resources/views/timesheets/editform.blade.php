@extends('layouts.app')

@section('title', 'Edit Timesheets')

@section('header')
    <h1>Edit timesheets</h1>
@endsection

@section('content')
    <?php
    echo '<div class="dashboard-tile text-right">';
    if($user->level == 3) {
    	echo '<a href="#" class="btn btn-warning" id="editAllWorkers"><i class="fa fa-pencil"></i> Edit workers</a>';
	}
	echo '</div></div></div></div>';
    echo '<form method="POST">';
    echo '<input type="hidden" name="jobs_id" value="'.$jobs_id.'">';
    echo '<div class="dashboard-tile">';
    echo '<table width="100%">';
    echo '<tr>
            <th>Worker</th>
            <th>Number</th>
            <th>Supervisor</th>
            <th>Task</th>
            <th>Role</th>
            <th>Start</th>
            <th>End</th>
            <th>Break</th>
            <th>Pub Hol</th>
            <th>Status</th>
            <th>Action</th>
        </tr>';
    $i = 0;
    
            foreach($timesheets as $work) {
            	//echo '<tr><td colspan="10"><pre>'; var_dump($work); echo '</pre></td></tr>';
                foreach ($work->timesheets as $sheet) {
                    //$absentrow = $work->absent ? 'absentrow' : 'presentrow';
                    $absentrow = 'presentrow';
                    echo '<tr data-id="'.$i.'" class="'.$absentrow.'"><td class="'.($work->absent ? 'workerabsent' : 'workerpresent').'">'.$work->given_name.' '.(((strlen($work->preferred) > 0) && (strcmp($work->preferred, $work->given_name) != 0)) ? '('.$work->preferred.') ' : '').$work->surname.'</td>';
                    echo '<td>'.$work->external_id.'</td>';
                    echo '<td><select name="supervisor_id['.$i.']" '.($sheet->locked ? ' disabled' : '').'><option value="0">(none)</option>';
                    foreach($supers as $super) {
                        echo '<option value="'.$super->id.'" '.($sheet->supervisor_id == $super->id ? 'selected' : '').'>'.$super->given_name.' '.$super->surname.'</option>';
                    }
                    echo '</select></td>';
                    echo '<td colspan="8" class="absent">Absent</td>';
                    echo '    <td class="notabsent">';
                    echo '<input type="hidden" name="time_id['.$i.']" value="' . $sheet->time_id . '"'.($sheet->locked ? ' disabled' : '').'>';
                    echo '<input type="hidden" name="tita_id['.$i.']" value="' . $sheet->tita_id . '"'.($sheet->locked ? ' disabled' : '').'>';
                    echo '<input type="hidden" name="user_id['.$i.']" value="' . $work->user_id . '"'.($sheet->locked ? ' disabled' : '').'>';
                    echo'<select name="task_id['.$i.']" '.($sheet->locked ? ' disabled' : '').' class="search">
<option value="0">(none)</option>';
                    foreach($tasks as $task) {
                        echo '<option value="'.$task->task_id.'" '.($sheet->task_id == $task->task_id ? 'selected' : '').'>'.$task->task_name.'</option>';
                    }
                    echo '</select></td>';
                    echo '    <td class="notabsent"><select name="role_id['.$i.']" '.($sheet->locked ? ' disabled' : '').' class="search">
        <option value="0">(none)</option>';
                    foreach($roles as $role) {
                        echo '<option value="'.$role->role_id.'" '.($sheet->role_id == $role->role_id ? 'selected' : '').'>'.$role->role_name.'</option>';
                    }
                    echo '</select></td>';
                    echo '    <td class="notabsent"><input type="text" name="tita_start['.$i.']" value="' . $sheet->tita_start . '"'.($sheet->locked ? ' disabled' : '').'></td>';
                    echo '    <td class="notabsent"><input type="text" name="tita_finish['.$i.']" value="' . $sheet->tita_finish . '"'.($sheet->locked ? ' disabled' : '').'></td>';
                    echo '    <td class="notabsent"><input type="text" name="tita_break_duration['.$i.']" value="' . $sheet->tita_break_duration . '"'.($sheet->locked ? ' disabled' : '').'></td>';
                    echo '	  <td class="notabsent"><input type="checkbox" name="public_holiday['.$i.']" '.($sheet->public_holiday == 1 ? 'checked' : '').'></td>';
                    echo '    <td class="notabsent">'.$sheet->processed.'</td>';
                    echo '</td>';
                    echo '<td class="notabsent">';
                    if($user->level == 3 && $sheet->time_id > 0) {
                        echo '<a href="'.url('timetasks/delete/'.$sheet->tita_id).'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>';
                        if($sheet->tita_id > 0) {
                            echo ' <a href="#" class="btn btn-warning copyTimeTask"><i class="fa fa-files-o"></i></a>';
                        }
                    }
                    echo '</td>';

                    $i++;
                }
            }
            echo '</table>';
            echo '<input type="hidden" id="maxid" value="'.$i.'">';
            echo '</div>';
            echo '<div class="container"><div class="row"><div class="col-xs-12"><div class="dashboard-tile text-center">';
        echo '<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Details</button>';
        echo '</div></div></div></div>';
    echo '</form>';
    ?>
@endsection