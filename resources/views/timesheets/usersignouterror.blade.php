@extends('layouts.app')

@section('title', 'Timesheets')

@section('header')
    <h1>Attention!</h1>
@endsection

@section('content')
    <div class="col-xs-12">
        <div class="dashboard-tile" id="signoutErrorMessage">
			<p>You cannot sign out for today as you have not been allocated to any tasks.</p>
			<p>If you meant to log out of the system, head to the top menu and select "Log out"</p>
			<div class="hidden-xs"><img src="{{ asset('/images/log-out-desktop.png') }}"></div>
			<div class="visible-xs-inline"><img src="{{ asset('/images/log-out-mobile.png') }}"></div>
			<p>If you feel that you should have been allocated some tasks, please advise your supervisor.</p>
        </div>
    </div>
@endsection