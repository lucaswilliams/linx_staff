@extends('layouts.app')

@section('title', 'Timesheets')

@section('header')
    <h1>Fix Timesheets</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form method="POST">
                <div class="dashboard-tile">
                    <table class="table table-striped">
                        <tr>
                            <th>Worker</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Break</th>
                            <th>Total</th>
                            <th>New End</th>
                            <th>New Break</th>
                        </tr>
                        <?php
                            foreach($sheets as $sheet) {
                                $difference = round(abs(strtotime($sheet->new_finish) - strtotime($sheet->tita_start)) / 60, 2);
                                $break = 0;
                                if($difference > 300) {
                                    $break = 30;
                                }
                                if(strtotime($sheet->tita_start) > strtotime($sheet->tita_finish)) {
                                	$finish = $sheet->new_finish;
								} else {
                                	$finish = $sheet->tita_finish;
								}
                                echo '<tr>';
                                echo '<td><input type="hidden" name="tita_id[]" value="'.$sheet->tita_id.'">';
                                echo $sheet->given_name.' '.(((strlen($sheet->preferred) > 0) && (strcmp($sheet->preferred, $sheet->given_name) != 0)) ? '('.$sheet->preferred.') ' : '').$sheet->surname.'</td>';
                                echo '<td>'.date('d/m/Y H:i', strtotime($sheet->tita_start)).'<input type="hidden" name="tita_start[]" value="'.date('Y-m-d H:i', strtotime($sheet->tita_start)).'"></td>';
                                echo '<td>'.date('d/m/Y H:i', strtotime($sheet->tita_finish)).'</td>';
                                echo '<td>'.$sheet->tita_break_duration.'</td>';
                                echo '<td>'.$sheet->tita_quantity.'</td>';
                                echo '<td><input type="text" name="tita_finish[]" value="'.$finish.'"></td>';
                                echo '<td><input type="text" name="tita_break_duration[]" value="'.$break.'"></td>';
                                echo '</tr>';
                            }
                        ?>
                    </table>
                </div>
                <div class="dashboard-tile text-center">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Timesheets</button>
                </div>
            </form>
        </div>
    </div>
@endsection