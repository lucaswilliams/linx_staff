@extends('layouts.app')

@section('title', 'Timesheets')

@section('content')
    <h1>Enter timesheets</h1>
    <p>The following shifts require your attention for the recording of a timesheet.  Without this information, we will be unable to pay you.</p>
    @include('timesheets.partials.user')

    @include('modals.timesheetuser')
    @include('modals.payrates')
@stop