@extends('layouts.app')

@section('title', 'Pay Rates')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Pay Rates</h1>
            <p>Here's a list of all the payrates in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="payrates/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Worker Rate</th>
                        <th>Charge Rate</th>
						<th>Active</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($payrates as $rate) { ?>
                    <tr>
                        <td>
                            <a href="payrates/<?php echo $rate->id; ?>"><?php echo $rate->name ?></a>
                        </td>
                        <td><?php echo ($rate->rate_type == 0 ? 'Hourly' : 'Contract'); ?></td>
                        <td class="text-right">$<?php echo number_format($rate->paru_staff, 2); ?></td>
                        <td class="text-right">$<?php echo number_format($rate->paru_client, 2); ?></td>
						<td><?php echo ($rate->rate_archived == 1 ? 'Inactive' : 'Active'); ?></td>
                        <td width="100px">
                            <a href="payrates/<?php echo $rate->id; ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="payrates/delete/<?php echo $rate->id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop