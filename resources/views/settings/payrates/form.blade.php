<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $rate->name;
}

if(strlen(old('staff_rate')) > 0) {
    $staff_rate = old('staff_rate');
} else {
    $staff_rate = $rate->staff_rate;
}

if(strlen(old('client_rate')) > 0) {
    $client_rate = old('client_rate');
} else {
    $client_rate = $rate->client_rate;
}

if(strlen(old('rate_type')) > 0) {
    $rate_type = old('rate_type');
} else {
    $rate_type = $rate->rate_type;
}

if(strlen(old('rate_guid')) > 0) {
    $rate_guid = old('rate_guid');
} else {
    $rate_guid = $rate->rate_guid;
}

if(old('rate_parent') > 0) {
    $rate_parent = old('rate_parent');
} else {
    $rate_parent = $rate->rate_parent_id;
}

if(strlen(old('rate_days')) > 0) {
    $rate_days = old('rate_days');
} else {
    $rate_days = $rate->rate_days;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            echo (isset($rate->id) ? 'Edit' : 'Add').' pay rate';
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/payrates') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="rate_id" value="<?php echo $rate->id; ?>">

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="rate_type">Rate Type:</label>
                        <select name="rate_type" id="rate_type" class="form-control">
                            <option value="0"<?php if($rate_type == 0) { echo ' selected'; } ?>>Hourly</option>
                            <option value="1"<?php if($rate_type == 1) { echo ' selected'; } ?>>Contract</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="rate_guid">Base Linked Rate:</label>
                        <select name="rate_guid" id="rate_guid" class="search">
                            <option value="">(No linked rate)</option>
                            <?php foreach($myob_rate as $myob) {
                                $selected = '';
                                if(strcmp($myob->UID, $rate->rate_guid) == 0) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$myob->UID.'"'.$selected.'>'.$myob->Name.'</option>';
                            }?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="rate_parent">Changes Into:</label>
                        <select name="rate_parent" id="rate_parent" class="form-control">
                            <option value="0">(none)</option>
                            <?php foreach($rates as $rate) {
                                echo '<option value="'.$rate->id.'"'.($rate->id == $rate_parent ? ' selected' : '').'>'.$rate->name.'</option>';
                            }?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="rate_days">After this many days:</label>
                        <input type="text" name="rate_days" id="rate_days" value="<?php echo $rate_days; ?>" class="form-control">
                    </div>

					<div class="form-group">
						<label for="rate_archived"><input type="checkbox" id="rate_archived" name="rate_archived" <?php echo (strtotime($rate->rate_archived) == 1 ? 'checked' : ''); ?>>Archived</label>
					</div>

					<div id="accordion" class="panel-group">
						<?php
						if($rules != null) {
							$i = 0;
							foreach($rules as $yearname => $yearrules) {
								$i++;
								$year = 0;
								foreach($years as $thisyear) {
									if($thisyear->year_name == $yearname) {
										$year = $thisyear->year_id;
									}
								}
						?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>"><?php echo $yearname; ?></a>
								</h4>
							</div>
							<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse">
								<div class="panel-body">
									<table class="table table-striped rateRuleTable" data-id="<?php echo $year; ?>">
										<tr>
											<th width="25%">Rate Rule</th>
											<th width="20%">MYOB Rate</th>
											<th width="15%">Worker Rate</th>
											<th width="15%">Charge Rate</th>
											<th width="15%">Seasonal Rate</th>
											<th width="10%">Action</th>
										</tr>
										<tr>
											<td colspan="6" align="right">
												<a href="#" class="btn btn-success" id="addRateRuleRow" data-id="<?php echo $year; ?>"><i class="fa fa-plus"></i></a>
											</td>
										</tr>
										<?php
										if($yearrules != null) {
										foreach($yearrules as $rule) {
										?>
										<tr>
											<td>
												<div style="width: 75%; float: left">
													<?php if($rule->paru_frequency == -1) {
														echo 'Ordinary Hours<input type="hidden" name="rule_type[]" value="-1">';
													} else {
													?>

													<select name="rule_type[]" class="form-control">
														<option value="0" <?php echo ($rule->paru_frequency == 0 ? 'selected' : ''); ?>>Daily</option>
														<option value="1" <?php echo ($rule->paru_frequency == 1 ? 'selected' : ''); ?>>Weekly</option>
														<option value="2" <?php echo ($rule->paru_frequency == 2 ? 'selected' : ''); ?>>Fortnightly</option>
														<option value="3" <?php echo ($rule->paru_frequency == 3 ? 'selected' : ''); ?>>Monthly</option>
														<option value="4" <?php echo ($rule->paru_frequency == 4 ? 'selected' : ''); ?>>Saturday</option>
														<option value="5" <?php echo ($rule->paru_frequency == 5 ? 'selected' : ''); ?>>Sunday</option>
														<option value="6" <?php echo ($rule->paru_frequency == 6 ? 'selected' : ''); ?>>Public Holiday</option>
													</select>
													<?php
														}
													?>
												</div>
												<div style="width: 25%; float: left; <?php echo ($rule->paru_frequency == -1 ? 'display: none;' : '' ); ?>">
													<input type="text" class="form-control" name="rule_hours[]" value="<?php echo $rule->paru_hours; ?>">
												</div>
											</td>
											<td>
												<select name="rule_guid[]" id="rate_guid" class="search">
													<option value="">(No linked rate)</option>
													<?php foreach($myob_rate as $myob) {
														$selected = '';
														if(strcmp($myob->UID, $rule->paru_guid) == 0) {
															$selected = ' selected';
														}
														echo '<option value="'.$myob->UID.'"'.$selected.'>'.$myob->Name.'</option>';
													}?>
												</select>
											</td>
											<td>
												<div class="input-group">
													<span class="input-group-addon">$</span>
													<input type="text" name="rule_staff[]" class="form-control" id="staff_rate" value="<?php echo number_format($rule->paru_staff, 2, '.', ''); ?>">
												</div>
											</td>
											<td>
												<div class="input-group">
													<span class="input-group-addon">$</span>
													<input type="text" name="rule_client[]" class="form-control" id="client_rate" value="<?php echo number_format($rule->paru_client, 2, '.', ''); ?>">
												</div>
											</td>
											<td>
												<div class="input-group">
													<span class="input-group-addon">$</span>
													<input type="text" name="rule_client_seasonal[]" class="form-control" id="client_rate_seasonal" value="<?php echo number_format($rule->paru_client_seasonal, 2, '.', ''); ?>">
												</div>
											</td>
											<td>
												<?php if($rule->paru_id != null) {
													echo '<input type="hidden" name="paru_id[]" value="'.$rule->paru_id.'">';
												} ?>
												<input type="hidden" name="year_id[]" value="<?php echo $year; ?>">
												<?php if($rule->paru_frequency != -1) {
													echo '<a class="btn btn-danger deleteRateRuleRow" href="#"><i class="fa fa-trash-o"></i></a>';
												} ?>
											</td>
										</tr>
										<?php
										}
										}
										?>
									</table>
								</div>
							</div>
						</div>
						<?php }} ?>
					</div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
    <div style="display: none">
        <table id="rateRuleTemplate">
            <tr>
                <td>
                    <div style="width: 75%; float: left">
                        <select name="rule_type[]" class="form-control">
                            <option value="0">Daily</option>
                            <option value="1">Weekly</option>
                            <option value="2">Fortnightly</option>
                            <option value="3">Monthly</option>
                            <option value="4">Saturday</option>
                            <option value="5">Sunday</option>
                            <option value="6">Public Holiday</option>
                        </select>
                    </div>
                    <div style="width: 25%; float: left">
                        <input type="text" class="form-control" name="rule_hours[]">
                    </div>
                </td>
                <td>
                    <select name="rule_guid[]" class="rate_guid">
                        <option value="">(No linked rate)</option>
                        <?php foreach($myob_rate as $myob) {
                            echo '<option value="'.$myob->UID.'">'.$myob->Name.'</option>';
                        }?>
                    </select>
                </td>
                <td>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="rule_staff[]" class="form-control" id="staff_rate">
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="rule_client[]" class="form-control" id="client_rate">
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="rule_client_seasonal[]" class="form-control" id="client_rate_seasonal" value="<?php echo number_format($rule->paru_client_seasonal, 2, '.', ''); ?>">
                    </div>
                </td>
                <td>
                    <a class="btn btn-danger deleteRateRuleRow" href="#"><i class="fa fa-trash-o"></i></a>
					<input type="hidden" name="year_id[]">
                </td>
            </tr>
        </table>
    </div>
@stop