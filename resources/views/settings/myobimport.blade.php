@extends('layouts.app')

@section('title', 'Import Employees')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Import employee</h1>
            <p>Enter an employee number from MYOB below to have them magically imported.</p>
            <div class="dashboard-tile">
                <form method="get" action="<?php echo url('/settings/myob/employees'); ?>">
                    <label for="external_id" class="control-label">Employee Number:</label>
                    <input type="text" name="external_id" id="external_id" class="form-control">
                    <button type="submit" class="btn btn-success"><i class="fa fa-download"></i> Import</button>
                </form>
            </div>
        </div>
    </div>
@stop