@extends('layouts.app')

@section('title', 'Financial Years')

@section('header')
    <h1>
        <?php
            if($year->year_id) {
                echo $year->year_name;
            } else {
                echo 'Add new financial year';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/years') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="year_id" value="<?php echo $year->year_id; ?>">

                    <div class="form-group">
                        <label for="year_name">Name:</label>
                        <input type="text" name="year_name" id="year_name" value="<?php echo $year->year_name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('year_name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="year_start">Name:</label>
                        <input type="date" name="year_start" id="year_start" value="<?php echo $year->year_start; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('year_start') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="year_finish">Name:</label>
                        <input type="date" name="year_finish" id="year_finish" value="<?php echo $year->year_finish; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('year_finish') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop