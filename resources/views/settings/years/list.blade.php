@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Financial Years</h1>
            <p>Here's a list of all the Financial Years in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="years/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Validity</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($years as $year) { ?>
                    <tr>
                        <td>
                            <a href="year/<?php echo $year->year_id; ?>"><?php echo $year->year_name ?></a>
                        </td>
                        <td>{{$year->year_start}} - {{$year->year_finish}}</td>
                        <td class="text-right">
                            <a href="<?php echo url('settings/years/'.$year->year_id); ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop