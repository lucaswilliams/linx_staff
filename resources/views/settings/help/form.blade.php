<?php
if(strlen(old('title')) > 0) {
    $title = old('title');
} else {
    $title = $help->title;
}

if(strlen(old('textcontent')) > 0) {
    $textcontent = old('textcontent');
} else {
    $textcontent = $help->content;
}

if(strlen(old('order')) > 0) {
    $order = old('order');
} else {
    $order = $help->order;
}

if(strlen(old('access')) > 0) {
    $access = old('access');
} else {
    $access = $help->access_level;
}

?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            if(strlen($title) > 0) {
                echo 'Edit help article';
            } else {
                echo 'Add new help article';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/help') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="id" value="<?php echo $help->help_id; ?>">

                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" name="title" id="title" value="<?php echo $title; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('title') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="textcontent">Content:</label>
                        <textarea name="textcontent" id="textcontent"><?php echo $textcontent; ?></textarea>
                        <script>
                            CKEDITOR.replace('textcontent');
                        </script>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-6">
                            <label for="order">Order:</label>
                            <input type="number" name="order" id="order" value="<?php echo $order; ?>" class="form-control">
                        </div>
                        <div class="col-xs-6">
                            <label for="access_level">Access:</label>
                            <select name="access_level[]" id="access_level" class="form-control" multiple>
                                <option value="1" <?php echo ($access & 1 ? 'selected' : ''); ?>>Worker</option>
                                <option value="2" <?php echo ($access & 2 ? 'selected' : ''); ?>>Supervisor</option>
                                <option value="4" <?php echo ($access & 4 ? 'selected' : ''); ?>>Administrator</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop