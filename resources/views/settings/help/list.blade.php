@extends('layouts.app')

@section('title', 'Help System')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Help System</h1>
            <p>Here's a list of all the help articles in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="help/0" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Title</th>
                        <th>Order</th>
                        <th>Access Level</th>
                        <th width="100px">Actions</th>
                    </tr>
                    <?php foreach($helps as $help) { ?>
                    <tr>
                        <td><?php echo $help->title; ?></td>
                        <td><?php echo $help->order; ?></td>
                        <td><?php echo ($help->access_level & 1 ? 'Worker, ' : '').($help->access_level & 2 ? 'Supervisor, ' : '').($help->access_level & 4 ? 'Administrator ' : ''); ?></td>
                        <td>
                            <a href="help/<?php echo $help->help_id; ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="help/delete/<?php echo $help->help_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop