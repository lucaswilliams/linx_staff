<?php
if(strlen(old('caqu_question')) > 0) {
    $caqu_question = old('caqu_question');
} else {
    $caqu_question = $question->caqu_question;
}

if(old('caqu_type') == null) {
    $caqu_type = $question->caqu_type;
} else {
    $caqu_type = old('caqu_type');
}

if(old('caqu_conditional') == null) {
    $caqu_conditional = $question->caqu_conditional;
} else {
    $caqu_conditional = old('caqu_conditional');
}

if(old('caqu_conditional_question') == null) {
    $caqu_conditional_question = $question->caqu_conditional_question;
} else {
    $caqu_conditional_question = old('caqu_conditional_question');
}

if(strlen(old('caqu_conditional_value')) > 0) {
	$caqu_conditional_value = old('caqu_conditional_value');
} else {
	$caqu_conditional_value = $question->caqu_conditional_value;
}

if(strlen(old('caqu_order')) > 0) {
	$caqu_order = old('caqu_order');
} else {
	$caqu_order = $question->caqu_order;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            echo (isset($question->caqu_id) ? 'Edit' : 'Add').' question';
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/campaigns/questions') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="camp_id" value="<?php echo $question->caqu_id; ?>">

                    <div class="form-group">
                        <label for="caqu_question">Question:</label>
                        <input type="text" name="caqu_question" id="caqu_question" value="<?php echo $caqu_question; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('caqu_question') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

					<div class="form-group">
						<label for="caqu_type">Type:</label>
                        <select name="caqu_type" id="caqu_type" class="form-control">
                            <option value="1" <?php if($caqu_type == 1) { echo 'selected'; } ?>>Text</option>
                            <option value="2" <?php if($caqu_type == 2) { echo 'selected'; } ?>>Radio</option>
                            <option value="3" <?php if($caqu_type == 3) { echo 'selected'; } ?>>Select</option>
                            <option value="4" <?php if($caqu_type == 4) { echo 'selected'; } ?>>Multi-Select</option>
                        </select>
						<?php
						foreach ($errors->get('camp_code') as $error) {
							echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
						}
						?>
					</div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop