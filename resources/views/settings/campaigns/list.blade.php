@extends('layouts.app')

@section('title', 'Pay Rates')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Campaigns</h1>
            <p>Here's a list of all the campaigns in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="campaigns/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Link</th>
                        <th width="150px">Actions</th>
                    </tr>
                    <?php foreach($campaigns as $camp) { ?>
                    <tr>
                        <td>
                            <a href="campaigns/<?php echo $camp->camp_id; ?>"><?php echo $camp->camp_name; ?></a>
                        </td>
						<td>
							<?php echo $camp->camp_desc; ?>
						</td>
						<td>
							<a href="https://staff.linxemployment.com.au/register/?camp=<?php echo $camp->camp_code; ?>"><?php echo $camp->camp_code; ?></a>
						</td>
                        <td>
                            <a href="campaigns/<?php echo $camp->camp_id; ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="campaigns/<?php echo $camp->camp_id; ?>/questions" class="btn btn-warning"><i class="fa fa-list-ul"></i></a>
							<a href="campaigns/delete/<?php echo $camp->camp_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop