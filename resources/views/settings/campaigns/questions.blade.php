@extends('layouts.app')

@section('title', 'Pay Rates')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Campaign Questions</h1>
            <p>Here's a list of all the questions for this campaign.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="campaigns/<?php echo $camp_id; ?>/questions/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th width="100px">Actions</th>
                    </tr>
                    <?php foreach($questions as $ques) { ?>
                    <tr>
                        <td>
                            <a href="<?php echo url('/settings/campaigns/questions/'.$ques->caqu_id); ?>"><?php echo $ques->caqu_question; ?></a>
                        </td>
						<td>
							<?php
							    switch($ques->caqu_type) {
                                    case 2 :
                                        echo 'Radio';
                                        break;
                                    case 3 :
                                        echo 'Select';
                                        break;
                                    case 4 :
                                        echo 'Multi-Select';
                                        break;
                                    case 1 :
                                    default :
                                        echo 'Text';
                                        break;
                                }
							?>
						</td>
                        <td>
                            <a href="<?php echo url('/settings/campaigns/'.$camp_id.'/questions/'.$ques->caqu_id); ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
							<a href="<?php echo url('/settings/campaigns/questions/delete/'.$ques->caqu_id); ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop