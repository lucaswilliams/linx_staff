<?php
if(strlen(old('camp_name')) > 0) {
    $camp_name = old('camp_name');
} else {
    $camp_name = $campaign->camp_name;
}

if(strlen(old('camp_code')) > 0) {
	$camp_code = old('camp_code');
} else {
	$camp_code = $campaign->camp_code;
}

if(strlen(old('camp_desc')) > 0) {
	$camp_desc = old('camp_desc');
} else {
	$camp_desc = $campaign->camp_desc;
}

if(old('camp_active') == null) {
	$camp_active = $campaign->camp_active;
} else {
	$camp_active = old('camp_active');
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            echo (isset($campaign->camp_id) ? 'Edit' : 'Add').' campaign';
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/campaigns') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="camp_id" value="<?php echo $campaign->camp_id; ?>">

                    <div class="form-group">
                        <label for="camp_name">Name:</label>
                        <input type="text" name="camp_name" id="camp_name" value="<?php echo $camp_name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('camp_name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

					<div class="form-group">
						<label for="camp_code">Code:</label>
						<input type="text" name="camp_code" id="camp_code" value="<?php echo $camp_code; ?>" class="form-control">
						<?php
						foreach ($errors->get('camp_code') as $error) {
							echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
						}
						?>
					</div>

					<div class="form-group">
						<label for="camp_desc">Description:</label>
						<textarea name="camp_desc" id="camp_desc" class="form-control"><?php echo $camp_desc; ?></textarea>
						<?php
						foreach ($errors->get('camp_desc') as $error) {
							echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
						}
						?>
					</div>

					<div class="form-group">
						<label for="camp_active">Active:</label>
						<select name="camp_active" id="camp_active" class="form-control">
							<option value="1" <?php if($camp_active == 1) { echo 'selected'; } ?>>Yes</option>
							<option value="0" <?php if($camp_active == 0) { echo 'selected'; } ?>>No</option>
						</select>
						<?php
						foreach ($errors->get('seasonal_worker') as $error) {
							echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
						}
						?>
					</div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop