@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Inductions</h1>
            <p>Here's a list of all the inductions in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="inductions/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($inductions as $induction) { ?>
                    <tr>
                        <td>
                            <a href="inductions/<?php echo $induction->ind_id; ?>"><?php echo $induction->ind_name ?></a>
                        </td>
                        <td class="text-right">
                            <a href="inductions/<?php echo $induction->ind_id; ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="inductions/delete/<?php echo $induction->ind_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop