<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $induction->ind_name;
}
?>

@extends('layouts.app')

@section('title', 'Inductions')

@section('header')
    <h1>
        <?php
            if(strlen($induction->ind_name) > 0) {
                echo $induction->ind_name;
            } else {
                echo 'Add new induction';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/inductions') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="ind_id" value="<?php echo $induction->ind_id; ?>">

                    <div class="form-group">
                        <label for="ind_name">Name:</label>
                        <input type="text" name="ind_name" id="ind_name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="ind_content">Induction Content:</label>
                        <textarea name="ind_content" id="ind_content"><?php echo $induction->ind_content; ?></textarea>
                        <script>
                            CKEDITOR.replace('ind_content');
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop