<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $induction->ind_name;
}
?>

@extends('layouts.app')

@section('title', 'Inductions')

@section('header')
    <h1>
        <?php
        if(strlen($induction->ind_name) > 0) {
            echo $induction->ind_name;
        } else {
            echo 'Add new induction';
        }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/inductions/finish') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                    {!! csrf_field() !!}
                    <input type="hidden" name="ind_id" value="<?php echo $induction->ind_id; ?>">

                    <div class="col-xs-12 text-right">
                        <a class="btn btn-success" id="add-ques-btn" data-toggle="modal" data-target="#addQuestionModal" data-id="0" data-ind="<?php echo $induction->ind_id; ?>"><i class="fa fa-plus"></i> Add new</a>
                    </div>

                    <table width="100%" id="ques-table">
                        <tr>
                            <th>Question</th>
                            <th>Options</th>
                            <th>Actions</th>
                        </tr>
                        <?php foreach($questions as $question) {
                            echo '<tr>
                            <td>'.$question->ques_text.'</td>
                            <td>
                                <a class="btn btn-success add-opt-btn" data-toggle="modal" data-target="#addAnswerModal" data-id="0" data-ques="'.$question->ques_id.'"><i class="fa fa-plus"></i> Add new</a>
                                <table class="opt_table">
                                    <tr>
                                        <th>Value</th>
                                        <th>Correct</th>
                                        <th>Actions</th>
                                    </tr>';
                            foreach($question->options as $option) {
                                echo '<tr>
                                            <td>'.$option->opt_text.'</td>
                                            <td>'.($option->opt_correct == 1 ? 'Yes': '').'</td>
                                            <td>
                                                <a class="btn btn-default edit-opt-btn" data-toggle="modal" data-target="#addAnswerModal" data-id="'.$option->opt_id.'" data-ques="'.$option->ques_id.'"><i class="fa fa-pencil"></i></a>
                                                <a href="'.url('/settings/inductions/answers/delete/'.$option->opt_id).'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>';
                            }
                            echo '</table>
                            </td>
                            <td>
                                <a class="btn btn-default edit-ques-btn" data-toggle="modal" data-target="#addQuestionModal" data-id="'.$question->ques_id.'" data-ind="'.$induction->ind_id.'"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
</td>
                        </tr>';
                        }?>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>

    <!-- MODALS HERE -->
    @include('modals.questionadd')
    @include('modals.answeradd')
@stop