@extends('layouts.app')

@section('title', 'MYOB Settings')

@section('header')
    <h1>MYOB Configuration</h1>
@stop

@section('content')
    <div class="dashboard-tile">
        <?php if(strlen($myob->cf_guid) > 0) { ?>
            <p>You are connected to MYOB with the GUID <?php echo $myob->cf_guid; ?>.</p>
            <p class="text-center"><a href="myob/delete" class="btn btn-danger"><i class="fa fa-cloud-download"></i> Disconnect MYOB</a></p>
            <form method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="wage_account">Default wages account:</label>
                    <select name="wage_account" id="wage_account" class="search">
                        <option value="">(None selected)</option>
                        <?php
                            if(is_array($accounts) && count($accounts) > 0) {
                                foreach($accounts as $account) {
                                    $selected = '';
                                    if(strcmp($account->UID, $myob->wage_account) == 0) {
                                        $selected = ' selected';
                                    }
                                    echo '<option value="'.$account->UID.'"'.$selected.'>'.$account->Name.' ('.$account->DisplayID.')</option>';
                                }
                            }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="tax_code">Default tax code for Tax Withholding:</label>
                    <select name="tax_code" id="tax_code" class="search">
                        <option value="">(None selected)</option>
                        <?php
                            if(is_array($tax_codes) && count($tax_codes) > 0) {
                                foreach($tax_codes as $code) {
                                    $selected = '';
                                    if(strcmp($code->UID, $myob->tax_code) == 0) {
                                        $selected = ' selected';
                                    }
                                    echo '<option value="'.$code->UID.'"'.$selected.'>'.$code->Description.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="tax_code">Default tax code for Invoices:</label>
                    <select name="invoice_tax_code" id="invoice_tax_code" class="search">
                        <option value="">(None selected)</option>
                        <?php
                        if(is_array($tax_codes) && count($tax_codes) > 0) {
                            foreach($tax_codes as $code) {
                                $selected = '';
                                if(strcmp($code->UID, $myob->invoice_tax_code) == 0) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$code->UID.'"'.$selected.'>'.$code->Description.'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="invoice_account">Default account for Invoices:</label>
                    <select name="invoice_account" id="invoice_account" class="search">
                        <option value="">(None selected)</option>
                        <?php
                        if(is_array($accounts) && count($accounts) > 0) {
                            foreach($accounts as $account) {
                                $selected = '';
                                if(strcmp($account->UID, $myob->invoice_account) == 0) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$account->UID.'"'.$selected.'>'.$account->Name.' ('.$account->DisplayID.')</option>';
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="range_start"></label>
                </div>

                <p class="text-center"><button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Details</button></p>
            </form>
        <?php } else { ?>
            <p>Please sign in with MYOB to choose a company file.</p>
            <p class="text-center"><a class="btn btn-success" href="https://secure.myob.com/oauth2/account/authorize?client_id=<?php echo $myob->api_key ?>&redirect_uri=<?php echo urlencode($myob->redirect_uri); ?>&response_type=code&scope=CompanyFile"><i class="fa fa-cloud-upload"></i> Connect with MYOB</a></p>
        <?php } ?>

        <?php if(isset($error)) { ?>
            <div class="error"><?php echo $error; ?></div>
        <?php } ?>
    </div>
@stop