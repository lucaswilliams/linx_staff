<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $cred->name;
}

if(strlen(old('has_description')) > 0) {
    $has_description = old('has_description');
} else {
    $has_description = $cred->has_description;
}

if(strlen(old('has_expiry')) > 0) {
    $has_expiry = old('has_expiry');
} else {
    $has_expiry = $cred->has_expiry;
}
?>

@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>
        <?php
            if(strlen($cred->name) > 0) {
                echo $cred->name;
            } else {
                echo 'Add new credential';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/credentials') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="cred_id" value="<?php echo $cred->id; ?>">

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="has_expiry">Has Expiry Date:</label>
                        <select name="has_expiry" id="has_expiry" class="form-control">
                            <option value="1" <?php if($has_expiry == 1) { echo 'selected'; } ?>>Yes</option>
                            <option value="0" <?php if($has_expiry == 0) { echo 'selected'; } ?>>No</option>
                        </select>
                        <?php
                        foreach ($errors->get('has_expiry') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="has_description">Has Description:</label>
                        <select name="has_description" id="has_description" class="form-control">
                            <option value="1" <?php if($has_description == 1) { echo 'selected'; } ?>>Yes</option>
                            <option value="0" <?php if($has_description == 0) { echo 'selected'; } ?>>No</option>
                        </select>
                        <?php
                        foreach ($errors->get('has_description') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop