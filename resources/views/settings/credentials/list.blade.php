@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Credentials</h1>
            <p>Here's a list of all the credentials in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="credentials/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Has Expiry</th>
                        <th>Has Description</th>
                        <th>View</th>
                        <th>Delete</th>
                    </tr>
                    <?php
                        foreach($credentials as $cred) {
                            if($cred->has_expiry == 1) {
                                $has_expiry = 'fa-check-square-o';
                            } else {
                                $has_expiry = 'fa-square-o';
                            }

                            if($cred->has_description == 1) {
                                $has_description = 'fa-check-square-o';
                            } else {
                                $has_description = 'fa-square-o';
                            }
                    ?>
                    <tr>
                        <td>
                            <a href="credentials/<?php echo $cred->id; ?>"><?php echo $cred->name ?></a>
                        </td>
                        <td><i class="fa <?php echo $has_expiry; ?>"></i></td>
                        <td><i class="fa <?php echo $has_description; ?>"></i></td>
                        <td>
                            <a href="credentials/<?php echo $cred->id; ?>"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>
                            <a href="credentials/delete/<?php echo $cred->id; ?>"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop