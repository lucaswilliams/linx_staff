<?php
if(strlen(old('super_name')) > 0) {
    $name = old('super_name');
} else {
    $name = $super->super_name;
}

if(strlen(old('super_guid')) > 0) {
    $uid = old('super_guid');
} else {
    $uid = $super->super_guid;
}
?>

@extends('layouts.app')

@section('title', 'Super Providers')

@section('header')
    <h1>
        <?php
            if(strlen($super->super_name) > 0) {
                echo $super->super_name;
            } else {
                echo 'Add new superannuation provider';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/super') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="super_id" value="<?php echo $super->super_id; ?>">

                    <div class="form-group">
                        <label for="super_name">Name:</label>
                        <input type="text" name="super_name" id="super_name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('super_name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="super_guid">MYOB Super Provider:</label>
                        <select name="super_guid" id="super_guid" class="search">
                            <option value="">(No super provider selected)</option>
                            <?php foreach($myob_super as $myob) {
                                $selected = '';
                                if(strcmp($myob->UID, $uid) == 0) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$myob->UID.'"'.$selected.'>'.$myob->Name.'</option>';
                            } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop