@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Superannuation</h1>
            <p>Here's a list of all the superannuation providers in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <form method="POST">
        <div class="row">
            <div class="col-xs-12">
                <div class="dashboard-tile text-right">
                    <a href="super/add" id="addUsi" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
                </div>
                <div class="dashboard-tile">
                    <table class="staff-table">
                        <tr>
                            <th>USI Number</th>
                            <th></th>
                        </tr>
                        <?php foreach($usis as $usi) { ?>
                        <tr>
                            <td>
                                <input type="text" class="form-control" name="usi[{{$usi->usi_id}}]" id="usi-{{$usi->usi_id}}" value="{{ $usi->usi_number }}">
                            </td>
                            <td>
                                <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>

            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('click', '.btn-danger', function(e) {
                e.preventDefault();
                $(this).parents('tr').remove();
            });

            $(document).on('click', '#addUsi', function(e) {
                e.preventDefault();
                $('.staff-table').append('<tr>' +
                '    <td>' +
                '       <input type="text" class="form-control" name="newusi[]">' +
                '    </td>' +
                '    <td>' +
                '        <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>' +
                '   </td>' +
                '</tr>');
            });
        });
    </script>
@endsection