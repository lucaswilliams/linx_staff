@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Superannuation</h1>
            <p>Here's a list of all the superannuation providers in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="super/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($supers as $super) { ?>
                    <tr>
                        <td>
                            <a href="super/<?php echo $super->super_id; ?>"><?php echo $super->super_name ?></a>
                        </td>
                        <td class="text-right">
                            <a href="super/<?php echo $super->super_id; ?>" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                            <a href="super/usi/<?php echo $super->super_id; ?>" class="btn btn-warning"><i class="fa fa-list"></i></a>
                            <a href="super/delete/<?php echo $super->super_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop