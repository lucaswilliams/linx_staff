@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Uniforms</h1>
            <p>Here's a list of all the uniforms in the system, from which you can add, edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile text-right">
                <a href="uniforms/add" class="btn btn-success"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>View</th>
                        <th>Delete</th>
                    </tr>
                    <?php foreach($uniforms as $uniform) { ?>
                    <tr>
                        <td>
                            <a href="uniforms/<?php echo $uniform->id; ?>"><?php echo $uniform->name ?></a>
                        </td>
                        <td>
                            <a href="uniforms/<?php echo $uniform->id; ?>"><i class="fa fa-pencil"></i></a>
                        </td>
                        <td>
                            <a href="uniforms/delete/<?php echo $uniform->id; ?>"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop