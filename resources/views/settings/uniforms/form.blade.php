<?php
if(strlen(old('name')) > 0) {
    $name = old('name');
} else {
    $name = $uniform->name;
}
?>

@extends('layouts.app')

@section('title', 'Uniforms')

@section('header')
    <h1>
        <?php
            if(strlen($uniform->name) > 0) {
                echo $uniform->name;
            } else {
                echo 'Add new uniform';
            }
        ?>
    </h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('settings/uniforms') }}">
            <div class="col-xs-12">
                <div class="dashboard-tile">
                {!! csrf_field() !!}
                    <input type="hidden" name="uniform_id" value="<?php echo $uniform->id; ?>">

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="form-control">
                        <?php
                        foreach ($errors->get('name') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop