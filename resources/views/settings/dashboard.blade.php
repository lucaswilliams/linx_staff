@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Settings Dashboard</h1>
            <p>This is the settings dashboard.  This section of the application will allow you to configure system-wide options that will affect all of the other users.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-file-text-o"></i> <a href="{{ URL::to('settings/credentials') }}">Credentials</a></h1>
            </div>
        </div>
    <!--
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-user"></i> <a href="{{ URL::to('settings/uniforms') }}">Uniforms</a></h1>
            </div>
        </div>-->
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-balance-scale"></i> <a href="{{ URL::to('settings/super') }}">Super</a></h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-money"></i> <a href="{{ URL::to('settings/myob') }}">Integration</a></h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-external-link-square fa-rotate-180"></i> <a href="{{ URL::to('settings/inductions') }}">Inductions</a></h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-file-text-o"></i> <a href="{{ URL::to('settings/campaigns') }}">Campaigns</a></h1>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-question-circle"></i> <a href="{{ URL::to('settings/help') }}">Help System</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-line-chart"></i> <a href="{{ URL::to('settings/reporting') }}">Report Setup</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-download"></i> <a href="{{ URL::to('settings/myob/import') }}">Import employees</a></h1>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="dashboard-tile">
                <h1><i class="fa fa-calendar"></i> <a href="{{ URL::to('settings/years') }}">Financial Years</a></h1>
            </div>
        </div>
    </div>
@stop