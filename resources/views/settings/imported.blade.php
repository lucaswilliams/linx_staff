@extends('layouts.app')

@section('title', 'Employees Imported')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Import Complete</h1>
            <div class="dashboard-tile">
                <p><?php echo $difference; ?> workers imported.</p>
                <pre><?php echo $content; ?></pre>
            </div>
        </div>
    </div>
@stop