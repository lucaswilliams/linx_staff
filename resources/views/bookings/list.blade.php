@extends('layouts.app')

@section('title', 'Bookings')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Bookings</h1>
            <p>Here's a list of all the bookings in the system, from which you can edit or delete them.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="dashboard-tile">
                <table class="staff-table">
                    <tr>
                        <th>Name</th>
                        <th>Room Type</th>
                        <th>Arrival Date</th>
                        <th>Departure Date</th>
                        <th colspan="2">Actions</th>
                    </tr>
                    <?php
                        foreach($bookings as $booking) {
                    ?>
                    <tr>
                        <td><?php echo $booking->given_name.' '.$booking->surname; ?></td>
                        <td><?php echo $booking->roomtype_name; ?></td>
                        <td><?php echo $booking->arrival_date; ?></td>
                        <td><?php echo $booking->departure_date; ?></td>
                        <td><a href="bookings/<?php echo $booking->booking_id; ?>"><i class="fa fa-pencil"></i> Edit</a></td>
                        <td><a href="bookings/delete/<?php echo $booking->booking_id; ?>"><i class="fa fa-trash-o"></i> Delete</a></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
@stop