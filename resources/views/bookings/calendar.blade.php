@extends('layouts.app')

@section('title', 'Booking')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Booking Calendar</h1>
            <p>Week commencing <input type="text" class="date calendar" value="<?php echo date('Y-m-d', strtotime('last Monday')); ?>"/></p>
            <table width="100%">
                <tr>
                    <th width="16%">Room</th>
                    <?php for($i = 0; $i < 7; $i++) { ?>
                        <th width="12%"><?php echo date("D j\nM", strtotime('+'.$i.' days')); ?></th>
                    <?php } ?>
                </tr>
            <?php foreach($bookings as $booking) { ?>
                <tr>
                    <td><?php echo $booking->beds_name.' ('.$booking->room_name.')'; ?></td>
                    <?php for($i = 0; $i < 7; $i++) { ?>
                    <td>
                        <?php
                            $date = strtotime('+'.$i.' days');
                            if(count($booking->bookings) > 0) {
                                foreach($booking->bookings as $book) {
                                    if($date >= strtotime($book->arrival_date) && $date <= strtotime($book->departure_date)) {
                                        echo $book->given_name.' '.$book->surname;

                                    }
                                }
                            }
                        ?>
                    </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </table>
        </div>
    </div>
@stop