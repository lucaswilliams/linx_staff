@extends('layouts.app')

@section('title', 'Booking')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Booking Complete</h1>
            <p>Thanks for filling out a booking request with us.  We will be back to you shortly.</p>
            <p>In the meantime, continue through to your <a href="{{ URL::to('dashboard') }}">dashboard</a> to continue using the system.</p>
        </div>
    </div>
@stop