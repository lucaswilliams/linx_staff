@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Register interest in Accommodation</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="{{ URL::to('booking') }}" id="bookingform">
            {!! csrf_field() !!}

            <div class="col-md-6">
                <div class="dashboard-tile">
                    <input type="hidden" name="user_id" value="<?php echo $user->id; ?>">
                    <div class="form-group">
                        <label for="date_arrival">Arrival date:</label>
                        <input type="date" name="date_arrival" id="date_arrival" class="form-control date">
                    </div>

                    <div class="form-group">
                        <label for="date_departure">Departure date:</label>
                        <input type="date" name="date_departure" id="date_departure" class="form-control date">
                    </div>

                    <div class="form-group">
                        <label for="roomtypes">Room Type:</label>
                        <select name="roomtypes" id="roomtypes" class="form-control">
                            <option value=""></option>
                            <?php foreach($roomtypes as $roomtype) { ?>
                            <option value="<?php echo $roomtype->roomtype_id; ?>" data-image="<?php echo $roomtype->roomtype_image; ?>"><?php echo $roomtype->roomtype_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="group_number">Number in your group:</label>
                        <div class="input-group spinner">
                            <input type="text" class="form-control" value="1" name="group_number" id="group_number">
                            <div class="input-group-btn-vertical">
                                <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="usernames">Names of people in your group <small>(please use Timesheet Management System usernames)</small>:</label>
                        <div class="usernames">
                            <input type="text" name="usernames[]" id="username" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="dashboard-tile">
                    <div id="roomtype-image"><img src=""></div>
                    <div id="room-image"><img src="images/rooms/floorplan.png" id="bg"><img src="" id="fg"></div>
                </div>
            </div>

            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Submit Application</button>
                </div>
            </div>
        </form>
    </div>

    <div id="roomlist">
        <?php foreach($rooms as $room) { ?>
        <option value="<?php echo $room->room_id; ?>" data-roomtype="<?php echo $room->roomtype_id; ?>"><?php echo $room->room_name; ?></option>
        <?php } ?>
    </div>
@stop