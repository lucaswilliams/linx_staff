<?php
if(strlen(old('arrival_date')) > 0) {
    $arrival_date = old('arrival_date');
} else {
    $arrival_date = $booking->arrival_date;
}

if(strlen(old('departure_date')) > 0) {
    $departure_date = old('departure_date');
} else {
    $departure_date = $booking->departure_date;
}

if($booking->separate_rooms == 0) {
    $separate = 'No';
} else {
    $separate = 'Yes';
}

if($booking->bunk_room == 0) {
    $bunkroom = 'No';
} else {
    $bunkroom = 'Yes';
}

if($booking->paid_flag == 1) {
    $confirmed = 'Yes';
    $conf_flag = true;
    $paid = 'Yes';
    $paid_flag = true;
} else {
    $paid = 'No';
    $paid_flag = false;
    if($booking->confirmed == 1) {
        $confirmed = 'Yes';
        $conf_flag = true;
    } else {
        $confirmed = 'No';
        $conf_flag = false;
    }
}
?>
@extends('layouts.app')

@section('title', 'Dashboard')

@section('header')
    <h1>Edit Booking</h1>
@stop

@section('content')
    <div class="row">
        <form method="POST" action="<?php echo url('kingsley/bookings'); ?>" id="bookingform">
            {!! csrf_field() !!}
            <input type="hidden" name="booking_id" value="<?php echo $booking->booking_id; ?>">

            <div class="col-xs-12">
                <div class="dashboard-tile">
                    <div class="form-group">
                        <label for="nothing">Booking made by: <?php echo $booking->given_name.' '.$booking->surname; ?></label>
                    </div>

                    <div class="form-group">
                        <label for="nothing">Number in group: <?php echo $booking->number_in_group; ?></label>
                    </div>

                    <div class="form-group">
                        <label for="nothing">Are separate rooms required: <?php echo $separate; ?></label>
                    </div>

                    <div class="form-group">
                        <label for="nothing">Happy to be in a shared dorm: <?php echo $bunkroom; ?></label>
                    </div>

                    <div class="form-group">
                        <label for="nothing">Requested room type: <?php echo $booking->roomtype_name; ?></label>
                    </div>

                    <div class="form-group">
                        <label for="date_arrival">Arrival date:</label>
                        <input type="date" name="date_arrival" id="date_arrival" value="<?php echo $arrival_date; ?>" class="form-control date">
                    </div>

                    <div class="form-group">
                        <label for="date_departure">Departure date:</label>
                        <input type="date" name="date_departure" id="date_departure" value="<?php echo $departure_date; ?>" class="form-control date">
                    </div>

                    <div class="form-group">
                        <label for="room_type">Room Type:</label>
                        <select name="room_type" id="room_type" class="form-control">
                            <?php foreach($roomtypes as $roomtype) { ?>
                            <option value="<?php echo $roomtype->roomtype_id; ?>" <?php if(strcmp($roomtype->roomtype_name, $booking->roomtype_name) == 0) { echo 'selected'; } ?>><?php echo $roomtype->roomtype_name; ?></option>
                            <?php } ?>
                        </select>
                        <?php
                        foreach ($errors->get('room_type') as $error) {
                            echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
                        }
                        ?>
                    </div>

                    <?php
                    if($paid_flag) {
                        //Booking is paid, show some details
                    ?>
                        <div class="form-group">
                            <label for="nothing">Paid: Yes</label>
                        </div>

                        <div class="form-group">
                            <label for="nothing">Payment Date: <?php echo $booking->paid_date; ?></label>
                        </div>

                        <div class="form-group">
                            <label for="nothing">Receipt Number: <?php echo $booking->paid_rrn; ?></label>
                        </div>
                    <?php
                    } else {
                        //Booking is not paid, still able to make it not confirmed (i.e. Cancelled)
                    ?>
                        <div class="form-group">
                            <label for="confirmed">Booking confirmed:</label>
                            <select name="confirmed" id="confirmed" class="form-control">
                                <option value="0" <?php if(!$conf_flag) { echo 'selected'; } ?>>No</option>
                                <option value="1" <?php if($conf_flag) { echo 'selected'; } ?>>Yes</option>
                            </select>
                        </div>
                    <?php
                    }
                    ?>

                    <h2>People on this booking</h2>
                    <table width="100%">
                        <tr>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Room</th>
                            <th>Bed</th>
                        </tr>

                    <?php foreach($users as $user) {
                        $gender = '';
                        if($user->gender == 1) { $gender = 'Male'; }
                        if($user->gender == 2) { $gender = 'Female'; }
                    ?>
                        <input type="hidden" name="user_ids[]" value="<?php echo $user->user_id; ?>">
                        <tr>
                            <td><?php echo $user->given_name.' '.$user->surname; ?></td>
                            <td><?php echo $gender; ?></td>
                            <td>
                                <select name="rooms[]" class="form-control select-rooms search" data-id="<?php echo $user->user_id; ?>">
                                    <option value="0"></option>
                                    <?php foreach($rooms as $room) { ?>
                                    <option value="<?php echo $room->room_id; ?>" <?php if($room->room_id == $user->room_id) { echo 'selected'; } ?>><?php echo $room->room_name; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>
                                <select name="beds[]" class="form-control select-beds search" data-id="<?php echo $user->user_id; ?>">
                                    <option value="0"></option>
                                    <?php foreach($beds as $bed) { ?>
                                    <option value="<?php echo $bed->beds_id; ?>" <?php if($bed->beds_id == $user->beds_id) { echo 'selected'; } ?>><?php echo $bed->beds_name; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                    <?php } ?>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <div class="dashboard-tile">
                    <button type="submit" class="btn btn-success btn-save"><i class="fa fa-floppy-o"></i> Save Details</button>
                </div>
            </div>
        </form>
    </div>
@stop