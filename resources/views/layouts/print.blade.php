<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Linx Timesheet Management System | @section('title') @show</title>

    <style media="all">
        body {
            font-family: "Roboto", Helvetica, Arial, sans-serif;
            font-size: 14px;
        }

        div.page {
            margin: 0 auto;
            width: 210mm;
            page-break-after: always;
        }

        td.header {
            background-color: #9A9A9A;
            font-weight: bold;
        }

        td {
            border-bottom: 1px solid #9A9A9A;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>