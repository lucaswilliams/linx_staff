<?php
$data = (array)$data;

if(strlen(old($name)) > 0) {
    $value = old($name);
} elseif(isset($data)) {
    $value = (isset($data[$name]) ? $data[$name] : '');
} else {
    $value = '';
}
?>

<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <label class="control-label" for="{{ $name }}">{{ $label }}<?php if($required) { echo '<span class="required">*</span>'; } ?>:</label>
    <input type="tel" class="form-control" name="{{ $name }}" id="{{ $name }}" value="{{ $value }}">
    @if ($errors->has($name))
        <div class="alert alert-danger alert-inline">
            {{ $errors->first($name) }}
        </div>
    @endif
</div>