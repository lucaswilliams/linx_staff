<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <label class="control-label" for="{{ $name }}">{{ $label }}<?php if($required) { echo '<span class="required">*</span>'; } ?>:</label>
    <input type="password" class="form-control" name="{{ $name }}" id="{{ $name }}">
    @if ($errors->has($name))
        <div class="alert alert-danger alert-inline">
            {{ $errors->first($name) }}
        </div>
    @endif
</div>