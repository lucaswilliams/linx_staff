<?php
    $hour = 0;
    $minute = 0;
    if(isset($value)) {
        $parts = explode(':', $value);
        if(count($parts) > 1) {
            $hour = (int)$parts[0];
            $minute = (int)$parts[1];
        }
    }

    $value = str_pad($hour, 2, '0', STR_PAD_LEFT).':'.str_pad($minute, 2, '0', STR_PAD_LEFT);

    echo '<div class="row">';
    echo '<div class="col-xs-12">';
    echo '<div style="width: 50%; float: left;">';
    echo '<select class="timepicker hour form-control">';
    for($i = 0; $i < 24; $i++) {
        echo '<option value="'.str_pad($i, 2, '0', STR_PAD_LEFT).'" '.($hour == $i ? ' selected' : '').'>'.str_pad($i, 2, '0', STR_PAD_LEFT).'</option>';
    }
    echo '</select>';
    echo '</div>';

    echo '<div style="width: 50%; float: left;">';
    echo '<select class="timepicker minute form-control">';
    echo '<option value="00" '.($minute == 0 ? ' selected' : '').'>00</option>';
    echo '<option value="15" '.($minute == 15 ? ' selected' : '').'>15</option>';
    echo '<option value="30" '.($minute == 30 ? ' selected' : '').'>30</option>';
    echo '<option value="45" '.($minute == 45 ? ' selected' : '').'>45</option>';
    echo '</select>';

    echo '<input type="text" class="timepicker time" style="display: none;" name="'.$name.'" value="'.$value.'">';
    echo '</div>';
    echo '</div>';
    echo '</div>';