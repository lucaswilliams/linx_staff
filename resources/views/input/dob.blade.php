<?php
    $data = (array)$data;

    if(strlen(old($name.'_month')) > 0) {
        $value_month = old($name.'_month');
    } elseif(isset($data)) {
        $value_month = (isset($data[$name]) ? date('m', strtotime($data[$name])) : '');
    } else {
        $value_month = '';
    }

    if(strlen(old($name.'_year')) > 0) {
        $value_year = old($name.'_year');
    } elseif(isset($data)) {
        $value_year = (isset($data[$name]) ? date('Y', strtotime($data[$name])) : '');
    } else {
        $value_year = '';
    }

    if(strlen(old($name.'_day')) > 0) {
        $value_day = old($name.'_day');
    } elseif(isset($data)) {
        $value_day = (isset($data[$name]) ? date('d', strtotime($data[$name])) : '');
    } else {
        $value_day = '';
    }

    $source = array (
        (object)array('key' => '01', 'value' => 'January'),
        (object)array('key' => '02', 'value' => 'February'),
        (object)array('key' => '03', 'value' => 'March'),
        (object)array('key' => '04', 'value' => 'April'),
        (object)array('key' => '05', 'value' => 'May'),
        (object)array('key' => '06', 'value' => 'June'),
        (object)array('key' => '07', 'value' => 'July'),
        (object)array('key' => '08', 'value' => 'August'),
        (object)array('key' => '09', 'value' => 'September'),
        (object)array('key' => '10', 'value' => 'October'),
        (object)array('key' => '11', 'value' => 'November'),
        (object)array('key' => '12', 'value' => 'December')
    );

    if(!isset($class)) {
        $class = '';
    }
?>

<div class="form-group date_of_birth {{ ($errors->has($name.'_month') || $errors->has($name.'_day') || $errors->has($name.'_year')) ? ' has-error' : '' }} {{ $class }}">
    <label for="{{ $name }}_month">{{ $label }}<?php if($required) { echo '<span class="required">*</span>'; } ?></label>
    <div style="height: 34px; margin-bottom: 15px;">
        <select name="{{ $name }}_month" id="{{ $name }}_month" class="noselect form-control" style="width: 50%; float:left;">
            <option></option>
            <?php
            foreach($source as $item) {
                $selected = '';
                if(strcmp($item->key, $value_month) == 0) {
                    $selected = ' selected';
                }
                echo '<option value="'.$item->key.'"'.$selected.'>'.$item->value.'</option>';
            }
            ?>
        </select>
        <input type="text" name="{{ $name }}_day" id="{{ $name }}_day" placeholder="Day" class="form-control" style="width: 25%; float:left;" value="{{ $value_day }}">
        <input type="text" name="{{ $name }}_year" id="{{ $name }}_year" placeholder="Year" class="form-control" style="width: 25%; float:left;" value="{{ $value_year }}">
    </div>
    <?php
    foreach ($errors->get($name.'_month') as $error) {
        echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
    }
    foreach ($errors->get($name.'_day') as $error) {
        echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
    }
    foreach ($errors->get($name.'_year') as $error) {
        echo '<div class="alert alert-danger alert-inline">'.$error.'</div>';
    }
    ?>
</div>