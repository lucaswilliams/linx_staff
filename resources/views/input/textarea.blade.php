<?php
    $data = (array)$data;

    if(strlen(old($name)) > 0) {
        $value = old($name);
    } elseif(isset($data)) {
        $value = (isset($data[$name]) ? $data[$name] : '');
    } else {
        $value = '';
    }
?>

<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <!--<div class="col-md-2">-->
        <label class="control-label" for="{{ $name }}">{{ $label }}:</label>
    <!--</div>
    <div class="col-md-10">-->
        <textarea class="form-control" name="{{ $name }}" id="{{ $name }}">{{ $value }}</textarea>
        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    <!--</div>-->
</div>