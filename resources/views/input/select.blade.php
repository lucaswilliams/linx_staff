<?php
    $data = (array)$data;

    if(strlen(old($name)) > 0) {
        $value = old($name);
    } elseif(isset($data)) {
        $value = (isset($data[$name]) ? $data[$name] : '');
    } else {
        $value = '';
    }

    if(!isset($required)) { $required = false; }
if(!isset($class)) {
    $class = '';
}
?>

<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }} {{ $class }}">
    <label class="control-label" for="{{ $name }}">{{ $label }}<?php if($required) { echo '<span class="required">*</span>'; } ?>:</label>
    <select class="form-control" name="{{ $name }}" id="{{ $name }}">
        <option value=""></option>
        <?php
        foreach($source as $item) {
            $selected = '';
            if(strcmp($item->key, $value) == 0) {
                $selected = ' selected="selected"';
            }
            echo '<option value="'.$item->key.'"'.$selected.'>'.$item->value.'</option>';
        }
        ?>
    </select>
    @if ($errors->has($name))
        <div class="alert alert-danger alert-inline">
            {{ $errors->first($name) }}
        </div>
    @endif
</div>