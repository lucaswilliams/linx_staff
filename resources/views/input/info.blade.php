<?php
    $data = (array)$data;

    if(strlen(old($name)) > 0) {
        $value = old($name);
    } elseif(isset($data)) {
        $value = (isset($data[$name]) ? $data[$name] : '');
    } else {
        $value = '';
    }
?>

<div class="form-group row{{ $errors->has($name) ? ' has-error' : '' }}">
    <div class="col-md-2">
        <strong>{{ $label }}:</strong>
    </div>
    <div class="col-md-10">
        {{ $value }}
    </div>
</div>