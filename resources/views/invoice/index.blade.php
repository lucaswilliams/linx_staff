@extends('layouts.app')

@section('title', 'Dashboard')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            loadInvoices();
        });

        $(document).on('click', '#refresh-invoices', function() {
            $('#table-loading').show();
            $('#invoices .invoice-row').remove();
            loadInvoices();
        });

        function loadInvoices() {
            $('#invoices .invoice-row').remove();
            $('#table-loading').show();

            $.ajax({
                url: BASE_URL + '/invoices/json',
                dataType: 'json',
                data: {
                    'client' : $('#search-client').val(),
                    'number' : $('#search-number').val(),
                    'date' : $('#search-date').val()
                },
                success: function (data) {
                    $('#table-loading').hide();
                    $.each(data, function(k,v) {
                        $inv = $('#invoice-template').clone().removeAttr('id');

                        $inv.find('.inv_customer').text(v.name);
                        $inv.find('.inv_date').text(v.invoice_date);
                        $inv.find('.inv_number').text(v.invoice_number);
                        $inv.find('.inv_amount').text(v.invoice_amount);

                        $inv.find('.btn-default').each(function(l) {
                            $(this).attr('href', $(this).attr('href') + '/' + v.invoice_id);
                        });

                        $inv.find('.btn-danger').each(function(l) {
                            $(this).attr('href', $(this).attr('href') + '/' + v.invoice_id);
                        });

                        $inv.find('.btn-primary').each(function(l) {
                            $(this).attr('href', $(this).attr('href') + '/' + v.invoice_id);
                        });

                        $inv.appendTo('#invoices');
                    });
                }
            });
        }
    </script>
@endsection

@section('header')
    <h1>Invoices</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12" id="search-user">
            <div class="dashboard-tile">
                <h2>Search Invoices</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <label for="search-client" class="control-label">Client:</label>
                        <select name="search-client" id="search-client" class="form-control">
                            <option></option>
                            <?php
                            foreach($clients as $client) {
                                echo '<option value="'.$client->id.'">'.$client->name.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <label for="search-number" class="control-label">Number:</label>
                        <input type="text" id="search-number" name="search-number" class="form-control">
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <label for="search-date" class="control-label">Date since:</label>
                        <input type="date" id="search-date" name="search-date" value="<?php echo date('Y-m-d', strtotime('2 weeks ago')); ?>" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 text-right">
                        <a class="btn btn-success pull-left" href="{{ url('/invoices/create') }}">
                            <i class="fa fa-plus"></i> Create Invoice
                        </a>
                        <a class="btn btn-primary" id="refresh-invoices">
                            <i class="fa fa-search"></i> Refresh search
                        </a>
                    </div>
                </div>
            </div>

            <div class="dashboard-tile">
                <table id="invoices" class="table table-striped">
                    <tr>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Number</th>
                        <th>Amount</th>
                        <th>Actions</th>
                    </tr>
                    <tr id="table-loading">
                        <td colspan="5" class="text-center">
                            <i class="fa fa-spinner fa-pulse fa-fw fa-4x"></i>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <table style="display: none">
        <tr id="invoice-template" class="invoice-row">
            <td class="inv_customer"></td>
            <td class="inv_date"></td>
            <td class="inv_number"></td>
            <td class="inv_amount"></td>
            <td>
                <a class="btn btn-default" href="{{ url('/invoices/print/') }}"><i class="fa fa-print"></i></a>
                <a class="btn btn-primary" href="{{ url('/invoices/email/') }}"><i class="fa fa-envelope-o"></i></a>
                <a class="btn btn-danger" href="{{ url('/invoices/delete/') }}"><i class="fa fa-trash-o"></i></a>
            </td>
        </tr>
    </table>
@endsection