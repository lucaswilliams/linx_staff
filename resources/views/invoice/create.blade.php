@extends('layouts.app')

@section('title', 'Dashboard')

@section('scripts')
    <script type="text/javascript">
        $(document).on('change', '#client_id', function(e) {
            var _val = $(this).find('option:selected').val();
            $('#jobs_id option:not(:first-child)').remove();
            $('#job_list option[data-client=' + _val + ']').clone().appendTo($('#jobs_id'));
        });

        $(document).on('click', '#invoice-client-search', function(e) {
            $('tr.time_task_row').remove();
            $.ajax({
                url: BASE_URL + '/invoices/createSearch',
                type: 'GET',
                dataType: 'json',
                data: {
                    'jobs_id' : $('#jobs_id').val(),
                    'client_id' : $('#client_id').val(),
                    'invoice_start' : $('#invoice_start').val(),
                    'invoice_finish' : $('#invoice_end').val()
                },
                success: function (data) {
                    if(data.length == 0) {
                        $('#time_task_table').append('<tr class="time_task_row">' +
                                    '<td colspan="5">No records to show</td>' +
                                '</tr>');
                    } else {
                        $.each(data, function (k, v) {
                            $('#time_task_table').append('<tr class="time_task_row">' +
                                    '<td>' + v.staff_name + '</td>' +
                                    '<td>' + v.time_start + '</td>' +
                                    '<td>' + v.task_name + '</td>' +
                                    '<td class="text-right"><input type="hidden" name="tita_total[' + v.tita_id + ']" value="' + v.tita_total + '">$' + v.tita_total + '</td>' +
                                    '<td><input type="checkbox" name="tita_id[' + v.tita_id + ']" value="1"></td>' +
                                    '</tr>');
                        });
                    }
                }
            });
        });
    </script>
@endsection

@section('header')
    <h1>Create Invoice</h1>
@endsection

@section('content')
    <form method="POST">
        <div class="dashboard-tile">
            <div class="row">
                <div class="col-xs-12">
                    <label for="client_id">Select a client for invoicing:</label>
                    <select name="client_id" id="client_id" class="form-control">
                        <option></option>
                        <?php
                        foreach($clients as $client) {
                            echo '<option value="'.$client->client_id.'">'.$client->client_name.'</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="col-xs-12">
                    <label for="jobs_id">Select a job for invoicing:</label>
                    <select name="jobs_id" id="jobs_id" class="form-control">
                        <option value="0">(all jobs)</option>
                    </select>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <label for="invoice_start">Shifts from:</label>
                    <input type="date" id="invoice_start" name="invoice_start" class="form-control" value="<?php echo date('Y-m-d', strtotime('Monday last week'));?>">
                </div>
                <div class="col-xs-12 col-sm-6">
                    <label for="invoice_end">Shifts to:</label>
                    <input type="date" id="invoice_end" name="invoice_end" class="form-control" value="<?php echo date('Y-m-d', strtotime('Sunday last week'));?>">
                </div>

                <div class="col-xs-12 text-right">
                    <a class="btn btn-success" id="invoice-client-search"><i class="fa fa-search"></i> Search</a>
                </div>
            </div>
        </div>

        <div class="dashboard-tile">
            <div class="row">
                <div class="col-xs-12">
                    <table id="time_task_table" class="table table-striped">
                        <tr>
                            <th>Worker</th>
                            <th>Date</th>
                            <th>Task</th>
                            <th>Amount</th>
                            <th width="60px"></th>
                        </tr>
                    </table>
                </div>
                <div class="col-xs-12 text-right">
                    <a class="btn btn-success select-all"><i class="fa fa-check"></i> Select All</a><br />
                    <button class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button>
                </div>
            </div>
        </div>
    </form>

    <div id="job_list" style="display:none">
        <?php
        foreach($items as $item) {
            echo '<option value="'.$item->jobs_id.'" data-client="'.$item->client_id.'">'.$item->jobs_name.'</option>';
        }
        ?>
    </div>
@endsection