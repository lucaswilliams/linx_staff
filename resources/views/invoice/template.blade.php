<page style="width: 10cm;" backtop="0mm" backbottom="0mm" backleft="0mm" backright="0mm">
    <page_header>
        <table style="width: 100%;" border="0">
            <tr>
                <td colspan="2" style="width:100%; text-align: center">
                    <img style="width: 50%; height: auto;" src="http://www.linxemployment.com.au/wp-content/themes/linx/img/Linx.png">
                    <p>A.B.N.  77 162 415 689</p>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%">
                    <h1 style="margin-bottom: 50px">Tax Invoice</h1>
                </td>
            </tr>
            <tr>
                <td style="width:60%">
                    <?php echo $client->name; ?><br />
                    <?php echo $client->address; ?><br />
                        <?php echo $client->city; ?> <?php echo $client->state; ?> <?php echo $client->postcode; ?>
                </td>
                <td style="width:40%; text-align: right">
                    <table style="width:100%">
                        <tr>
                            <td style="width:50%">Invoice: </td>
                            <td style="width:50%; text-align: right"><?php echo $invoice->invoice_number; ?></td>
                        </tr>
                        <tr>
                            <td style="width:50%">Date: </td>
                            <td style="width:50%; text-align: right"><?php echo date('d/m/Y', strtotime($invoice->invoice_date)); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td colspan="2"></td></tr>
        </table>
    </page_header>

    <table border="1" cellspacing="0" style="width:100%; margin-top: 320px;">
        <tr>
            <td style="width:10%; text-align: left;">Quantity</td>
            <td style="width:15%; text-align: left;">Item Code</td>
            <td style="width:55%; text-align: left;">Description</td>
            <td style="width:10%; text-align: right;">Unit Price<br />(ex GST)</td>
            <td style="width:10%; text-align: right;">Total<br />(ex GST)</td>
        </tr>
        <?php $subtotal = 0; foreach($invoice->items as $item) { ?>
        <tr>
            <td style="width:10%; text-align: left;"><?php echo number_format($item->quantity, 2); ?></td>
            <td style="width:15%; text-align: left;"><?php echo $item->code; ?></td>
            <td style="width:55%; text-align: left;"><?php echo $item->description; ?></td>
            <td style="width:10%; text-align: right;">$<?php echo number_format(round($item->unit_price, 2, PHP_ROUND_HALF_EVEN), 2); ?></td>
            <td style="width:10%; text-align: right;">$
                <?php
                $linetotal = ($item->unit_price * $item->quantity);
                echo round($linetotal, 2);
                $subtotal += $linetotal;
                ?>
            </td>
        </tr>
        <?php } ?>
    </table>

    <table border="0" style="width:100%; margin-bottom: 10px;">
        <tr>
            <td style="width:50%; text-align: center; font-size: 20px;">
                <p>Strict trading terms apply</p>
                <p>Please see below</p>
            </td>
            <td style="width:40%; text-align: right;">
                <p>Subtotal:</p>
                <p>GST:</p>
                <p>Total (inc GST):</p>
            </td>

            <td style="width:10%; text-align: right">
                <p>$<?php echo number_format(round($subtotal, 2, PHP_ROUND_HALF_EVEN), 2); ?></p>
                <p>$<?php echo number_format(round($subtotal * 0.1, 2, PHP_ROUND_HALF_EVEN), 2); ?></p>
                <p>$<?php echo number_format(round($subtotal * 1.1, 2, PHP_ROUND_HALF_EVEN), 2); ?></p>
            </td>
        </tr>
    </table>

    <hr />

    <h2>How to pay</h2>

    <table border="0" style="width:100%;">
        <tr>
            <td style="width: 50%; vertical-align: top">
                    <h3>Bank Details</h3>
                    Name: Linx Employment Tas Pty Ltd<br />
                    BSB: 06 7024<br />
                    Account: 1008 2500<br /><br />
                    Use your invoice number as the reference
            </td>
            <td style="width: 50%; vertical-align: top">
                    <h3>Trading Terms</h3>
                    All funds are to be credited to the bank account provided.<br /><br />No payments later than Friday of the invoiced week.
            </td>
        </tr>
    </table>

    <page_footer>
        <table border="0" style="width:100%; font-size: 12px;">
            <tr>
                <td style="width:50%; text-align: left">Address: 2/16 Freshwater Point Road, Legana TAS 7277</td>
                <td style="width:50%; text-align: right">E: admin@linxemployment.com.au</td>
            </tr>
        </table>
        <table border="0" style="width:100%; font-size: 12px;">
            <tr>
                <td style="width: 33%; text-align: left">PO Box 41, Legana TAS 7277</td>
                <td style="width: 34%; text-align: center">PH: 03 6330 2471</td>
                <td style="width: 33%; text-align: right">FAX: 03 6394 3994</td>
            </tr>
        </table>
        <table border="0" style="width:100%; font-size: 12px;">
            <tr>
                <td style="width: 100%; text-align: center">www.linxemployment.com.au</td>
            </tr>
        </table>
    </page_footer>
</page>