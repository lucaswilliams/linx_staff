<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['given_name', 'surname', 'date_of_birth', 'email', 'username', 'password', 'level', 'referral', 'preferred', 'accommodation', 'current_location', 'date_arrive', 'loca_arrive', 'visa_88_days', 'visa_expiry', 'passport_number', 'stay_length', 'country', 'telephone', 'mobilephone', 'has_transport', 'num_in_group', 'share_details', 'address', 'city', 'state', 'postcode', 'date_of_birth', 'gender', 'account_name', 'account_bsb', 'account_number', 'tfn', 'tax_resident', 'tax_free_threshold', 'senior_tax_offset', 'help_debt', 'fs_debt', 'provider_id_other', 'provider_id', 'super_number', 'job_network', 'job_network_tick', 'usi_id', 'campaign_id', 'usi_id_other', 'no_address'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function setImpersonating($id)
    {
        \Session::put('impersonate', $id);
    }

    public function stopImpersonating()
    {
        \Session::forget('impersonate');
    }

    public function isImpersonating()
    {
        return \Session::has('impersonate');
    }
}
