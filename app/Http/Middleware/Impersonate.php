<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Impersonate
{
    /**
     * Handle an incoming request.
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('impersonate'))
        {
            $id = $request->session()->get('impersonate');
            //die($id);
            Auth::onceUsingId($id);
        }

        return $next($request);
    }
}