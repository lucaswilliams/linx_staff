<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class LinxMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //TODO: Call logging code here.

        if(Auth::check()) {
            if (Auth::user()->level >= 2) {
                return $next($request);
            } else {
                return view('errors.access');
            }
        } else {
            return redirect('login');
        }
    }
}
