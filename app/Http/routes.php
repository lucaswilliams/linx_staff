<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    if(Auth::check()) {
        //The user is logged in, home page.
        return redirect()->route('dashboard');
    } else {
        //The user is not logged in, redirect to login
        return redirect()->route('login');
    }
});

Route::get('settings/myob', 'Settings\MYOBController@getSettings');
Route::get('help', 'Settings\HelpController@showHelp');
Route::get('special', 'HomeController@special');

Route::get('lw/payrates', 'Settings\MYOBController@getAllSuper');

//Forgotten passwords
// Password reset link request routes...
Route::get('password', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
Route::post('password', 'Auth\ForgotPasswordController@sendResetLinkEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.request');
Route::post('password/reset', 'Auth\ResetPasswordController@postReset')->name('password.reset');

Auth::routes();
Route::get('register', 'Auth\RegisterController@getRegister');
Route::get('impersonate/stop', 'UserController@impersonateStop');

Route::post('postWorker', 'UserController@updateFromZapier');

Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::get('myobtest', 'Settings\MYOBController@test');

//Must be logged in
Route::group(['middleware' => 'auth'], function() {
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'HomeController@dashboard']);
    Route::get('medical', 'HomeController@showMedical');
    Route::post('medical', 'HomeController@saveMedical');
    Route::get('resume', 'HomeController@showResume');
    Route::post('resume', 'HomeController@saveResume');
    Route::get('profile/{id}/resume', 'UserController@showResume')->where('id', '[0-9]+');
    Route::get('certificates', 'HomeController@showCredentials');
    Route::post('certificates', 'HomeController@saveCredentials');
    Route::get('home', ['as' => 'home', 'uses' => 'HomeController@dashboard']);
    Route::post('induct', 'Settings\InductionsController@saveUserInduction');

    Route::get('profile', 'UserController@showProfile');
    Route::post('profile', 'UserController@saveProfile');
    Route::post('profile/confirm', 'UserController@approveJob');
    Route::post('profile/reject', 'UserController@rejectJob');

    Route::get('changepw', 'UserController@changePassword');
    Route::post('changepw', 'UserController@savePassword');

    Route::get('booking', 'BookingController@showForm');
    Route::post('booking', 'BookingController@saveForm');

    //Jobs.  These have their own "security" set up later
    Route::get('jobs', 'JobsController@showList');
	Route::get('jobs/all', 'JobsController@showAllList');
    Route::get('jobs/{id}', 'JobsController@showForm')->where('id', '[0-9]+');
    Route::get('jobs/{id}/staff', 'JobsController@staffForm')->where('id', '[0-9]+');
    Route::get('jobs/{id}/super', 'JobsController@superForm')->where('id', '[0-9]+');
    Route::get('jobs/{jobs_id}/super/{id}', 'JobsController@makeSupervisorDefault')->where('id', '[0-9]+')->where('jobs_id', '[0-9]+');
    Route::get('jobs/{jobs_id}/super/delete/{id}', 'JobsController@deleteSupervisor')->where('id', '[0-9]+')->where('jobs_id', '[0-9]+');
    Route::get('jobs/{id}/tasks', 'JobsController@taskForm')->where('id', '[0-9]+');
    Route::post('jobs/{id}/tasks', 'TasksController@addTask')->where('id', '[0-9]+');
    //Route::get('jobs/{id}/shifts/tasks', 'JobsController@addTaskForm')->where('id', '[0-9]+');
    //Route::post('jobs/{id}/shifts/tasks', 'JobsController@saveTaskForm');
    Route::get('jobs/{id}/print', 'JobsController@printDetails')->where('id', '[0-9]+');
    Route::get('jobs/delete/{id}', 'JobsController@deleteJob')->where('id', '[0-9]+');
    Route::get('jobs/add', 'JobsController@showForm');
    Route::post('jobs', 'JobsController@saveForm');
    Route::get('jobs/{id}/payrates', 'JobsController@payratesForm');
    Route::post('jobs/{id}/payrates', 'JobsController@savePayratesForm');
    Route::get('jobs/tasks/delete/{id}', 'JobsController@deleteTask')->where('id', '[0-9]+');
    Route::get('jobs/tasks/edit/{id}', 'JobsController@editTask')->where('id', '[0-9]+');
    Route::post('jobs/tasks/edit/{id}', 'JobsController@saveTask');
    Route::get('jobs/{jobs_id}/roles', 'JobsController@getRoles')->where('jobs_id', '[0-9]+');
    Route::get('jobs/roles/edit/{role_id}', 'JobsController@getRole')->where('jobs_id', '[0-9]+')->where('role_id', '[0-9]+');
    Route::post('jobs/roles', 'JobsController@saveRole')->where('jobs_id', '[0-9]+');
    Route::get('jobs/roles/delete/{role_id}', 'JobsController@deleteRole');

    Route::get('jobs/{id}/shifts/{worker}', 'JobsController@workerShifts')->where('id', '[0-9]+')->where('worker', '[0-9]+');
    
    Route::get('jobs/sendworker/{id}', 'JobsController@SendWorkerToZapier')->where('id', '[0-9]+');

    Route::get('timesheets/signin', 'TimesheetsController@signIn');
    Route::get('timesheets/signout', 'TimesheetsController@signOutForm');
    Route::post('timesheets/signoutY', 'TimesheetsController@acceptDetails');
    Route::post('timesheets/signoutN', 'TimesheetsController@disputeDetails');
    Route::get('timesheets/signinSuper', 'TimesheetsController@signInSuper');
    Route::post('timesheets/signinSuper', 'TimesheetsController@saveSuper');
    Route::get('timesheets/deleteSuper/{id}', 'TimesheetsController@deleteSuper')->where('id', '[0-9]+');

    Route::get('timesheets/signoutWorker/{id}', 'TimesheetsController@signoutWorker');
    Route::get('timesheets/signinWorker/{id}', 'TimesheetsController@signinWorker');
    Route::get('timesheets/notrequiredWorker/{id}', 'TimesheetsController@notrequiredWorker');
    Route::get('timesheets/sickWorker/{id}', 'TimesheetsController@sickWorker');

    Route::get('timesheets/completed/{id}', 'TimesheetsController@completedWorkers')->where('id', '[0-9]+');
    Route::get('timesheets/uncomplete/{id}', 'TimesheetsController@uncompleteWorker')->where('id', '[0-9]+');

    Route::get('tasks/children/{id}', 'TasksController@getListChildren');

    Route::get('timesheets/shift/{id}', 'TimesheetsController@showShift')->where('id', '[0-9]+');
    Route::get('timesheets/shift/{id}/rates', 'JobsController@getPayRatesByShift')->where('id', '[0-9]+');
    Route::post('timesheets/shift/', 'TimesheetsController@saveShift');
    Route::get('timesheets/shift/{id}/copy', 'TimesheetsController@copyShift')->where('id', '[0-9]+');
    Route::get('timesheets/shift/{id}/cancel', 'TimesheetsController@deleteShift')->where('id', '[0-9]+');
    Route::get('timesheets/payrate/delete/{id}', 'TimesheetsController@deletePayrate')->where('id', '[0-9]+');
    Route::get('timesheets/delete/{id}', 'TimesheetsController@deleteTimesheet')->where('id', '[0-9]+');
    Route::get('timetasks/delete/{id}', 'TimesheetsController@deleteTimetask')->where('id', '[0-9]+');

    //Route::get('timesheets/{id}', 'TimesheetsController@getDetails')->where('id', '[0-9]+');
    //Route::get('timesheets/signOut/{id}', 'TimesheetsController@signOutForm')->where('id', '[0-9]+');
    //Route::get('timesheets/approve/{id}', 'TimesheetsController@approveForm')->where('id', '[0-9]+');
    Route::post('timesheets/signOut', 'TimesheetsController@signout');
    Route::post('timesheets/update', 'TimesheetsController@updateDetails');
    Route::post('timesheets/accept', 'TimesheetsController@acceptDetails');
    Route::post('timesheets/dispute', 'TimesheetsController@disputeDetails');
    Route::get('timesheets/payslips', 'TimesheetsController@preparePayslips');

    Route::get('timesheets/workers', 'TimesheetsController@assignWorkers');
    Route::post('timesheets/workers', 'TimesheetsController@saveAssignWorkers');

    Route::get('notifications', 'NotificationsController@getNotifications');
    Route::get('notifications/detail', 'NotificationsController@getNotificationDetail');
    Route::get('notifications/read', 'NotificationsController@markRead');

    Route::get('reporting', 'ReportsController@home');
    Route::get('reports/weekly', 'ReportsController@weeklyReport');

    Route::get('payslips', 'PayslipsController@index');
    Route::get('payslips/{id}', 'PayslipsController@show')->where('id', '[0-9]+');
    Route::get('payslips/json', 'PayslipsController@search');
    Route::get('payslips/email/{id}', 'PayslipsController@email')->where('id', '[0-9]+');

    //September manual timesheet entry
    Route::get('timesheets', 'TimeEntryController@selectJob');
    Route::post('timesheets', 'TimesheetsController@importDriscolls');
    Route::get('timesheets/import/{id}', 'TimesheetsController@importProcess')->where('id', '[0-9]+');
    Route::get('timesheets/{jobs_id}', 'TimeEntryController@selectView')->where('jobs_id', '[0-9]+');
    //Route::get('timesheets/dodge', 'TimeEntryController@jam');
    Route::get('timesheets/{jobs_id}/worker/{id}', 'TimeEntryController@byWorker')->where('jobs_id', '[0-9]+');
    Route::post('timesheets/{jobs_id}/worker/{id}', 'TimeEntryController@saveDay')->where('jobs_id', '[0-9]+');
    Route::get('timesheets/{jobs_id}/{day}', 'TimeEntryController@byDay')->where('jobs_id', '[0-9]+');
    Route::post('timesheets/{jobs_id}/{day}', 'TimeEntryController@saveDay')->where('jobs_id', '[0-9]+');

    /*Route::get('/reportico/mode/execute', 'ModeController@execute');
    Route::get('/reportico/mode/prepare', 'ModeController@prepare');
    Route::get('/reportico/mode/admin', 'ModeController@admin');
    Route::get('/reportico/mode/menu', 'ModeController@menu');
    Route::get('/reportico', 'ReporticoController@reportico');
    Route::get('/reportico/graph', 'ReporticoController@graph');
    Route::get('/reportico/dbimage', 'ReporticoController@dbimage');
    Route::get('/reportico/ajax', 'ReporticoController@ajax');*/
});

//Must be a linx staff member.
Route::group(['middleware' => 'auth.linx'], function() {
    //Staff
    Route::get('staff', 'UserController@showList');
    Route::get('staff/filter', 'UserController@jsonList');
    Route::get('staff/json', 'UserController@simpleSearch');
    Route::post('staff/approve', 'UserController@approveUser');
    Route::get('staff/add', 'UserController@addUser');
    Route::get('staff/myob/{id}', 'UserController@createInMYOBWrapper')->where('id', '[0-9]+');
    Route::get('profile/{id}', 'UserController@showProfile')->where('id', '[0-9]+');
    Route::get('profile/{id}/print', 'UserController@printProfile')->where('id', '[0-9]+');
    Route::get('profile/{id}/approve', 'UserController@addToJobs')->where('id', '[0-9]+');
    Route::get('profile/delete/{id}', 'UserController@deleteProfile')->where('id', '[0-9]+');
    Route::get('profile/refresh/{id}', 'UserController@refreshProfile')->where('id', '[0-9]+');
    Route::get('profile/{id}/notes', 'UserController@getNotes')->where('id', '[0-9]+');
    Route::post('profile/notes', 'UserController@saveNotes');

    //Clients
    Route::get('client', 'ClientController@showList');
    Route::get('client/add', 'ClientController@showProfile');
    Route::get('client/{id}', 'ClientController@showProfile')->where('id', '[0-9]+');
    Route::get('client/{id}/contacts', 'ClientController@showContacts')->where('id', '[0-9]+');
    Route::get('client/delete/{id}', 'ClientController@deleteProfile')->where('id', '[0-9]+');
    Route::post('client', 'ClientController@saveProfile');

    Route::post('client/{client_id}/contact/{user_id}', 'ClientController@addContact')->where('client_id', '[0-9]+')->where('user_id', '[0-9]+');
    Route::get('client/contact/{id}/supervisor', 'ClientController@contactSupervisor');
    Route::get('client/contact/{id}/manager', 'ClientController@contactManager');
    Route::get('client/contact/{id}/delete', 'ClientController@deleteContact');

    //Bookings - note, plural for admin
    Route::get('kingsley/bookings', 'BookingController@showList');
    Route::get('kingsley/bookings/{id}', 'BookingController@showAdminForm')->where('id', '[0-9]+');
    Route::post('kingsley/bookings', 'BookingController@saveAdminForm');
    Route::get('kingsley/bookings/delete/{id}', 'BookingController@deleteBooking')->where('id', '[0-9]+');
    Route::get('kingsley/bookings/calendar', 'BookingController@bookingCalendar');

    //JSON-returning routes for the list of available beds and rooms
    Route::get('kingsley/rooms/available/{datestart}/{datefinish}', 'Kingsley\RoomsController@getAvailableRooms');
    Route::get('kingsley/beds/available/{room_id}/{datestart}/{datefinish}', 'Kingsley\RoomsController@getAvailableBeds');

    //Processing of timesheets
    Route::get('timesheets/process', 'TimesheetsController@processTimesheetForm');
    Route::post('timesheets/process', 'TimesheetsController@markTimesheets');
    Route::get('timesheets/manage', 'TimesheetsController@manageTimesheets');
    Route::get('timesheets/errors', 'TimesheetsController@errorList');
    Route::get('timesheets/errors/{id}', 'TimesheetsController@resolveError')->where('id', '[0-9]+');
    Route::get('timesheets/finishTimes', 'TimesheetsController@finishTimes');
    Route::post('timesheets/finishTimes', 'TimesheetsController@doFinishTimes');

    //Payslip retrieval and sending
    Route::get('payslips/retrieve', 'PayslipsController@retrieve');

    //Invoices
    Route::get('invoices', 'InvoicesController@index');
    Route::get('invoices/{id}', 'InvoicesController@view')->where('id', '[0-9]+');
    Route::get('invoices/json', 'InvoicesController@jsonList');
    Route::get('invoices/create', 'InvoicesController@create');
    Route::post('invoices/create', 'InvoicesController@store');
    Route::get('invoices/createSearch', 'InvoicesController@createSearch');

    Route::get('invoices/print/{id}', 'InvoicesController@printInvoice')->where('id', '[0-9]+');
    Route::get('invoices/email/{id}', 'InvoicesController@email')->where('id', '[0-9]+');
    Route::get('invoices/delete/{id}', 'InvoicesController@delete')->where('id', '[0-9]+');

    Route::get('settings/myob/test/worker/{id}', 'UserController@testCreate')->where('id', '[0-9]+');

    Route::get('visa', 'VisaController@index');
    Route::post('visa/display/{id}', 'VisaController@display');
    Route::post('visa/send/{id}', 'VisaController@send');
    Route::get('visa/{id}', 'VisaController@index');
});

Route::group(['middleware' => 'auth.linx', 'namespace' => 'Settings'], function() {
    //Roles
    Route::get('settings/super', 'SuperController@showList');
    Route::post('settings/super', 'SuperController@saveForm');
    Route::get('settings/super/add', 'SuperController@showForm');
    Route::get('settings/super/{id}', 'SuperController@showForm')->where('id', '[0-9]+');
    Route::get('settings/super/delete/{id}', 'SuperController@delete')->where('id', '[0-9]+');
    Route::get('settings/super/usi/{id}', 'SuperController@showUSI')->where('id', '[0-9]+');
    Route::post('settings/super/usi/{id}', 'SuperController@saveUSI')->where('id', '[0-9]+');

    //Inductions
    Route::get('settings/inductions', 'InductionsController@showList');
    Route::get('settings/inductions/{id}', 'InductionsController@showForm')->where('id', '[0-9]+');
    Route::get('settings/inductions/{id}/questions', 'InductionsController@showQuestionForm')->where('id', '[0-9]+');
    Route::get('settings/inductions/add', 'InductionsController@showForm');
    Route::post('settings/inductions', 'InductionsController@saveForm');
    Route::post('settings/inductions/questions', 'InductionsController@saveQuestion');
    Route::post('settings/inductions/finish', 'InductionsController@saveQuestionForm');
    Route::post('settings/inductions/answers', 'InductionsController@saveAnswer');
    Route::get('settings/inductions/questions/delete/{id}', 'InductionsController@deleteQuestion')->where('id', '[0-9]+');
    Route::get('settings/inductions/answers/delete/{id}', 'InductionsController@deleteAnswer')->where('id', '[0-9]+');
});

Route::group(['middleware' => 'auth.admin'], function() {
    //Impersonate
    Route::get('impersonate/{id}', 'UserController@impersonate')->where('id', '[0-9]+');

    //Report Config
    Route::get('settings/reporting', 'ReportsController@adminIndex');
    Route::get('settings/reporting/{id}', 'ReportsController@adminForm')->where('id', '[0-9]+');
    Route::get('settings/reporting/add', 'ReportsController@adminForm');
    Route::get('settings/reporting/delete/{id}', 'ReportsController@delete')->where('id', '[0-9]+');
    Route::post('settings/reporting', 'ReportsController@adminSave');

	Route::get('retrieveSuper', 'UserController@retrieveSuper');
});

//Super restricted routes for admin users.
Route::group(['middleware' => 'auth.admin', 'namespace' => 'Settings'], function() {
    Route::get('settings', 'SettingsController@dashboard');

    //Credentials
    Route::get('settings/credentials', 'CredentialsController@showList');
    Route::post('settings/credentials', 'CredentialsController@saveForm');
    Route::get('settings/credentials/add', 'CredentialsController@showForm');
    Route::get('settings/credentials/{id}', 'CredentialsController@showForm')->where('id', '[0-9]+');
    Route::get('settings/credentials/delete/{id}', 'CredentialsController@delete')->where('id', '[0-9]+');

    //Pay rates
    Route::get('settings/payrates', 'PayRatesController@showList');
    Route::post('settings/payrates', 'PayRatesController@saveForm');
    Route::get('settings/payrates/add', 'PayRatesController@showForm');
    Route::get('settings/payrates/{id}', 'PayRatesController@showForm')->where('id', '[0-9]+');
    Route::get('settings/payrates/delete/{id}', 'PayRatesController@delete')->where('id', '[0-9]+');

    //Uniforms
    Route::get('settings/uniforms', 'UniformsController@showList');
    Route::post('settings/uniforms', 'UniformsController@saveForm');
    Route::get('settings/uniforms/add', 'UniformsController@showForm');
    Route::get('settings/uniforms/{id}', 'UniformsController@showForm')->where('id', '[0-9]+');
    Route::get('settings/uniforms/delete/{id}', 'UniformsController@delete')->where('id', '[0-9]+');

    //MYOB Integration
    Route::post('settings/myob', 'MYOBController@saveSettings');
    Route::get('settings/myob/delete', 'MYOBController@clearSettings');

    Route::get('settings/myob/employees', 'MYOBController@importEmployees');
    Route::get('settings/myob/customers', 'MYOBController@listClients');

    Route::get('settings/myob/employee/{id}', 'MYOBController@getGUID');
    Route::get('settings/myob/client/{id}', 'MYOBController@getGUIDClient');

    Route::get('settings/myob/import', 'MYOBController@importForm');

    Route::get('settings/help', 'HelpController@showHelpList');
    Route::get('settings/help/{id}', 'HelpController@showHelpForm')->where('id', '[0-9]+');
    Route::get('settings/help/delete/{id}', 'HelpController@delete')->where('id', '[0-9]+');
    Route::post('settings/help', 'HelpController@saveHelpForm');

    //Charge Rates
    Route::get('settings/charge_rates', 'ChargeRatesController@index');
    Route::get('settings/charge_rates/add', 'ChargeRatesController@addForm');
    Route::get('settings/charge_rates/{id}', 'ChargeRatesController@editForm')->where('id', '[0-9]+');
    Route::get('settings/charge_rates/delete/{id}', 'ChargeRatesController@delete')->where('id', '[0-9]+');
    Route::get('settings/charge_rates/test', 'ChargeRatesController@test');
    Route::post('settings/charge_rates', 'ChargeRatesController@save');


    Route::get('settings/myob/bank', 'MYOBController@bankDetails');

    Route::get('settings/myob/logs', 'MYOBLogController@getList');
    Route::get('settings/myob/logs/delete', 'MYOBLogController@purge');

    Route::get('settings/campaigns', 'CampaignsController@showList');
    Route::get('settings/campaigns/{id}', 'CampaignsController@getForm')->where('id', '[0-9]+');
    Route::get('settings/campaigns/add', 'CampaignsController@add');
    Route::get('settings/campaigns/delete/{id}', 'CampaignsController@delete')->where('id', '[0-9]+');
    Route::post('settings/campaigns', 'CampaignsController@saveForm');

    Route::get('settings/campaigns/{id}/questions', 'CampaignsController@questionList')->where('id', '[0-9]+');
    Route::get('settings/campaigns/{camp_id}/questions/{id}', 'CampaignsController@questionForm')->where('id', '[0-9]+')->where('camp_id', '[0-9]+');
    Route::get('settings/campaigns/{camp_id}/questions/add', 'CampaignsController@addQuestion')->where('camp_id', '[0-9]+');

    Route::get('settings/years', 'YearsController@getYears');
    Route::get('settings/years/{id}', 'YearsController@getYear')->where('id', '[0-9]+');
    Route::get('settings/years/add', 'YearsController@getYear');
    Route::post('settings/years', 'YearsController@saveYear');
});

/*
Route::group(['middleware' => 'auth.admin', 'namespace' => 'Kingsley'], function() {
    Route::get('kingsley', 'KingsleyController@dashboard');

    Route::get('kingsley/rooms', 'RoomsController@showList');
    Route::post('kingsley/rooms', 'RoomsController@saveForm');
    Route::get('kingsley/rooms/add', 'RoomsController@showForm');
    Route::get('kingsley/rooms/{id}', 'RoomsController@showForm')->where('id', '[0-9]+');
    Route::get('kingsley/rooms/delete/{id}', 'RoomsController@delete')->where('id', '[0-9]+');

    Route::get('kingsley/roomtypes', 'RoomTypesController@showList');
    Route::post('kingsley/roomtypes', 'RoomTypesController@saveForm');
    Route::get('kingsley/roomtypes/add', 'RoomTypesController@showForm');
    Route::get('kingsley/roomtypes/{id}', 'RoomTypesController@showForm')->where('id', '[0-9]+');
    Route::get('kingsley/roomtypes/delete/{id}', 'RoomTypesController@delete')->where('id', '[0-9]+');
});
*/