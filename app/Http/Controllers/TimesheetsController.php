<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Response;
use Storage;
use View;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Settings\MYOBController;
use App\Http\Controllers\NotificationsController;

class TimesheetsController extends Controller {
    public function manageTimesheets() {
        return view('timesheets.dashboard');
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function getShifts()
    {
        $user = Auth::user();

        $shiftarray = DB::select('select distinct shifts.start_time, shifts.end_time, shifts.id as shift_id, timesheets.time_start, timesheets.time_end, job_worklist.user_id, timesheets.time_id, jobs.name as job_name, timesheets.approved_user, timesheets.approved_super, timesheets.payment_type from shifts inner join jobs on jobs.id = shifts.jobs_id inner join job_worklist on job_worklist.jobs_id = shifts.jobs_id AND shifts.start_time >= job_worklist.startdate AND (shifts.end_time <= job_worklist.enddate OR job_worklist.enddate is null) AND job_worklist.user_id = '.$user->id.' left join timesheets on timesheets.shift_id = shifts.id and timesheets.user_id = '.$user->id.' where (timesheets.approved_user is null or timesheets.approved_user = 0) and (timesheets.processed is null or timesheets.processed = 0) and shifts.start_time >= \''.date('Y-m-d H:i:s', strtotime('- 10 days', strtotime('tomorrow'))).'\' and shifts.start_time < \''.date('Y-m-d H:i:s', strtotime('+ 1 hour')).'\' order by shifts.start_time asc, shifts.end_time asc');

        $shifts = array();
        foreach($shiftarray as $shift) {
            $rates = DB::table('shift_tasks')
                ->join('tasks', 'tasks.task_id', '=', 'shift_tasks.task_id')
                ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
                ->where('shift_id', '=', $shift->shift_id)
                ->where('rate_type', '=', 1)
                ->count();
            $shift->rates = $rates;
            $shifts[] = $shift;
        }

        if($user->is_supervisor == 0 && $user->level < 2) {
            //Not a supervisor, just show them all their shifts that are in the past and not processed
            return view('timesheets.user', ['shifts' => $shifts]);
        } else {
            //A supervisor, show list of shifts
            $superQ = 'select distinct shifts.start_time, shifts.end_time, shifts.id as shift_id, jobs.name as job_name, clients.name as client_name 
from shifts 
inner join jobs on jobs.id = shifts.jobs_id 
inner join clients on clients.id = jobs.client_id
inner join job_worklist on job_worklist.jobs_id = shifts.jobs_id 
AND shifts.start_time >= job_worklist.startdate 
AND (shifts.end_time <= job_worklist.enddate OR job_worklist.enddate is null)
left join timesheets on timesheets.shift_id = shifts.id and timesheets.user_id = job_worklist.user_id 
where (timesheets.processed is null or timesheets.processed = 0)
and ((SELECT level FROM users WHERE id = '.$user->id.') > 1)
and shifts.start_time < \''.date('Y-m-d H:i:s', strtotime('+ 1 hour')).'\' 
and shifts.start_time > \''.date('Y-m-d H:i:s', strtotime('- 10 days')).'\' 
order by shifts.start_time asc, shifts.end_time asc';

            $super = DB::select($superQ);

            //probably need to process shifts here to see if there are outstanding timesheets.
            $shiftArray = array();
            foreach($super as $shift) {
                $times = DB::table('timesheets')
                    ->where('shift_id', '=', $shift->shift_id)
                    ->where('approved_user', '=', 1)
                    ->count();

                $workers = count(DB::select('select * from shifts 
inner join job_worklist on job_worklist.jobs_id = shifts.jobs_id AND
shifts.start_time >= job_worklist.startdate AND
(shifts.end_time <= job_worklist.enddate OR
job_worklist.enddate is null) 
where ((SELECT level FROM users WHERE id = '.$user->id.') > 1) and shifts.id = '.$shift->shift_id));

                if($times == 0) {
                    //none are entered
                    $shift->status = '<i class="fa fa-ban fa-2x text-danger" data-toggle="tooltip" data-placement="bottom" title="No times entered"></i>';
                    $shift->canCancel = true;
                } elseif ($times == $workers) {
                    //all are approved
                    $shift->status = '<i class="fa fa-check fa-2x text-success" data-toggle="tooltip" data-placement="bottom" title="All times entered"></i>';
                    $shift->canCancel = false;
                } else {
                    //some are approved/disputed.
                    $shift->status = '<i class="fa fa-exclamation-triangle fa-2x text-warning" data-toggle="tooltip" data-placement="bottom" title="Oustanding entries"></i>';
                    $shift->canCancel = true;
                }

                $shiftArray[$shift->client_name][$shift->job_name][] = $shift;
            }

            return view('timesheets.super', ['shifts' => $shifts, 'super' => $shiftArray, 'mode' => 'You are currently viewing un-processed timesheets.']);
        }
    }

    public function getShiftHistory() {
        $user = Auth::user();

        $super = DB::table('shifts')
            ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
            ->join('job_worklist', function($join) use ($user) {
                $join->on('job_worklist.jobs_id', '=', 'shifts.jobs_id');
                $join->where('job_worklist.user_id', '=', $user->id);
            })
            ->join('timesheets', 'timesheets.shift_id', '=', 'shifts.id')
            ->where('shifts.end_time', '<', date('Y-m-d H:i:s'))
            ->where('shifts.end_time', '>', date('Y-m-d H:i:s', strtotime('28 days ago')))
            ->whereNotNull('timesheets.signed_out')
            ->select('shifts.start_time', 'shifts.end_time', 'shifts.id as shift_id', 'jobs.name as job_name', 'timesheets.payment_type')
            ->distinct()
            ->orderBy('shifts.start_time', 'desc')
            ->orderBy('jobs.name', 'asc')
            ->get();

        //probably need to process shifts here to see if there are outstanding timesheets.
        $shiftArray = array();
        foreach($super as $shift) {
            $shift->status = '<i class="fa fa-history fa-2x" data-toggle="tooltip" data-placement="bottom" title="Historical entry only"></i>';
            $shiftArray[] = $shift;
        }

        return view('timesheets.super', ['shifts' => array(), 'super' => $shiftArray, 'mode' => 'You are currently viewing timesheet history.']);
    }

    private function checkSupervisor($shift_id) {
        $user = Auth::user();

        if($user->level == 3) {
            return true;
        }

        if($user->is_supervisor == 0) {
            return false;
        }

        $superQ = DB::table('job_worklist')
            ->join('shifts', 'shifts.jobs_id', '=', 'job_worklist.jobs_id')
            ->where('shifts.id', '=', $shift_id)
            ->select('job_superlist.user_id')
            ->get();

        $supers = array();
        foreach($superQ as $sq) {
            $supers[] = $sq->supervisor_id;
        }

        if(!in_array($user->id, $supers)) {
            return false;
        }

        return true;
    }

    public function updateDetails(Request $request) {
        $user = Auth::user();

        $shiftQ = DB::table('timesheets')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
            ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
            ->join('job_worklist', function($join) {
                $join->on('job_worklist.jobs_id', '=', 'shifts.jobs_id')
                    ->on('job_worklist.user_id', '=', 'timesheets.user_id');
            })
            ->where('timesheets.time_id', '=', $request->time_id)
            ->select('timesheets.shift_id', 'job_worklist.user_id',
                'shifts.start_time', 'shifts.end_time', 'shifts.break_duration',
                'jobs.name as job_name', 'timesheets.time_id', 'timesheets.payment_type',
                'timesheets.time_notes', 'timesheets.approved_user', 'timesheets.approved_super', 'timesheets.signed_out')
            ->get();

        if(count($shiftQ) > 0) {
            $sheet = $shiftQ[0];
            $supervisor_id = $shiftQ[0]->supervisor_id;
            $user_id = $shiftQ[0]->user_id;
        } else {
            return view('errors.user', ['error_message' => 'Timesheet not found']);
        }
        if($user->id != $user_id && $user->id != $supervisor_id && $user->level < 2) {
            return view('errors.access', ['message' => 'This is not your timesheet']);
        }

        //This is used in notes, when one is left.
        $user_name = $user->given_name.' '.$user->surname;

        $notes = $request->old_notes;
        if(strlen($request->notes)) {
            $notes.= '<p><strong>'.$user_name.' ('.date('d/m/Y H:i').'):</strong> '.$request->notes.'</p>';
        }

        $fields = array(
            'signed_out' => date('Y-m-d H:i:s'),
            'time_notes' => $notes,
            'approved_user' => 1,
            'approved_super' => 1
        );

        //update
        DB::table('timesheets')
            ->where('time_id', '=', $request->time_id)
            ->update($fields);

        foreach($request->tita_id as $key => $tita_id) {
            if($user->id == $user_id) {
                $task_fields = array(
                    'tita_start' => date('Y-m-d', strtotime($sheet->start_time)) . ' ' . $request->tita_user_start[$key],
                    'tita_finish' => date('Y-m-d', strtotime($sheet->start_time)) . ' ' . $request->tita_user_finish[$key],
                    'tita_break_duration' => $request->tita_user_break_duration[$key],
                    'tita_quantity' => $request->tita_quantity[$key]
                );
            } else {
                $task_fields = array(
                    'tita_start' => date('Y-m-d', strtotime($sheet->start_time)) . ' ' . $request->tita_start[$key],
                    'tita_finish' => date('Y-m-d', strtotime($sheet->start_time)) . ' ' . $request->tita_finish[$key],
                    'tita_break_duration' => $request->tita_break_duration[$key],
                    'tita_quantity' => $request->tita_quantity[$key]
                );
            }

            $task_fields['signed_in_by'] = $user_id;

            DB::table('time_tasks')
                ->where('tita_id', '=', $tita_id)
                ->update($task_fields);

			$this->copyToHistory($tita_id);
        }

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function acceptDetails(Request $request) {
        $user = Auth::user();
        DB::table('timesheets')
            ->where('time_id', '=', $request->time_id)
            ->update([
                'approved_user' => 1
            ]);

        DB::table('time_tasks')
            ->where('time_id', '=', $request->time_id)
            ->update(['tita_user_finish' => $request->tita_finish]);

        return redirect('/dashboard')->with('success', 'Thank you.  You have signed out for today.');
    }

    public function disputeDetails(Request $request) {
        $user = Auth::user();
        DB::table('timesheets')
            ->where('time_id', '=', $request->time_id)
            ->update(['approved_user' => -1]);

        DB::table('users')
            ->where('id', '=', $user->id)
            ->update(['current_signin' => 0]);

        //TODO: send mail to Linx Admin that x has disputed

        return redirect('/dashboard');
    }

    public function signIn() {
        //Insert
        $signedIn = date('Y-m-d H:i');
        $startTime = date('Y-m-d');
        $user = Auth::user();

        //do they have a timesheet already?
        $sheets = DB::table('timesheets')
            ->where('timesheets.time_id', '=', $user->current_signin)
            ->count();
        if($sheets == 0) {
            $id = DB::table('timesheets')
                ->insertGetId([
                    'user_id' => $user->id,
                    'signed_in' => $signedIn,
                    'time_start' => $startTime
                ]);

            DB::table('users')
                ->where('id', '=', $user->id)
                ->update([
                    'current_signin' => $id,
                    'last_time_id' => $id
                ]);
        } else {
            //They do, which means they are late!
            DB::table('timesheets')
                ->where('time_id', '=', $user->current_signin)
                ->update([
                    'signed_in' => $signedIn,
                    'time_start' => $signedIn
                ]);

            DB::table('time_tasks')
                ->where('time_id', '=', $user->current_signin)
                ->whereNull('tita_finish')
                ->update(['tita_start' => $signedIn]);
        }

        return redirect('dashboard')->with('success', 'You have signed in successfully for today.');
    }

    public function signOutForm() {
        $user = Auth::user();

        $shiftQ = DB::table('timesheets')
            ->join('users', 'users.current_signin', '=', 'timesheets.time_id')
			->join('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
            ->where('users.id', '=', $user->id)
            ->select('timesheets.time_id')
            ->get();

        if(count($shiftQ) == 0) {
            return view('timesheets.usersignouterror');
        }
		$shift = $shiftQ[0];

        $tasks = DB::table('time_tasks')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
			->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
            ->join('users', 'users.current_signin', '=', 'timesheets.time_id')
			->where('timesheets.user_id', '=', $user->id)
            ->select('tita_start', 'tita_finish', 'task_name')
            ->get();

        return view('timesheets.usersignout', ['shift' => $shift, 'tasks' => $tasks]);
    }

    public function signInSuper() {
        $user = Auth::user();
        $tc = new TasksController();
		$id = null;
		if(isset($_GET['id'])) {
			$id = $_GET['id'];
		}
		if($id == null) {
			$jobs = DB::table('job_superlist')
				->join('jobs', 'jobs.id', '=', 'job_superlist.jobs_id')
                ->join('users', 'users.id', '=', 'job_superlist.user_id');

			if($user->level == 3) {
			    //
            } else {
			   $jobs = $jobs->where('user_id', '=', $user->id);
            }

            $jobs = $jobs->whereNull('jobs.end_date')
				->select('jobs.id', 'jobs.name')
                ->distinct()
				->get();

            if (count($jobs) == 0) {
                //try a different query.  They might be a contact
                $jobs = DB::table('jobs')
                    ->join('client_users', 'client_users.client_id', '=', 'jobs.client_id')
                    ->where('client_users.user_id', '=', $user->id)
                    ->whereNull('jobs.end_date')
                    ->select('jobs.id', 'jobs.name')
                    ->distinct()
                    ->get();
            }

            if (count($jobs) == 0) {
				return view('errors.access', ['message' => 'You do not have any jobs to manage']);
			} elseif (count($jobs) == 1) {
				return redirect('timesheets/signinSuper?id=' . $jobs[0]->id);
			} else {
				return view('timesheets.selectSignin', ['jobs' => $jobs]);
			}
		} else {
			$jobs = DB::table('job_superlist')
				->join('jobs', 'jobs.id', '=', 'job_superlist.jobs_id')
                ->join('users', 'users.id', '=', 'job_superlist.user_id');

            if($user->level == 3) {
                //
            } else {
                $jobs = $jobs->where('user_id', '=', $user->id);
            }

            $jobs = $jobs->where('jobs.id', '=', $id)
				->whereNull('jobs.end_date')
				->select('jobs.id')
                ->distinct()
				->get();

            if (count($jobs) == 0) {
                //try a different query.  They might be a contact
                $jobs = DB::table('jobs')
                    ->join('client_users', 'client_users.client_id', '=', 'jobs.client_id')
                    ->where('client_users.user_id', '=', $user->id)
                    ->where('jobs.id', '=', $id)
                    ->select('jobs.id')
                    ->distinct()
                    ->get();
            }

			$job = $jobs[0];
		}

        //We need a list of all workers who are active on this job
        $workersQ = 'select 
          `users`.`id`, `users`.`given_name`, `users`.`surname`, `users`.`external_id`,
          `users`.`preferred`, `users`.`nickname`, `users`.`last_time_id`, 
          `users`.`current_working`, `users`.`current_signin`,`time_tasks`.`tita_id`,
          `job_worklist`.`supervisor_id`, `tasks`.`task_name`, `time_tasks`.`tita_start`,
          `time_tasks`.`tita_finish`, `time_tasks`.`tita_user_finish`, `tasks`.`task_id` 
          from `job_worklist` 
          inner join `users` on `users`.`id` = `job_worklist`.`user_id` 
          left join `time_tasks` on `time_tasks`.`time_id` = `users`.`last_time_id` 
          left join `tasks` on `tasks`.`task_id` = `time_tasks`.`task_id` 
          where `job_worklist`.`jobs_id` = '.$job->id.' 
          and `job_worklist`.`startdate` <= \''.date('Y-m-d').'\' 
          and (`job_worklist`.`enddate` >= \''.date('Y-m-d', strtotime('tomorrow')).'\' or `job_worklist`.`enddate` is null)';
        //die($workersQ);

        $workers = DB::select($workersQ);

        //Then split into three - available, working, not signed in
        $notSignedIn = array();
        $working = array();
        $available = array();
        foreach($workers as $worker) {
            //Get timesheet information
            if($worker->current_signin == 0 && $worker->current_working == 0) {
                //Haven't signed in
                $notSignedIn[$worker->id] = $worker;
            } else {
                if($worker->current_signin == 0 && $worker->last_time_id > 0) {
                    //they have signed out, but the supervisor hasn't signed them out
                    $working[$worker->id] = $worker;
                } else {
                    if ($worker->current_working == 0) {
                        $available[$worker->id] = $worker;
                    } elseif ($worker->current_working == 1) {
                        $working[$worker->id] = $worker;
                    }
                }
            }
        }

        //Also need the tasks
        $tasks = $tc->getTopLevelForJob($job->id);

        /*echo '<pre>';
        var_dump($working);
        var_dump($available);
        var_dump($notSignedIn);
        die();*/

        $roles = DB::table('roles')
			->where('roles.jobs_id', '=', $job->id)
			->select('roles.role_id', 'roles.role_name', 'roles.role_default')
			->orderBy('roles.role_name')
			->get();

        return view('timesheets.superform', [
            'tasks' => $tasks,
			'roles' => $roles,
            'working' => $working,
            'available' => $available,
            'unsigned' => $notSignedIn,
			'jobs_id' => $job->id
        ]);
    }

    public function saveSuper(Request $request) {
        //we know the start and end times
		$jobs_id = $request->jobs_id;
		//echo '<h1>Job: '.$jobs_id.'</h1>';
        if(isset($request->available)) {
            foreach($request->available as $available) {
                //echo '<pre>'; var_dump($available); echo '</pre>';
                //create timetask entry w/ start time
                DB::table('timesheets')
                    ->where('time_id', '=', $available)
                    ->update([
                        'jobs_id' => $request->jobs_id
                    ]);

                $times = DB::table('timesheets')
                    ->where('time_id', '=', $available)
                    ->select('time_start', 'user_id')
                    ->get();

                $time = $times[0];
                //echo '<pre>'; var_dump($time); echo '</pre>';
                $start_time = date('Y-m-d', strtotime($time->time_start)).' '.$request->start_time;

                $superQ = DB::table('job_worklist')
					->join('timesheets', 'timesheets.user_id', '=', 'job_worklist.user_id')
					->where('job_worklist.jobs_id', '=', $jobs_id)
					->where('timesheets.time_id', '=', $available)
					->select('job_worklist.supervisor_id');

                //echo $superQ->toSql();
                $supers = $superQ->get();

                $super_id = 0;
                if(count($supers) > 0) {
                	$super = $supers[(count($supers) - 1)];
                	//echo '<pre>'; var_dump($super); echo '</pre><br />';
                	$super_id = $super->supervisor_id;
                	//die('super: '.$super_id);
				}

                $rate_id = 0;

                //work out the rate id and save it.
                $rates = DB::table('roles')
                    ->join('payrates', 'payrates.id', '=', 'roles.rate_id')
                    ->where('roles.role_id', '=', $request->role_id)
                    ->get();

                if(count($rates) > 0) {
                    $rate = $rates[0];
                    $rate_id = $rate->rate_id;
                    if($rate->rate_parent_id != null) {
                        $jws = DB::table('job_worklist')
                            ->where('user_id', '=', $time->user_id)
                            ->where('jobs_id', '=', $request->jobs_id)
                            ->where('startdate', '>', '2000-01-01')
                            ->orderBy('startdate')
                            ->get();
                        if(count($jws) > 0) {
                            $sdate = strtotime($jws[0]->startdate);
                            $fdate = strtotime(date ('Y-m-d H:i', strtotime($time->time_start)));

                            $diff = floor(($fdate - $sdate) / (60 * 60 * 24));

                            if($diff > $rate->rate_days) {
                                $rate_id = $rate->rate_parent_id;
                            }
                        }
                    }
                }

                $tita_id = DB::table('time_tasks')
                    ->insertGetId([
                        'time_id' => $available,
                        'task_id' => $request->task_id,
                        'role_id' => $request->role_id,
                        'rate_id' => $rate_id,
                        'tita_start' => $start_time,
						'supervisor_id' => $super_id,
                        'public_holiday' => isset($request->public_holiday),
						'signed_in_by' => Auth::user()->id
                    ]);

				$this->copyToHistory($tita_id);

                DB::table('users')
                    ->where('current_signin', '=', $available)
                    ->update(['current_working' => 1]);
            }
        }

        if(isset($request->working)) {
            foreach($request->working as $working) {
                //update timetask entry w/ end time
                $times = DB::table('time_tasks')
                    ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
                    ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                    ->where('tita_id', '=', $working)
                    ->select('tita_start', 'time_tasks.time_id', 'public_holiday', 'time_tasks.role_id', 'timesheets.user_id', 'time_tasks.task_id')
                    ->get();
                if (count($times) > 0) {
                    $time = $times[0];
                    $start_time = date('Y-m-d H:i', strtotime($time->tita_start));
                    $end_time = date('Y-m-d', strtotime($time->tita_start)) . ' ' . $request->end_time;

                    $difference = abs((strtotime($end_time) - strtotime($time->tita_start)) / 3600.0);
                    $break_duration = 0;
                    if (!isset($request->lunch_break) && $difference > 5.0) {
                        $break_duration = 30;
                        $difference -= 0.5;
                    }

                    //work out the rate id and save it.
                    $rates = DB::table('roles')
                        ->join('payrates', 'payrates.id', '=', 'roles.rate_id')
                        ->where('roles.role_id', '=', $time->role_id)
                        ->get();

                    $rate_id = null;
                    if(count($rates) > 0) {
                        $rate = $rates[0];
                        $rate_id = $rate->rate_id;
                        if($rate->rate_parent_id != null) {
                            $jws = DB::table('job_worklist')
                                ->where('user_id', '=', $time->user_id)
                                ->where('jobs_id', '=', $request->jobs_id)
                                ->where('startdate', '>', '2000-01-01')
                                ->orderBy('startdate')
                                ->get();
                            if(count($jws) > 0) {
                                $sdate = strtotime($jws[0]->startdate);
                                $fdate = strtotime(date ('Y-m-d H:i', strtotime($time->tita_start)));

                                $diff = floor(($fdate - $sdate) / (60 * 60 * 24));

                                if($diff > $rate->rate_days) {
                                    $rate_id = $rate->rate_parent_id;
                                }
                            }
                        }
                    }

                    //die($difference);

                    $tita_data = array(
                        'tita_break_duration' => $break_duration,
                        'tita_quantity' => $difference
                    );
                    if (strcmp($request->end_time, '00:00') != 0) {
                        $tita_data['tita_finish'] = $end_time;
                    }

                    //work out some hours
                    if ($time->public_holiday == 1) {
                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '=', 6)
                            ->select('paru_hours', 'paru_frequency')
                            ->get();
                        $rule = 0;
                        if (count($rules) > 0) {
                            $rule = $rules[0]->paru_hours;
                        }
                        $tita_data['hours_publicholiday'] = max($difference, $rule);
                        $tita_data['tita_quantity'] = $tita_data['hours_publicholiday'];
                    } elseif (date('N', strtotime($time->tita_start)) == 6) {
                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '=', 4)
                            ->select('paru_hours', 'paru_frequency')
                            ->get();
                        $rule = 0;
                        if (count($rules) > 0) {
                            $rule = $rules[0]->paru_hours;
                        }
                        //how many days have they worked this week?
                        $days = DB::table('time_tasks')
                            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                            ->where('timesheets.user_id', '=', $time->user_id)
                            ->where('time_tasks.tita_start', '>=', date('Y-m-d', strtotime('last monday ' . $time->tita_start)))
                            ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($time->tita_start)))
                            ->select(DB::raw('DATE(time_tasks.tita_start)'))
                            ->distinct()
                            ->count();
                        //echo $time['user_id'] . ' days: ' . $days . '; rule: ' . $rule . ';';
                        if ($days >= $rule) {
                            //They get overtime
                            $tita_data['hours_saturday'] = $difference;
                        } else {
                            $tita_data['hours_ordinary'] = $difference;
                        }
                    } elseif (date('N', strtotime($time->tita_start)) == 7) {
                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '=', 5)
                            ->select('paru_hours', 'paru_frequency')
                            ->get();
                        $rule = 0;
                        if (count($rules) > 0) {
                            $rule = $rules[0]->paru_hours;
                        }
                        $tita_data['hours_sunday'] = max($difference, $rule);
                        $tita_data['tita_quantity'] = $tita_data['hours_sunday'];
                    } else {
                        //Have they exceed their daily, weekly or monthly allocation?
                        $daily = 10000;
                        $weekly = 10000;
                        $monthly = 10000;

                        $tita_data['hours_ordinary'] = $difference;

                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '<', 4)
                            ->select('paru_hours', 'paru_frequency')
                            ->get();

                        foreach ($rules as $rule) {
                            switch ($rule->paru_frequency) {
                                case 0:
                                    $daily = $rule->paru_hours;
                                    if ($difference > $daily) {
                                        $tita_data['hours_ordinary'] = $daily;
                                        $tita_data['hours_overtime'] = $difference - $daily;
                                    }
                                    break;
                                case 1:
                                    $weekly = $rule->paru_hours;
                                    $days = DB::table('time_tasks')
                                        ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                                        ->where('timesheets.user_id', '=', $time->user_id)
                                        ->where('time_tasks.tita_start', '>=', date('Y-m-d', strtotime('last monday ' . $time->tita_start)))
                                        ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($time->tita_start)))
                                        ->select(DB::raw('SUM(hours_ordinary) as tita_quantity'))
                                        ->get();
                                    $hours_this_week = 0;
                                    if ($days) {
                                        $hours_this_week = $days[0]->tita_quantity;
                                    }

                                    if ($hours_this_week >= $weekly) {
                                        $tita_data['hours_ordinary'] = $hours_this_week - $weekly;
                                        $tita_data['hours_overtime'] = ($hours_this_week + $difference) - $weekly;
                                    }
                                    break;
                                case 3:
                                    $monthly = $rule->paru_hours;
                                    $date = DB::table('job_worklist')
                                        ->join('tasks', 'tasks.jobs_id', '=', 'job_worklist.jobs_id')
                                        ->where('user_id', '=', $time->user_id)
                                        ->where('tasks.task_id', '=', $time->task_id)
                                        ->select('job_worklist.startdate')
                                        ->get();

                                    if(count($date) > 0) {
                                        $startdate = $date[0]->startdate;
                                    }
                                    $periods = array();
                                    $startdate = strtotime($startdate);
                                    while($startdate < time()) {
                                        $periods[] = date('Y-m-d', $startdate);
                                        $startdate = strtotime(date('Y-m-d', $startdate).' +28 days');
                                    }

                                    //var_dump($periods);
                                    $date = end($periods);
                                    $days = DB::table('time_tasks')
                                        ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                                        ->where('timesheets.user_id', '=', $time->user_id)
                                        ->where('time_tasks.tita_start', '>=', $date)
                                        ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($time->tita_start)))
                                        ->select(DB::raw('SUM(hours_ordinary) as tita_quantity'))
                                        ->get();

                                    $tita_data['hours_overtime'] = 0;
                                    $tita_data['hours_ordinary'] = $difference;

                                    if(count($days) > 0) {
                                        $this_month = $days[0]->tita_quantity;

                                        if(($this_month + $difference) >= $monthly) {
                                            $tita_data['hours_ordinary'] = $monthly - $this_month;
                                            $tita_data['hours_overtime'] = ($this_month + $difference) - $monthly;
                                        }
                                    }
                                    break;
                            }
                        }
                    }

                    $tita_data['signed_in_by'] = Auth::user()->id;

                    DB::table('time_tasks')
                        ->where('tita_id', '=', $working)
                        ->update($tita_data);

                    $this->copyToHistory($working);

                    if (isset($request->more_work)) {
                        DB::table('users')
                            ->where('current_signin', '=', $time->time_id)
                            ->orWhere('last_time_id', '=', $time->time_id)
                            ->update(['current_working' => 0]);
                    } else {
                        DB::table('users')
                            ->where('current_signin', '=', $time->time_id)
                            ->orWhere('last_time_id', '=', $time->time_id)
                            ->update([
                                'current_working' => 2,
                                'last_time_id' => 0
                            ]);

                        $data = array(
                            'approved_super' => 1,
                            'time_start' => $time->tita_start
                        );
                        if (strcmp($request->end_time, '00:00') != 0) {
                            $data['time_end'] = $end_time;
                        }
                        DB::table('timesheets')
                            ->where('time_id', '=', $time->time_id)
                            ->update($data);
                    }
                }
            }
        }

        if(isset($request->unsigned)) {
            foreach($request->unsigned as $unsigned) {
                //echo '<pre>'; var_dump($unsigned); echo '</pre>';
            }
        }

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function signoutWorker($id) {
        $userQ = DB::table('users')
            ->where('id', '=', $id);

        $users = $userQ->select('current_signin')
            ->get();
        $user = $users[0];
        DB::table('tita_history')
			->where('time_id', '=', $user->current_signin)
			->delete();

        DB::table('time_tasks')
            ->where('time_id', '=', $user->current_signin)
            ->delete();

        DB::table('timesheets')
            ->where('time_id', '=', $user->current_signin)
            ->delete();

        $userQ->update(['current_signin' => 0]);
        DB::table('user_absence')
            ->insert([
                'user_id' => $id,
                'timestamp' => date('Y-m-d H:i:s'),
                'absence_type' => 0,
                'reason' => 'Incorrectly signed in.'
            ]);

        return redirect('/timesheets/signinSuper');
    }

    public function signinWorker($id) {
        $signedIn = date('Y-m-d H:i:s');
        $startTime = date('Y-m-d');
        $user_id = $id;

        $id = DB::table('timesheets')
            ->insertGetId([
                'user_id' => $user_id,
                'signed_in' => $signedIn,
                'time_start' => $startTime
            ]);

        //echo $id.' '.$user_id;

        DB::table('users')
            ->where('id', '=', $user_id)
            ->update([
                'current_signin' => $id,
                'last_time_id' => $id
            ]);

        return redirect($_SERVER['HTTP_REFERER'])->with('success', 'The worker has been signed in successfully.');
    }

    public function notrequiredWorker($id) {
        DB::table('users')
            ->where('id', '=', $id)
            ->update([
                'current_working' => 2,
                'last_time_id' => 0
            ]);

        DB::table('user_absence')
            ->insert([
                'user_id' => $id,
                'timestamp' => date('Y-m-d H:i:s'),
                'absence_type' => 1,
                'reason' => 'Not required for this shift.'
            ]);

        return redirect('/timesheets/signinSuper');
    }

    public function sickWorker($id) {
        DB::table('users')
            ->where('id', '=', $id)
            ->update([
                'current_working' => 2,
                'last_time_id' => 0
            ]);

        DB::table('user_absence')
            ->insert([
                'user_id' => $id,
                'timestamp' => date('Y-m-d H:i:s'),
                'absence_type' => 2,
                'reason' => 'Sickness.'
            ]);

        return redirect('/timesheets/signinSuper');
    }

    public function approveForm($id) {
        $user = Auth::user();

        $shiftQ = DB::table('timesheets')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
            ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
            ->join('job_worklist', function($join) {
                $join->on('job_worklist.jobs_id', '=', 'shifts.jobs_id')
                    ->on('job_worklist.user_id', '=', 'timesheets.user_id');
            })
            ->where('timesheets.time_id', '=', $id)
            ->select('timesheets.shift_id', 'job_worklist.user_id',
                'shifts.start_time', 'shifts.end_time', 'shifts.break_duration',
                'jobs.name as job_name', 'timesheets.time_id', 'timesheets.payment_type',
                'timesheets.time_notes', 'timesheets.approved_user', 'timesheets.approved_super', 'timesheets.signed_out')
            ->get();

        if(count($shiftQ) > 0) {
            $sheet = $shiftQ[0];
            $supervisor_id = $shiftQ[0]->supervisor_id;
            $user_id = $shiftQ[0]->user_id;
        } else {
            return view('errors.user', ['error_message' => 'Timesheet not found']);
        }
        if($user->id != $user_id && $user->id != $supervisor_id) {
            return view('errors.access', ['message' => 'This is not your timesheet']);
        }

        $taskQ = DB::table('tasks')
            ->join('time_tasks', 'time_tasks.task_id', '=', 'tasks.task_id')
            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
            ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->join('shift_tasks', function($join) {
                $join->on('shift_tasks.task_id', '=', 'tasks.task_id')
                    ->on('shift_tasks.shift_id', '=', 'timesheets.shift_id');
            })
            ->where('time_tasks.time_id', '=', $id)
            ->select('tasks.*', 'payrates.rate_type', 'time_tasks.*')
            ->get();

        $tasks = array();
        foreach($taskQ as $task) {
            $tasks[$task->role_name][] = $task;
        }

        return view('timesheets.usersapprove', ['shift' => $sheet, 'tasks' => $tasks]);
    }

    public function processTimesheetForm() {
        //show the timesheets to be processed
        $shiftQ = DB::table('timesheets')
            ->join('users', 'users.id', '=', 'timesheets.user_id')
            ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
            ->join('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
            ->join('jobs', 'jobs.id', '=', 'tasks.jobs_id')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->where('timesheets.processed', '<', 1)
            ->whereNotNull('time_tasks.tita_finish')
            ->where('time_tasks.tita_quantity', '>', 0)
            ->select('clients.name as client_name', 'jobs.name as job_name', 'timesheets.time_id', 'time_tasks.tita_start as time_start', 'job_worklist.user_id', 'timesheets.approved_user', 'timesheets.approved_super', 'users.surname', 'users.given_name', 'users.external_id', 'users.myob_uid', 'timesheets.time_id', 'tasks.rate_id', 'timesheets.signed_out', 'timesheets.processed', 'users.myob_uid')
            ->distinct()
            ->orderBy('clients.name', 'asc')
            ->orderBy('jobs.name', 'asc')
            ->orderBy('time_tasks.tita_start')
            ->orderBy(DB::raw('RIGHT(users.external_id, 5)'));

        $shifts = $shiftQ->get();
        $errors = array();
        foreach($shifts as $shift) {
            //check for errors here
            if(strlen($shift->myob_uid) == 0) {
                $string = 'Staff member '.$shift->external_id.' is not in MYOB';
                if(!in_array($string, $errors)) {
                    $errors[] = $string;
                }
            }

            /*$rates = DB::table('payrates')
                ->where('id', '=', $shift->rate_id)
                ->get();
            if(count($rates) > 0){
                $rate = $rates[0];
                if(strlen($rate->rate_guid) == 0) {
                    $string = 'Pay rate '.$rate->name.' is not in MYOB';
                    if(!in_array($string, $errors)) {
                        $errors[] = $string;
                    }
                }
            }

            $rates = DB::table('time_payrates')
                ->join('shift_payrates', 'shift_payrates.shpa_id', '=', 'time_payrates.shpa_id')
                ->join('payrates', 'payrates.id', '=', 'shift_payrates.rate_id')
                ->where('time_id', '=', $shift->time_id)
                ->get();
            foreach($rates as $rate) {
                if (strlen($rate->rate_guid) == 0) {
                    $string = 'Pay rate ' . $rate->name . ' is not in MYOB';
                    if (!in_array($string, $errors)) {
                        $errors[] = $string;
                    }
                }
            }*/
        }

		$user = Auth::user();
        return view('timesheets.process', ['shifts' => $shifts, 'errs' => $errors, 'user' => $user]);
    }

    public function deletePayrate($shpa_id) {
        $shiftQ = DB::table('shift_payrates')
            ->where('shpa_id', '=', $shpa_id)
            ->select('shift_id')
            ->get();
        $shift_id = $shiftQ[0]->shift_id;

        DB::table('shift_payrates')
            ->where('shpa_id', '=', $shpa_id)
            ->delete();

        return redirect('timesheets/shift/'.$shift_id);
    }

    public function deleteShift($shift_id) {
        DB::table('shifts')
            ->where('id', '=', $shift_id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function deleteTimesheet($time_id) {
        DB::table('timesheets')
            ->where('time_id', '=', $time_id)
            ->delete();

        DB::table('users')
            ->where('current_signin', '=', $time_id)
            ->update(['current_signin' => 0, 'current_working' => 0]);

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function deleteTimetask($tita_id) {
        DB::table('time_tasks')
            ->where('tita_id', '=', $tita_id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function markTimesheets(Request $request) {
        if(is_array($request->time_id)) {
			foreach ($request->time_id as $time_id) {
				DB::table('timesheets')
					->where('time_id', '=', $time_id)
					->update(['processed' => -1]);
			}
		}

		if(is_array($request->done_id)) {
			foreach ($request->done_id as $time_id) {
				DB::table('timesheets')
					->where('time_id', '=', $time_id)
					->update(['processed' => 1]);
			}
		}

        return redirect('/timesheets/process')->with('success', 'The selected timesheets have been queued for processing, or marked as complete, as appropriate.');
    }

    public function preparePayslips() {
        /*$mc = new MYOBController();
        $report = $mc->getReport(array());
        foreach($report->Items as $payslip) {
            //this should run as a separate process to set up each payslip and send it
            $clients = DB::table('users')
                ->where('myob_uid', '=', $payslip->Employee->UID)
                ->get();
            if(count($clients) > 0) {
                $client = $clients[0];
                $linesArray = array();

                foreach($payslip->Lines as $line) {
                    //echo '<pre>'; var_dump($line); echo '</pre>';
                    $shifts = array();
                    if(strcmp($line->PayrollCategory->Name, "Base Rate") != 0) {
                        if($line->PayrollCategory->Type == "Wage") {
                            if ($line->PayrollCategory->Name == "Contract Rate") {
                                $line->CalculationRate = null;
                                $line->Hours = null;

                                //get contract shifts
                                $shifts = DB::table('timesheets')
                                    ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
                                    ->join('shift_payrates', 'shift_payrates.shift_id', '=', 'shifts.id')
                                    ->join('payrates', 'payrates.id', '=', 'shift_payrates.rate_id')
                                    ->join('time_payrates', function ($join) {
                                        $join->on('time_payrates.shpa_id', '=', 'shift_payrates.shpa_id')
                                            ->on('time_payrates.time_id', '=', 'timesheets.time_id');
                                    })
                                    ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
                                    ->join('clients', 'clients.id', '=', 'jobs.client_id')
                                    ->where('time_start', '>=', $payslip->PayPeriodStartDate)
                                    ->where('time_start', '<=', date('Y-m-d', strtotime($payslip->PayPeriodEndDate . ' +1 day')))
                                    ->where('timesheets.user_id', '=', $client->id)
                                    ->where('timesheets.payment_type', '=', 1)
                                    ->where('timesheets.processed', '=', 1)
                                    ->select('clients.name as client_name', 'jobs.name as job_name', 'payrates.name as rate_name', 'shift_payrates.shpa_rate', 'time_payrates.tipa_quantity', 'timesheets.time_start', 'timesheets.payment_type')
                                    ->get();
                            } else {
                                //get hourly shifts
                                $shifts = DB::table('timesheets')
                                    ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
                                    ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
                                    ->join('clients', 'clients.id', '=', 'jobs.client_id')
                                    ->join('payrates', 'payrates.id', '=', 'shifts.rate_id')
                                    ->where('time_start', '>=', $payslip->PayPeriodStartDate)
                                    ->where('time_start', '<=', date('Y-m-d', strtotime($payslip->PayPeriodEndDate . ' +1 day')))
                                    ->where('timesheets.user_id', '=', $client->id)
                                    ->where('timesheets.payment_type', '=', 0)
                                    ->where('payrates.rate_guid', '=', $line->PayrollCategory->UID)
                                    ->where('timesheets.processed', '=', 1)
                                    ->select('clients.name as client_name', 'jobs.name as job_name', 'payrates.name as rate_name', 'timesheets.time_start', 'timesheets.num_units', 'timesheets.payment_type')
                                    ->get();
                            }
                        }

                        //process shifts into a better thing
                        $shiftArray = array();
                        foreach($shifts as $shift) {
                            $shiftArray[$shift->client_name][$shift->job_name][$shift->time_start][] = $shift;
                        }

                        $line->shifts = $shiftArray;

                        $linesArray[] = $line;
                    }
                }

                $view = View::make('timesheets.payslip', ['payslip' => $payslip, 'lines' => $linesArray, 'client' => $client]);
                $contents = $view->render();

                //echo $contents;

                try {
                    //$outfile = '/home/lw_linxemployment/clients/linxstaff/payslips/'.$client->external_id.'_'.date('Y-m-d').'.pdf';
                    $outfile = 'I:\Linx\payslip\\'.$client->external_id.'_'.date('Y-m-d-His').'.pdf';
                    $outname = date('Y-m-d-His').'.pdf';

                    $pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
                    $pdf->pdf->SetDisplayMode('fullpage');
                    $pdf->writeHTML($contents);
                    $pdf->Output($outfile, 'F');

                    //return Response::make(file_get_contents($outfile), 200, [
                    //    'Content-Type' => 'application/pdf',
                    //    'Content-Disposition' => 'inline; '.$outname,
                    //]);
                } catch (\HTML2PDF_exception $ex) {
                    echo $ex.'<br />';
                }
            }
        }*/
    }

    public function assignWorkers() {
        $user = Auth::user();
        $id = null;
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        if($id == null) {
            $jobs = DB::table('job_superlist')
                ->join('jobs', 'jobs.id', '=', 'job_superlist.jobs_id')
                ->join('users', 'users.id', '=', 'job_superlist.user_id');

            if($user->level == 3) {
                //
            } else {
                $jobs = $jobs->where('user_id', '=', $user->id);
            }

            $jobs = $jobs->whereNull('jobs.end_date')
                ->select('jobs.id', 'jobs.name')
                ->distinct()
                ->get();

            if (count($jobs) == 0) {
                //try a different query.  They might be a contact
                $jobs = DB::table('jobs')
                    ->join('client_users', 'client_users.client_id', '=', 'jobs.client_id')
                    ->where('client_users.user_id', '=', $user->id)
                    ->whereNull('jobs.end_date')
                    ->select('jobs.id', 'jobs.name')
                    ->distinct()
                    ->get();
            }

            if (count($jobs) == 0) {
                return view('errors.user', ['error_message' => 'You do not have any jobs to manage']);
            } elseif (count($jobs) == 1) {
                return redirect('timesheets/workers?id=' . $jobs[0]->id);
            } else {
                return view('timesheets.selectAssign', ['jobs' => $jobs]);
            }
        } else {
            $workers = DB::select('select DISTINCT
              `users`.`given_name` as `worker_given`, `users`.`surname` as `worker_surname`, `users`.`external_id`, `users`.`preferred` as `worker_preferred`, `users`.`nickname` as `worker_nickname`, `users`.`id` as `worker_id`, `supervisors`.`given_name` as `super_given`, `supervisors`.`surname` as `super_surname`, `supervisors`.`id` as `super_id` 
              from `job_worklist` 
              inner join `users` on `users`.`id` = `job_worklist`.`user_id` 
              inner join `job_superlist` on `job_superlist`.`jobs_id` = `job_worklist`.`jobs_id` 
              left join `users` as `supervisors` on `supervisors`.`id` = `job_worklist`.`supervisor_id` 
              where `job_worklist`.`jobs_id` = '.$id.'  
              and `job_worklist`.`startdate` <= \''.date('Y-m-d').'\' and (`job_worklist`.`enddate` >= \''.date('Y-m-d', strtotime('tomorrow')).'\' or `job_worklist`.`enddate` is null)');

            $jobs = DB::table('job_superlist')
                ->where('job_superlist.jobs_id', '=', $id)
                ->select('job_superlist.jobs_id')
                ->get();

            if (count($jobs) > 0) {
                $job = $jobs[0];
            } else {
                return view('errors.access', ['message' => 'You aren\'t the supervisor for any jobs.']);
            }

            $supers = DB::table('job_superlist')
                ->join('users', 'users.id', '=', 'job_superlist.user_id')
                ->where('job_superlist.jobs_id', '=', $job->jobs_id)
                ->select('users.given_name', 'users.surname', 'users.id')
                ->get();

            return view('timesheets.assign', ['workers' => $workers, 'supers' => $supers, 'jobs_id' => $job->jobs_id]);
        }
    }

    public function saveAssignWorkers(Request $request) {
        //echo '<pre>'.$request->jobs_id.' '.$request->super_id; var_dump($request->worker_id); var_dump($request->nickname); die('</pre>');
        if(is_array($request->worker_id)) {
			foreach ($request->worker_id as $worker => $blah) {
				//echo 'S '.$worker.' '.$request->jobs_id.' '.$request->super_id.'<br />';
				DB::table('job_worklist')
					->where('user_id', '=', $worker)
					->where('jobs_id', '=', $request->jobs_id)
					->update([
						'supervisor_id' => $request->super_id
					]);
			}
		}

		if(is_array($request->nickname)) {
			foreach ($request->nickname as $worker => $blah) {
				//echo 'N: '.$worker.' '.$blah.'<br />';
				DB::table('users')
					->where('id', '=', $worker)
					->update([
						'nickname' => $blah
					]);
			}
		}

        return redirect('timesheets/workers')->with('success', 'Your shift assignment was successful');
    }

    public function finishTimes() {
        $sheets = DB::select('SELECT
            us.given_name, us.preferred, us.surname, us.external_id,
            tt.tita_id, tt.tita_start, tt.tita_finish, tt.tita_break_duration, tt.tita_quantity, addtime(tt.tita_finish, \'12:00:00\') as new_finish
            
            FROM
            time_tasks tt
            join timesheets ts on ts.time_id = tt.time_id
            join users us on us.id = ts.user_id
            
            WHERE
            tt.tita_start > tt.tita_finish
            OR 
            tt.tita_quantity > 24
            OR 
            (tt.tita_quantity = 0 and tt.tita_finish is not null)');
        //and ts.processed = 0
        return view('timesheets.fix', ['sheets' => $sheets]);
    }

    public function doFinishTimes(Request $request) {
        foreach($request->tita_id as $key=>$tita_id) {
            $tita_start = $request->tita_start[$key];
            $tita_finish = $request->tita_finish[$key];
            $tita_break_duration = $request->tita_break_duration[$key];

            if(strlen($tita_finish) > 0 && strlen($tita_break_duration) > 0) {

				//$quantity = round(abs((strtotime($tita_finish) - strtotime($tita_start))) / 60, 2);

				$minutes = abs((strtotime($tita_finish) - strtotime($tita_start)) / 60) - $tita_break_duration;
				$hours = round($minutes / 60, 2);

				//echo $tita_start.' - '.$tita_finish.' ('.$tita_break_duration.') = '.$minutes.' - '.$hours.'<br />';

				DB::table('time_tasks')
					->where('tita_id', '=', $tita_id)
					->update([
						'tita_finish' => $tita_finish,
						'tita_break_duration' => $tita_break_duration,
						'tita_quantity' => $hours
					]);
			}
        }

        return redirect('timesheets/finishTimes')->with('success', 'Timesheets have been updated successfully.');
    }

    public function deleteSuper($id) {
        $time_ids = DB::table('time_tasks')
            ->where('tita_id', '=', $id)
            ->get();

        $time_id = $time_ids[0]->time_id;

        DB::table('users')
            ->where('current_signin', '=', $time_id)
            ->update(['current_working' => 0]);

        DB::table('tita_history')
            ->where('tita_id', '=', $id)
            ->delete();

		DB::table('time_tasks')
			->where('tita_id', '=', $id)
			->delete();

        return redirect($_SERVER['HTTP_REFERER'])->with('success', 'Timesheets have been cancelled successfully.');
    }

    public function completedWorkers($jobs_id) {
		$users = DB::table('users')
			->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
			->where('users.current_working', '=', 2)
			->where('job_worklist.jobs_id', '=', $jobs_id)
			->select('users.id', 'users.given_name', 'users.surname', 'users.preferred', 'users.nickname', 'users.external_id')
			->get();

		return view('timesheets.completed', ['users' => $users]);
	}

	public function uncompleteWorker($id) {
    	$users = DB::table('users')
			->where('id', '=', $id)
			->get();
    	$user = $users[0];

    	if($user->current_signin > 0) {
			DB::table('users')
				->where('id', '=', $id)
				->update([ 'current_working' => 0 ]);
		} elseif($user->last_time_id > 0) {
			DB::table('users')
				->where('id', '=', $id)
				->update(['current_working' => 0, 'current_signin' => $user->last_time_id]);
		} else {
			DB::table('users')
				->where('id', '=', $id)
				->update([ 'current_working' => 0 ]);
		}

    	return redirect('/timesheets/signinSuper');
	}

	private function copyToHistory($tita_id) {
    	DB::insert(DB::raw('insert into tita_history (time_id, tita_id, task_id, role_id, supervisor_id, signed_in_by, tita_start, tita_finish, tita_break_duration, tita_quantity, public_holiday, hours_ordinary, hours_overtime, hours_saturday, hours_sunday, hours_publicholiday) 
select time_id, tita_id, task_id, role_id, supervisor_id, signed_in_by, tita_start, tita_finish, tita_break_duration, tita_quantity, public_holiday, hours_ordinary, hours_overtime, hours_saturday, hours_sunday, hours_publicholiday from time_tasks where tita_id = '.$tita_id));
	}

	public function errorList() {
        $times = DB::table('timesheets')
            ->join('myob_log', 'myob_log.time_id', '=', 'timesheets.time_id')
            ->join('users', 'users.id', '=', 'timesheets.user_id')
            ->join('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
            ->join('roles', 'roles.role_id', '=', 'time_tasks.role_id')
            ->join('payrates', 'payrates.id', '=', 'roles.rate_id')
            ->select('myob_log.id as myob_id', 'users.given_name', 'users.surname', 'payrates.name as rate_name', 'tasks.task_name', 'myob_log.response as log_message', 'time_tasks.tita_start', 'time_tasks.tita_finish')
            ->where('timesheets.processed', '=', 99)
            ->where('myob_log.resolved', '=', 0)
            ->get();

        return view('timesheets.errorlist', ['times' => $times]);
    }

    public function resolveError($id) {
        $log = DB::table('myob_log')
            ->where('id', '=', $id)
            ->get();

        if(count($log) > 0) {
            DB::table('myob_log')
                ->where('id', '=', $log[0]->id)
                ->update(['resolved' => 1]);

            DB::table('timesheets')
                ->where('time_id', '=', $log[0]->time_id)
                ->update(['processed' => -1]);
        }

        return redirect('/timesheets/errors');
    }

    public function importProcess($id) {
        $rows = DB::table('spreadsheet_import')
            ->where('batch_id', $id)
            ->get();

        $errors = [];

        foreach($rows as $row) {
            //echo '<pre>'; var_dump($row); echo '</pre>';
            $workers = DB::table('users')
                ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
                ->where('external_id', 'LINX'.str_pad($row->worker_id, 5, '0', STR_PAD_LEFT))
                ->where('job_worklist.jobs_id', 68)
                ->select('users.id')
                ->get();

            if(count($workers) == 0) {
                $errors[] = 'Could not process worker '.$row->worker_id.'. Please check that they are in the worker list for the Driscoll\'s job.';
            } else {
                $user = $workers[0]->id;
                $startdt = new \DateTime($row->date);
                /*$start = $startdt->format('Y-m-d H:i:s');
                $finish = $startdt
                    ->add(new \DateInterval(sprintf('P%sDT%sH%sM%sS', 0, floor($row->hours), (($row->hours - floor($row->hours)) * 60), 0)))
                    ->format('Y-m-d H:i:s');*/

                $sdate = str_pad($row->start, 4, '0', STR_PAD_LEFT);
                $fdate = str_pad($row->finish, 4, '0', STR_PAD_LEFT);
                $start = $startdt->format('Y-m-d').' '.substr($sdate,0,2).':'.substr($sdate, 2, 2);
                $finish = $startdt->format('Y-m-d').' '.substr($fdate,0,2).':'.substr($row->finish, 2, 2);
                $break = $row->break;

                //echo $start.'<br>'.$finish.'<br>'.$break;

                //die();

                //echo $start.'<br>'.$finish;
                //put it in as a time_task.
                $timedata = [
                    'user_id' => $user,
                    'jobs_id' => 68,
                    'time_start' => $start,
                    'time_end' => $finish,
                    'user_name' => $row->user,
                    'import_id' => $id
                ];
                //echo '<pre>'; var_dump($timedata); echo '</pre>';

                //$time_id = 0;
                $time_id = DB::table('timesheets')
                    ->insertGetId($timedata);

                $location = $row->location;
                $task = $row->task;
                $band = $row->payband;

                $role_id = 0;
                //Get a role
                $rolename = 'Worker';
                if(in_array($task, ['Supervison'])) {
                    $rolename = 'Team Leader Level 3';
                }

                if(in_array($task, ['Tractor Driver'])) {
                    $rolename = 'Machinery Operator';
                }

                $roleQ = DB::table('roles')
                    ->where('jobs_id', 68)
                    ->where('role_name', $rolename)
                    ->get();
                if(count($roleQ) > 0) {
                    $role_id = $roleQ[0]->role_id;
                }

                //Does this location exist as a top level task?
                $ltask = DB::table('tasks')
                    ->where('jobs_id', 68)
                    ->where('task_name', $location)
                    ->where('task_parent_id', 0)
                    ->get();
                if(count($ltask) == 0) {
                    $toptask_id = DB::table('tasks')
                        ->insertGetId([
                            'task_name' => $location,
                            'jobs_id' => 68,
                            'task_parent_id' => 0
                        ]);
                } else {
                    $toptask_id = $ltask[0]->task_id;
                }

                if($toptask_id == 0) {
                    die('The top level task did not exist and was not created.');
                }

                $ttask = DB::table('tasks')
                    ->where('jobs_id', 68)
                    ->where('task_name', $task)
                    ->where('task_parent_id', $toptask_id)
                    ->get();
                if(count($ttask) == 0) {
                    $task_id = DB::table('tasks')
                        ->insertGetId([
                            'task_name' => $task,
                            'task_parent_id' => $toptask_id,
                            'jobs_id' => 68
                        ]);
                } else {
                    $task_id = $ttask[0]->task_id;
                }

                if($task_id == 0) {
                    die('The second level task did not exist and was not created.');
                }

                $payrate = 'DRS Nursery '.str_replace('Linx - Pay Grade ', '', $band);
                $rate_id = 0;
                $rateSQL = DB::table('payrates')
                    ->where('name', $payrate)
                    ->select('id');

                $rateQ = $rateSQL->get();
                if(count($rateQ) > 0) {
                    $rate_id = $rateQ[0]->id;
                }

                if($rate_id == 0) {
                    die('Pay rate ('.$payrate.') could not be determined');
                }

                //Work out special cases for hours
                $difference = 0;
                $hours_ordinary = 0;
                $hours_overtime = 0;
                $hours_saturday = 0;
                $hours_sunday = 0;
                $hours_publicholiday = 0;
                if($finish != null) {
                    $difference = (abs((strtotime($finish) - strtotime($start)) / 60) - $break) / 60;

                    //work out some hours
                    if ($row->public_holiday == 1) {
                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '=', 6)
                            ->select('paru_hours')
                            ->get();
                        $rule = 0;
                        if (count($rules) > 0) {
                            $rule = $rules[0]->paru_hours;
                        }
                        $hours_publicholiday = max($difference, $rule);
                        $difference = $hours_publicholiday;
                    } elseif (date('N', strtotime($start)) == 6) {
                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '=', 4)
                            ->select('paru_hours')
                            ->get();
                        $rule = 0;
                        if (count($rules) > 0) {
                            $rule = $rules[0]->paru_hours;
                        }
                        //how many days have they worked this week?
                        $days = DB::table('time_tasks')
                            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                            ->where('timesheets.user_id', '=', $user)
                            ->where('time_tasks.tita_start', '>=', date('Y-m-d', strtotime('last monday ' . $start)))
                            ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($start)))
                            ->select(DB::raw('DATE(time_tasks.tita_start)'))
                            ->distinct()
                            ->count();
                        //echo $time['user_id'].' days: '.$days.'; rule: '.$rule.';';
                        if ($days >= $rule) {
                            //They get overtime
                            $hours_saturday = $difference;
                        } else {
                            $hours_ordinary = $difference;
                        }
                    } elseif (date('N', strtotime($start)) == 7) {
                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '=', 5)
                            ->select('paru_hours')
                            ->get();
                        $rule = 0;
                        if (count($rules) > 0) {
                            $rule = $rules[0]->paru_hours;
                        }
                        $hours_sunday = max($difference, $rule);
                        $difference = $hours_sunday;
                    } else {
                        //Have they exceed their daily, weekly or monthly allocation?
                        $daily = 10000;
                        $weekly = 10000;
                        $monthly = 10000;

                        $hours_ordinary = $difference;

                        $rules = DB::table('payrate_rules')
                            ->where('payrate_rules.rate_id', '=', $rate_id)
                            ->where('payrate_rules.paru_frequency', '<', 4)
                            ->select('paru_hours', 'paru_frequency')
                            ->get();

                        foreach ($rules as $rule) {
                            switch ($rule->paru_frequency) {
                                case 0:
                                    $daily = $rule->paru_hours;
                                    if ($difference > $daily) {
                                        $hours_ordinary = $daily;
                                        $hours_overtime = $difference - $daily;
                                    }
                                    break;
                                case 1:
                                    $weekly = $rule->paru_hours;
                                    $days = DB::table('time_tasks')
                                        ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                                        ->where('timesheets.user_id', '=', $user)
                                        ->where('time_tasks.tita_start', '>=', date('Y-m-d', strtotime('last monday ' . $start)))
                                        ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($start)))
                                        ->select(DB::raw('SUM(hours_ordinary) as tita_quantity'))
                                        ->get();
                                    $hours_this_week = 0;
                                    if ($days) {
                                        $hours_this_week = $days[0]->tita_quantity;
                                    }

                                    if (($hours_this_week + $difference) >= $weekly) {
                                        $hours_ordinary = $hours_this_week - $weekly;
                                        $hours_overtime = ($hours_this_week + $difference) - $weekly;
                                    }
                                    break;
                                case 3:
                                    $monthly = $rule->paru_hours;
                                    $date = DB::table('job_worklist')
                                        ->join('tasks', 'tasks.jobs_id', '=', 'job_worklist.jobs_id')
                                        ->where('user_id', '=', $user)
                                        ->where('tasks.task_id', '=', $task_id)
                                        ->select('job_worklist.startdate')
                                        ->get();

                                    if (count($date) > 0) {
                                        $startdate = $date[0]->startdate;
                                    }
                                    $periods = array();
                                    $startdate = strtotime($startdate);
                                    while ($startdate < time()) {
                                        $periods[] = date('Y-m-d', $startdate);
                                        $startdate = strtotime(date('Y-m-d', $startdate) . ' +28 days');
                                    }

                                    //var_dump($periods);
                                    $date = end($periods);
                                    $days = DB::table('time_tasks')
                                        ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                                        ->where('timesheets.user_id', '=', $user)
                                        ->where('time_tasks.tita_start', '>=', $date)
                                        ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($start)))
                                        ->select(DB::raw('SUM(hours_ordinary) as tita_quantity'))
                                        ->get();

                                    $overtime = 0;
                                    $ordinary = $difference;

                                    if (count($days) > 0) {
                                        $this_month = $days[0]->tita_quantity;

                                        if (($this_month + $difference) >= $monthly) {
                                            $hours_ordinary = $monthly - $this_month;
                                            $hours_overtime = ($this_month + $difference) - $monthly;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }

                //Now the task.
                $taskdata = [
                    'time_id' => $time_id,
                    'task_id' => $task_id,
                    'rate_id' => $rate_id,
                    'role_id' => $role_id,
                    'tita_start' => $start,
                    'tita_finish' => $finish,
                    'tita_quantity' => floatval($row->hours),
                    'tita_break_duration' => $break,
                    'hours_ordinary' => $hours_ordinary,
                    'hours_overtime' => $hours_overtime,
                    'hours_saturday' => $hours_saturday,
                    'hours_sunday' => $hours_sunday,
                    'hours_publicholiday' => $hours_publicholiday,
                    'import_id' => $id
                ];
                DB::table('time_tasks')
                    ->insert($taskdata);
                //echo '<pre>'; var_dump($taskdata); echo '</pre>';
            }
        }
        return view('import.success', ['importerrors' => $errors]);
        //return redirect('/timesheets')->with(['success' => 'The batch was imported successfully']);
    }

    public function importDriscolls(Request $request) {
        $file = $request->file('importFile');
        $moveto = base_path('storage'.DIRECTORY_SEPARATOR.'uploads');
        $extn = $file->extension();
        $filename = 'import'.time().'.'.$extn;
        $xlFile = $moveto.DIRECTORY_SEPARATOR.$filename;
        $batch = time();

        $file->move($moveto, $filename);

        $reader = null;
        if($extn == 'xls') {
            $xls = \SimpleXLS::parseFile($xlFile);
        }

        if($extn == 'xlsx') {
            $xls = \SimpleXLSX::parse($xlFile);
        }

        if($xls == null) {
            return redirect('/timesheets')->with(['error' => 'Please select an .xls or .xlsx file.']);
        }

        $xls->setDateTimeFormat('Y-m-d H:i');
        //Go through all of the columns
        foreach ( $xls->rows() as $r => $row ) {
            //var_dump($r);
            if($r > 0 && strlen($row[0]) > 0) {
                $rr = [];
                foreach ($row as $c => $cell) {
                    if ($c == 0) {
                        try {
                            $rr[$c] = self::OADateToDateTime($cell);
                        } catch(\Exception $ex) {
                            $rr[$c] = $cell;
                        }
                    } else {
                        $rr[$c] = $cell;
                    }
                }

                /*DB::table('spreadsheet_import')
                    ->insert([
                        'batch_id' => $batch,
                        'date' => $rr[0],
                        'worker_id' => $rr[1],
                        'worker' => $rr[2],
                        'hours' => $rr[3],
                        'payband' => $rr[4],
                        'location' => $rr[5],
                        'task' => $rr[6],
                        'user'  => $rr[7]
                    ]);*/

                DB::table('spreadsheet_import')
                    ->insert([
                        'batch_id' => $batch,
                        'date' => $rr[0],
                        'worker_id' => $rr[1],
                        'worker' => $rr[2],
                        'start' => date("Hi", strtotime($rr[3])),
                        'finish' => date("Hi", strtotime($rr[4])),
                        'break' => $rr[5],
                        'hours' => $rr[6],
                        'payband' => $rr[7],
                        'location' => $rr[8],
                        'task' => $rr[9]
                    ]);
            }
        }

        //echo '<a href="/timesheets/import/'.$batch.'">Here</a>';
        return view('import.import', ['batch' => $batch]);
        //return redirect('/timesheets/import/'.$batch)->with(['success' => 'The excel file was imported successfully as batch '.$batch]);
    }

    public static function OADateToDateTime($oaDate)
    {
        $baseDateTime = new \DateTime('1899-12-30 00:00:00');
        $days = floor($oaDate);
        $msecsFloat = ($oaDate - $days) * 86400000;
        $msecs = floor($msecsFloat);
        $hours = floor($msecs / 3600000);
        $msecs %= 3600000;
        $mins = floor($msecs / 60000);
        $msecs %= 60000;
        $secs = floor($msecs / 1000);
        $msecs %= 1000;

        $dateTime = $baseDateTime
            ->add(new \DateInterval(sprintf('P%sDT%sH%sM%sS', $days, $hours, $mins, $secs)))
            ->format('Y-m-d H:i:s');

        return $dateTime.$msecs;
    }
}