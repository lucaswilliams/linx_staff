<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Controllers\Settings\MYOBController;
use App\Http\Requests;

class VisaController extends Controller
{
    public function index($id = 0) {
        //Is the user valid?
        $users = DB::table('users')
            ->where('id', '=', $id)
            ->select('myob_uid', 'external_id', 'given_name', 'surname', 'email')
            ->get();

        if(count($users) == 0) {
            return view('errors.user', ['error_message' => 'The selected staff member does not exist']);
        }

        $user = $users[0];

        $myob = new MYOBController();
        $tresponse = $myob->getTimesheetsForEmployee($user->myob_uid);

        $pay_entries = array();
        if(isset($tresponse->Lines)) {
            foreach ($tresponse->Lines as $line) {
                foreach ($line->Entries as $entry) {
                    $pay_entries[] = (array)$entry;
                }
            }

            usort($pay_entries, array($this, 'cmp'));

            $totalDays = 0;

            if (count($pay_entries) > 0) {
                $startdate = date('Y-m-d', strtotime($pay_entries[0]['Date']));
                $enddate = date('Y-m-d', strtotime($pay_entries[count($pay_entries) - 1]['Date']));
            } else {
                $startdate = '';
                $enddate = '';
            }

            $countarray = array();

            foreach ($pay_entries as $date) {
                $thisweek = date('W', strtotime($date['Date']));
                $thisday = date('Y-m-d', strtotime($date['Date']));
                //Indicate that they did, in fact, work on this day
                $countarray[$thisweek][$thisday] = 1;
            }

            $totalDays = 0;

            foreach ($countarray as $week => $days) {
                if (count($days) > 4) {
                    $totalDays += 7;
                } else {
                    $totalDays += count($days);
                }
            }
        } else {
            $totalDays = 0;
            $startdate = null;
            $enddate = null;
        }

        //talk to myob with this user's UID
        return view('visa.index', [
            'id' => $id,
            'contactUID' => $user->myob_uid,
            'empl_id' => $user->external_id,
            'contactName' => $user->given_name.' '.$user->surname,
            'contactEmail' => $user->email,
            'startdate' => $startdate,
            'enddate' => $enddate,
            'totalDays' => $totalDays
        ])->with('error', 'No timesheets found in MYOB for this worker.');
    }

    private static function cmp($a, $b) {
        return strtotime($a['Date']) - strtotime($b['Date']);
    }


    public function display() {
        $contactName = $_POST['emp_name'];
        $contactEmail = $_POST['emp_mail'];
        $employeeNumber = $_POST['emp_id'];
        $totaldays = $_POST['total_days'];
        $startdate = $_POST['start'];
        $enddate = $_POST['finish'];

        ob_start();
        ?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <style>
                img, div {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 785px;
                    height: 1120px;
                    overflow: hidden;
                }

                span {
                    font-size: 14px;
                    font-family: Arial, sans-serif;
                    display: block;
                    position: absolute;
                }

                #employee_name {
                    top: 572px;
                    left: 65px;
                    width:  768px;
                }

                #type_of_work {
                    top: 600px;
                    left: 120px;
                    width: 900px;
                }

                #days_worked {
                    top: 647px;
                    left: 470px;
                    width: 400px;
                }

                #start_date {
                    top: 650px;
                    left: 111px;
                    width: 76px;
					background-color: #FFFFFF;
                }

                #end_date {
                    top: 650px;
                    left: 271px;
                    width: 76px;
					background-color: #FFFFFF;
                }

                #postcode_worked {
                    top: 648px;
                    left: 670px;
                    width: 200px;
                }

                #busi_name {
                    top: 690px;
                    left: 65px;
                    width: 760px;
                }

                #busi_addr_1 {
                    top: 710px;
                    left: 65px;
                    width: 760px;
                }

                #busi_suburb {
                    top: 732px;
                    left: 65px;
                    width: 760px;
                }

                #busi_pcode {
                    top: 732px;
                    left: 316px;
                    width: 760px;
                }

                #busi_abn {
                    top: 732px;
                    left: 380px;
                    width: 760px;
                }

                #busi_phone {
                    top: 733px;
                    left: 580px;
                    width: 900px;
                }

                #busi_contact {
                    top: 690px;
                    left: 380px;
                    width: 400px;
                }

                #verify_name {
                    top: 795px;
                    left: 65px;
                    width: 760px;
                }

                #verify_email {
                    top: 775px;
                    left: 310px;
                    width: 900px;
                }

                #verify_phone {
                    top: 775px;
                    left: 580px;
                    width: 400px;
                }
            </style>
        </head>
        <body>
        <img src="https://staff.linxemployment.com.au/visa-bg.jpg">
        <div id="fields">
            <span id="employee_name"><?php echo $contactName; ?></span>
            <span id="type_of_work">Farm Labourer</span>
            <span id="days_worked"><?php echo $totaldays; ?></span>
            <span id="start_date"><?php echo date('d/m/Y', strtotime($startdate)); ?></span>
            <span id="end_date"><?php echo date('d/m/Y', strtotime($enddate)); ?></span>
            <span id="postcode_worked">7277</span>
            <span id="busi_name">Linx Employment Tas Pty Ltd</span>
            <span id="busi_addr_1">PO Box 41</span>
            <span id="busi_suburb">Legana</span>
            <span id="busi_pcode">7277</span>
            <span id="busi_phone">03 6330 2471</span>
            <span id="busi_contact">Kim Slater</span>
            <span id="busi_abn">77 162 415 689</span>
            <span id="verify_name">Kim Slater</span>
            <span id="verify_email">admin@linxemployment.com.au</span>
            <span id="verify_phone">03 6330 2471</span>
        </div>

        <?php
        $content = ob_get_clean();
        //echo $content;
        //die();

        try {
            $outfile = storage_path('form'.$employeeNumber.'.pdf');

            $pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(0,0,0,0));
            $pdf->pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($content);
            $pdf->Output($outfile, 'F');
            $pdf->Output($outfile);
        }
        catch(HTML2PDF_exception $ex) {
            echo $ex.'<br />';
            echo $content;
        }
    }

    public function send(Request $request)
    {
        require_once 'Settings/includes/PHPMailerAutoload.php';
        $empl_id = $request->empl_id;
        $contactName = $request->contactName;
        $contactEmail = $request->contactEmail;
        $outfile = storage_path('form'.$empl_id.'.pdf');
        $outname = 'form'.$empl_id.'.pdf';
        if(!file_exists($outfile)) {
            return response()->json(['success' => false, 'error' => 'There was an error sending the email - please generate the form with the "Update" button first.']);
        }
        $email_txt = "Hi " . $contactName . ",<br />Please find attached your second visa form.";

        //Create a new PHPMailer instance
        $mail = new \PHPMailer;
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $mail->Host = "smtp.office365.com";
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = 587;
        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $mail->Username = "admin@linxemployment.com.au";
        //Password to use for SMTP authentication
        $mail->Password = "Rosevears03";
        //Set who the message is to be sent from
        $mail->setFrom('admin@linxemployment.com.au', 'Linx Employment Admin');
        //Set who the message is to be sent to
        $mail->addAddress($contactEmail, $contactName);
        $mail->addAddress('admin@linxemployment.com.au', 'Linx Employment Admin');
        //Set the subject line
        $mail->Subject = 'Second Visa form';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->Body = $email_txt;
        //Replace the plain text body with one created manually
        $mail->AltBody = $email_txt;
        //Attach an image file
        $mail->addAttachment($outfile);

        //send the message, check for errors
        $mail_sent = $mail->send();

        //if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
        if($mail_sent) {
            unlink($outfile);
        }
        return response()->json(['success' => $mail_sent, 'error' => ($mail_sent ? '' : 'There was an error sending the email')]);
    }
}