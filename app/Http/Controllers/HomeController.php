<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Mail;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class HomeController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function dashboard()
    {
        $user = Auth::user();
        $supervisor = ((DB::table('job_superlist')->where('user_id', '=', $user->id)->count() > 0) || (DB::table('client_users')->where('user_id', '=', $user->id)->where('level', '=', 1)->count() > 0));
        $worker = (DB::table('job_worklist')
                ->where('user_id', '=', $user->id)
                ->where('startdate', '<=', date('Y-m-d'))
                ->where(function ($where) {
                    $where->whereNull('enddate')
                        ->orWhere('enddate', '>=', date('Y-m-d', strtotime('tomorrow')));
                })
                ->count() > 0);
    
        $signin = $user->current_signin;
        $ts = DB::table('timesheets')
            ->where('time_id', '=', $signin)
            ->select('approved_user')
            ->get();
        if (count($ts) > 0 && $ts[0]->approved_user == 1) {
            $signin = 0;
        }
    
        $toAccept = DB::table('job_worklist')
            ->where('user_id', '=', $user->id)
            ->where('jobs_id', '>', 0)
            ->where('needs_accepting', '=', 1)
            ->count();
        if($user->confirmed == 4) {
            //Are they a worker or a supervisor?
            if ($toAccept > 0) {
                return $this->acceptJobForm($user);
            } else {
                $report = (DB::table('report_users')->where('user_id', '=', $user->id)->count() > 0);
        
                $statuses = new \stdClass();
                $rec = DB::table('users')
                    ->where('id', '=', $user->id)
                    ->select('has_resume', 'has_credentials', 'has_medical')
                    ->get();
        
                $statuses->resume = $rec[0]->has_resume;
                $statuses->credentials = $rec[0]->has_credentials;
                $statuses->medical = $rec[0]->has_medical;
        
                return view('user.dashboard', ['super' => $supervisor, 'worker' => $worker, 'report' => $report, 'signin' => $signin, 'statuses' => $statuses]);
            }
        } elseif($user->confirmed == 3) {
            $induction = DB::table('inductions')
                ->join('userinductions', 'userinductions.ind_id', '=', 'inductions.ind_id')
                ->where('userinductions.user_id', '=', $user->id)
                ->where('userinductions.usin_completed', '=', '0000-00-00')
                ->get();
    
            if (count($induction) > 0) {
        
                $questions = DB::table('questions')
                    ->where('questions.ind_id', '=', $induction[0]->ind_id)
                    ->orderByRaw("RAND()")
                    ->take(5)
                    ->get();
        
                $questionarray = array();
                foreach ($questions as $question) {
                    $answers = DB::table('options')
                        ->where('options.ques_id', '=', $question->ques_id)
                        ->orderByRaw("RAND()")
                        ->get();
            
                    $question->answers = $answers;
                    $questionarray[] = $question;
                }
                return view('user.induction', ['induction' => $induction[0], 'questions' => $questionarray]);
            } else {
                DB::table('users')
                    ->where('id', '=', $user->id)
                    ->update(['confirmed' => 4]);
                return view('user.dashboard', ['super' => $supervisor, 'worker' => $worker, 'report' => false, 'signin' => $signin]);
            }
        } else {
            if ($toAccept > 0 || $user->confirmed == 1) {
                return $this->acceptJobForm($user);
            } else {
                $payments = DB::table('payments')
                    ->where('user_id', '=', $user->id)
                    ->where('payment_made', '<', '2015-01-01')
                    ->get();

                $super = DB::table('super_providers')
                    ->get();

                $usis = DB::table('super_usi')
                    ->get();

                if ($user->profile_complete == 1) {
                    if ($user->has_resume == 0) {
                        $hasResume = $user->has_resume;
                        return redirect('/resume');
                    } elseif ($user->has_credentials == 0) {
                        return redirect('/certificates');
                    } elseif ($user->has_medical == 0) {
                        return redirect('/medical');
                    }
                    return view('user.dashboard', ['super' => $supervisor, 'worker' => $worker, 'report' => false, 'signin' => $signin]);
                } else {
                    return view('user.register-more', ['user' => $user, 'payments' => $payments, 'super' => $super, 'usis' => $usis]);
                }
            }
        }
    }

    public function acceptJobForm($user) {
		$us = DB::table('users')
			->select('users.*', 'users.id as uid')
			->where('users.id', '=', $user->id)
			->get();

		$jobs = DB::table('jobs')
			->join('clients', 'clients.id', '=', 'jobs.client_id')
			->join('job_worklist', 'job_worklist.jobs_id', '=', 'jobs.id')
			->where('job_worklist.user_id', '=', $user->id)
			->where('job_worklist.needs_accepting', '=', 1)
			->select('clients.name as client_name', 'jobs.name as jobs_name', 'jobs.start_date', 'jobs.end_date', 'job_worklist.id')
			->distinct()
			->get();

		return view('user.acceptjob', ['user' => $us[0], 'jobs' => $jobs]);
	}

    public function showResume() {
        $user = Auth::user();
        return view('user.resume', ['user' => $user]);
    }

    public function saveResume(Request $request) {
        $user = Auth::user();
        if(isset($request->skip)) {
            //The user has chosen to skip the uploading of their resume.
            DB::table('users')
                ->where('id', '=', $user->id)
                ->where('has_resume', '<>', 1)
                ->update(['has_resume' => -1]);
        }

        if(isset($request->upload)) {
            if($request->hasFile('resume_file')) {
                if(strcmp($request->resume_file->extension(), 'pdf') == 0 ||
                    strcmp($request->resume_file->extension(), 'docx') == 0 ||
                    strcmp($request->resume_file->extension(), 'doc') == 0) {
                    $request->file('resume_file')->move(base_path('storage' . DIRECTORY_SEPARATOR . 'uploads'), 'resume' . $user->id . '.pdf');

                    $file_name = base_path('storage' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'resume' . $user->id . '.pdf');
                    $fp = fopen($file_name, 'r');
                    $content = fread($fp, filesize($file_name));
                    $content = base64_encode($content);
                    fclose($fp);

                    DB::table('user_resume')
                        ->insert([
                            'resume_content' => $content,
                            'user_id' => $user->id
                        ]);
    
                    DB::table('users')
                        ->where('id', '=', $user->id)
                        ->update([
                            'has_resume' => 1
                        ]);

                    unlink($file_name);
                } else {
                    return redirect('/resume')->with('error', 'Please upload a PDF or Word copy of your resume.');
                }
            } else {
                return redirect('/resume')->with('error', 'Please select a PDF or Word copy of your resume to continue.  If you do not wish to upload a resume at this time, please click the "Skip this step" button.');
            }
        }

        return redirect('/home');
    }

    public function showCredentials() {
        $user = Auth::user();
        $user_id = $user->id;
        $credentials = DB::table('credentials')
            ->leftjoin('staff_credentials', function($join) use($user_id) {
                $join->on('credentials.id', '=', 'staff_credentials.credential_id')
                    ->where('staff_credentials.staff_id', '=', $user_id);
            })
            ->select('credentials.id as cred_id', 'credentials.name', 'credentials.has_expiry', 'credentials.has_description', 'staff_credentials.*')
            ->orderBy('credentials.has_description')
            ->orderBy('credentials.name')
            ->get();

        if(count($credentials) == 0) {
            DB::table('users')
                ->where('id', '=', $user->id)
                ->update(['has_credentials' => -1]);

            return redirect('/home');
        }

        return view('user.credentials', ['user' => $user, 'creds' => $credentials]);
    }

    public function saveCredentials(Request $request) {
        $user = Auth::user();
        if(isset($request->skip)) {
            DB::table('users')
                ->where('id', '=', $user->id)
                ->where('has_credentials', '<>', 1)
                ->update(['has_credentials' => -1]);
        } else {
            DB::table('users')
                ->where('id', '=', $user->id)
                ->update(['has_credentials' => 1]);
        }

        DB::table('staff_credentials')
            ->where('staff_id', '=', $user->id)
            ->delete();

        if(is_array($request->cred_id)) {
            foreach($request->cred_id as $key=>$value) {
                echo $key.'~'.$value.'<br />';
                DB::table('staff_credentials')
                    ->insert([
                        'staff_id' => $user->id,
                        'credential_id' => $key,
                        'expiry_date' => $request->expiry[$key],
                        'description' => $request->description[$key]
                    ]);
            }
        }

        return redirect('/home');
    }

    public function showMedical() {
        $user = Auth::user();
    
        $medical = DB::table('medical_questions')
            ->get();
        
        if($user->has_medical == 1) {
            $usermedical = DB::table('user_medical')
                ->where('user_id', '=', $user->id)
                ->get()[0];
            
            $medques = [];
            foreach($medical as $question) {
                $response = DB::table('user_medical_questions')
                    ->where('medical_id', '=', $usermedical->medical_id)
                    ->where('ques_id', '=', $question->ques_id)
                    ->get();
                
                if(count($response) > 0) {
                    $question->duration = $response[0]->duration;
                    $question->status = $response[0]->status;
                } else {
                    $question->duration = null;
                    $question->status = null;
                }
                $medques[] = $question;
            }
            
            $medical = $medques;
        } else {
            $usermedical = new \stdClass();
            $usermedical->lost_time = null;
            $usermedical->medication = null;
            $usermedical->allergies = null;
            $usermedical->allergies_medication = null;
            $usermedical->allergies_additional = null;
            $usermedical->sport_hobbies = null;
            $usermedical->existing_condition = null;
            $usermedical->stand = null;
            $usermedical->current_smoker = null;
            $usermedical->smokes_daily_cigarettes = null;
            $usermedical->smokes_daily_cigars = null;
            $usermedical->smokes_daily_pipes = null;
            $usermedical->smoker_quit_year = null;
            $usermedical->smoker_duration = null;
            $usermedical->alcohol_per_week = null;
            $usermedical->drink_alcohol = null;
            $usermedical->alcohol_per_week = null;
            $usermedical->alcohol_type = null;
            $usermedical->vaccinate_hep_a = null;
            $usermedical->vaccinate_hep_b = null;
            $usermedical->vaccinate_tetanus = null;
            $usermedical->doctor_1_name = null;
            $usermedical->doctor_1_address = null;
            $usermedical->doctor_1_phone = null;
    
            $medques = [];
            foreach($medical as $question) {
                $question->duration = null;
                $question->status = null;
                $medques[] = $question;
            }
            
            $medical = $medques;
        }

        return view('user.medical', ['medical' => $medical, 'user' => $user, 'usermedical' => $usermedical]);
    }

    public function saveMedical(Request $request) {
        $user = Auth::user();
    
        $send = DB::table('users')
            ->where('id', '=', $user->id)
            ->get();
        if($send[0]->has_medical == 0) {
            $usr = $send[0];
            //Send email to admin and the worker
            try {
                Mail::send('emails.newworker', ['user' => $usr], function ($message) use ($usr) {
                    $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                    $message->to($usr->email);
                    $message->subject('Your registration with Linx Employment');
                });
                
                Mail::send('emails.newregister', ['user' => $usr], function ($message) {
                    $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                    $message->to('work@linxemployment.com.au');
                    $message->subject('New registration received');
                });
            } catch(Exception $ex) {
                //Ignore
            }
        }
    
        if(isset($request->skip)) {
            DB::table('users')
                ->where('id', '=', $user->id)
                ->where('has_medical', '<>', 1)
                ->update(['has_medical' => -1]);
        } else {
            $medical_data = array(
                'user_id' => $user->id,
                'lost_time' => (isset($request->lost_time) ? $request->lost_time : null),
                'medication' => (isset($request->medication) ? $request->medication : null),
                'allergies' => (isset($request->allergies) ? $request->allergies : null),
                'allergies_medication' => (isset($request->allergies_medication) ? $request->allergies_medication : null),
                'allergies_additional' => (isset($request->allergies_additional) ? $request->allergies_additional : null),
                'stand' => (isset($request->stand) ? $request->stand : null),
                'vaccinate_hep_a' => (isset($request->vaccinate_hep_a) ? $request->vaccinate_hep_a : 0),
                'vaccinate_hep_b' => (isset($request->vaccinate_hep_b) ? $request->vaccinate_hep_b : 0),
                'vaccinate_tetanus' => (isset($request->vaccinate_tetanus) ? $request->vaccinate_tetanus : 0),
                'current_smoker' => (isset($request->current_smoker) ? $request->current_smoker : 0),
                'smokes_daily_cigarettes' => (isset($request->smokes_daily_cigarettes) ? $request->smokes_daily_cigarettes : 0),
                'smokes_daily_cigars' => (isset($request->smokes_daily_cigars) ? $request->smokes_daily_cigars : 0),
                'smokes_daily_pipes' => (isset($request->smokes_daily_pipes) ? $request->smokes_daily_pipes : 0),
                'ever_smoker' => (isset($request->ever_smoker) ? $request->ever_smoker : 0),
                'smoker_quit_year' => (isset($request->smoker_quit_year) ? $request->smoker_quit_year : 0),
                'smoker_duration' => (isset($request->smoker_duration) ? $request->smoker_duration : 0),
                'drink_alcohol' => (isset($request->drink_alcohol) ? $request->drink_alcohol : 0),
                'alcohol_per_week' => (isset($request->alcohol_per_week) ? $request->alcohol_per_week : 0),
                'alcohol_type' => (isset($request->alcohol_type) ? $request->alcohol_type : null),
                'sport_hobbies' => (isset($request->sport_hobbies) ? $request->sport_hobbies : null),
                'existing_condition' => (isset($request->existing_condition) ? $request->existing_condition : null),
                'doctor_1_name' => (isset($request->doctor_1_name) ? $request->doctor_1_name : null),
                'doctor_1_address' => (isset($request->doctor_1_address) ? $request->doctor_1_address : null),
                'doctor_1_phone' => (isset($request->doctor_1_phone) ? $request->doctor_1_phone : null),
                'height' => (isset($request->height) ? $request->height : 0),
                'weight' => (isset($request->weight) ? $request->weight : 0)
            );
    
            $medical_alert = 0;
    
            if (isset($request->lost_time) || isset($request->medication) || isset($request->allergies) || isset($request->allergies_medication) || isset($request->allergies_additional) || isset($request->existing_condition)) {
                $medical_alert = 1;
            }
    
            $medical_id = DB::table('user_medical')
                ->insertGetId($medical_data);
    
            foreach ($request->ques as $id => $ques) {
                if (isset($request->duration[$id])) {
                    $medical_alert = 1;
                }
                DB::table('user_medical_questions')
                    ->insert([
                        'medical_id' => $medical_id,
                        'ques_id' => $id,
                        'duration' => (isset($request->duration[$id]) ? $request->duration[$id] : null),
                        'status' => (isset($request->status[$id]) ? $request->status[$id] : null)
                    ]);
            }
    
            DB::table('users')
                ->where('id', '=', $user->id)
                ->update([
                    'emergency_name' => (isset($request->emergency_name) ? $request->emergency_name : null),
                    'emergency_number' => (isset($request->emergency_number) ? $request->emergency_number : null),
                    'has_medical' => 1
                ]);
        }
        return redirect('/dashboard');
    }

    public function special() {
        DB::table('tasks')
            ->where('task_id', '>', 172)
            ->orWhere('task_id', '=', 170)
            ->orWhere('task_id', '=', 171)
            ->update(['archived' => 1]);

        DB::table('tasks')
            ->whereIn('task_parent_id', [172, 186])
            ->update(['archived' => 0]);

        $ids = array(167, 168, 169);

        $level2 = array('Bare root',
            'Misted tip/plugs',
            'Long cane',
            'Pots');

        $level3 = array('Planting',
            'Crop maintenance',
            'Harvest');

        $level4 = array('Planting preparation',
            'Planting',
            'Collect material',
            'Lay pots/trays',
            'Physical maintenance',
            'Pest/disease',
            'Weeding/de-flowering',
            'Irrigation/fertiliser',
            'Crop maintenance',
            'Harvest/dig',
            'Trimming',
            'Wash/dip',
            'Support crew',
            'Store/shipping');

        foreach($ids as $id) {
            foreach($level2 as $l2) {
                $l2id = DB::table('tasks')
                    ->insertGetId([
                        'task_parent_id' => $id,
                        'task_name' => $l2,
                        'rate_id' => 0
                ]);

                foreach($level3 as $l3) {
                    $l3id = DB::table('tasks')
                        ->insertGetId([
                            'task_parent_id' => $l2id,
                            'task_name' => $l3,
                            'rate_id' => 0
                        ]);

                    foreach($level4 as $l4) {
                        DB::table('tasks')
                            ->insert([
                                'task_parent_id' => $l3id,
                                'task_name' => $l4,
                                'rate_id' => 54
                            ]);
                    }
                }
            }
        }
    }
}