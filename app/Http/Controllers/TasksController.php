<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;

class TasksController extends Controller
{
    public function getSelectList() {
        $tasks = DB::table('tasks')
            ->select('task_id', 'task_name')
            ->where('task_parent_id', '=', 0)
            ->where('archived', '=', 0)
            ->orderBy('task_name')
            ->get();

        $tasklist = array();
        foreach($tasks as $task) {
            $tasklist[$task->task_id] = $task;
            $children = $this->getChildren($task->task_id, 1);
            //echo '<pre>'; var_dump($children); echo '</pre>';
            $tasklist += $children;
        }

        return $tasklist;
    }

    public function getSelectListForJob($job_id) {
        $tasks = DB::table('tasks')
            ->leftJoin('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->select('task_id', 'task_name', 'name as rate_name', 'id as rate_id')
            ->where('task_parent_id', '=', 0)
            ->where('archived', '=', 0)
            ->where('jobs_id', '=', $job_id)
            ->orderBy('task_name')
            ->get();

        $tasklist = array();
        foreach($tasks as $task) {
            $tasklist[$task->task_id] = $task;
            $children = $this->getChildren($task->task_id, 1);
            //echo '<pre>'; var_dump($children); echo '</pre>';
            $tasklist += $children;
        }

        return $tasklist;
    }

    public function getFullSelectListForJob($job_id) {
        $tasks = DB::table('tasks')
            ->leftJoin('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->select('task_id', 'task_name', 'name as rate_name', 'id as rate_id')
            ->where('task_parent_id', '=', 0)
            ->where('archived', '=', 0)
            ->where('jobs_id', '=', $job_id)
            ->orderBy('task_name')
            ->get();

        $tasklist = array();
        foreach($tasks as $task) {
            $tasklist[$task->task_id] = $task;
            $children = $this->getFullChildren($task->task_id, $task->task_name);
            //echo '<pre>'; var_dump($children); echo '</pre>';
            $tasklist += $children;
        }

        return $tasklist;
    }

    public function getTopLevelForJob($job_id) {
        $tasks = DB::table('tasks')
            ->leftJoin('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->select('task_id', 'task_name', 'name as rate_name', 'id as rate_id')
            ->where('task_parent_id', '=', 0)
            ->where('archived', '=', 0)
            ->where('jobs_id', '=', $job_id)
            ->orderBy('task_name')
            ->get();

        return $tasks;
    }

    private function getChildren($id, $iterations) {
        $tasks = DB::table('tasks')
            ->leftJoin('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->select('task_id', 'task_name', 'name as rate_name', 'id as rate_id')
            ->where('task_parent_id', '=', $id)
            ->where('archived', '=', 0)
            ->orderBy('task_name')
            ->get();

        $tasklist = array();
        foreach($tasks as $task) {
            for($i = 0; $i < $iterations; $i++) {
                $task->task_name = '- ' . $task->task_name;
            }
            $tasklist[$task->task_id] = $task;
            $tasklist += $this->getChildren($task->task_id, $iterations + 1);
        }

        return $tasklist;
    }

    private function getFullChildren($id, $name) {
        $tasks = DB::table('tasks')
            ->leftJoin('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->select('task_id', 'task_name', 'name as rate_name', 'id as rate_id')
            ->where('task_parent_id', '=', $id)
            ->where('archived', '=', 0)
            ->orderBy('task_name')
            ->get();

        $tasklist = array();
        foreach($tasks as $task) {
            $task->task_name = $name.' \\ '.$task->task_name;
            $tasklist[$task->task_id] = $task;
            $tasklist += $this->getFullChildren($task->task_id, $task->task_name);
        }

        return $tasklist;
    }

    public function getListChildren($id) {
        if($id > 0) {
            $tasks = DB::table('tasks')
                ->leftJoin('payrates', 'payrates.id', '=', 'tasks.rate_id')
                ->select('task_id', 'task_name', 'name as rate_name', 'id as rate_id')
                ->where('task_parent_id', '=', $id)
                ->where('archived', '=', 0)
                ->orderBy('task_name')
                ->get();

            $tasklist = array();
            foreach ($tasks as $task) {
                $tasklist[$task->task_id] = $task;
            }

            return $tasklist;
        } else {
            return null;
        }
    }

    public function addTask(Request $request) {
        DB::table('tasks')
            ->insert([
                'task_name' => $request->task_name,
                'task_desc' => $request->task_desc,
                'task_parent_id' => $request->task_parent_id,
                'jobs_id' => $request->jobs_id,
                'rate_id' => $request->rate_id,
                'archived' => 0
            ]);

        return redirect('/jobs/'.$request->jobs_id.'/tasks');
    }

    public function getParentTree($id) {
        $tasks = DB::table('tasks')
            ->where('task_id', '=', $id)
            ->get();

        $tasklist = array();
        foreach($tasks as $task) {
            $tasklist[$task->task_id] = $task->task_name;
            $tasklist += $this->getParentTree($task->task_parent_id);
        }

        return $tasklist;
    }
}
