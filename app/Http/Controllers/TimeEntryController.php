<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use Response;
use Storage;
use View;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Settings\MYOBController;
use App\Http\Controllers\NotificationsController;

class TimeEntryController extends Controller {
    public function selectJob() {
    	if(isset($_GET['id'])) {
			return redirect('timesheets/'.$_GET['id']);
		}

    	$user = Auth::user();

        //Get a list of the jobs we can select from, and a button for "by worker" or "by date"
		$jobs = DB::table('job_superlist')
			->join('jobs', 'jobs.id', '=', 'job_superlist.jobs_id')
			->join('users', 'users.id', '=', 'job_superlist.user_id');

		if($user->level != 3) {
			$jobs = $jobs->where('user_id', '=', $user->id);
		}

		$jobs = $jobs->whereNull('jobs.end_date')
			->select('jobs.id', 'jobs.name')
			->distinct()
			->get();
		if (count($jobs) == 0) {
            //try a different query.  They might be a contact
            $jobs = DB::table('jobs')
                ->join('client_users', 'client_users.client_id', '=', 'jobs.client_id')
                ->where('client_users.user_id', '=', $user->id)
                ->whereNull('jobs.end_date')
                ->select('jobs.id', 'jobs.name')
                ->distinct()
                ->get();
        }

        if (count($jobs) == 0) {
		    return view('errors.access', ['message' => 'You do not have any jobs to manage']);
		} elseif (count($jobs) == 1) {
			return redirect('timesheets/'.$jobs[0]->id);
		} else {
			return view('timesheets.selectTimesheet', ['jobs' => $jobs]);
		}
    }

    public function selectView($jobs_id) {
    	$user = Auth::user();
        //get all the workers
		$workers = DB::table('users')
			->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
			->where('job_worklist.jobs_id', '=', $jobs_id);

		if($user->level != 3) {
		    //are we dealing with a supervisor or a contact?
            $isContact = DB::table('client_users')
                ->join('jobs', 'jobs.client_id', '=', 'client_users.client_id')
                ->where('jobs.id', '=', $jobs_id)
                ->where('client_users.user_id', '=', $user->id)
                ->where('client_users.level', '=', 1)
                ->count();
            if($isContact == 0) {
                //This person is a supervisor, only show their own.
                $workers = $workers->where('job_worklist.supervisor_id', '=', $user->id);
            }
        }

        $workers = $workers->select('users.given_name', 'users.surname', 'users.preferred', 'users.nickname', 'users.external_id', 'users.id', 'job_worklist.enddate')
            ->orderBy(DB::raw('IFNULL(enddate, \'1970-01-01\')'))
            ->orderBy(DB::raw('right(users.external_id, 4)'))
			->distinct()
			->get();
		//get all dates since the startdate
		$jobs = DB::table('jobs')
			->where('id', '=', $jobs_id)
			->select('start_date', 'end_date')
			->get();
		if(count($jobs) > 0) {
			$job = $jobs[0];
		} else {
			return view('errors.access', 'There is no matching job');
		}
		$days = array();
		$start_date = strtotime($job->start_date);
		if($job->end_date == null || strtotime($job->end_date) > time()) {
			$end_date = time();
		} else {
			$end_date = strtotime($job->end_date);
		}
		for($i = $end_date; $i >= $start_date; $i -= 86400) {
			$days[] = array(
				'value' => date('Y-m-d', $i),
				'display' => date('d/m/Y', $i)
			);
		}
    	return view('timesheets.selectView', ['workers' => $workers, 'days' => $days, 'jobs_id' => $jobs_id]);
	}

    public function byWorker($jobs_id, $id) {
        $maxtimes = DB::table('timesheets')
            ->where('processed', '=', 1)
            ->where('jobs_id', '=', $jobs_id)
            ->select(DB::raw('max(DATE(time_start)) as time_start'))
            ->get();
        $locked = false;
        $maxtime = strtotime('1970-01-01');
        if(count($maxtimes) > 0) {
            $maxtime = strtotime($maxtimes[0]->time_start);
        }

        $workers = DB::table('users')
			->where('id', '=', $id)
			->select('users.id as user_id', 'users.given_name', 'users.surname', 'users.external_id', 'users.preferred')
			->select('users.id as user_id', 'users.given_name', 'users.surname', 'users.external_id', 'users.preferred')
			->get();

		$timesheets = array();
		foreach($workers as $worker) {
			//get their timesheets for this day
			$time = DB::table('timesheets')
				->leftJoin('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
				->leftJoin('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
				->where('timesheets.user_id', '=', $worker->user_id)
				->where('tasks.jobs_id', '=', $jobs_id)
				->select('timesheets.time_id', 'time_tasks.tita_id', 'tasks.task_id', 'tasks.task_name', 'time_tasks.tita_start', 'time_tasks.tita_finish', 'time_tasks.tita_break_duration', 'timesheets.processed', 'time_tasks.supervisor_id', 'time_tasks.role_id', 'time_tasks.public_holiday')
				->get();

			$entry = $worker;
			if(count($time) > 0) {
				$sheets = array();
				foreach ($time as $item) {
                    $absent = false;
                    if($maxtime >= strtotime(substr($item->tita_start, 0, 10))) {
                        $locked = true;
                    }

                    switch($item->processed) {
                        case -1 : $processed = 'Processing';
                            break;
                        case 1 : $processed = 'Processed';
                            break;
                        case 99 : $processed = 'Processing Error';
                            break;
                        default : $processed = '';
                            break;
                    }

					$sheet = new \stdClass();
					$sheet->time_id = $item->time_id;
					$sheet->tita_id = $item->tita_id;
					$sheet->task_id = $item->task_id;
					$sheet->supervisor_id = $item->supervisor_id;
					$sheet->role_id = $item->role_id;
					$sheet->task_name = $item->task_name;
					$sheet->tita_start = $item->tita_start;
					$sheet->tita_finish = $item->tita_finish;
					$sheet->tita_break_duration = $item->tita_break_duration;
					$sheet->processed = $processed;
					$sheet->public_holiday = $item->public_holiday;
                    $sheet->locked = $locked || ($item->processed != 0);
					$sheets[] = $sheet;
				}
				$entry->timesheets = $sheets;
				$entry->absent = $absent;
			} else {
				$sheet = new \stdClass();
				$sheet->time_id = null;
				$sheet->tita_id = null;
				$sheet->task_id = null;
				$sheet->supervisor_id = null;
				$sheet->role_id = null;
				$sheet->task_name = null;
				$sheet->tita_start = null;
				$sheet->tita_finish = null;
				$sheet->tita_break_duration = null;
                $sheet->processed = '';
                $sheet->public_holiday = 0;
                $sheet->locked = false;
                $entry->absent = false;
				$entry->timesheets = array($sheet);
			}
			$timesheets[] = $entry;
		}

        $tc = new TasksController();
        $tasks = $tc->getFullSelectListForJob($jobs_id);

        $supers = DB::table('users')
			->join('job_superlist', 'job_superlist.user_id', '=', 'users.id')
			->where('job_superlist.jobs_id', '=', $jobs_id)
			->select('users.given_name', 'users.surname', 'users.id')
			->get();

		$roles = DB::table('roles')
			->where('jobs_id', '=', $jobs_id)
			->get();

		$user = Auth::user();

		return view('timesheets.editform', [
		    'timesheets' => $timesheets,
            'tasks' => $tasks,
			'supers' => $supers,
			'roles' => $roles,
			'user' => $user,
            'jobs_id' => $jobs_id
        ]);
    }

    public function byDay($jobs_id, $date) {
        $user = Auth::user();
        $maxtimes = DB::table('timesheets')
            ->where('processed', '=', 1)
            ->where('jobs_id', '=', $jobs_id)
            ->select(DB::raw('max(DATE(time_start)) as time_start'))
            ->get();
        $locked = false;
        if(count($maxtimes) > 0) {
            $maxtime = strtotime($maxtimes[0]->time_start);
            if($maxtime >= strtotime($date)) {
                $locked = true;
            }
        }

        //Show a form to enter times for all workers on a given day.
        $workers = DB::table('users')
            ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
            ->where('job_worklist.jobs_id', '=', $jobs_id)
            ->where('job_worklist.startdate', '<=', $date.' 23:59:59')
            ->where(function($where) use ($date) {
                $where->whereNull('job_worklist.enddate')
                    ->orWhere('job_worklist.enddate', '>=', $date);
            });

        if($user->level != 3) {
            //are we dealing with a supervisor or a contact?
            $isContact = DB::table('client_users')
                ->join('jobs', 'jobs.client_id', '=', 'client_users.client_id')
                ->where('jobs.id', '=', $jobs_id)
                ->where('client_users.user_id', '=', $user->id)
                ->where('client_users.level', '=', 1)
                ->count();
            if($isContact == 0) {
                //This person is a supervisor, only show their own.
                $workers = $workers->where('job_worklist.supervisor_id', '=', $user->id);
            }
        }

        $workers = $workers->select('users.id as user_id', 'users.given_name', 'users.surname', 'users.external_id', 'users.preferred', 'job_worklist.supervisor_id')
            ->distinct()
            ->orderBy(DB::raw('right(users.external_id, 5)'))
            ->get();

        $timesheets = array();
        foreach($workers as $worker) {
            //get their timesheets for this day
            $timeQ = DB::table('timesheets')
                ->leftJoin('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
                ->leftJoin('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
				->where('timesheets.user_id', '=', $worker->user_id)
                ->where('time_tasks.tita_start', '>=', $date)
                ->where('time_tasks.tita_start', '<=', $date.' 23:59:59')
                ->where('timesheets.jobs_id', '=', $jobs_id)
                //->where(function($where) use ($date) {
                //    $where->whereNull('time_tasks.tita_finish')
                //        ->orWhere('time_tasks.tita_finish', '<=', $date.' 23:59:59');
                //})
                ->select('timesheets.time_id', 'time_tasks.tita_id', 'tasks.task_id', 'tasks.task_name', 'time_tasks.tita_start', 'time_tasks.tita_finish', 'time_tasks.tita_break_duration', 'timesheets.processed', 'time_tasks.supervisor_id', 'time_tasks.role_id', 'time_tasks.public_holiday')
                ->orderBy('time_tasks.tita_start');
            //echo $timeQ->toSql().'<br>'; die();
			$time = $timeQ->get();

            $absents = DB::table('user_absence')
                ->where('user_id', '=', $worker->user_id)
                ->where('timestamp', '>=', $date)
                ->where('timestamp', '<=', $date.' 23:59:59')
                ->where('absence_type', '=', 2)
                ->select('reason')
                ->get();

            $absent = false;
            if(count($absents) > 0) {
                $absent = true;
            }

            $entry = $worker;
            if(count($time) > 0) {
                $sheets = array();
                foreach ($time as $item) {
                    switch($item->processed) {
                        case -1 : $processed = 'Processing';
                        break;
                        case 1 : $processed = 'Processed';
                        break;
                        case 99 : $processed = 'Processing Error';
                        break;
                        default : $processed = '';
                        break;
                    }

                    $sheet = new \stdClass();
                    $sheet->time_id = $item->time_id;
                    $sheet->tita_id = $item->tita_id;
                    $sheet->task_id = $item->task_id;
                    $sheet->role_id = $item->role_id;
                    $sheet->supervisor_id = $item->supervisor_id;
                    $sheet->task_name = $item->task_name;
                    $sheet->tita_start = $item->tita_start;
                    $sheet->tita_finish = $item->tita_finish;
                    $sheet->tita_break_duration = $item->tita_break_duration;
                    $sheet->processed = $processed;
                    $sheet->public_holiday = $item->public_holiday;
                    $sheet->locked = $locked || ($item->processed != 0);
                    $entry->absent = $absent;
                    $sheets[] = $sheet;
                }
                $entry->timesheets = $sheets;
            } else {
                $sheet = new \stdClass();
                $sheet->time_id = null;
                $sheet->tita_id = null;
                $sheet->task_id = null;
                $sheet->role_id = null;
                $sheet->supervisor_id = $worker->supervisor_id;
                $sheet->task_name = null;
                $sheet->tita_start = null;
                $sheet->tita_finish = null;
                $sheet->tita_break_duration = null;
                $sheet->processed = '';
                $sheet->public_holiday = 0;
                $sheet->locked = $locked;
                $entry->absent = $absent;
                $entry->timesheets = array($sheet);
            }
            $timesheets[] = $entry;
        }

        $tc = new TasksController();
        $tasks = $tc->getFullSelectListForJob($jobs_id);

		$supers = DB::table('users')
			->join('job_superlist', 'job_superlist.user_id', '=', 'users.id')
			->where('job_superlist.jobs_id', '=', $jobs_id)
			->select('users.given_name', 'users.surname', 'users.id')
			->get();

		$roles = DB::table('roles')
			->where('jobs_id', '=', $jobs_id)
			->get();

		$user = Auth::user();

        return view('timesheets.editform', [
            'timesheets' => $timesheets,
            'tasks' => $tasks,
			'supers' => $supers,
			'roles' => $roles,
			'user' => $user,
            'locked' => $locked,
            'jobs_id' => $jobs_id
        ]);
    }

    public function saveDay(Request $request) {
        //echo '<pre>'; var_dump($_POST); die('</pre>');
        //loop through all of these and make up some better objects
        $times = array();

        foreach($request->time_id as $key => $time) {
            //don't worry about those without a task or start date
            if(strlen($request->tita_start[$key]) > 0 && strlen($request->task_id[$key]) > 0) {
            	$newtime = array(
                    'user_id' => $request->user_id[$key],
                    'time_id' => $request->time_id[$key],
                    'tita_id' => $request->tita_id[$key],
                    'task_id' => $request->task_id[$key],
                    'role_id' => $request->role_id[$key],
                    'tita_start' => $request->tita_start[$key],
                    'tita_break_duration' => is_numeric($request->tita_break_duration[$key]) ? $request->tita_break_duration[$key] : 0,
					'supervisor_id' => $request->supervisor_id[$key],
					'public_holiday' => isset($request->public_holiday[$key]) ? 1 : 0,
                    'jobs_id' => $request->jobs_id
                );

				//We might need to edit them in the middle of the day.
				if (strlen($request->tita_finish[$key]) > 0) {
					$newtime['tita_finish'] = $request->tita_finish[$key];
				}
                $times[] = $newtime;
            }
        }

        //echo '<pre>'; var_dump($times); echo '</pre>'; //die();

        foreach($times as $time) {
            $start_time = date ('Y-m-d H:i', strtotime($time['tita_start']));
            $end_time = (isset($time['tita_finish']) ? date('Y-m-d H:i', strtotime($time['tita_finish'])) : null);
            $break_duration = $time['tita_break_duration'];
			$supervisor_id = $time['supervisor_id'];
			$public_holiday = $time['public_holiday'];

			//work out the rate id and save it.
            $rates = DB::table('roles')
                ->join('payrates', 'payrates.id', '=', 'roles.rate_id')
                ->where('roles.role_id', '=', $time['role_id'])
                ->get();

            if(count($rates) > 0) {
                $rate = $rates[0];
                $rate_id = $rate->rate_id;
                if($rate->rate_parent_id != null) {
                    $jws = DB::table('job_worklist')
                        ->where('user_id', '=', $time['user_id'])
                        ->where('jobs_id', '=', $request->jobs_id)
                        ->where('startdate', '>', '2000-01-01')
                        ->orderBy('startdate')
                        ->get();
                    if(count($jws) > 0) {
                        $sdate = strtotime($jws[0]->startdate);
                        $fdate = strtotime(date ('Y-m-d H:i', strtotime($time['tita_start'])));

                        $diff = floor(($fdate - $sdate) / (60 * 60 * 24));

                        if($diff > $rate->rate_days) {
                            $rate_id = $rate->rate_parent_id;
                        }
                    }
                }
            }

            //echo $start_time.'<br />'.$end_time.'<br />'.$break_duration;

            $difference = 0;
            $hours_ordinary = 0;
            $hours_overtime = 0;
            $hours_saturday = 0;
            $hours_sunday = 0;
            $hours_publicholiday = 0;
            if($end_time != null) {
                $difference = (abs((strtotime($end_time) - strtotime($start_time)) / 60) - $break_duration) / 60;

                //work out some hours
                if ($time['public_holiday'] == 1) {
                    $rules = DB::table('payrate_rules')
                        ->where('payrate_rules.rate_id', '=', $rate_id)
                        ->where('payrate_rules.paru_frequency', '=', 6)
                        ->select('paru_hours')
                        ->get();
                    $rule = 0;
                    if (count($rules) > 0) {
                        $rule = $rules[0]->paru_hours;
                    }
                    $hours_publicholiday = max($difference, $rule);
                    $difference = $hours_publicholiday;
                } elseif (date('N', strtotime($time['tita_start'])) == 6) {
                    $rules = DB::table('payrate_rules')
                        ->where('payrate_rules.rate_id', '=', $rate_id)
                        ->where('payrate_rules.paru_frequency', '=', 4)
                        ->select('paru_hours')
                        ->get();
                    $rule = 0;
                    if (count($rules) > 0) {
                        $rule = $rules[0]->paru_hours;
                    }
                    //how many days have they worked this week?
                    $days = DB::table('time_tasks')
                        ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                        ->where('timesheets.user_id', '=', $time['user_id'])
                        ->where('time_tasks.tita_start', '>=', date('Y-m-d', strtotime('last monday ' . $time['tita_start'])))
                        ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($time['tita_start'])))
                        ->select(DB::raw('DATE(time_tasks.tita_start)'))
                        ->distinct()
                        ->count();
                    //echo $time['user_id'].' days: '.$days.'; rule: '.$rule.';';
                    if ($days >= $rule) {
                        //They get overtime
                        $hours_saturday = $difference;
                    } else {
                        $hours_ordinary = $difference;
                    }
                } elseif (date('N', strtotime($time['tita_start'])) == 7) {
                    $rules = DB::table('payrate_rules')
                        ->where('payrate_rules.rate_id', '=', $rate_id)
                        ->where('payrate_rules.paru_frequency', '=', 5)
                        ->select('paru_hours')
                        ->get();
                    $rule = 0;
                    if (count($rules) > 0) {
                        $rule = $rules[0]->paru_hours;
                    }
                    $hours_sunday = max($difference, $rule);
                    $difference = $hours_sunday;
                } else {
                    //Have they exceed their daily, weekly or monthly allocation?
                    $daily = 10000;
                    $weekly = 10000;
                    $monthly = 10000;

                    $hours_ordinary = $difference;

                    $rules = DB::table('payrate_rules')
                        ->where('payrate_rules.rate_id', '=', $rate_id)
                        ->where('payrate_rules.paru_frequency', '<', 4)
                        ->select('paru_hours', 'paru_frequency')
                        ->get();

                    foreach ($rules as $rule) {
                        switch ($rule->paru_frequency) {
                            case 0:
                                $daily = $rule->paru_hours;
                                if ($difference > $daily) {
                                    $hours_ordinary = $daily;
                                    $hours_overtime = $difference - $daily;
                                }
                                break;
                            case 1:
                                $weekly = $rule->paru_hours;
                                $days = DB::table('time_tasks')
                                    ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                                    ->where('timesheets.user_id', '=', $time['user_id'])
                                    ->where('time_tasks.tita_start', '>=', date('Y-m-d', strtotime('last monday ' . $time['tita_start'])))
                                    ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($time['tita_start'])))
                                    ->select(DB::raw('SUM(hours_ordinary) as tita_quantity'))
                                    ->get();
                                $hours_this_week = 0;
                                if ($days) {
                                    $hours_this_week = $days[0]->tita_quantity;
                                }

                                if (($hours_this_week + $difference) >= $weekly) {
                                    $hours_ordinary = $hours_this_week - $weekly;
                                    $hours_overtime = ($hours_this_week + $difference) - $weekly;
                                }
                                break;
                            case 3:
                                $monthly = $rule->paru_hours;
                                $date = DB::table('job_worklist')
                                    ->join('tasks', 'tasks.jobs_id', '=', 'job_worklist.jobs_id')
                                    ->where('user_id', '=', $time['user_id'])
                                    ->where('tasks.task_id', '=', $time['task_id'])
                                    ->select('job_worklist.startdate')
                                    ->get();

                                if (count($date) > 0) {
                                    $startdate = $date[0]->startdate;
                                }
                                $periods = array();
                                $startdate = strtotime($startdate);
                                while ($startdate < time()) {
                                    $periods[] = date('Y-m-d', $startdate);
                                    $startdate = strtotime(date('Y-m-d', $startdate) . ' +28 days');
                                }

                                //var_dump($periods);
                                $date = end($periods);
                                $days = DB::table('time_tasks')
                                    ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                                    ->where('timesheets.user_id', '=', $time['user_id'])
                                    ->where('time_tasks.tita_start', '>=', $date)
                                    ->where('time_tasks.tita_start', '<', date('Y-m-d', strtotime($time['tita_start'])))
                                    ->select(DB::raw('SUM(hours_ordinary) as tita_quantity'))
                                    ->get();

                                $overtime = 0;
                                $ordinary = $difference;

                                if (count($days) > 0) {
                                    $this_month = $days[0]->tita_quantity;

                                    if (($this_month + $difference) >= $monthly) {
                                        $hours_ordinary = $monthly - $this_month;
                                        $hours_overtime = ($this_month + $difference) - $monthly;
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            //echo 'ordinary: '.$hours_ordinary.'; overtime: '.$hours_overtime.'<br />';

            $user = Auth::user();

            if(strlen($time['tita_id']) > 0) {
                //update
                DB::table('time_tasks')
                    ->where('tita_id', '=', $time['tita_id'])
                    ->update([
                        'tita_start' => $start_time,
                        'tita_finish' => $end_time,
						'task_id' => $time['task_id'],
						'role_id' => $time['role_id'],
                        'rate_id' => $rate_id,
                        'tita_break_duration' => $break_duration,
                        'tita_quantity' => $difference,
						'supervisor_id' => $supervisor_id,
						'signed_in_by' => $user->id,
						'public_holiday' => $public_holiday,
						'hours_ordinary' => $hours_ordinary,
						'hours_overtime' => $hours_overtime,
						'hours_saturday' => $hours_saturday,
						'hours_sunday' => $hours_sunday,
						'hours_publicholiday' => $hours_publicholiday
			        ]);

				$this->copyToHistory($time['tita_id']);
            } elseif (strlen($time['time_id']) > 0) {
                //update TS, insert tita
                DB::table('timesheets')
                    ->where('time_id', '=', $time['time_id'])
                    ->update([
                        'jobs_id' => $request->jobs_id,
                        'time_start' => $start_time,
                        'time_end' => ($end_time == null ? $start_time : $end_time),
                        'approved_user' => 1,
                        'approved_super' => 1
                    ]);

                $tita_id = DB::table('time_tasks')
                    ->insertGetId([
                        'time_id' => $time['time_id'],
						'task_id' => $time['task_id'],
						'role_id' => $time['role_id'],
                        'rate_id' => $rate_id,
                        'tita_start' => $start_time,
                        'tita_finish' => $end_time,
                        'tita_break_duration' => $break_duration,
                        'tita_quantity' => $difference,
						'supervisor_id' => $supervisor_id,
						'signed_in_by' => $user->id,
						'public_holiday' => $public_holiday,
                        'hours_ordinary' => $hours_ordinary,
                        'hours_overtime' => $hours_overtime,
                        'hours_saturday' => $hours_saturday,
                        'hours_sunday' => $hours_sunday,
                        'hours_publicholiday' => $hours_publicholiday
                    ]);

				$this->copyToHistory($tita_id);
            } else {
                //insert both
                $time_id = DB::table('timesheets')
                    ->insertGetId([
                        'jobs_id' => $request->jobs_id,
                        'user_id' => $time['user_id'],
                        'time_start' => $start_time,
                        'time_end' => ($end_time == null ? $start_time : $end_time),
                        'approved_user' => 1,
                        'approved_super' => 1
                    ]);

                $tita_id = DB::table('time_tasks')
                    ->insertGetId([
                        'time_id' => $time_id,
                        'task_id' => $time['task_id'],
                        'role_id' => $time['role_id'],
                        'rate_id' => $rate_id,
                        'tita_start' => $start_time,
                        'tita_finish' => $end_time,
                        'tita_break_duration' => $break_duration,
                        'tita_quantity' => $difference,
						'supervisor_id' => $supervisor_id,
						'signed_in_by' => $user->id,
						'public_holiday' => $public_holiday,
                        'hours_ordinary' => $hours_ordinary,
                        'hours_overtime' => $hours_overtime,
                        'hours_saturday' => $hours_saturday,
                        'hours_sunday' => $hours_sunday,
                        'hours_publicholiday' => $hours_publicholiday
                    ]);

				$this->copyToHistory($tita_id);
            }

            //If they were absent, we need to remove that.
            DB::table('user_absence')
                ->where('user_id', '=', $time['user_id'])
                ->where(DB::raw('DATE(timestamp)'), '=', date('Y-m-d', strtotime($start_time)))
                ->delete();

            //echo 'time '.$time['user_id'].'<br />';
            //echo date('Y-m-d', strtotime($start_time)).'<br /><br />';
        }

        return redirect('timesheets')->with('success', 'The timesheets have been updated successfully.');
    }

	private function copyToHistory($tita_id) {
		DB::insert(DB::raw('insert into tita_history (time_id, tita_id, task_id, role_id, supervisor_id, signed_in_by, tita_start, tita_finish, tita_break_duration, tita_quantity, public_holiday, hours_ordinary, hours_overtime, hours_saturday, hours_sunday, hours_publicholiday) 
select time_id, tita_id, task_id, role_id, supervisor_id, signed_in_by, tita_start, tita_finish, tita_break_duration, tita_quantity, public_holiday, hours_ordinary, hours_overtime, hours_saturday, hours_sunday, hours_publicholiday from time_tasks where tita_id = '.$tita_id));
	}
}