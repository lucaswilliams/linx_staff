<?php namespace App\Http\Controllers\Settings;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UniformsController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $uniforms = DB::table('uniforms')
            ->select('*')
            ->get();
        //Display it as a list.
        return view('settings.uniforms.list', ['uniforms' => $uniforms]);
    }

    /**
     * Show the form for the given rateential.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesUniformExist = DB::table('uniforms')
            ->where('id', '=', $id)
            ->get();
        if($id > 0 && count($doesUniformExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified uniform does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $uni = DB::table('uniforms')
            ->select('*')
            ->where('id', '=', $id)
            ->get();

        if(count($uni) > 0) {
            return view('settings.uniforms.form', ['uniform' => $uni[0]]);
        } else {
            return view('settings.uniforms.form', ['uniform' => (object)array(
                'id' => null,
                'name' => null
            )]);
        }
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($request->uniform_id > 0) {
            $uni_id = $request->uniform_id;


            if($validator->fails()) {
                return redirect('settings/uniforms/'.$uni_id)->withErrors($validator)->withInput();
            }

            DB::table('uniforms')
                ->where('id', $uni_id)
                ->update([
                    'name' => $request->name
                ]);

            return redirect('settings/uniforms/'.$uni_id)->with('success', 'Your uniform has updated successfully');
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('settings/uniforms/add')->withErrors($validator)->withInput();
            }

            DB::table('uniforms')
                ->insert([
                    'name' => $request->name
                ]);
            return redirect('settings/uniforms')->with('success', 'Your uniform has been added successfully');
        }
    }

    public function delete($id)
    {
        DB::table('uniforms')
            ->where('id', $id)
            ->delete();
        //Display it as a list.
        return redirect('settings/uniforms')->with('success', 'The uniform has been deleted');
    }
}