<?php namespace App\Http\Controllers\Settings;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpController extends Controller {

    public function showHelp()
    {
        $user = \Auth::user();
        $super = (DB::table('job_superlist')->where('user_id', '=', $user->id)->count() > 0) || (DB::table('client_users')->where('user_id', '=', $user->id)->where('level', '=', 1)->count() > 0);
        $admin = ($user->level == 3);

        $helpQ = DB::table('help_article')->where('access', '&', 1);
        if($super) {
            $helpQ = DB::table('help_article')->where('access', '&', 1)->where('access', '&', 2);
        }

        if($admin) {
            $helpQ = DB::table('help_article');
        }

        $helps = $helpQ->get();

        echo '<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="helpModalLabel">Timesheet Management System Help</h4>
		</div>
		<div class="modal-body" data-spy="scroll" data-target="#helpText">
			<div class="row">
				<div class="col-xs-12 col-sm-9" id="helpText">';
			if(count($helps) > 0) {
				foreach($helps as $help) {
					echo '<a name="help'.$help->help_id.'"></a><h1>'.$help->title.'</h1>';
					echo $help->content;
				}
			}
		echo '		</div>
			<div class="col-xs-12 col-sm-3" id="helpContents">
					<ul id="helpScroller">';
			if(count($helps) > 0) {
				foreach($helps as $help) {
					echo '<li><a href="#help'.$help->help_id.'">'.$help->title.'</a></li>';
				}
			}
		echo '</ul>
				</div>
			</div>
		</div>';
    }

    public function delete($id) {
        DB::table('help_article')
            ->where('help_id', '=', $id)
            ->delete();

        return redirect('/settings/help');
    }

    public function showHelpList()
    {
        $helps = DB::table('help_article')
			->orderBy('order')
            ->get();

        return View('settings.help.list', ['helps' => $helps]);
    }

    public function showHelpForm($id)
    {
        $helps = DB::table('help_article')
            ->where('help_id', '=', $id)
            ->get();

        if(count($helps) > 0) {
            $help = $helps[0];
        } else {
            $help = (object)array(
                'help_id' => 0,
                'title' => '',
                'content' => '',
                'order' => 0,
                'access_level' => 0
            );
        }

        return View('settings.help.form', ['help' => $help]);
    }

    public function saveHelpForm(Request $request)
    {
        $access = 0;
        foreach($request->access_level as $ac) {
            $access += $ac;
        }

        if($request->id == 0) {
            DB::table('help_article')
                ->insert([
                    'title' => $request->title,
                    'content' => $request->textcontent,
                    'order' => $request->order,
                    'access_level' => $access
                ]);
        } else {
            DB::table('help_article')
                ->where('help_id', '=', $request->id)
                ->update([
                    'title' => $request->title,
                    'content' => $request->textcontent,
                    'order' => $request->order,
                    'access_level' => $access
                ]);
        }
        return redirect('/settings/help');
    }
}