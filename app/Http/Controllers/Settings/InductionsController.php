<?php namespace App\Http\Controllers\Settings;

use App\Http\Controllers\UserController;
use Auth;
use DB;
use Validator;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InductionsController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $inductions = DB::table('inductions')
            ->select('*')
            ->get();
        //Display it as a list.
        return view('settings.inductions.list', ['inductions' => $inductions]);
    }

    /**
     * Show the form for the given rateential.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesinductionExist = DB::table('inductions')
            ->where('ind_id', '=', $id)
            ->get();
        if($id > 0 && count($doesinductionExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified induction does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $induction = DB::table('inductions')
            ->select('*')
            ->where('ind_id', '=', $id)
            ->get();

        if(count($induction) > 0) {
            return view('settings.inductions.form', [
                'induction' => $induction[0]
            ]);
        } else {
            return view('settings.inductions.form', ['induction' => (object)array(
                'ind_id' => null,
                'ind_name' => null,
                'ind_content' => null
            )]);
        }
    }

    public function showQuestionForm($id)
    {
        $induction = DB::table('inductions')
            ->select('*')
            ->where('ind_id', '=', $id)
            ->get();

        $questions = DB::table('questions')
            ->where('ind_id', '=', $id)
            ->get();

        $q = array();
        foreach($questions as $question) {
            $options = DB::table('options')
                ->where('ques_id', '=', $question->ques_id)
                ->get();
            $q[] = (object)array(
                'ques_id' => $question->ques_id,
                'ques_text' => $question->ques_text,
                'options' => $options
            );
        }

        return view('settings.inductions.questionform', [
            'induction' => $induction[0],
            'questions' => (object)$q
        ]);
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'ind_name' => 'required',
            'ind_content' => 'required'
        ]);

        if ($request->ind_id > 0) {
            $induction_id = $request->ind_id;


            if($validator->fails()) {
                return redirect('settings/inductions/'.$induction_id)->withErrors($validator)->withInput();
            }

            DB::table('inductions')
                ->where('ind_id', $induction_id)
                ->update([
                    'ind_name' => $request->ind_name,
                    'ind_content' => $request->ind_content
                ]);
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('settings/inductions/add')->withErrors($validator)->withInput();
            }

            $induction_id = DB::table('inductions')
                ->insertGetId([
                    'ind_name' => $request->ind_name,
                    'ind_content' => $request->ind_content
                ]);
        }

        return redirect('settings/inductions/'.$induction_id.'/questions');
    }

    public function saveQuestionForm() {
        return redirect('settings/inductions')->with('success', 'Your changes have been saved');
    }

    public function saveQuestion(Request $request) {
        if($request->ques_id > 0) {
            DB::table('questions')
                ->where('ques_id', '=', $request->ques_id)
                ->update([
                    'ques_text' => $request->ques_text
                ]);
        } else {
            DB::table('questions')
                ->insert([
                    'ques_text' => $request->ques_text,
                    'ind_id' => $request->ind_id
                ]);
        }
    }

    public function saveAnswer(Request $request) {
        if($request->opt_correct == 1) {
            DB::table('options')
                ->where('ques_id', '=', $request->ques_id)
                ->update(['opt_correct' => 0]);
        }

        if($request->opt_id > 0) {
            DB::table('options')
                ->where('opt_id', '=', $request->opt_id)
                ->update([
                    'opt_text' => $request->opt_text,
                    'opt_correct' => ($request->opt_correct == 1 ? 1 : 0)
                ]);
        } else {
            DB::table('options')
                ->insert([
                    'opt_text' => $request->opt_text,
                    'opt_correct' => ($request->opt_correct == 1 ? 1 : 0),
                    'ques_id' => $request->ques_id
                ]);
        }
    }

    public function deleteQuestion($ques_id) {
        DB::table('questions')
            ->where('ques_id', '=', $ques_id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function deleteAnswer($opt_id) {
        DB::table('options')
            ->where('opt_id', '=', $opt_id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function saveUserInduction(Request $request) {
        $user = Auth::user();

        $questionCount = 0;
        $correctCount = 0;
        if(isset($request->opt_id)) {
            foreach($request->opt_id as $ques_id => $opt_id) {
                $correctQ = DB::table('options')
                    ->where('ques_id', '=', $ques_id)
                    ->where('opt_correct', '=', 1)
                    ->get()[0];

                $questionCount++;
                $correct = 0;
                if($opt_id == $correctQ->opt_id) {
                    $correctCount++;
                    $correct = 1;
                }

                DB::table('usinques')
                    ->insert([
                        'usin_id' => $request->usin_id,
                        'ques_id' => $ques_id,
                        'opti_id' => $opt_id,
                        'correct' => $correct
                    ]);
            }

            DB::table('userinductions')
                ->where('usin_id', '=', $request->usin_id)
                ->update(['usin_completed' => date('Y-m-d H:i:s')]);

            if($questionCount == $correctCount) {
                DB::table('users')
                    ->where('id', '=', $user->id)
                    ->update(['confirmed' => 4]);

                $uc = new UserController();
                //$uc->createInMYOB($user->id);
            } else {
                //If the number of questions correct is not the same as the number of questions, another induction is required.
                DB::table('userinductions')
                    ->insert([
                        'user_id' => $user->id,
                        'ind_id' => $request->ind_id,
                        'usin_created' => date('Y-m-d H:i:s')
                    ]);
            }

            try {
                Mail::send('emails.inducted', ['user' => $user], function ($message) use ($user) {
                    $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                    $message->to($user->email);
                    $message->subject('Linx Employment induction');
                });
            } catch(Exception $ex) {
                //Ignore
            }

            return redirect('/dashboard');
        } else {
            return redirect('/dashboard')->with('error', 'You didn\'t answer any questions');
        }
    }

    public function delete($id)
    {
        DB::table('inductions')
            ->where('id', $id)
            ->delete();
        //Display it as a list.
        return redirect('settings/inductions')->with('success', 'The induction has been deleted');
    }

    public function getInductions() {
        return DB::table('inductions')
            ->get();
    }
}