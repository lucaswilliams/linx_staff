<?php

/**
 * AccountRightV2 API class
 *
 * Development Center AccountRight Live API v2: http://developer.myob.com/api/accountright/v2/
 *
 * @author Lucas Williams <http://lucas-williams.com>
 * @version 1.0
 * @description Base code provided from Michael Kimpton <https://github.com/sketchthat>
 *
 */
class AccountRightV2 {
    /**
     * The API OAuth URL.
     */
    const API_OAUTH_URL = 'https://secure.myob.com/oauth2/account/authorize';

    /**
     * The API Token Authorize URL.
     */
    const API_TOKEN_URL = 'https://secure.myob.com/oauth2/v1/authorize';

    public $proxy;

    /**
     * The MYOB API Key.
     *
     * @var string
     */
    private $_apikey;

    /**
     * The MYOB OAuth API secret.
     *
     * @var string
     */
    private $_apisecret;

    /**
     * The callback URL.
     *
     * @var string
     */
    private $_callbackurl;

    /**
     * The user access token.
     *
     * @var string
     */
    private $_accesstoken;

    /**
     * The oauth scope.
     *
     * @var string
     */
    private $_scope;

    /**
     * The API URI
     *
     * @var string
     */
    private $_uri;

    /**
     * The CompanyFile GUID.
     *
     * @var string
     */
    private $_guid;

    /**
     * The user Username.
     *
     * @var string
     */
    private $_username;

    /**
     * The user Password.
     *
     * @var string
     */
    private $_password;

    /**
     * The location of a saved object.
     *
     * @var string
     */
    private $_location;

    private $db;

    /**
     *  The API Methods.
     */
    const GET = 0;
    const PUT = 1;
    const POST = 2;
    const DELETE = 3;

    public function __construct($config, $db = null) {
        if(is_array($config)) {
            if(isset($config['apiKey'])) {
                $this->_apikey = $config['apiKey'];
            }

            if(isset($config['apiSecret'])) {
                $this->_apisecret = $config['apiSecret'];
            }

            if(isset($config['apiCallback'])) {
                $this->_callbackurl = $config['apiCallback'];
            }

            if(isset($config['apiScope'])) {
                $this->_scope = $config['apiScope'];
            } else {
                $this->_scope = 'CompanyFile';
            }

            if(isset($config['username'])) {
                $this->_username = $config['username'];
            }

            if(isset($config['password'])) {
                $this->_password = $config['password'];
            }

            if(isset($db)) {
                $this->db = $db;
            }
        } else {
            throw new \Exception('Error: __construct() - Configuration data is invalid.');
        }
    }

    /**
     * Generates the OAuth login URL.
     *
     * @return string MYOB OAuth login URL
     */
    public function getLoginUrl() {
        $params = array(
            'client_id' => $this->_apikey,
            'redirect_uri' => $this->_callbackurl,
            'response_type' => 'code',
            'scope' => $this->_scope
        );

        return self::API_OAUTH_URL.'?'.http_build_query($params);
    }

    /**
     * Gets Access Token from MYOB.
     *
     * @return string MYOB Access Token
     */
    public function getAccessToken($accessToken) {
        $params = array(
            'client_id' => $this->_apikey,
            'client_secret' => $this->_apisecret,
            'scope' => $this->_scope,
            'code' => $accessToken,
            'redirect_uri' => $this->_callbackurl,
            'grant_type' => 'authorization_code'
        );

        $json = $this->_makeRequest(self::API_TOKEN_URL, $params);
        //echo '<pre>'; var_dump($json); echo '</pre>';

        return $this->saveAccessToken($json);
    }

    /**
     * Refreshes Access Token from MYOB.
     *
     * @return string MYOB Access Token
     */
    public function refreshToken($refreshToken) {
        $params = array(
            'client_id' => $this->_apikey,
            'client_secret' => $this->_apisecret,
            'refresh_token' => $refreshToken,
            'grant_type' => 'refresh_token'
        );

        $json = $this->_makeRequest(self::API_TOKEN_URL, $params);

        //var_dump($json);

        return $this->saveAccessToken($json);
    }

    /**
     * Saves the Access Token from MYOB in Sessions
     *
     * @return json Access Token
     */
    public function saveAccessToken($json) {
        if(isset($json->error)) {
            throw new \Exception($json->error);
        }

        if(isset($json->Errors)) {
            throw new \Exception(print_r($json->Errors, true));
        }

        if(!isset($json->access_token)) {
            throw new \Exception("No data was returned from the MYOB API.");
        }

        //$_SESSION['access_token'] = $json->access_token;
        //$_SESSION['refresh'] = $json->refresh_token;

        $date = time() + $json->expires_in;

        //$_SESSION['expires'] = $date;

        if(isset($this->db)) {
            $this->db->query('update myob set access_token = \''.$json->access_token.'\', refresh_token = \''.$json->refresh_token.'\', token_expire = \''.date('Y-m-d H:i:s', $date).'\'');
        } else {
            DB::table('myob')
                ->update([
                    'access_token' => $json->access_token,
                    'refresh_token' => $json->refresh_token,
                    'token_expire' => date('Y-m-d H:i:s', $date)
                ]);
        }

        $this->retriveAccessToken();

        return $json;
    }

    /**
     * Gets the Access Token for MYOB from Sessions
     *
     * @return bool
     */
    public function retriveAccessToken() {
        if(isset($this->db)) {
            $row = $this->db->query('select * from myob');
            $myob = $row->fetch_object();
        } else {
            $myob = DB::table('myob')
                ->get();

            $myob = $myob[0];
        }

        $this->_uri = $myob->myob_uri;
        if(strlen($this->_uri) == 0) {
            $this->_uri = 'https://api.myob.com/accountright/';
        }
        if($myob->myob_testing == 1) {
            if(isset($myob->cf_guid)) {
                $this->_guid = $myob->cf_guid;
            }

            $this->proxy = true;

            //return true;
        } else {
            $this->proxy = false;
        }

        if(isset($myob->access_token) && $myob->access_token != null) {
            $currentDate = time();

            //echo 'current time: '.$currentDate.'; expire time: '.strtotime($myob->token_expire);

            if(strtotime($myob->token_expire) < $currentDate) {
                $this->refreshToken($myob->refresh_token);
            }

            $this->_accesstoken = $myob->access_token;

            if(isset($myob->cf_guid)) {
                $this->_guid = $myob->cf_guid;
            }

            return true;
        }

        //throw new \Exception('Error: retriveAccessToken() - Session invalid.');
    }

    /**
     * Gets the Company File and sets the guid
     *
     * @return bool
     */
    public function getCompanyFile() {
        $this->_guid = '';

        $companyFiles = $this->_makeGetRequest();

        return $companyFiles;
    }

    /**
     * Returns the GUID
     *
     * @return string
     */
    public function getGuid() {
        return $this->_guid;
    }

    /**
     * Makes curl requests to MYOB for core authentication actions
     *
     * @return json object
     */
    private function _makeRequest($url, $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if($this->proxy) {
            curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
        }

        $response = curl_exec($ch);

        curl_close($ch);

        $json_response = json_decode($response);
        //var_dump($json_response);
        /*DB::table('myob_log')
            ->insert([
                'url' => $url,
                'params' => json_encode($params),
                'response' => $response,
                'timestamp' => date('Y-m-d H:i:s')
            ]);*/
        return $json_response;
    }

    /**
     * Makes curl requests to MYOB for most actions
     *
     * @return json object or string
     */
    private function _doCurl($function = '', $method = self::GET, $data = array())
    {
        $this->retriveAccessToken();

        //Log::info('API Data for '.$function.': '.print_r($data, true));

        $url = 'https://api.myob.com/accountright/';
        $headers = array(
            'Authorization: Bearer ' . $this->_accesstoken,
            'x-myobapi-key: ' . $this->_apikey,
            'x-myobapi-version: v2',
        );

        if ($this->_guid != '') {
            $url .= $this->_guid . '/';

            array_push($headers, 'x-myobapi-cftoken: ' . base64_encode($this->_username . ':' . $this->_password));
        }

        $url .= $function;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        if ($method == self::POST) {
            curl_setopt($ch, CURLOPT_POST, true);

            $data_string = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

            array_push($headers, 'Content-Type: application/json');
            array_push($headers, 'Content-Length: ' . strlen($data_string));

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        } elseif ($method == self::GET) {
            if (is_array($data) && !empty($data)) {
                $url .= '?json=' . urlencode(json_encode($data));
            } elseif (!is_array($data) && strlen($data) > 0) {
                $url .= '/' . $data;
            }
        } elseif ($method == self::PUT) {
            if (isset($data['UID'])) {
                $url .= '/' . $data['UID'];
            }
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

            $data_string = json_encode($data);
            //echo $data_string;
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

            array_push($headers, 'Content-Type: application/json');
            array_push($headers, 'Content-Length: ' . strlen($data_string));
        }

        //echo $method.': '.$url.'<br>';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if ($this->proxy) {
            curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
        }

        $jsonData = curl_exec($ch);

        if (strpos($jsonData, 'HTTP/1.1 401 Unauthorized') !== false) {
            die('Bad access token');
            $this->retriveAccessToken();
        }

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($jsonData, 0, $header_size);
        $body = substr($jsonData, $header_size);

        $this->_location = '';

        if (preg_match('/Location: ' . str_replace('/', '\/', $url) . '\/(([a-zA-Z0-9]{8})-([a-z0-9]{4})-([a-z0-9]{4})-([a-z0-9]{4})-([a-z0-9]{12}))/i', $header, $pregs)) {
            if (isset($pregs[1])) {
                $this->_location = $pregs[1];
            }
        }

        $headers = $this->get_headers_from_curl_response($jsonData);

        curl_close($ch);

        if($method == self::POST) {
            if ($this->isJson($body)) {
                $json = json_decode($body);
                $return = array('header' => $headers, 'body' => $json);
            } else {
                $return = array('header' => $headers, 'body' => $body);
            }
        } else {
            if ($this->isJson($body)) {
                $json = json_decode($body);
                $return = $json;
            } else {
                $return = $body;
            }
        }

        //var_dump($return);
        return $return;
    }

    function get_headers_from_curl_response($response)
    {
        $headers = array();

        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

        foreach (explode("\r\n", $header_text) as $i => $line)
            if ($i === 0)
                $headers['http_code'] = $line;
            else
            {
                list ($key, $value) = explode(': ', $line);

                $headers[$key] = $value;
            }

        return $headers;
    }

    /**
     * Makes GET curl requests to MYOB for most actions
     *
     * @return json object or string
     */
    private function _makeGetRequest($function = '', $data = array()) {
        $req = $this->_doCurl($function, self::GET, $data);
        return $req;
    }

    /**
     * Makes POST curl requests to MYOB for most actions
     *
     * @return json object or string
     */
    private function _makePostRequest($function = '', $data) {
        $req = $this->_doCurl($function, self::POST, $data);
        return $req;
    }

    /**
     * Makes PUT curl requests to MYOB for most actions
     *
     * @return json object or string
     */
    private function _makePutRequest($function = '', $data) {
        $req = $this->_doCurl($function, self::PUT, $data);
        return $req;
    }

    /**
     * Checks if a string is JSON or something else
     *
     * @return bool
     */
    private function isJson($string) {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     *  @return string
     */
    public function getLocation() {
        return $this->_location;
    }

    /**
     *  Returns company information details for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/company/
     *
     *  @return json
     */
    public function Company() {
        return $this->_makeGetRequest('Company');
    }

    /**
     *  Return all contact types for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/contact/
     *
     *  @return json
     */
    public function Contact($data = array()) {
        return $this->_makeGetRequest('Contact', $data);
    }

    /**
     *  Return all employee contacts for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/contact/employee
     *
     *  @return json
     */
    public function ContactEmployee($data = array(), $method = 'GET') {
        switch($method) {
            case 'GET' :
                return $this->_makeGetRequest('Contact/Employee', $data);
                break;
            case 'POST' :
                return $this->_makePostRequest('Contact/Employee', $data);
                break;
            case 'PUT' :
                return $this->_makePutRequest('Contact/Employee', $data);
                break;
        }
    }

    public function getTimesheetsForEmployee($uid) {
        $startyear = ((int)date('Y')) - 2;
        return $this->_makeGetRequest('Payroll/Timesheet/'.$uid.'?StartDate='.$startyear.'-01-01T00:00:00&EndDate='.date('Y').'-12-31T00:00:00');
    }

    /**
     *  Return all employee contact payment details for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/contact/employee
     *
     *  @return json
     */
    public function ContactEmployeePayment($data = array(), $method = 'GET') {
        switch($method) {
            case 'GET' :
                return $this->_makeGetRequest('Contact/EmployeePaymentDetails', $data);
                break;
            case 'POST' :
                return $this->_makePostRequest('Contact/EmployeePaymentDetails', $data);
                break;
            case 'PUT' :
                return $this->_makePutRequest('Contact/EmployeePaymentDetails', $data);
                break;
        }
    }

    /**
     *  Return all employee contact payroll for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/contact/employee
     *
     *  @return json
     */
    public function ContactEmployeePayroll($data = array(), $method = 'GET') {
        switch($method) {
            case 'GET' :
                return $this->_makeGetRequest('Contact/EmployeePayrollDetails', $data);
                break;
            case 'POST' :
                return $this->_makePostRequest('Contact/EmployeePayrollDetails', $data);
                break;
            case 'PUT' :
                return $this->_makePutRequest('Contact/EmployeePayrollDetails', $data);
                break;
        }
    }

    /**
     *  Return all employee contact payroll for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/contact/employee-standard-pay/
     *
     *  @return json
     */
    public function ContactEmployeeStandard($data = array(), $method = 'GET') {
        switch($method) {
            case 'GET' :
                return $this->_makeGetRequest('Contact/EmployeeStandardPay', $data);
                break;
            case 'POST' :
                return $this->_makePostRequest('Contact/EmployeeStandardPay', $data);
                break;
            case 'PUT' :
                return $this->_makePutRequest('Contact/EmployeeStandardPay', $data);
                break;
        }
    }

    /**
     *  Return, update, create and delete a customer contact for an AccountRight company file
     *  http://developer.myob.com/api/accountright/v2/contact/customer/
     *
     *  @return json
     */
    public function ContactCustomer($data = array(), $method = 'GET') {
        switch($method) {
            case 'GET' :
                return $this->_makeGetRequest('Contact/Customer', $data);
                break;
            case 'POST' :
                return $this->_makePostRequest('Contact/Customer', $data);
                break;
            case 'PUT' :
                return $this->_makePutRequest('Contact/Customer', $data);
                break;
        }
    }

    /**
     * Returns the payroll categories for an AccountRight company file
     * http://developer.myob.com/api/accountright/v2/payroll/payroll-category/
     *
     * @return json
     */
    public function PayrollCategory($filter = '$filter=Type+eq+\'Wage\'&') {
        $ret = $this->_makeGetRequest('Payroll/PayrollCategory/?'.$filter.'$top=1000&skip=1000');
        return $ret;
    }

    /**
     * Returns the super funds for an AccountRight company file
     * http://developer.myob.com/api/accountright/v2/payroll/superannuationfund/
     *
     * @return json
     */
    public function SuperProvider() {
        return $this->_makeGetRequest('Payroll/SuperannuationFund');
    }

    /**
     * Returns the account codes for an AccountRight company file
     * http://developer.myob.com/api/accountright/v2/generalledger/account/
     */
    public function Accounts() {
        $accounts = $this->_makeGetRequest('GeneralLedger/Account');
        //echo '<pre>'; var_dump($accounts); echo '</pre>';
        return $accounts;
    }

    /**
     * Returns the tax codes for an AccountRight company file
     * http://developer.myob.com/api/accountright/v2/generalledger/taxcode/
     */
    public function TaxCodes() {
        return $this->_makeGetRequest('GeneralLedger/TaxCode');
    }

    /**
     * Saves timesheet entries to an AccountRight company file
     * http://developer.myob.com/api/accountright/v2/payroll/timesheet/
     */
    public function Timesheet($data) {
        echo 'Timesheet started: '.print_r($data, true).'<br>';
        $output = $this->_makePutRequest('Payroll/Timesheet', $data);
        echo 'Timesheet finished: '.print_r($output, true).'<br>';

        return $output;
    }

    public function Payslips($date) {

        return $this->_makeGetRequest('Report/Payroll/EmployeePayrollAdvice?$filter=PaymentDate+ge+DateTime\''.date('Y-m-d', strtotime($date)).'\'', array());
    }

    public function Jobs($data) {
        return $this->_makeGetRequest('/GeneralLedger/Job');
    }

    public function Invoice($data, $method = 'GET') {
        switch($method) {
            case 'POST' :
                return $this->_makePostRequest('Sale/Invoice/Service', $data);
                break;

            default :
                return $this->_makeGetRequest('Sale/Invoice/Service', $data);
                break;
        }
    }

    public function InvoiceItems() {
        return $this->_makeGetRequest('Inventory/Item');
    }

    public function SuperCategories($data) {
        return $this->_makeGetRequest('Payroll/PayrollCategory/Superannuation', $data);
    }

    public function TaxCategories($data) {
        return $this->_makeGetRequest('Payroll/PayrollCategory/Tax', $data);
    }

    public function TaxTables($data) {
        return $this->_makeGetRequest('Payroll/PayrollCategory/TaxTable', $data);
    }

    public function PayrollAdvice($data) {
    	return $this->_makeGetRequest('Report/Payroll/EmployeePayrollAdvice/', $data);
	}
}