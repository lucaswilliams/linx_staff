<?php namespace App\Http\Controllers\Settings;

use DB;
use Auth;
use View;
use Response;
use App\Http\Controllers\Settings\MYOBController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class MYOBLogController extends Controller {
    public function getList() {
        $logs = DB::table('myob_log')
            ->orderBy('timestamp')
            ->get();
        echo '<table width="100%">';
        echo '<tr>';
        echo '<td>timestamp</td>';
        echo '<td>url</td>';
        echo '<td>params</td>';
        echo '<td>response</td>';
        echo '</tr>';

        foreach($logs as $log) {
            echo '<tr>';
            echo '<td>'.$log->timestamp.'</td>';
            echo '<td>'.$log->url.'</td>';
            echo '<td>'.$log->params.'</td>';
            echo '<td>'.$log->response.'</td>';
            echo '</tr>';
        }

        echo '</table>';
    }

    public function purge() {
        DB::table('myob_log')
            ->delete();
    }
}