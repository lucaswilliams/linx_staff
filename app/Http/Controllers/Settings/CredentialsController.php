<?php namespace App\Http\Controllers\Settings;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CredentialsController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $credentials = DB::table('credentials')
            ->select('*')
            ->get();
        //Display it as a list.
        return view('settings.credentials.list', ['credentials' => $credentials]);
    }

    /**
     * Show the form for the given credential.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesCredentialExist = DB::table('credentials')
            ->where('id', '=', $id)
            ->get();
        if($id > 0 && count($doesCredentialExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified credential does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $cred = DB::table('credentials')
            ->select('*')
            ->where('id', '=', $id)
            ->get();

        if(count($cred) > 0) {
            return view('settings.credentials.form', ['cred' => $cred[0]]);
        } else {
            return view('settings.credentials.form', ['cred' => (object)array(
                'id' => null,
                'name' => null,
                'has_description' => null,
                'has_expiry' => null
            )]);
        }
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($request->cred_id > 0) {
            $cred_id = $request->cred_id;


            if($validator->fails()) {
                return redirect('client/'.$cred_id)->withErrors($validator)->withInput();
            }

            DB::table('credentials')
                ->where('id', $cred_id)
                ->update([
                    'name' => $request->name,
                    'has_expiry' => $request->has_expiry,
                    'has_description' => $request->has_description
                ]);

            return redirect('settings/credentials/'.$cred_id)->with('success', 'Your credential has updated successfully');
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('settings/credentials/add')->withErrors($validator)->withInput();
            }

            DB::table('credentials')
                ->insert([
                    'name' => $request->name,
                    'has_expiry' => $request->has_expiry,
                    'has_description' => $request->has_description
                ]);
            return redirect('settings/credentials')->with('success', 'Your credential has been added successfully');
        }
    }

    public function delete($id)
    {
        DB::table('credentials')
            ->where('id', $id)
            ->delete();
        //Display it as a list.
        return redirect('settings/credentials')->with('success', 'The credential has been deleted');
    }
}