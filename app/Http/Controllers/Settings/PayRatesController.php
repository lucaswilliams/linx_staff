<?php namespace App\Http\Controllers\Settings;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayRatesController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $payrates = DB::select('SELECT payrates.id, payrates.name, payrates.rate_archived, payrates.rate_type, payrate_rules.paru_client, payrate_rules.paru_staff, payrate_rules.paru_client_seasonal FROM payrates LEFT JOIN payrate_rules on payrate_rules.rate_id = payrates.id AND payrate_rules.paru_frequency = -1 AND year_id = (SELECT year_id FROM financial_years WHERE year_start <= \''.date('Y-m-d').'\' AND year_finish > \''.date('Y-m-d').'\') WHERE payrates.active = 1 ORDER BY payrates.rate_archived, payrates.name');
        //Display it as a list.
        return view('settings.payrates.list', ['payrates' => $payrates]);
    }

    /**
     * Show the form for the given rateential.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesRateExist = DB::table('payrates')
            ->where('id', '=', $id)
            ->get();
        if($id > 0 && count($doesRateExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified pay rate does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $rate = DB::table('payrates')
            ->select('*')
            ->where('id', '=', $id)
            ->get();

        $mc = new MYOBController();
        $myobRates = $mc->getPayrates();

        $rules = [];

        $rates = DB::table('payrates')
            ->where('id', '<>', $id)
            ->get();

        $years = DB::table('financial_years')
			->orderBy('year_name')
			->get();

        foreach($years as $year) {
			$yearrules = DB::table('payrate_rules')
				->where('rate_id', '=', $id)
				->where('year_id', '=', $year->year_id)
				->orderBy('paru_frequency')
				->get();

			if(count($yearrules) > 0) {
				$rules[$year->year_name] = $yearrules;
			} else {
				$rule = new \stdClass();
				$rule->paru_id = null;
				$rule->rate_id = null;
				$rule->paru_frequency = -1;
				$rule->paru_hours = 0;
				$rule->paru_guid = null;
				$rule->paru_staff = 0;
				$rule->paru_client = 0;
				$rule->paru_client_seasonal = 0;
				$rule->year_id = $year->year_id;
				//array_unshift($rules[$year->year_name], $rule);
				$rules[$year->year_name][] = $rule;
			}
		}

        if(count($rate) > 0) {
            return view('settings.payrates.form', ['rate' => $rate[0], 'myob_rate' => $myobRates, 'rules' => $rules, 'rates' => $rates, 'years' => $years]);
        } else {
        	foreach($years as $year) {
        		$rule = new \stdClass();
        		$rule->paru_id = null;
        		$rule->rate_id = null;
        		$rule->paru_frequency = -1;
        		$rule->paru_hours = 0;
        		$rule->paru_guid = null;
        		$rule->paru_staff = 0;
        		$rule->paru_client = 0;
				$rule->paru_client_seasonal = 0;
        		$rule->year_id = $year->year_id;
        		$rules[$year->year_name][] = $rule;
			}
            return view('settings.payrates.form', ['rate' => (object)array(
                'id' => null,
                'name' => null,
                'staff_rate' => null,
                'client_rate' => null,
                'rate_type' => null,
                'rate_guid' => 0,
				'rate_archived' => 0,
                'rate_parent_id' => null,
                'rate_days' => null,
            ), 'myob_rate' => $myobRates, 'rules' => $rules, 'rates' => $rates, 'years' => $years]);
        }
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($request->rate_id > 0) {
            $rate_id = $request->rate_id;


            if($validator->fails()) {
                return redirect('client/'.$rate_id)->withErrors($validator)->withInput();
            }

            DB::table('payrates')
                ->where('id', $rate_id)
                ->update([
                    'name' => $request->name,
                    'staff_rate' => 0,
                    'client_rate' => 0,
                    'rate_type' => $request->rate_type,
                    'rate_guid' => $request->rate_guid,
                    'rate_parent_id' => $request->rate_parent,
                    'rate_days' => $request->rate_days,
					'rate_archived' => isset($request->rate_archived) ? 1 : 0
                ]);
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('settings/payrates/add')->withErrors($validator)->withInput();
            }

            $rate_id = DB::table('payrates')
                ->insertGetId([
                    'name' => $request->name,
                    'staff_rate' => 0,
                    'client_rate' => 0,
                    'rate_type' => $request->rate_type,
                    'rate_guid' => '',
                    'rate_parent_id' => $request->rate_parent,
                    'rate_days' => $request->rate_days,
					'rate_archived' => isset($request->rate_archived) ? 1 : 0
                ]);

        }

        //Sort rules into "already got", "need to delete" and "need to update"
        $rules = DB::table('payrate_rules')
            ->where('rate_id', '=', $rate_id)
            ->select('paru_id')
            ->get();

        $rulearray = array();
        foreach($rules as $rule){
            $rulearray[] = $rule->paru_id;
        }


        if(is_array($request->rule_type)) {
            foreach($request->rule_type as $idx => $val) {
                $rule_staff = $request->rule_staff[$idx];
                $rule_client = $request->rule_client[$idx];
                $rule_seasonal = $request->rule_client_seasonal[$idx];
                if($rule_seasonal == null) { $rule_seasonal = 0.00; }
                $rule_hours = $request->rule_hours[$idx];
                $rule_type = $request->rule_type[$idx];
                $rule_guid = $request->rule_guid[$idx];
                $year_id = $request->year_id[$idx];

                if(isset($request->paru_id[$idx])) {
                    $paru_id = $request->paru_id[$idx];
                    //update
                    DB::table('payrate_rules')
                        ->where('paru_id', '=', $paru_id)
                        ->update([
                            'paru_staff' => $rule_staff,
                            'paru_client' => $rule_client,
                            'paru_client_seasonal' => $rule_seasonal,
                            'paru_hours' => $rule_hours,
                            'paru_frequency' => $rule_type,
                            'paru_guid' => $rule_guid,
							'year_id' => $year_id
                        ]);
                    unset($rulearray[array_search($paru_id, $rulearray)]);
                } else {
                    //add
                    DB::table('payrate_rules')
                        ->insert([
                            'rate_id' => $rate_id,
                            'paru_staff' => $rule_staff,
                            'paru_client' => $rule_client,
							'paru_client_seasonal' => $rule_seasonal,
                            'paru_hours' => $rule_hours,
                            'paru_frequency' => $rule_type,
                            'paru_guid' => $rule_guid,
							'year_id' => $year_id
                        ]);
                }
            }
        }

        //None are set, delete them all
        DB::table('payrate_rules')
            ->whereIn('paru_id', $rulearray)
            ->delete();

        return redirect('settings/payrates')->with('success', 'Your pay rate has saved successfully');
    }

    public function delete($id)
    {
        DB::table('payrates')
            ->where('id', $id)
            ->delete();
        //Display it as a list.
        return redirect('settings/payrates')->with('success', 'The pay rate has been deleted');
    }
}




































