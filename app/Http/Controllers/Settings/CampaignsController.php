<?php namespace App\Http\Controllers\Settings;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignsController extends Controller {
	public function showList() {
		$campaigns = DB::table('campaigns')
			->orderBy('camp_name')
			->get();

		return view('settings.campaigns.list', ['campaigns' => $campaigns]);
	}

	public function add() {
		return getForm(0);
	}

	public function getForm($id) {
		if($id == 0) {
			$campaign = new \stdClass();
			$campaign->camp_id = 0;
			$campaign->camp_name = '';
			$campaign->camp_desc = '';
			$campaign->camp_active = 1;
			$campaign->camp_code = '';
		} else {
			$campaigns = DB::table('campaigns')
				->where('camp_id', '=', $id)
				->get();
			if(count($campaigns) == 0) {
				return view('errors.user', ['error_message' => 'The campaign you are looking for does not exist.']);
			} else {
				$campaign = $campaigns[0];
			}
		}

		return view('settings.campaigns.form', ['campaign' => $campaign]);
	}

	public function saveForm(Request $request) {
		$campaignData = [
			'camp_name' => $request->camp_name,
			'camp_desc' => $request->camp_desc,
			'camp_code' => $request->camp_code,
			'camp_active' => $request->camp_active
		];

		if($request->camp_id == 0) {
			DB::table('campaigns')
				->insert($campaignData);
		} else {
			DB::table('campaigns')
				->where('camp_id', '=', $request->camp_id)
				->update($campaignData);
		}

		return redirect('/settings/campaigns');
	}

	public function delete($id) {
		DB::table('campaigns')
			->where('camp_id', '=', $id)
			->delete();

		return redirect('/settings/campaigns');
	}

	public function questionList($id) {
        $questions = db::table('campaign_questions')
            ->where('camp_id', '=', $id)
            ->orderBy('caqu_order')
            ->orderBy('caqu_id')
            ->get();

	    return view('settings.campaigns.questions', ['questions' => $questions, 'camp_id' => $id]);
    }

    public function addQuestion($camp_id) {
	    return $this->questionForm($camp_id, 0);
    }

    public function questionForm($camp_id, $id) {
	    if($id == 0) {
	        $orders = DB::table('campaign_questions')
	            ->where('camp_id', '=', $camp_id)
                ->orderBy('caqu_order', 'desc')
                ->limit(1)
                ->get();

	        if(count($orders) > 0) {
	            $order = $orders[0] + 1;
            } else {
	            $order = 1;
            }

            $question = new \stdClass();
            $question->caqu_id = 0;
            $question->caqu_question = '';
            $question->caqu_type = 1;
            $question->caqu_conditional = 0;
            $question->caqu_conditional_question = null;
            $question->caqu_conditional_value = '';
            $question->caqu_order = $order;
        } else {
	        $questions = DB::table('campaign_questions')
                ->where('caqu_id', '=', $camp_id)
                ->get();
	        if(count($questions) > 0) {
                $question = $questions[0];
            } else {
	            return view('errors.user', ['error_message' => 'The question you have requested does not exist.']);
            }
        }

        return view('settings.campaigns.questionform', ['question' => $question]);
    }
}