<?php

namespace App\Http\Controllers\Settings;

use DB;
use Auth;
use View;
use Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChargeRatesController extends Controller
{
    public function index() {
        //Get a list of all the charge rates set up in the system
        $charge_rates = DB::table('charge_rates')
            ->leftJoin('clients', 'clients.id', '=', 'charge_rates.client_id')
            ->get();

        return view('charge_rates.list', ['charge_rates' => $charge_rates]);
    }

    public function addForm() {
        //Show the form with no data
        $clients = DB::table('clients')
            ->select('id as key', 'name as value')
            ->get();
        $charge_rate = DB::table('charge_rates')
            ->where('charge_id', '=', 0)
            ->get();
        return view('charge_rates.form', ['charge_rate' => $charge_rate, 'clients' => $clients]);
    }

    public function editForm($id) {
        //Edit the specified charge rate
        $charge_rates = DB::table('charge_rates')
            ->where('charge_id', '=', $id)
            ->get();

        $clients = DB::table('clients')
            ->select('id as key', 'name as value')
            ->get();

        if(count($charge_rates) == 0) {
            return view('errors.user', ['error_message' => 'The specified charge rate does not exist.']);
        } else {
            $charge_rate = $charge_rates[0];
            return view('charge_rates.form', ['charge_rate' => $charge_rate, 'clients' => $clients]);
        }
    }

    public function save(Request $request) {
        //save the thing
        $rate = array(
            'charge_name' => $request->charge_name,
            'charge_rate' => $request->charge_rate
        );

        if($request->client_id > 0) {
            $rate['client_id'] = $request->client_id;
        } else {
            $rate['client_id'] = null;
        }

        if($request->charge_id == 0) {
            //add
            DB::table('charge_rates')
                ->insert($rate);
            $addedit = 'added';
        } else {
            //edit
            DB::table('charge_rates')
                ->where('charge_id', '=', $request->charge_id)
                ->update($rate);
            $addedit = 'edited';
        }

        return redirect('/settings/charge_rates')->with('success', 'The charge rate was '.$addedit.' successfully');
    }

    public function delete($id) {
        DB::table('charge_rates')
            ->where('charge_id', '=', $id)
            ->delete();

        return redirect('/settings/charge_rates')->with('success', 'The selected charge rate has been deleted.');
    }

    public function test() {
        $clients = DB::table('clients')
            ->get();

        foreach($clients as $client) {
            $charges = DB::table('charge_rates')
                ->whereNull('client_id')
                ->orWhere('client_id', '=', $client->id)
                ->get();

            echo '<pre>'; var_dump($charges); echo '</pre>';
        }
    }
}