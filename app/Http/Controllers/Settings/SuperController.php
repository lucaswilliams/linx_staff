<?php namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Settings\MYOBController;
use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuperController extends Controller {
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $supers = $this->getSuperProviders();
        //Display it as a list.
        return view('settings.super.list', ['supers' => $supers]);
    }

    /**
     * Show the form for the given rateential.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesSuperExist = DB::table('super_providers')
            ->where('super_id', '=', $id)
            ->get();
        if($id > 0 && count($doesSuperExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified super provider does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $super = DB::table('super_providers')
            ->select('*')
            ->where('super_id', '=', $id)
            ->get();

        $mc = new MYOBController();
        $myob_super = $mc->getSuperFunds();

        if(count($super) > 0) {
            return view('settings.super.form', ['super' => $super[0], 'myob_super' => $myob_super]);
        } else {
            return view('settings.super.form', ['super' => (object)array(
                'super_id' => null,
                'super_name' => null,
                'super_guid' => null
            ), 'myob_super' => $myob_super]);
        }
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'super_name' => 'required'
        ]);

        if ($request->super_id > 0) {
            $super_id = $request->super_id;


            if($validator->fails()) {
                return redirect('settings/super/'.$super_id)->withErrors($validator)->withInput();
            }

            DB::table('super_providers')
                ->where('super_id', $super_id)
                ->update([
                    'super_name' => $request->super_name,
                    'super_guid' => $request->super_guid
                ]);
            $msg = 'updated';
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('settings/super/add')->withErrors($validator)->withInput();
            }

            $super_id = DB::table('super_providers')
                ->insertGetId([
                    'super_name' => $request->super_name,
                    'super_guid' => $request->super_guid
                ]);
            $msg = 'added';
        }

        return redirect('settings/super')->with('success', 'Your provider has been '.$msg.' successfully');
    }

    public function delete($id)
    {
        DB::table('super_providers')
            ->where('super_id', $id)
            ->delete();
        //Display it as a list.
        return redirect('settings/super')->with('success', 'The provider has been deleted');
    }

    public function loadFromMYOB() {
        //
        $mc = new MYOBController();
    }

    public function getSuperProviders() {
        $supers = DB::table('super_providers')
            ->select('*')
            ->orderBy('super_name')
            ->get();

        return $supers;
    }

    public function getSuperUSIs() {
    	$usis = DB::table('super_usi')
			->orderBy('usi_number')
			->get();

    	return $usis;
	}

    public function showUSI($id) {
        $usis = DB::table('super_usi')
            ->where('super_id', $id)
            ->get();

        return view('settings.super.usi', ['usis' => $usis]);
    }

    public function saveUSI($id) {
        $warn = [];

        if(isset($_POST['usi'])) {
            $saving_usis = array_keys($_POST['usi']);

            $usis = DB::table('super_usi')
                ->where('super_id', $id)
                ->get();

            foreach ($usis as $usi) {
                if (!in_array($usi->usi_id, $saving_usis)) {
                    $userQ = DB::table('users')
                        ->where('usi_id', $usi->usi_id);
                    echo $userQ->toSql();
                    $users = $userQ->count();

                    if ($users == 0) {
                        DB::table('super_usi')
                            ->where('usi_id', $usi->usi_id)
                            ->delete();
                    } else {
                        $warn[] = 'USI ' . $usi->usi_number . ' could not be removed as it is in use by ' . $users . ' users.';
                    }
                } else {
                    DB::table('super_usi')
                        ->where('usi_id', $usi->usi_id)
                        ->update(['usi_number' => $_POST['usi'][$usi->usi_id]]);
                }
            }
        }

        if(isset($_POST['newusi'])) {
            foreach ($_POST['newusi'] as $usi) {
                DB::table('super_usi')
                    ->insert([
                        'super_id' => $id,
                        'usi_number' => $usi
                    ]);
            }
        }

        if(count($warn) == 0) {
            return redirect('/settings/super')->with('success', 'Super USIs have been updated successfully');
        } else {
            return redirect('/settings/super')->with('error', inplode("\n", $warn));
        }
    }
}