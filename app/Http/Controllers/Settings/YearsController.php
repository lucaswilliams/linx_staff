<?php

namespace App\Http\Controllers\Settings;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class YearsController extends Controller
{
    public function getYears() {
        //Get all the staff in the system
        $years = DB::table('financial_years')
            ->select('*')
            ->get();
        //Display it as a list.
        return view('settings.years.list', ['years' => $years]);
    }

    public function getYear($id = 0) {
        //check that this user actually exists
        $doesfinancial_yearExist = DB::table('financial_years')
            ->where('year_id', '=', $id)
            ->get();
        if($id > 0 && count($doesfinancial_yearExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified financial year does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $uni = DB::table('financial_years')
            ->select('*')
            ->where('year_id', '=', $id)
            ->get();

        if(count($uni) > 0) {
            return view('settings.years.form', ['year' => $uni[0]]);
        } else {
            return view('settings.years.form', ['year' => (object)array(
                'year_id' => null,
                'year_name' => null,
                'year_start' => null,
                'year_finish' => null
            )]);
        }
    }

    public function saveYear(Request $request) {
        $validator = Validator::make($request->all(), [
            'year_name' => 'required',
            'year_start' => 'required',
            'year_finish' => 'required'
        ]);

        if ($request->year_id > 0) {
            $year_id = $request->year_id;


            if($validator->fails()) {
                return redirect('settings/years/'.$year_id)->withErrors($validator)->withInput();
            }

            DB::table('financial_years')
                ->where('year_id', $year_id)
                ->update([
                    'year_name' => $request->year_name,
                    'year_start' => $request->year_start,
                    'year_finish' => $request->year_finish
                ]);

            return redirect('settings/years')->with('success', 'Your financial year has updated successfully');
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('settings/years/add')->withErrors($validator)->withInput();
            }

            DB::table('financial_years')
                ->insert([
                    'year_name' => $request->year_name,
                    'year_start' => $request->year_start,
                    'year_finish' => $request->year_finish
                ]);
            return redirect('settings/years')->with('success', 'Your financial year has been added successfully');
        }
    }
}
