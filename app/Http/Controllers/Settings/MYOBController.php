<?php namespace App\Http\Controllers\Settings;

use DB;
use Auth;
use Validator;
use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MYOBController extends Controller {
    public $myobUID;
    private $myob;

    public function __construct() {
        $this->connectMYOB();
    }

    public function test() {
        $this->connectMYOB();
        $data = $this->getEmployeePayroll('3c356385-df06-4d83-b0ee-5422133fbae9');
        //$data = $this->getEmployee('4b09fb37-8ac5-4230-afdf-dc1adcf1c751');
        echo '<pre>'; var_dump($data); echo '</pre>';
    }

    private function connectMYOB() {
        $myob = DB::table('myob')
            ->get()[0];

        $myobConfig = array(
            'apiKey' => $myob->api_key,
            'apiSecret' => $myob->api_secret,
            'apiCallback' => $myob->redirect_uri,
            'username' => 'Administrator',
            'password' => ''
        );

        require_once((__DIR__).'/includes/AccountRightV2.php');
        //echo '<pre>'; var_dump($myobConfig); echo '</pre>';
        $this->myob = new \AccountRightV2($myobConfig);

        if(strlen($myob->cf_guid) > 0) {
            $this->myobUID = $myob->cf_guid;
            $this->myob->retriveAccessToken();
        } else {
            $this->myobUID = null;
        }
    }

    public function getSettings() {
        if(isset($_GET['cfguid'])) {
            DB::table('myob')
                ->update(['cf_guid' => $_GET['cfguid']]);

            return redirect('/settings/myob');
        } else {
            if(isset($_GET['code'])) {
                $json = $this->myob->getAccessToken($_GET['code']);
                // Set Company File
                $files = $this->myob->getCompanyFile();

                if (count($files) > 0) {
                    foreach ($files as $file) {
                        echo '<a href="'.env('APP_URL').'/settings/myob/?code='.$_GET['code'].'&cfguid='.$file->Id.'">'.$file->Name.'</a><br>';
                    }
                } else {
                    die('You don\'t have access to any company files.');
                }
            } else {
                //No return code, so we just show the view
                $myob = DB::table('myob')
                    ->get()[0];

                if(strlen($myob->cf_guid) > 0) {
                    $accounts = $this->getAccounts();
                    //echo '<pre>'; var_dump($accounts); echo '</pre>';
                    $taxcodes = $this->getTaxCodes();
                    //echo '<pre>'; var_dump($taxcodes); echo '</pre>';

                    return view('settings.myob.config', [
                        'myob' => $myob,
                        'accounts' => $accounts,
                        'tax_codes' => $taxcodes
                    ]);
                } else {
                    return view('settings.myob.config', [
                        'myob' => $myob,
                        'accounts' => null,
                        'tax_codes' => null
                    ]);
                }
            }
        }
    }

    public function clearSettings(){
        DB::table('myob')
            ->update([
                'access_token' => null,
                'token_expire' => null,
                'refresh_token' => null,
                'cf_guid' => null
            ]);

        return redirect('/settings/myob');
    }

    public function saveSettings(Request $request) {
        DB::table('myob')
            ->update([
                'wage_account' => $request->wage_account,
                'tax_code' => $request->tax_code,
                'invoice_tax_code' => $request->invoice_tax_code,
                'invoice_account' => $request->invoice_account
            ]);

        return redirect('/settings/myob');
    }

    public function getInvoiceItems() {
        return $this->myob->InvoiceItems()->Items;
    }

    public function getInvoice($data) {
        return $this->myob->Invoice($data, 'GET');
    }

    public function sendInvoice($data) {
        $ret = $this->myob->Invoice($data, 'POST', true);

        if($this->myob->proxy) {
            echo '<pre>';
            var_dump($ret);
            echo '</pre>';
            //die();
        }

        if (isset($ret['header']['Location'])) {
            $split = $ret['header']['Location'];
            $locpart = explode('/', $split);
            echo '<hr />'.end($locpart).'<hr />';
            return $this->getInvoice(end($locpart));
        } else {
            return $ret['body']->Errors;
        }
    }

    /**
     * Returns information about a single Employee card from the MYOB API
     * @param $criteria A key value pair of what to search for, e.g. 'DisplayID' => '1234'
     */
    public function getEmployee($criteria) {
        try {
            if (is_array($criteria)) {
                $filterText = '$filter=' . $criteria['field'] . '+eq+\'' . $criteria['value'] . '\'';
            } else {
                $filterText = $criteria;
            }

            $contact = $this->myob->ContactEmployee($filterText);

            return $contact;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function getTimesheetsForEmployee($uid) {

        try {
            $contact = $this->myob->getTimesheetsForEmployee($uid);

            return $contact;
        } catch (\Exception $ex) {
            return null;
        }
    }

    /**
     * Returns payroll information about a single Employee card from the MYOB API
     * @param $criteria A key value pair of what to search for, e.g. 'DisplayID' => '1234'
     */
    public function getEmployeePayroll($criteria) {
        try {
            if(is_array($criteria)) {
                $filterText = '$filter=' . $criteria['field'] . '+eq+\'' . $criteria['value'] . '\'';
            } else {
                $filterText = $criteria;
            }
            $contact = $this->myob->ContactEmployeePayroll($filterText);

            return $contact;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function getEmployeePayment($criteria) {
        try {
            if(is_array($criteria)) {
                $filterText = '$filter=' . $criteria['field'] . '+eq+\'' . $criteria['value'] . '\'';
            } else {
                $filterText = $criteria;
            }
            $contact = $this->myob->ContactEmployeePayment($filterText);

            return $contact;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function getEmployeeStandard($criteria) {
        try {
            if (is_array($criteria)) {
                $filterText = '$filter=' . $criteria['field'] . '+eq+\'' . $criteria['value'] . '\'';
            } else {
                $filterText = $criteria;
            }
            $contact = $this->myob->ContactEmployeeStandard($filterText);

            return $contact;
        } catch (\Exception $ex) {
            return null;
        }
    }

    /**
     * Returns all of the pay rates in MYOB
     */
    public function getPayrates() {
        try {
            return $this->myob->PayrollCategory()->Items;
        } catch (\Exception $ex) {
            return array(
                (object)array('UID' => '', 'Name' => '')
            );
        }
    }

    /**
     * Returns all of the super funds in MYOB
     */
    public function getSuperFunds() {
        try {
            return $this->myob->SuperProvider()->Items;
        } catch (\Exception $ex) {
            return array(
                (object)array('UID' => '', 'Name' => '')
            );
        }
    }

    public function getAccounts() {
        try {
            $accounts = $this->myob->Accounts();
            return $accounts->Items;
        } catch (\Exception $ex) {
            //echo '<pre>'; var_dump($ex); echo '</pre>';
            return null;
        }
    }

    public function getTaxCodes() {
        try {
            return $this->myob->TaxCodes()->Items;
        } catch (\Exception $ex) {
            //echo '<pre>'; var_dump($ex); echo '</pre>';
            return null;
        }
    }

    public function importForm() {
        return view('settings.myobimport');
    }

    public function importEmployees()
    {
        $content = '';
        if(isset($_GET['external_id'])) {
            $data = $this->myob->ContactEmployee('?$filter=DisplayID+eq+\''.$_GET['external_id'].'\'');
        } elseif(isset($_GET['top']) && isset($_GET['skip'])) {
            $data = $this->myob->ContactEmployee('?$top=' . $_GET['top'] . '&$skip=' . $_GET['skip']);
        } elseif(isset($_GET['active'])) {
            $data = $this->myob->ContactEmployee('?$filter=IsActive+eq+true&top=50&$skip=50');
        } else {
            die('No parameters were specified.  Please specify either an external_id or a top and skip');
        }

        $oldusers = DB::table('users')->count();

        foreach ($data->Items as $item) {
            $recs = DB::table('users')
                ->where('myob_uid', '=', $item->UID)
                ->get();

            if (count($recs) == 0) {
                $user = array();

                //get the rest of the data from MYOB
                $paymentDetails = $this->myob->ContactEmployeePayment($item->EmployeePaymentDetails->UID);
                if($this->myob->proxy) {
                    $content .= '<pre>';
                    $content .= print_r($paymentDetails, true);
                    $content .= '</pre>';
                }

                $payrollDetails = $this->myob->ContactEmployeePayroll($item->EmployeePayrollDetails->UID);
                if($this->myob->proxy) {
                    $content .= '<pre>';
                    $content .= print_r($payrollDetails, true);
                    $content .= '</pre>';
                }

                $user['myob_uid'] = $item->UID;
                if (isset($item->FirstName)) {
                    $user['given_name'] = $item->FirstName;
                    $user['surname'] = $item->LastName;
                } else {
                    $user['given_name'] = $item->CompanyName;
                    $user['surname'] = '';
                }
                $user['external_id'] = $item->DisplayID;
                $user['level'] = 1;
                $user['confirmed'] = 4;
                $user['username'] = $item->DisplayID;
                $user['password'] = bcrypt($item->DisplayID);
                $user['email'] = ($item->Addresses[0]->Email == null) ? '' : $item->Addresses[0]->Email;
                $user['address'] = ($item->Addresses[0]->Street == null) ? '' : $item->Addresses[0]->Street;
                $user['city'] = ($item->Addresses[0]->City == null) ? '' : $item->Addresses[0]->City;
                $user['state'] = ($item->Addresses[0]->State == null) ? '' : $item->Addresses[0]->State;
                $user['postcode'] = ($item->Addresses[0]->PostCode == null) ? '' : $item->Addresses[0]->PostCode;
                $user['telephone'] = ($item->Addresses[0]->Phone2 == null) ? '' : $item->Addresses[0]->Phone2;
                $user['mobilephone'] = ($item->Addresses[0]->Phone1 == null) ? '' : $item->Addresses[0]->Phone1;

                if (isset($paymentDetails->BankAccounts)) {
                    $user['account_bsb'] = $paymentDetails->BankAccounts[0]->BSBNumber;
                    $user['account_number'] = $paymentDetails->BankAccounts[0]->BankAccountNumber;
                    $user['account_name'] = $paymentDetails->BankAccounts[0]->BankAccountName;
                }

                if (isset($payrollDetails->Tax)) {
                    $user['tfn'] = ($payrollDetails->Tax->TaxFileNumber == null) ? '' : $payrollDetails->Tax->TaxFileNumber;
                    switch($payrollDetails->Tax->TaxTable->Name) {
                        case 'Withholding Variation':
                            $user['visa_88_days'] = 1;
                            $user['tax_free_threshold'] = 0;
                            $user['help_debt'] = 0;
                            $user['fs_debt'] = 0;
                            break;
                        case 'Tax Free Threshold HELP + FS':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 1;
                            $user['help_debt'] = 1;
                            $user['fs_debt'] = 1;
                            break;
                        case 'Tax Free Threshold + FS':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 1;
                            $user['help_debt'] = 0;
                            $user['fs_debt'] = 1;
                            break;
                        case 'No Tax Free Threshold HELP + FS':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 0;
                            $user['help_debt'] = 1;
                            $user['fs_debt'] = 1;
                            break;
                        case 'No Tax Free Threshold + FS':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 0;
                            $user['help_debt'] = 0;
                            $user['fs_debt'] = 1;
                            break;
                        case 'Tax Free Threshold + HELP':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 1;
                            $user['help_debt'] = 1;
                            $user['fs_debt'] = 0;
                            break;
                        case 'Tax Free Threshold':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 1;
                            $user['help_debt'] = 0;
                            $user['fs_debt'] = 0;
                            break;
                        case 'No Tax Free Threshold + HELP':
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 0;
                            $user['help_debt'] = 1;
                            $user['fs_debt'] = 0;
                            break;
                        default :
                            $user['visa_88_days'] = 0;
                            $user['tax_free_threshold'] = 0;
                            $user['help_debt'] = 0;
                            $user['fs_debt'] = 0;
                            break;
                    }
                }

                if (isset($payrollDetails->Superannuation) && isset($payrollDetails->Superannuation->SuperannuationFund)) {
                    $providerDetails = DB::table('super_providers')
                        ->where('super_guid', '=', $payrollDetails->Superannuation->SuperannuationFund->UID)
                        ->get();
                    $provider = '';
                    if (count($providerDetails) > 0) {
                        $provider = $providerDetails[0]->super_name;
                    }
                    $user['provider_id'] = $provider;
                    $user['super_number'] = ($payrollDetails->Superannuation->EmployeeMembershipNumber == null) ? '' : $payrollDetails->Superannuation->EmployeeMembershipNumber;
                }

                if(isset($payrollDetails->Gender)) {
                    switch ($payrollDetails->Gender) {
                        case 'Male' :
                            $gender = 1;
                            break;
                        case 'Female' :
                            $gender = 2;
                            break;
                        default :
                            $gender = 0;
                            break;
                    }
                } else {
                    $gender = 0;
                }
                $user['gender'] = $gender;
                $user['date_of_birth'] = (!isset($payrollDetails->DateOfBirth) || $payrollDetails->DateOfBirth == null) ? '' : $payrollDetails->DateOfBirth;

                //echo '<pre>'; var_dump($user); echo '</pre>';
                DB::table('users')->insert($user);
            }
        }

        $users = DB::table('users')->count();
        $difference = $users - $oldusers;
        return view('settings.imported', ['difference' => $difference, 'content' => $content]);
    }

    public function listClients() {
        $data = $this->myob->ContactCustomer('?$filter=IsActive+eq+true');
        echo '<pre>'.json_encode($data).'</pre>';

        $data = $this->getJobs(array());
        echo '<pre>'.json_encode($data).'</pre>';
    }

    public function getGuid($id) {
        try {
            $data = $this->myob->ContactEmployee('?$filter=DisplayID+eq+\''.$id.'\'');

            echo '<pre>';
            var_dump($data);
            echo '</pre>';
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function getGuidClient($id) {
        try {
            $filter = '?$filter=CompanyName+eq+\''.str_replace(' ', '+', urldecode($id)).'\'';
            echo '<pre>Input: '.$filter.'</pre>';

            $data = $this->myob->ContactCustomer($filter);

            echo '<pre>';
            var_dump($data);
            echo '</pre>';
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function updateEmployee($data, $debug = false) {
        if(strlen($this->myobUID) > 0) {
            //Does this client exist already? look at their external ID
            if (strlen($data['DisplayID']) > 0) {
                $existing = $this->myob->ContactEmployee('?$filter=DisplayID+eq+\'' . $data['DisplayID'] . '\'');
                if (isset($existing->Items[0]->UID)) {
                    $data['UID'] = $existing->Items[0]->UID;
                }
            }

            if (isset($data['UID'])) {
                $employee = $this->myob->ContactEmployee($data['UID']);
                //echo '<pre>'; var_dump($employee); echo '</pre>';
                $data['RowVersion'] = $employee->RowVersion;
                $ret = $this->myob->ContactEmployee($data, 'PUT');

                $uid = array('uid' => $data['UID']);
            } else {
                $ret = $this->myob->ContactEmployee($data, 'POST', true);
                if (isset($ret['header']['Location'])) {
                    $split = $ret['header']['Location'];
                    $locpart = explode('/', $split);
                    $uid = array('uid' => end($locpart));
                } else {
                    //There's probably some error.
                    throw new \Exception(json_encode($ret));
                }
            }

            if($debug) {
                echo '<h1>Employee Data</h1></h1><pre>';
                var_dump($data);
                echo '</pre>';
                echo '<h1>Employee Response</h1><pre>';
                var_dump($ret);
                echo '</pre>';
            }

            return $uid;
        }
    }

    public function updateEmployeePayment($data, $debug = false)
    {
        if (strlen($this->myobUID) > 0) {
            $employee = $this->myob->ContactEmployee($data['Employee']['UID']);
            if (isset($employee->EmployeePaymentDetails)) {
                $uid = $employee->EmployeePaymentDetails->UID;
                $data['UID'] = $uid;

                $payment = $this->myob->ContactEmployeePayment($uid);

                $data['RowVersion'] = $payment->RowVersion;
                $outData = $this->myob->ContactEmployeePayment($data, 'PUT');
            } else {
                $outData = $this->myob->ContactEmployeePayment($data, 'POST');
            }

            if($debug) {
                echo '<h1>Payment Data</h1></h1><pre>';
                var_dump($data);
                echo '</pre>';
                echo '<h1>Payment Response</h1><pre>';
                var_dump($outData);
                echo '</pre>';
            }

            if(isset($outData->Errors)) {
                return array('Errors' => $outData->Errors);
            }
        }
    }

    public function updateEmployeePayroll($data, $debug = false) {
        if(strlen($this->myobUID) > 0) {
            $employee = $this->myob->ContactEmployee($data['Employee']['UID']);

            if (isset($employee->EmployeePayrollDetails)) {
                $uid = $employee->EmployeePayrollDetails->UID;
                $data['UID'] = $uid;

                $payment = $this->myob->ContactEmployeePayroll($uid);

                $payrollar = (array)$payment;

                if($debug) {
                    echo '<h1>From MYOB - RAW</h1><pre>';
                    var_dump($payment);
                    echo '</pre>';
                }

                $payroll = json_decode(json_encode($payrollar), true);

                if($debug) {
                    echo '<h1>From MYOB - processed</h1><pre>';
                    var_dump($payroll);
                    echo '</pre>';
                }

                //Add these payroll categories
                foreach($payroll['Wage']['WageCategories'] as $line) {
                    //echo '<h1>Line</h1><pre>'; var_dump($line); echo '</pre>';
                    $inarray = false;
                    foreach($data['Wage']['WageCategories'] as $cats) {
                        if(strcmp($cats['UID'], $line['UID']) == 0) {
                            $inarray = true;
                        }
                    }

                    if(!$inarray) {
                        $data['Wage']['WageCategories'][] = array(
                            'UID' => $line['UID']
                        );
                    }
                }

                $data['RowVersion'] = $payment->RowVersion;
                //$data['Tax']['TaxCategory']['UID'] = $payment->Tax->TaxCategory->UID;
                //$data['Tax']['TaxTable']['UID'] = $payment->Tax->TaxTable->UID;
                $outData = $this->myob->ContactEmployeePayroll($data, 'PUT');
            } else {
                $outData = $this->myob->ContactEmployeePayroll($data, 'POST');
            }

            if($debug) {
                echo '<h1>Payroll Data</h1></h1><pre>';
                var_dump($data);
                echo '</pre>';
                echo '<h1>Payroll Response</h1><pre>';
                var_dump($outData);
                echo '</pre>';
            }

            if(isset($outData->Errors)) {
                return array('Errors' => $outData->Errors);
            }
        }
    }

    public function updateEmployeeStandard($data) {
        if($this->myob->proxy) {
            echo '<h1>Standard Data</h1><pre>';
            var_dump($data);
            echo '</pre>';
        }
        if(strlen($this->myobUID) > 0) {
            $employee = $this->myob->ContactEmployee($data['Employee']['UID']);
            if (isset($employee->EmployeeStandardPay)) {
                $uid = $employee->EmployeeStandardPay->UID;
                $data['UID'] = $uid;

                $payment = $this->myob->ContactEmployeeStandard($uid);
                if($this->myob->proxy) {
                    echo '<h1>Payroll Details Out</h1><pre>';
                    var_dump($payment);
                    echo '</pre>';
                }

                $data['RowVersion'] = $payment->RowVersion;
                $ret = $this->myob->ContactEmployeeStandard($data, 'PUT');
            } else {
                $ret = $this->myob->ContactEmployeeStandard($data, 'POST');
            }

            if($this->myob->proxy) {
                echo '<h1>Standard Response</h1><pre>';
                var_dump($ret);
                echo '</pre>';
            }
        }
    }

    public function getClient($id) {
        try {
            $data = $this->myob->ContactCustomer('?$filter=DisplayID+eq+\''.$id.'\'');
            return $data;
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function updateClient($data) {
        if(strlen($this->myobUID) > 0) {
            //Does this client exist already? look at their external ID
            if (strlen($data['DisplayID']) > 0) {
                $existing = $this->myob->ContactCustomer('?$filter=DisplayID+eq+\'' . $data['DisplayID'] . '\'');
                if (isset($existing->Items[0]->UID)) {
                    $data['UID'] = $existing->Items[0]->UID;
                }
            }

            if (isset($data['UID'])) {
                $employee = $this->myob->ContactCustomer($data['UID']);
                $data['RowVersion'] = $employee->RowVersion;
                $ret = $this->myob->ContactCustomer($data, 'PUT');
                if(isset($ret->Errors)) {
                    throw new \Exception(json_encode($ret->Errors));
                }
                return $data['UID'];
            } else {
                $ret = $this->myob->ContactCustomer($data, 'POST', true);

                if(isset($ret['body']->Errors)) {
                    throw new \Exception(json_encode($ret['body']->Errors));
                }
                $split = $ret['header']['Location'];
                $locpart = explode('/', $split);
                return end($locpart);
            }
        }
    }

    public function saveTimesheet($data) {
        try {
            $request_res = $this->myob->Timesheet($data);
            return $request_res;
        } catch(\Exception $ex) {
            return null;
        }
    }

    public function getPayslips($date) {
        try {
            $response = $this->myob->Payslips($date);
            return $response;
        } catch(\Exception $ex) {
            return null;
        }
    }

    public function getJobs() {
        try {
            $response = $this->myob->Jobs('?$filter=IsActive+eq+true');
            return $response;
        } catch(\Exception $ex) {
            return null;
        }
    }

    public function getSuperCategories() {
        try {
            $response = $this->myob->SuperCategories('?$filter=Name+eq+\'Superannuation+Guarantee+-+Tas\'');
            return $response;
        } catch(\Exception $ex) {
            return null;
        }
    }

    public function getTaxCategories() {
        try {
            $response = $this->myob->TaxCategories('?$filter=Name+eq+\'PAYG+Withholding\'');
            return $response;
        } catch(\Exception $ex) {
            return null;
        }
    }

    public function getTaxTables($table_name) {
        try {
            $table_name = str_replace(array('+', ' '), array('%2B', '+'), $table_name);
            $response = $this->myob->TaxTables('?$filter=Name+eq+\''.$table_name.'\'');
            return $response;
        } catch(\Exception $ex) {
            return null;
        }
    }

    public function bankDetails() {
        $response = $this->myob->ContactEmployeePayment('?filter=BSBNumber+eq+null');
        echo '<pre>'; var_dump($response); echo '</pre>';
        //foreach($response->Items as $)
    }

    public function payrollAdvice($uid) {
    	$response = $this->myob->PayrollAdvice('');
		echo '<pre>'; var_dump($response); echo '</pre>';
	}

	public function getAllPayrates() {
        $response = $this->myob->PayrollCategory('');
        //echo '<pre>'; var_dump($response); echo '</pre>';
        echo '<table>';
        foreach($response->Items as $rate) {
            echo '<tr><td>'.$rate->Name.'</td><td>'.$rate->UID.'</td></tr>';
        }
        echo '</table>';
    }

    public function getAllWorkers() {
        $response = $this->myob->ContactEmployee();
        //echo '<pre>'; var_dump($response); echo '</pre>';
        echo '<table>';
        foreach($response->Items as $rate) {
            echo '<tr><td>'.$rate->UID.'</td><td>'.$rate->DisplayID.'</td></tr>';
        }
        echo '</table>';
    }

    public function getAllJobs() {
        $response = $this->myob->Jobs('?$filter=IsActive+eq+true');
        //echo '<pre>'; var_dump($response); echo '</pre>';
        echo '<table>';
        foreach($response->Items as $rate) {
            echo '<tr><td>'.$rate->UID.'</td><td>'.$rate->Number.'</td></tr>';
        }
        echo '</table>';
    }

    public function getAllSuper() {
        $response = $this->myob->SuperProvider();
        //echo '<pre>'; var_dump($response); echo '</pre>';
        echo '<table>';
        foreach($response->Items as $rate) {
            echo '<tr><td>'.$rate->UID.'</td><td>'.$rate->Name.'</td></tr>';
        }
        echo '</table>';
    }
}