<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Settings\MYOBController;

class ClientController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showProfile($id = 0)
    {
        //check that this user actually exists
        $doesClientExist = DB::table('clients')
            ->where('id', '=', $id)
            ->get();
        if($id > 0 && count($doesClientExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified client does not exist.']);
        }

        $client = DB::table('clients')
                    ->where('id', '=', $id)
                    ->get();

        $clientEmails = DB::table('client_emails')
            ->where('client_id', '=', $id)
            ->orderBy('emai_primary', 'desc')
            ->orderBy('emai_address')
            ->get();

        $jc = new JobsController();
        $jobsQ = $jc->getJobList($id);
        $jobs = $jobsQ->get();

        try {
            $mc = new MYOBController();
            $myobAccounts = $mc->getAccounts();
        }
        catch (Exception $e) {
            //Log::info($e);
            $myobAccounts = array();
        }

        if(count($client) > 0) {
            return view('client.profile', ['client' => $client[0], 'jobs' => $jobs, 'clie' => false, 'accounts' => $myobAccounts, 'emails' => $clientEmails]);
        } else {
            return view('client.profile', ['client' => (object)array(
                'id' => null,
                'name' => null,
                'abn' => null,
                'telephone' => null,
                'email' => null,
                'address' => null,
                'city' => null,
                'state' => null,
                'postcode' => null,
                'status' => 1,
                'external_id' => null,
                'income_account' => null
            ), 'jobs' => null, 'clie' => false, 'accounts' => $myobAccounts, 'emails' => null]);
        }
    }

    public function saveProfile(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        $myobDB = DB::table('myob')
            ->select('cf_guid')
            ->get()[0];

        $haveMYOB = (strlen($myobDB->cf_guid) > 0);

        if($haveMYOB) {
            $myob = new MYOBController();
            $expense = DB::table('myob')
                ->get();
            $taxCode = $expense[0]->tax_code;
        } else {
            $taxCode = '';
        }

        if(strlen($request->abn) > 0) {
            $temp_abn = preg_replace('/[^0-9+]/', '', $request->abn);
            $abn = substr($temp_abn, 0, 2) . ' ' . substr($temp_abn, 2, 3) . ' ' . substr($temp_abn, 5, 3) . ' ' . substr($temp_abn, 8, 3);
        } else {
            $abn = '';
        }

        if ($request->client_id > 0) {
            $client_id = $request->client_id;


            if($validator->fails()) {
                return redirect('client/'.$client_id)->withErrors($validator)->withInput();
            }

            DB::table('clients')
                ->where('id', $client_id)
                ->update([
                    'name' => $request->name,
                    'abn' => $request->abn,
                    'telephone' => $request->telephone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'state' => $request->state,
                    'postcode' => $request->postcode,
                    'status' => $request->status,
                    'external_id' => $request->external_id,
                    'myob_uid' => $request->myob_uid,
                    'income_account' => $request->income_account
                ]);

            $successMsg = 'The client, '.$request->name.', has updated successfully';
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('client/add')->withErrors($validator)->withInput();
            }

            $client_id = DB::table('clients')
                ->insertGetId([
                    'name' => $request->name,
                    'abn' => $request->abn,
                    'telephone' => $request->telephone,
                    'address' => $request->address,
                    'city' => $request->city,
                    'state' => $request->state,
                    'postcode' => $request->postcode,
                    'status' => $request->status,
                    'external_id' => $request->external_id,
                    'income_account' => $request->income_account
                ]);

            $successMsg = 'The client, '.$request->name.', has been added successfully';
        }

        //Do email stuff
        if(isset($request->email)) {
            $primaryEmail = $request->email[0];
            DB::table('client_emails')
                ->where('client_id', '=', $client_id)
                ->delete();

            foreach($request->email as $ekey => $email) {
                DB::table('client_emails')
                    ->insert([
                        'emai_address' => $email,
                        'emai_primary' => ($ekey == 0 ? 1 : 0),
                        'client_id' => $client_id
                    ]);
            }
        }

        //Do MYOB stuff
        $myobdata = $myob->getClient($request->external_id);
        //echo '<pre>'; var_dump($myobdata); echo '</pre>';
        if(isset($myobdata->Items) && count($myobdata->Items) > 0) {
            $data = $myobdata->Items[0];
            $data = (array)$data;
            $data['UID'] = $request->myob_uid;

            $address = $data['Addresses'][0];

            if(strlen($request->address) == 0 &&
                strlen($request->city) == 0 &&
                strlen($request->state) == 0 &&
                strlen($request->postcode) == 0) {
                DB::table('clients')
                    ->where('id', '=', $client_id)
                    ->update([
                        'address' => $address->Street,
                        'city' => $address->City,
                        'state' => $address->State,
                        'postcode' => $address->PostCode
                    ]);
            }
            
            $address = (array)$address;
            $noaddress = false;
        } else {
            //Not in MYOB, doing an add
            $data = array();
            $noaddress = true;
            $address = array();
        }

        $data['CompanyName'] = $request->name;
        $data['IsIndividual'] = false;
        $data['DisplayID'] = $request->external_id;
        $data['IsActive'] = ($request->status == 1) ? true : false;

        if(strlen($request->address) > 0) {
            $address['Street'] = $request->address;
            $noaddress = false;
        }

        if(strlen($request->city) > 0) {
            $address['City'] = $request->city;
            $noaddress = false;
        }

        if(strlen($request->state) > 0) {
            $address['State'] = $request->state;
            $noaddress = false;
        }

        if(strlen($request->postcode) > 0) {
            $address['PostCode'] = $request->postcode;
            $noaddress = false;
        }

        if(strlen($request->telephone) > 0) {
            $address['Phone1'] = $request->telephone;
            $noaddress = false;
        }

        if(isset($primaryEmail) && strlen($primaryEmail) > 0) {
            $address['Email'] = $primaryEmail;
            $noaddress = false;
        }

        if(isset($data['Addresses'][0])) {
            $address['Location'] = 1;
        }

        if(!$noaddress) {
            $data['Addresses'][0] = $address;
        }

        $data['SellingDetails'] = array(
            'ABN' => $abn,
            'TaxCode' => array(
                'UID' => $taxCode
            ),
            'FreightTaxCode' => array(
                'UID' => $taxCode
            )
        );

        //echo '<pre>'; var_dump($data); echo '</pre>';
        try {
            $uid = $myob->updateClient($data);

            DB::table('clients')
                ->where('id', '=', $client_id)
                ->update([
                    'myob_uid' => $uid,
                    'external_id' => $request->external_id
                ]);

            return redirect('client')->with('success', $successMsg);
        } catch(\Exception $ex) {
            $errors = array();
            $err = json_decode($ex->getMessage());
            echo '<pre>'; var_dump($err); echo '</pre>';
            if(is_array($err) && count($err) > 0) {
                foreach ($err as $error) {
                    $errors[] = $error->Message;
                }
            }
            return redirect('client')->with('danger', implode('<br />', $errors));
        }
    }

    /**
     * Show the entire list of users.
     *
     * @return The view, and its data.
     */
    public function deleteProfile($id)
    {
        DB::table('clients')
            ->where('id', $id)
            ->delete();
        //Display it as a list.
        return redirect('client')->with('success', 'The client has been deleted');
    }

    /**
     * Show the entire list of users.
     *
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $clients = DB::table('clients')
            ->leftJoin('client_emails', function($join) {
                $join->on('client_emails.client_id', '=', 'clients.id')
                    ->where('client_emails.emai_primary', '=', 1);
            })
            ->select('*')
            ->orderBy('name')
            ->get();
        //Display it as a list.
        return view('client.list', ['clients' => $clients]);
    }

    public function showContacts($id) {
        $clients = DB::table('clients')
            ->where('id', '=', $id)
            ->get();
        if(count($clients) == 0) {
            return view('errors.access', 'The supplied client ID in invalid');
        }
        $client = $clients[0];
        $contacts = DB::table('client_users')
            ->join('users', 'users.id', '=', 'client_users.user_id')
            ->where('client_id', '=', $id)
            ->select('client_users.id', 'users.given_name', 'users.surname', 'users.preferred', 'users.nickname', 'users.external_id', 'client_users.level')
            ->get();

		$jobsQ = DB::table('jobs')
			->join('clients', 'clients.id', '=', 'jobs.client_id')
			->select('jobs.name', 'jobs.id', 'jobs.start_date', 'jobs.end_date', 'clients.name as client_name');

		$jobs = $jobsQ->get();

        return view('client.contactlist', ['client' => $client, 'contacts' => $contacts, 'jobs' => $jobs]);
    }

    public function addContact($client_id, $user_id) {
    	DB::table('client_users')
			->insert([
				'client_id' => $client_id,
				'user_id' => $user_id,
				'level' => 0
			]);
	}

	public function contactSupervisor($id) {
		DB::table('client_users')
			->where('id', '=', $id)
			->update(['level' => 0]);

		return redirect($_SERVER['HTTP_REFERER']);
	}

	public function contactManager($id) {
		DB::table('client_users')
			->where('id', '=', $id)
			->update(['level' => 1]);

		return redirect($_SERVER['HTTP_REFERER']);
	}

	public function deleteContact($id) {
    	DB::table('client_users')
			->where('id', '=', $id)
			->delete();

    	return redirect($_SERVER['HTTP_REFERER']);
	}
}