<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Mail;
use View;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Settings\MYOBController;

class InvoicesController extends Controller
{
    /**
     * Display a listing of Invoices.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $clients = DB::table('clients')->get();
        return view('invoice.index', ['clients' => $clients]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function jsonList(Request $request) {
        $invoiceQ = DB::table('invoices')
            ->join('clients', 'clients.id', '=', 'invoices.client_id');

        if($request->client > 0) {
            $invoiceQ->where('invoices.client_id', '=', $request->client);
        }

        if($request->date > 0) {
            $invoiceQ->where('invoices.invoice_date', '>=', $request->date);
        }

        if($request->number > 0) {
            $invoiceQ->where('invoices.invoice_number', '=', $request->number);
        }

        $invoiceQ->orderBy('invoice_date', 'desc')
            ->orderBy('invoice_number', 'desc')
            ->take(50);

        $invoices = $invoiceQ->get();

        return response()->json($invoices);
    }

    /**
     * Show the form for creating a new Invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $taskQ = DB::table('time_tasks')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
            ->join('users', 'users.id', '=', 'timesheets.user_id')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
            ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->whereNull('time_tasks.invoice_id')
            ->where('timesheets.processed', '=', 1);

        $clients = $taskQ->select('clients.name as client_name', 'clients.id as client_id')
            ->orderBy('clients.name')
            ->distinct()
            ->get();

        $tasks = $taskQ->select('jobs.name as jobs_name', 'clients.name as client_name', 'clients.id as client_id', 'jobs.id as jobs_id')
            ->orderBy('jobs.name')
            ->distinct()
            ->get();

        return view('invoice.create', ['items' => $tasks, 'clients' => $clients]);
    }

    public function createSearch(Request $request) {
        $tasksQ = DB::table('time_tasks')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
            ->join('users', 'users.id', '=', 'timesheets.user_id')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
            ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->join('shift_tasks', function($join) {
                $join->on('shift_tasks.shift_id', '=', 'shifts.id')
                    ->on('shift_tasks.task_id', '=', 'tasks.task_id');
            })
            ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
            ->whereNull('time_tasks.invoice_id')
            ->where('timesheets.processed', '=', 1);

        if($request->jobs_id > 0) {
            $tasksQ->where('jobs.id', '=', $request->jobs_id);
        } elseif($request->client_id > 0) {
            $tasksQ->where('jobs.client_id', '=', $request->client_id);
        }

        if($request->invoice_start > 0) {
            $tasksQ->where('shifts.start_time', '>=', $request->invoice_start);
        }

        if($request->invoice_finish > 0) {
            $tasksQ->where('shifts.end_time', '<=', $request->invoice_finish.' 23:59:59');
        }

        $tasksQ->select('users.given_name', 'users.surname', 'users.external_id', 'tasks.task_name', 'time_tasks.tita_quantity', 'payrates.client_rate as shta_rate', 'time_tasks.tita_id', 'shifts.start_time', 'jobs.name as jobs_name', 'shifts.start_time as time_start')
            ->orderBy('shifts.start_time')
            ->orderBy(DB::raw('right(users.external_id, 5)'));

        //die($tasksQ->toSql());

        $tasks = $tasksQ->get();

        $taskarray = array();
        foreach($tasks as $task) {
            $the_amount = $task->shta_rate * $task->tita_quantity;

            $taskarray[] = (object)array(
                'staff_name' => $task->given_name.' '.$task->surname.' ('.$task->external_id.')',
                'task_name' => $task->task_name,
                'jobs_name' => $task->jobs_name,
                'time_start' => $task->time_start,
                'tita_id' => $task->tita_id,
                'tita_total' => round($the_amount, 2, PHP_ROUND_HALF_EVEN)
            );
        }

        return response()->json($taskarray);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //echo '<pre>'; var_dump($_POST); echo '</pre>'; die();
        if(isset($request->tita_id)) {
            //make an invoice record - only want the date, the rest comes back from MYOB
            $clientQ = DB::table('jobs')
                ->join('clients', 'clients.id', '=', 'jobs.client_id')
                ->where('clients.id', '=', $request->client_id)
                ->select('clients.myob_uid', 'clients.id as client_id', 'jobs.jobs_uid', 'clients.income_account');

            //echo $clientQ->toSql();die();

            $clients = $clientQ->get();

            echo '<pre>'; var_dump($clients); echo '</pre>';

            if(count($clients) > 0) {
                $client = $clients[0];
                $client_id = $client->myob_uid;
            }

            $invoice_id = DB::table('invoices')
                ->insertGetId([
                    'invoice_date' => date('Y-m-d'),
                    'client_id' => $client->client_id
                ]);

            $invoice_total = 0;
            foreach($request->tita_id as $tita_id => $value) {
                $item_total = (float)str_replace(',', '', $request->tita_total[$tita_id]);
                //Add GST to the item.
                $item_total = round($item_total * 1.1, 2, PHP_ROUND_HALF_EVEN);
                //Add the item to the invoice total
                $invoice_total += $item_total;
                //echo (float)$request->tita_total[$tita_id].': '.$invoice_total.'<br />';
                //update the selected records to have the invoice_id set
                DB::table('time_tasks')
                    ->where('time_tasks.tita_id', '=', $tita_id)
                    ->update(['invoice_id' => $invoice_id]);
            }

            DB::table('invoices')
                ->where('invoice_id', '=', $invoice_id)
                ->update(['invoice_amount' => $invoice_total ]);

            //echo $invoice_total;
            //echo '<pre>'; var_dump($_POST); echo '</pre>';

            //build what we need to send it off to myob.
            $taxCode = DB::table('myob')->select('invoice_tax_code')->get()[0];

            $invoicedata = array(
                'Date' => date('Y-m-d H:i:s'),
                'Customer' => array(
                    'UID' => $client_id
                ),
                'Lines' => array(
                    array(
                        'Total' => $invoice_total,
                        'Description' => 'Linx',
                        'Account' => array(
                            'UID' => $client->income_account
                        ),
                        'Job' => array(
                            'UID' => $client->jobs_uid
                        ),
                        'TaxCode' => array(
                            'UID' => $taxCode->invoice_tax_code
                        )
                    )
                )
            );

            $myobController = new MYOBController();
            if($myobController->myobUID == null) {
                return;
            }

            echo '<h1>Invoice data</h1>';
            echo '<pre>'; var_dump($invoicedata); echo '</pre>';

            $invoice = $myobController->sendInvoice($invoicedata);

            echo '<h1>Response</h1>';
            echo '<pre>'; var_dump($invoice); echo '</pre>';

            if(isset($invoice->Number)) {
                $inv_number = $invoice->Number;
                echo '<pre>';
                var_dump($inv_number);
                echo '</pre>';
                DB::table('invoices')
                    ->where('invoices.invoice_id', '=', $invoice_id)
                    ->update(['invoice_number' => $inv_number]);
                return redirect('/invoices')->with('success', 'Your invoice has been created and sent to MYOB successfully)');
            } else {
                DB::table('time_tasks')
                    ->where('invoice_id', '=', $invoice_id)
                    ->update(['invoice_id' => null]);

                DB::table('invoices')
                    ->where('invoice_id', '=', $invoice_id)
                    ->delete();

                $errorString = '';

                foreach($invoice as $error) {
                    $errorString.="\n".$error->Name.': '.($error->Message == null ? '' : $error->Message.' ').($error->AdditionalDetails == null ? '' : $error->AdditionalDetails.' ');
                }

                //return redirect('/invoices')->with('error', "The invoice could not be created, as there was a problem sending it to MYOB:\n".$errorString);
            }

        } else {
            return View('errors.user', ['error_message' => 'You need to select at least one task to be invoiced.']);
        }
    }

    public function email($id) {
        $payslip = $this->generateInvoice($id);

        try {
            Mail::send('emails.invoice', ['client' => $payslip['client']], function ($message) use ($payslip) {
                $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                $message->to($payslip['client']->emai_address);
                $message->subject('Linx Employment Invoice');
                $message->attach($payslip['outfile'], ['as' => $payslip['outname'], 'mime' => 'application/pdf']);
            });

            Mail::send('emails.invoice', ['client' => $payslip['client']], function ($message) use ($payslip) {
                $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                $message->to('admin@linxemployment.com.au');
                $message->subject('Linx Employment Invoice');
                $message->attach($payslip['outfile'], ['as' => $payslip['outname'], 'mime' => 'application/pdf']);
            });
        }
        catch(Exception $ex) {
            //Ignore
        }
    }

    public function printInvoice($id) {
        $generated = $this->generateInvoice($id);
        $outfile = $generated['outfile'];
        $outname = $generated['outname'];

        return Response::make(file_get_contents($outfile), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; '.$outname,
        ]);
    }

    public function generateInvoice($id) {
        $invoices = DB::table('invoices')
            ->where('invoice_id', '=', $id)
            ->get();

        $invoice = $invoices[0];

        $itemsQ = DB::table('time_tasks')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
            ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
            ->join('shift_tasks', function($join) {
                $join->on('shift_tasks.shift_id', '=', 'timesheets.shift_id')
                    ->on('shift_tasks.task_id', '=', 'tasks.task_id');
            })
            ->where('time_tasks.invoice_id', '=', $invoice->invoice_id)
            ->groupBy('tasks.task_name')
            ->groupBy('payrates.name')
            ->groupBy('payrates.client_rate')
            ->select('tasks.task_name as description', 'payrates.name as code', 'payrates.client_rate as unit_price', DB::raw('sum(time_tasks.tita_quantity) as quantity'));

        //die($itemsQ->toSql());

        $items = $itemsQ->get();

        $invoice->items = $items;

        $clients = DB::table('clients')
            ->join('client_emails', 'client_emails.client_id', '=', 'clients.id')
            ->where('id', '=', $invoice->client_id)
            ->orderBy('emai_primary', 'asc')
            ->get();

        $client = $clients[0];

        $view = View::make('invoice.template', ['invoice' => $invoice, 'client' => $client]);

        //echo $view;
        //die();

        try {
            $outname = $client->external_id.'_'.$invoice->invoice_number.'.pdf';
            $outfile = storage_path('/'.$outname);

            $pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
            $pdf->pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($view->render());
            $pdf->Output($outfile, 'F');

            return array (
                'outfile' => $outfile,
                'outname' => $outname,
                'client' => $client
            );
        } catch (\HTML2PDF_exception $ex) {
            echo $ex.'<br />';
        }
    }

    public function delete($id) {
        DB::table('invoices')
            ->where('invoice_id', '=', $id)
            ->delete();

        return redirect('/invoices');
    }
}
