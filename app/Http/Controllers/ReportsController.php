<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;

class ReportsController extends Controller {
    public function home()
    {
        $user = Auth::user();

        if($user->level == 3) {
            $reports = DB::table('reports')
                ->get();
        } else {
            $reports = DB::table('report_users')
                ->join('reports', 'reports.report_id', '=', 'report_users.report_id')
                ->join('users', 'users.id', '=', 'report_users.user_id')
                ->where('report_users.user_id', '=', $user->id)
                ->get();
        }

        return view('reports.home', ['reports' => $reports]);
    }

    public function adminIndex()
    {
        //Show a list of all configured reports
        $reports = DB::table('reports')
            ->orderBy('report_name')
            ->get();

        $user = Auth::user();

        return view('reports.index', ['reports' => $reports, 'user' => $user]);
    }

    public function adminForm($id = 0)
    {
        //Show the form to add or edit a report permissions.
        $reports = DB::table('reports')
            ->where('report_id', '=', $id)
            ->get();

        if(count($reports) > 0) {
            $report = $reports[0];
        } else {
            $report = null;
        }

        $reports = $this->getReportsOnDisk();
		//$reports = array();

        $uc = new UserController();
        $users = $uc->getSuperAndAdmin();

        $report_users = DB::table('report_users')
            ->where('report_id', '=', $id)
            ->get();

        return view('reports.form', ['report' => $report, 'reports' => $reports, 'users' => $users, 'report_users' => $report_users]);
    }

    public function delete($id) {
        DB::table('report_users')
            ->where('report_id', '=', $id)
            ->delete();

        DB::table('reports')
            ->where('report_id', '=', $id)
            ->delete();

        return redirect('/settings/reporting')->with('success', 'Your report has been deleted successfully');
    }

    public function adminSave(Request $request)
    {
        //Save the report's permissions
        $reportData = array(
            'report_name' => $request->report_name,
            'report_filename' => $request->report_filename,
            'report_description' => $request->report_description
        );

        if(strlen($request->report_url) > 0) {
			$reportData['report_url'] = $request->report_url;
		} else {
        	$reportData['report_url'] = null;
		}

        if($request->report_id == 0) {
            //Adding
            $report_id = DB::table('reports')
                ->insertGetID($reportData);
        } else {
            //Editing
            $report_id = $request->report_id;
            DB::table('reports')
                ->where('report_id', '=', $report_id)
                ->update($reportData);
        }

        //Permissions, using $report_id. Later?
        DB::table('report_users')
            ->where('report_id', '=', $report_id)
            ->delete();

        if(is_array($request->users)) {
            foreach ($request->users as $user) {
                DB::table('report_users')
                    ->insert([
                        'report_id' => $report_id,
                        'user_id' => $user
                    ]);
            }
        }

        return redirect('/settings/reporting')->with('success', 'Your report has been updated successfully');
    }

    private function getReportsOnDisk() {
        //Get an array of all reports on disk
        $reportArray = array();

        $dircontent = array();
        try {
            $dir = '/var/www/reports/htdocs/projects';
            $dircontent = scandir($dir);
        }
        catch (\Exception $e) {
            $dir = 'I:\\webserver\\htdocs\\linx\\staff\\public\\reports\\projects';
            $dircontent = scandir($dir);
        }

        foreach($dircontent as $project) {
            if($project != '.' && $project != '..' && $project != 'admin' && is_dir($dir.DIRECTORY_SEPARATOR.$project)) {
                $projectcontent = scandir($dir.DIRECTORY_SEPARATOR.$project);
                foreach($projectcontent as $report) {
                    $ext = substr($report, strrpos($report, '.') + 1);
                    if(in_array($ext, array("xml"))) {
                        $reportArray[$project][] = array(
//                            'project' => $project,
                            'report' => $report,
                            'filename' => $dir.DIRECTORY_SEPARATOR.$project.DIRECTORY_SEPARATOR.$report
                        );
                    }
                }
            }
        }

        return $reportArray;
    }

    public function weeklyReport() {
		$user = Auth::user();
		if(empty($_GET)) {
			//show a form with the date (restricted to Mondays) and job list (appropriate for the user)
			$jobs = DB::select(DB::raw('SELECT 
				j.id, j.name 
				
				FROM 
				jobs j
				join job_superlist js on js.jobs_id = j.id
				
				where
				js.user_id = '.$user->id.'
				AND
				(j.end_date is null or j.end_date >= \'NOW\')
				
				UNION
				
				SELECT 
				j.id, j.name
				
				FROM 
				jobs j
				JOIN client_users cu on cu.client_id = j.client_id
				
				where
				cu.user_id = '.$user->id.' and cu.level = 1
				AND
				(j.end_date is null or j.end_date >= \'NOW\')
				UNION
				
				SELECT
				j.id, j.name
				
				FROM 
				jobs j,
				users u
				
				WHERE
				u.id = '.$user->id.' and u.level = 3
				AND
				(j.end_date is null or j.end_date >= \'NOW\')'));
			return view('reports.weekly.search', [ 'jobs' => $jobs ]);
		} else {
			$query = 'SELECT us.given_name, us.surname, us.external_id, 
				jo.name, 
				ta.task_name, 
				ro.role_name,
				date(tt.tita_start) as tita_start,
				sum(tt.hours_ordinary + tt.hours_overtime + tt.hours_saturday + tt.hours_sunday + tt.hours_publicholiday) as tita_quantity
				 
				FROM 
				users us 
				JOIN job_worklist jw on jw.user_id = us.id 
				JOIN jobs jo on jo.id = jw.jobs_id 
				JOIN timesheets ts on ts.user_id = us.id 
				JOIN time_tasks tt on tt.time_id = ts.time_id 
				JOIN tasks ta on ta.task_id = tt.task_id and ta.jobs_id = jo.id
				LEFT JOIN roles ro on ro.role_id = tt.role_id
				 
				WHERE 
				(
				tt.supervisor_id = '.$user->id.' 
				OR
				((select level from client_users cu where cu.client_id = jo.client_id and cu.user_id = '.$user->id.' and cu.level) = 1)
				OR
				(select level from users where id = '.$user->id.') = 3
				)
				AND jo.id = '.$_GET['job'].'
				AND ts.time_start between \''.$_GET['start_date'].'\' and DATE(DATE_ADD(\''.$_GET['start_date'].'\', INTERVAL 8 DAY)) 
				AND
				(jw.enddate is null OR jw.enddate >= DATE(DATE_ADD(\''.$_GET['start_date'].'\', INTERVAL -1 DAY)))
				
				GROUP BY
				us.given_name, us.surname, us.external_id, 
				jo.name, ta.task_name, ro.role_name, date(tt.tita_start)
				
				ORDER BY
				jo.name, right(us.external_id, 4), tt.tita_start, us.given_name, us.surname';

			$reportdata = DB::select(DB::raw($query));
			$data = array();
			foreach($reportdata as $entry) {
				$data[$entry->external_id]['header'] = $entry->given_name.' '.$entry->surname.' ('.$entry->external_id.')';
				$data[$entry->external_id][$entry->tita_start] = $entry->tita_quantity;
			}
			if(isset($_GET['export']) && $_GET['export'] == 'csv') {
				//export
			} else {
				//Show results
				return view('reports.weekly.results', [ 'data' => $data ]);
			}
		}
	}
}