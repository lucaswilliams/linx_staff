<?php
namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\UserController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getRegister() {
    	$campaign_code = '';
    	if(isset($_GET['camp'])) {
    		$campaign_code = $_GET['camp'];
		}

		$campaign = array();
		$questions = DB::table('campaigns')
			->leftJoin('campaign_questions', 'campaign_questions.camp_id', '=', 'campaigns.camp_id')
			->where('campaigns.camp_code', '=', $campaign_code)
            ->where('campaigns.camp_active', '=', 1)
            ->orderBy('campaign_questions.caqu_order')
            ->orderBy('campaign_questions.caqu_id')
            ->select('campaign_questions.*', 'campaigns.camp_name', 'campaigns.camp_desc', 'campaigns.camp_id as campaign_id')
			->get();
		
		if(count($questions) > 0) {
		    $campaign['id'] = $questions[0]->campaign_id;
		    $campaign['name'] = $questions[0]->camp_name;
		    $campaign['desc'] = $questions[0]->camp_desc;
		    $campaign['questions'] = array();
        }

		foreach($questions as $question) {
		    if(strlen($question->caqu_question) > 0) {
                $options = DB::table('campaign_question_options')
                    ->where('caqu_id', '=', $question->caqu_id)
                    ->get();
                $question->options = $options;
                $campaign['questions'][] = $question;
            }
		}

    	return view('auth.register', ['campaign' => $campaign]);
	}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'given_name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:60|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);

        /*$uc = new UserController();

        if(strcmp($data['tfn'], 'xxx xxx xxx') != 0) {
            //something other than ignore.
            if(!$uc->ValidateTFN($data['tfn'])) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('tfn', 'Your Tax File Number does not match the required format');
                });
            }
        }*/

        return $validator;
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $camp_id = null;
        if(isset($data['campaign_id'])) {
        	$camp_id = $data['campaign_id'];
		}

		//echo '<pre>'; var_dump($data); echo '</pre>';
		//die();

    	$user = User::create([
            'given_name' => $data['given_name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'level' => 0,
            //'referral' => $data['referral'],
            'preferred' => $data['preferred'],
            //'accommodation' => $data['accommodation'],
            //'current_location' => $data['current_location'],
            //'date_arrive' => $data['date_arrive'],
            //'loca_arrive' => $data['loca_arrive'],
            //'visa_88_days' => $data['visa_88_days'],
            //'visa_expiry' => $data['visa_expiry'],
            //'passport_number' => $data['passport_number'],
            //'stay_length' => $data['stay_length'],
            //'country' => $data['country'],
            'telephone' => $data['telephone'],
            'mobilephone' => $data['mobilephone'],
            //'has_transport' => $data['has_transport'],
            //'num_in_group' => $data['num_in_group'],
            //'share_details' => $data['share_details'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state' => $data['state'],
            'postcode' => $data['postcode'],
            'no_address' => $data['no_address'] ?? 0,
            'date_of_birth' => $data['date_of_birth_year'].'-'.$data['date_of_birth_month'].'-'.$data['date_of_birth_day'],
            'gender' => $data['gender'],
            //'account_name' => $data['account_name'],
            //'account_bsb' => $data['account_bsb'],
            //'account_number' => $data['account_number'],
            //'tfn' => $data['tfn'],
            //'tax_resident' => $data['tax_resident'],
            //'tax_free_threshold' => $data['tax_free_threshold'],
            //'senior_tax_offset' => $data['senior_tax_offset'],
            //'help_debt' => $data['help_debt'],
            //'fs_debt' => $data['fs_debt'],
            //'provider_id_other' => $data['provider_id_other'],
            //'provider_id' => $data['provider_id'],
            //'super_number' => $data['super_number'],
            //'job_network' => $data['job_network'],
            //'job_network_tick' => $data['job_network_tick'],
            //'usi_id' => $data['usi_id'],
			//'usi_id_other' => $data['usi_id_other'],
			'campaign_id' => $camp_id
        ]);
        
        $reid = [];
        foreach($_POST as $k => $v) {
            if(strpos($k, 'reid-') !== false) {
                $reid[$k] = $v;
            }
        }
        
        if(count($reid) > 0) {
            DB::table('users')
                ->where('id', '=', $user->id)
                ->update($reid);
        }

    	if(isset($data['caqu'])) {
    		foreach($data['caqu'] as $caqu => $cqop) {
    			$insertData = array(
					'user_id' => $user->id,
					'caqu_id' => $caqu
				);

    			if(is_numeric($cqop)) {
    				$insertData['response_integer'] = $cqop;
					$insertData['response_string'] = $cqop;
				} else {
					$insertData['response_integer'] = 0;
					$insertData['response_string'] = $cqop;
				}

    			DB::table('user_campaign_questions')
					->insert($insertData);
			}
		}

		return $user;
    }
}