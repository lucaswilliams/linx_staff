<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Validator;
use DB;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers, RegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/dashboard';
    protected $loginPath = '/login';

    protected $username = 'username';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'given_name' => 'required|max:50',
            'surname' => 'required|max:50',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:60|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //die('Auth/Create');
        return User::create([
            'given_name' => $data['given_name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'level' => $data['level'],
            'referral' => $data['referral']
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        //This postregister hook is NEVER called.
    	die('Auth/postRegister');
        $this->validate($request, [
            'given_name' => 'required|max:50',
            'surname' => 'required|max:50',
            'date_of_birth_month' => 'required',
            'date_of_birth_day' => 'required',
            'date_of_birth_year' => 'required',

            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'postcode' => 'required',

            'email' => 'required|max:255',
            'username' => 'required|min:6',
            'password' => 'required|min:6|confirmed',

            'account_bsb' => 'required|min:6|max:7',
            'account_number' => 'required|min:5|max:10',
            'account_name' => 'required|max:32',

            'tfn' => 'required|min:9|max:11',
            'tax_resident' => 'required',
            'tax_free_threshold' => 'required',
            'senior_tax_offset' => 'required',
            'help_debt' => 'required',
            'fs_debt' => 'required',

            'current_location' => 'required',
        ], [
            'given_name.required' => 'You need to supply your first given name',
            'given_name.max' => 'Please enter less than 50 characters',
            'surname.required' => 'You need to supply your surname',
            'surname.max' => 'Please enter less than 50 characters',
            'email.required' => 'You need to supply your email address',
            'email.max' => 'Please enter less than 50 characters',
            'username.required' => 'You need to supply a username',
            'username.min' => 'Please enter at least 6 characters',
            'password.required' => 'You need to supply a password',
            'password.min' => 'Please enter at least 6 characters',
            'password.confirmed' => 'Your passwords don\'t match',
            'account_bsb.required' => 'You need to supply your BSB',
            'account_bsb.min' => 'Your BSB is not long enough. It must be 6 characters, or 7 with a separator',
            'account_bsb.max' => 'Your BSB is too long. It must be 6 characters, or 7 with a separator',
            'account_number.required'=> 'You need to supply your account number',
            'account_name.required' => 'You need to supply your account name',
            'tfn.required' => 'You need to supply your Tax File Number, or xxx xxx xxx if you do not yet have one',
            'tfn.min' => 'Tax file numbers are 9 digits, or formatted like xxx xxx xxx',
            'tfn.max' => 'Tax file numbers are 9 digits, or formatted like xxx xxx xxx',
            'date_of_birth_month.required' => 'You need to supply a month for your date of birth',
            'date_of_birth_day.required' => 'You need to supply a day for your date of birth',
            'date_of_birth_year.required' => 'You need to supply a year for your date of birth',
            'tax_resident.required' => 'Please select an option',
            'tax_free_threshold.required' => 'Please select an option',
            'senior_tax_offset.required' => 'Please select an option',
            'help_debt.required' => 'Please select an option',
            'fs_debt.required' => 'Please select an option',
            'current_location.required' => 'Please select an option'
        ]);

        if($request->job_network_tick == 1) {
            $this->validate($request, [
                'job_network' => 'required'
            ], [
                'job_network.required' => 'If you are registered with a Job Network, please provide their name'
            ]);
        }

        if($request->visa_88_days > 0) {
            $this->validate($request, [
                'passport_number' => 'required',
                'visa_expiry' => 'required'
            ], [
                'passport_number.required' => 'Please enter your passport number.',
                'visa_expiry.required' => 'Please enter your visa expiry date.'
            ]);
        }

        //Must have at least one phone number
		if(strlen($request->mobilephone) == 0 && strlen($request->telephone) == 0) {
			$this->validate($request, [
				'telephone' => 'required',
				'mobilephone' => 'required'
			], [
				'mobilephone.required' => 'Either your Mobile or Landline phone number is required.'
			]);
		}

        Auth::login($this->create(array(
            'given_name' => $request->given_name,
            'surname' => $request->surname,
            'email' => $request->email,
            'username' => $request->username,
            'password' => $request->password,
            'level' => 0,
            'referral' => $request->referral
        )));

        $user = Auth::user();
        //Do staff details for the user we just made.
        //This takes out the need to see if it exists when they edit their profile (thus breaking other things)
        DB::table('users')
            ->where('id', '=', $user->id)
            ->update([
                'preferred' => $request->preferred,
                'accommodation' => $request->accommodation,
                'current_location' => $request->current_location,
                'date_arrive' => $request->date_arrive,
                'loca_arrive' => $request->loca_arrive,
                'visa_88_days' => $request->visa_88_days,
                'visa_expiry' => $request->visa_expiry,
                'passport_number' => $request->passport_number,
                'stay_length' => $request->stay_length,
                'country' => $request->country,
                'telephone' => $request->telephone,
                'mobilephone' => $request->mobilephone,
                'has_transport' => $request->has_transport,
                'num_in_group' => $request->num_in_group,
                'share_details' => $request->share_details,
                'address' => $request->address,
                'city' => $request->city,
                'state' => $request->state,
                'postcode' => $request->postcode,
                'date_of_birth' => $request->date_of_birth_year.'-'.$request->date_of_birth_month.'-'.$request->date_of_birth_day,
                'gender' => $request->gender,
                'account_name' => $request->account_name,
                'account_bsb' => $request->account_bsb,
                'account_number' => $request->account_number,
                'tfn' => $request->tfn,
                'tax_resident' => $request->tax_resident,
                'tax_free_threshold' => $request->tax_free_threshold,
                'senior_tax_offset' => $request->senior_tax_offset,
                'help_debt' => $request->help_debt,
                'fs_debt' => $request->fs_debt,
                'referral' => $request->referral,
                'provider_id_other' => $request->provider_id_other,
                'usi_id_other' => $request->usi_id_other,
                'provider_id' => $request->provider_id,
                'super_number' => $request->super_number,
                'job_network' => $request->job_network,
                'job_network_tick' => $request->job_network_tick
            ]);

        //Capture all the user data
        $userdata = array(
            'given_name' => $request->given_name,
            'surname' => $request->surname,
            'email' => $request->email,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'level' => 0,
            'referral' => $request->referral,
            'user_id' => $user->id,
            'current_location' => $request->current_location,
            'date_arrive' => $request->date_arrive,
            'loca_arrive' => $request->loca_arrive,
            'visa_88_days' => $request->visa_88_days,
            'stay_length' => $request->stay_length,
            'country' => $request->country,
            'preferred' => $request->preferred
        );

        $email = $request->email;

        /*
        //Send an email to linx
        Mail::send('emails.newregister', ['user' => $userdata], function ($message) {
            $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
            $message->to('kim@linxemployment.com.au');
            $message->subject('New sign-up with Linx Employment');
        });
        */

        //Send one to the user too
        try {
            Mail::send('emails.welcome', ['user' => $userdata], function ($message) use ($email) {
                $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                $message->to($email);
                $message->subject('Your sign-up with Linx Employment');
            });
        } catch(Exception $ex) {
            //Ignore
        }

        return redirect($this->redirectPath);
    }
}