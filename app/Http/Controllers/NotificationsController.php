<?php namespace App\Http\Controllers;

use App\Http\Controllers\Settings\MYOBController;
use App\Http\Controllers\Settings\SuperController;
use App\Http\Controllers\Settings\InductionsController;
use DB;
use Auth;
use Mail;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function getNotifications() {
        $user = Auth::user();
        $notQ = DB::table('notifications')
            ->where('user_to_id', '=', $user->id)
            ->where('noti_read', '=', 0)
            ->whereRaw('noti_type & 1 > 0');

        $nots = $notQ->count();

        return response()->json($nots);
    }

    public function getNotificationDetail() {
        $user = Auth::user();
        $nots = DB::table('notifications')
            ->where('user_to_id', '=', $user->id)
            ->where('noti_read', '=', 0)
            ->whereRaw('noti_type & 1 > 0')
            ->get();

        return response()->json($nots);
    }

    public function markRead() {
        $user = Auth::user();
        DB::table('notifications')
            ->where('user_to_id', '=', $user->id)
            ->update(['noti_read' => 1]);
    }

    public function addNotification($data) {
        //notification types
        //1 - Screen
        //2 - Email
        //4 - Admin email
        //8 - SMS

        DB::table('notifications')
            ->insert($data);

        if($data['noti_type'] & 2 > 0) {
            //send user an email
        }

        if($data['noti_type'] & 4 > 0) {
            //send Linx an email
        }

        if($data['noti_type'] & 8 > 0) {
            //TODO: Implement SMS
        }
    }
}