<?php namespace App\Http\Controllers\Kingsley;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KingsleyController extends Controller {

    public function dashboard()
    {
        return view('kingsley.dashboard');
    }
}