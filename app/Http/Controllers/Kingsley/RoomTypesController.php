<?php namespace App\Http\Controllers\Kingsley;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomTypesController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $rooms = DB::table('roomtypes')
            ->select('*')
            ->orderBy('roomtype_name')
            ->get();
        //Display it as a list.
        return view('kingsley.roomtypes.list', ['roomtypes' => $rooms]);
    }

    /**
     * Show the form for the given room type.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesroomExist = DB::table('roomtypes')
            ->where('roomtype_id', '=', $id)
            ->get();
        if($id > 0 && count($doesroomExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified room type does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $uni = DB::table('roomtypes')
            ->select('*')
            ->where('roomtype_id', '=', $id)
            ->get();

        if(count($uni) > 0) {
            return view('kingsley.roomtypes.form', ['roomtype' => $uni[0]]);
        } else {
            return view('kingsley.roomtypes.form', ['roomtype' => (object)array(
                'roomtype_id' => null,
                'roomtype_name' => null,
                'roomtype_image' => null,
                'roomtype_position_image' => null
            )]);
        }
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($request->roomtype_id > 0) {
            $uni_id = $request->roomtype_id;


            if($validator->fails()) {
                return redirect('kingsley/roomtypes/'.$uni_id)->withErrors($validator)->withInput();
            }

            DB::table('roomtypes')
                ->where('roomtype_id', $uni_id)
                ->update([
                    'roomtype_name' => $request->name,
                    'roomtype_image' => $request->image,
                    'roomtype_position_image' => $request->layout_image
                ]);

            return redirect('kingsley/roomtypes')->with('success', 'Your room type has updated successfully');
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('kingsley/roomtypes/add')->withErrors($validator)->withInput();
            }

            DB::table('roomtypes')
                ->insert([
                    'roomtype_name' => $request->name,
                    'roomtype_image' => $request->image,
                    'roomtype_position_image' => $request->layout_image
                ]);
            return redirect('kingsley/roomtypes')->with('success', 'Your room type has been added successfully');
        }
    }

    public function delete($id)
    {
        DB::table('roomtypes')
            ->where('roomtype_id', $id)
            ->delete();
        //Display it as a list.
        return redirect('kingsley/roomtypes')->with('success', 'The room type has been deleted');
    }
}