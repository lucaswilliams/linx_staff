<?php namespace App\Http\Controllers\Kingsley;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomsController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList()
    {
        //Get all the staff in the system
        $rooms = DB::table('rooms')
            ->join('roomtypes', 'roomtypes.roomtype_id', '=', 'rooms.roomtype_id')
            ->select('rooms.*', 'roomtypes.roomtype_name')
            ->orderBy('rooms.room_name')
            ->get();
        //Display it as a list.
        return view('kingsley.rooms.list', ['rooms' => $rooms]);
    }

    /**
     * Show the form for the given rateential.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm($id = 0)
    {
        //check that this user actually exists
        $doesroomExist = DB::table('rooms')
            ->where('room_id', '=', $id)
            ->get();
        if($id > 0 && count($doesroomExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified room does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $uni = DB::table('rooms')
            ->select('*')
            ->where('room_id', '=', $id)
            ->get();

        $beds = DB::table('beds')
            ->select('*')
            ->where('room_id', '=', $id)
            ->get();

        $roomtypes = DB::table('roomtypes')
                         ->get();

        if(count($uni) > 0 && count($beds) > 0) {
            return view('kingsley.rooms.form', ['room' => $uni[0], 'beds' => $beds, 'roomtypes' => $roomtypes]);
        } else {
            return view('kingsley.rooms.form', ['room' => (object)array(
                    'room_id' => null,
                    'room_name' => null,
                    'roomtype_id' => null,
                    'room_capacity' => null,
                    'beds' => null
                ),
                'beds' => null,
                'roomtypes' => $roomtypes]);
        }
    }

    public function saveForm(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($request->room_id > 0) {
            $room_id = $request->room_id;

            if($validator->fails()) {
                return redirect('kingsley/rooms/'.$room_id)->withErrors($validator)->withInput();
            }

            DB::table('rooms')
                ->where('room_id', $room_id)
                ->update([
                    'room_name' => $request->name,
                    'roomtype_id' => $request->roomtype_id,
                    'room_capacity' => count($request->bed_names)
                ]);

            $this->save_beds($room_id, $request);

            return redirect('kingsley/rooms')->with('success', 'Your room has updated successfully');
        } else {
            //Perform an add
            if($validator->fails()) {
                return redirect('kingsley/rooms/add')->withErrors($validator)->withInput();
            }

            $room_id = DB::table('rooms')
                ->insertGetId([
                    'room_name' => $request->name,
                    'roomtype_id' => $request->roomtype_id,
                    'room_capacity' => count($request->bed_names)
                ]);

            $this->save_beds($room_id, $request);
            return redirect('kingsley/rooms')->with('success', 'Your room has been added successfully');
        }
    }

    private function save_beds($room_id, $request) {
        foreach($request->bed_names as $key=>$value) {
            if(isset($request->bed_ids[$key]) && $request->bed_ids[$key] > 0) {
                //Update
                DB::table('beds')
                    ->where('beds_id', '=', $request->bed_ids[$key])
                    ->update(['beds_name' => $value]);
            } else {
                //Insert
                DB::table('beds')
                    ->insert(['room_id' => $room_id, 'beds_name' => $value]);
            }
        }
    }

    public function delete($id)
    {
        DB::table('rooms')
            ->where('room_id', $id)
            ->delete();
        //Display it as a list.
        return redirect('kingsley/rooms')->with('success', 'The room has been deleted');
    }

    public function getAvailableRooms($datestart, $datefinish) {
        $rooms = DB::table('rooms')
            ->get();
        $roomdata = array();
        foreach($rooms as $room) {
            //Check to see if it's in use
            $resp = $this->getAvailableBeds($room->room_id, $datestart, $datefinish, true);
            $active = false;
            foreach($resp as $rm) {
                if($rm['active']) {
                    //Done this way so that the last result being false doesn't screw the rest.
                    $active = true;
                }
            }

            $roomdata[] = array(
                'value' => $room->room_id,
                'display' => $room->room_name,
                'active' => $active
            );
        }
        return response()->json($roomdata);
    }

    public function getAvailableBeds($room_id, $datestart, $datefinish, $backend = false) {
        $beds = DB::table('beds')
            ->where('room_id', $room_id)
            ->get();
        $bedsdata = array();
        foreach($beds as $bed) {
            //Is it available during this booking?
            $activeQ = DB::table('booking_users')
                ->join('bookings', 'bookings.booking_id', '=', 'booking_users.booking_id')
                ->where('booking_users.beds_id', $bed->beds_id)
                ->where('bookings.departure_date', '>', $datestart)
                ->where('bookings.arrival_date', '<', $datefinish)
                ->get();

            //Set active-ness based on whether or not there are 0 rows returned.
            $active = (count($activeQ) == 0);

            //populate the array
            $bedsdata[] = array(
                'value' => $bed->beds_id,
                'display' => $bed->beds_name,
                'active' => $active
            );
        }
        if($backend) {
            return $bedsdata;
        } else {
            return response()->json($bedsdata);
        }
    }
}