<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Log;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Settings\MYOBController;

class JobsController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showList($client_id = null)
    {
        $showClie = true;

        if($client_id) {
            $showClie = false;
        }
        $jobsQ = $this->getJobList($client_id);
        $jobs = $jobsQ->get();

        return view('jobs.list', ['jobs' => $jobs, 'clie' => $showClie, 'all' => false]);
    }

    public function showAllList() {
		$showClie = true;

		$jobsQ = $this->getJobList(null, true);
		$jobs = $jobsQ->get();

		return view('jobs.list', ['jobs' => $jobs, 'clie' => $showClie, 'all' => true]);
	}

    public function showForm($id = 0)
    {
        if($id > 0) {
            $jobs = DB::table('jobs')
                ->join('clients', 'clients.id', '=', 'jobs.client_id')
                ->select('jobs.*', 'clients.name as client_name')
                ->where('jobs.id', '=', $id)
                ->get();
        } else {
            $jobs = array((object)array(
                'client_name' => null,
                'id' => null,
                'name' => null,
                'start_date' => null,
                'end_date' => null,
                'client_id' => 0,
                'role_id' => null
            ));
        }

        $clients = DB::table('clients')
            ->select('clients.name', 'clients.id')
            ->whereRaw('clients.id = '.$jobs[0]->client_id.' or clients.status = 1')
            ->get();

        try {
            $mc = new MYOBController();
            $myobJobs = $mc->getJobs();
        }
        catch (Exception $e) {
            //Log::info($e);
            $myobJobs = array();
        }


        $worklist = DB::table('job_worklist')
            ->join('users', 'users.id', '=', 'job_worklist.user_id')
            ->where('job_worklist.jobs_id', '=', $id)
            ->select('users.id', 'users.given_name', 'users.surname', 'users.external_id', 'job_worklist.startdate', 'job_worklist.enddate', 'job_worklist.id as jw_id')
            ->orderBy('job_worklist.startdate')
            ->orderBy('users.external_id')
            ->get();

        $shiftlist = array();
        $shiftQ = DB::table('shifts')
            ->where('jobs_id', '=', $id)
            ->orderBy('start_time', 'asc')
            ->get();

        foreach($shiftQ as $shift) {
            $shiftlist[] = $shift;
        }

        $uc = new UserController();
        $supervisors = $uc->getSupervisors(1);

        $hrate = DB::table('payrates')
            ->where('rate_type', '=', 0)
            ->where('rate_archived', '=', 0)
            ->get();

        $prate = DB::table('payrates')
            ->where('rate_type', '=', 1)
            ->where('rate_archived', '=', 0)
            ->get();

        $jobsQ = $this->getJobList();
        $jobsearch = $jobsQ->get();

        return view('jobs.form', [
            'job' => $jobs[0],
            'clients' => $clients,
            'workers' => $worklist,
            'jobs' => $jobsearch,
            'mjobs' => $myobJobs,
            'shifts' => $shiftlist,
            'hrate' => $hrate,
            'prate' => $prate,
            'supervisors' => $supervisors
        ]);
    }

    public function shiftForm($id)
    {
        $jobs = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->select('jobs.*', 'clients.name as client_name')
            ->where('jobs.id', '=', $id)
            ->get();
        $shiftlist = array();
        $shiftQ = DB::table('shifts')
            ->where('jobs_id', '=', $id)
            ->orderBy('start_time', 'asc')
            ->get();

        foreach($shiftQ as $shift) {
            $taskslist = DB::table('tasks')
                ->join('shift_tasks', 'shift_tasks.task_id', '=', 'tasks.task_id')
                ->where('shift_tasks.shift_id', '=', $shift->id)
				->orderBy('tasks.task_name')
                ->get();

            $tasks = array();
            foreach($taskslist as $task) {
                $tasks[] = $task->task_name;
            }

            $shift->tasks = $tasks;
            $shiftlist[] = $shift;
        }

        $hrate = DB::table('payrates')
            ->where('rate_type', '=', 0)
            ->where('rate_archived', '=', 0)
            ->get();

        $prate = DB::table('payrates')
            ->where('rate_type', '=', 1)
            ->where('rate_archived', '=', 0)
            ->get();

        return view('jobs.shiftform', [
            'job' => $jobs[0],
            'shifts' => $shiftlist,
            'hrate' => $hrate,
            'prate' => $prate
        ]);
    }

    public function staffForm($id)
    {
        $jobs = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->select('jobs.*', 'clients.name as client_name')
            ->where('jobs.id', '=', $id)
            ->get();
        $worklist = DB::table('job_worklist')
            ->join('users', 'users.id', '=', 'job_worklist.user_id')
            ->where('job_worklist.jobs_id', '=', $id)
            ->select('job_worklist.id', 'users.given_name', 'users.surname', 'users.external_id', 'users.confirmed', 'users.has_medical', 'job_worklist.startdate', 'job_worklist.enddate', 'job_worklist.supervisor_id', 'job_worklist.id as jw_id')
            ->orderBy('job_worklist.enddate')
            ->orderBy(DB::raw('right(users.external_id, 5)'))
            ->get();

        $jobsQ = $this->getJobList();
        $jobsearch = $jobsQ->get();

        return view('jobs.staffform', [
            'job' => $jobs[0],
            'workers' => $worklist,
            'jobs' => $jobsearch,
        ]);
    }

    public function superForm($id)
    {
        $jobs = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->select('jobs.*', 'clients.name as client_name')
            ->where('jobs.id', '=', $id)
            ->get();
        $worklist = DB::table('job_superlist')
            ->join('users', 'users.id', '=', 'job_superlist.user_id')
            ->where('job_superlist.jobs_id', '=', $id)
            ->select('job_superlist.id', 'users.given_name', 'users.surname', 'users.preferred', 'users.external_id', 'job_superlist.default')
            ->orderBy(DB::raw('right(users.external_id, 5)'))
            ->get();

        $jobsQ = $this->getJobList();
        $jobsearch = $jobsQ->get();

        return view('jobs.superform', [
            'job' => $jobs[0],
            'workers' => $worklist,
            'jobs' => $jobsearch
        ]);
    }

    public function makeSupervisorDefault($jobs_id, $id) {
        DB::update(DB::raw('UPDATE job_superlist SET `default` = 0 WHERE jobs_id = '.$jobs_id));
        DB::table('job_superlist')
            ->where('id', '=', $id)
            ->update(['default' => 1]);

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function deleteSupervisor($jobs_id, $id) {
        DB::table('job_superlist')
            ->where('id', '=', $id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function taskForm($id)
    {
        $jobs = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->select('jobs.*', 'clients.name as client_name')
            ->where('jobs.id', '=', $id)
            ->get();
        $tc = new TasksController();

        $jobsQ = $this->getJobList();
        $jobsearch = $jobsQ->get();

        $job = $jobs[0];
        $tasklist = $tc->getSelectListForJob($job->id);

        return view('jobs.taskform', [
            'job' => $job,
            'tasks' => $tasklist,
            'jobs' => $jobsearch
        ]);
    }

    public function deleteTask($id) {
        //but only if it does not have any children
        $children = DB::table('tasks')
            ->where('task_parent_id', '=', $id)
            ->count();
        if($children > 0) {
            return redirect($_SERVER['HTTP_REFERER'])->with('error', 'Cannot delete this task as it has sub-tasks');
        }

        //Can't delete it if it's in use either
        $usage = DB::table('time_tasks')
            ->where('task_id', '=', $id)
            ->count();
        if($usage > 0) {
            return redirect($_SERVER['HTTP_REFERER'])->with('error', 'Cannot delete this task as it appears on time sheets.');
        }

        DB::table('tasks')
            ->where('task_id', '=', $id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER'])->with('success', 'The task was deleted successfully');
    }

    public function editTask($id) {
        //Show a form to edit this task.
        $tasks = DB::table('tasks')
            ->where('task_id', '=', $id)
            ->get();
        if(count($tasks) == 0) {
            return view('errors.access', ['message' => 'That task does not exist']);
        }

        $task = $tasks[0];
        $tc = new TasksController();
        $tasklist = $tc->getSelectListForJob($task->jobs_id);

        $rates = DB::table('payrates')
            ->where('rate_archived', '=', 0)
            ->orderBy('name')
            ->select('id', 'name')
            ->get();

        $task_roles = DB::table('task_roles')
			->join('roles', 'roles.role_id', '=', 'task_roles.role_id')
			->where('task_id', '=', $id)
			->select('task_roles.taro_id', 'task_roles.rate_id', 'task_roles.task_id', 'task_roles.role_id', 'roles.role_name')
			->get();

        return view('jobs.edittask', [
            'rates' => $rates,
            'tasks' => $tasklist,
            'task' => $task,
			'taskroles' => $task_roles
        ]);
    }

    public function saveTask(Request $request) {
        DB::table('tasks')
            ->where('task_id', '=', $request->task_id)
            ->update([
                'task_name' => $request->task_name,
                //'rate_id' => $request->rate_id,
                'task_parent_id' => $request->task_parent_id,
                'task_desc' => $request->task_desc
            ]);

        foreach($request->rate_id as $taro_id => $rate_id) {
        	DB::table('task_roles')
				->where('taro_id', '=', $taro_id)
				->update(['rate_id' => $rate_id]);
		}

        return redirect('jobs/'.$request->jobs_id.'/tasks')->with('success', 'The task was edited successfully');
    }

    public function printDetails($id = 0)
    {
        if($id > 0) {
            $jobs = DB::table('jobs')
                ->join('clients', 'clients.id', '=', 'jobs.client_id')
                ->select('jobs.*', 'clients.name as client_name')
                ->where('jobs.id', '=', $id)
                ->get();
        } else {
            return view('errors.user', ['error_message' => 'The specified job does not exist.']);
        }

        $worklist = DB::table('job_worklist')
            ->join('users', 'users.id', '=', 'job_worklist.user_id')
            ->where('job_worklist.jobs_id', '=', $id)
            ->where('job_worklist.startdate', '<=', date('Y-m-d'))
            ->where(function($where) {
                $where->whereNull('job_worklist.enddate')
                    ->orWhere('job_worklist.enddate', '>=', date('Y-m-d'));
            })
            ->select('users.id', 'users.given_name', 'users.surname', 'users.external_id',
                'users.mobilephone', 'users.email', 'job_worklist.startdate', 'job_worklist.enddate')
            ->get();

        return view('jobs.listprint', [
            'job' => $jobs[0],
            'workers' => $worklist
        ]);
    }

    public function saveForm(Request $request)
    {
        $type = $request->form_type; // 0 - details; 1 - shifts; 2 - staff; 4 - tasks
        if ($request->job_id == 0) {
            $msg = 'added';
        } else {
            $msg = 'edited';
        }

        if($type == 0) {
            $enddate = null;
            if(strtotime($request->job_end) > 0) {
                $enddate = $request->job_end;
            }
            if ($request->job_id == 0) {
                //Do an add
                $job_id = DB::table('jobs')
                    ->insertGetId([
                        'client_id' => $request->client,
                        'name' => $request->name,
                        'start_date' => $request->job_start,
                        'end_date' => $enddate,
                        'jobs_uid' => $request->job_uid
                    ]);
            } else {
                //Do an edit
                $job_id = $request->job_id;
                DB::table('jobs')
                    ->where('id', '=', $job_id)
                    ->update([
                        'client_id' => $request->client,
                        'name' => $request->name,
                        'start_date' => $request->job_start,
                        'end_date' => $enddate,
                        'jobs_uid' => $request->job_uid
                    ]);
            }
        }

        if($type == 1) {
            $job_id = $request->job_id;
        }

        if($type == 2) {
            $job_id = $request->job_id;

            $users = DB::table('job_worklist')
                ->where('jobs_id', '=', $job_id)
                ->select('user_id')
                ->get();

            $userArray = array();
            foreach($users as $user) {
                $userArray[] = $user->user_id;
            }

            /*DB::table('job_worklist')
                ->where('jobs_id', '=', $job_id)
                ->delete();*/

            $defaultsuper = null;
            $supers =DB::table('job_superlist')
                ->where('jobs_id', '=', $job_id)
                ->where('default', '=', 1)
                ->select('user_id')
                ->get();
            //var_dump($supers);
            if(count($supers) > 0) {
                $defaultsuper = $supers[0]->user_id;
            }


            if(isset($request->user_ids) && is_array($request->user_ids)) {
                foreach($request->user_ids as $key => $user) {
                    $enddate = null;
                    if(strtotime($request->w_end_date[$key]) > 0) {
                        $enddate = date('Y-m-d H:i:s', strtotime($request->w_end_date[$key] . ' 23:59:59'));
                    }

                    if(isset($request->supervisor_ids[$key]) && $request->supervisor_ids[$key] > 0) {
                        $super = $request->supervisor_ids[$key];
                    } else {
                        $super = $defaultsuper;
                    }

                    DB::table('job_worklist')
                        ->where('id', '=', $user)
                        ->update([
                            'startdate' => $request->w_start_date[$key],
                            'enddate' => $enddate,
                            'supervisor_id' => $super
                        ]);
                }
            }
            
            if (isset($request->new_user_ids) && is_array($request->new_user_ids)) {
                foreach ($request->new_user_ids as $key => $user) {
                    //Generate an employee ID if they don't have one
                    $users = DB::table('users')
                        ->where('id', '=', $user)
                        ->whereRaw('nullif(external_id, \'\') is null')
                        ->get();
                    
                    if(count($users) > 0) {
                        $sql = 'SELECT right(external_id, 5) as external_id FROM users WHERE right(external_id, 5) REGEXP(\'[0-9]{5}\') order by right(external_id, 5) desc';
    
                        $externalQ = DB::select($sql);
    
                        $external = $externalQ[0];
                        $temp_id = intval($external->external_id);
                        $emp_id =  'LINX'.str_pad(($temp_id + 1), 5, '0', STR_PAD_LEFT);
                        
                        DB::table('users')
                            ->where('id', '=', $user)
                            ->update(['external_id' => $emp_id]);
                    }
                    
                    $enddate = null;
                    if(strtotime($request->n_end_date[$key]) > 0) {
                        $enddate = date('Y-m-d H:i:s', strtotime($request->n_end_date[$key] . ' 23:59:59'));
                    }

                    $worker_id = DB::table('job_worklist')
                        ->insertGetId([
                            'jobs_id' => $job_id,
                            'user_id' => $user,
                            'startdate' => $request->n_start_date[$key],
                            'enddate' => $enddate,
                            'supervisor_id' => $defaultsuper,
                            'needs_accepting' => 1
                        ]);
                    
                    //Send off to Zapier
                    //$this->SendWorkerToZapier($worker_id);

                    $userdata = DB::table('users')
                        ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
                        ->join('jobs', 'jobs.id', '=', 'job_worklist.jobs_id')
                        ->where('users.id', '=', $user)
                        ->where('jobs.id', '=', $job_id)
                        ->get();

                    if(count($userdata) > 0 && $request->send_email == 1) {
                        $userd = $userdata[0];
                        $to_address = $userd->email;
                        if(strlen($to_address) > 0) {
                            Log::info('Worker ' . $userd->external_id . ' added to job ' . $job_id . ' and emailed at ' . $to_address);
                            try {
                                Mail::send('emails.approved', ['user' => $userd], function ($message) use ($userd, $to_address) {
                                    $message->from('admin@linxemployment.com.au', 'Linx Employment Staff System');
                                    $message->to($to_address);
                                    $message->subject('Your job with Linx Employment');
                                });
                            } catch(Exception $ex) {
                                //Ignore
                            }
                        }
                    }
                }
            }
        }

        if($type == 4) {
            $job_id = $request->job_id;
        }

        if($type == 8) {
            $job_id = $request->job_id;

            //are there any supervisors already?
            $supers = DB::table('job_superlist')
                ->where('jobs_id', '=', $job_id)
                ->count();

            if(isset($request->new_user_ids) && is_array($request->new_user_ids)) {
                foreach($request->new_user_ids as $key => $user) {
                    DB::table('job_superlist')
                        ->insert([
                            'jobs_id' => $job_id,
                            'user_id' => $user,
                            'default' => ($supers == 0)
                        ]);

                    $supers++;
                }
            }
        }

        if(isset($request->save_details)) {
            return redirect('jobs/'.$job_id)->with('success', 'Your job has been '.$msg.' successfully.');
        }

        if(isset($request->save_super)) {
            return redirect('jobs/'.$job_id.'/super')->with('success', 'Your job has been '.$msg.' successfully.');
        }

        if(isset($request->save_tasks)) {
            return redirect('jobs/'.$job_id.'/tasks')->with('success', 'Your job has been '.$msg.' successfully.');
        }

		if(isset($request->save_roles)) {
			return redirect('jobs/'.$job_id.'/roles')->with('success', 'Your job has been '.$msg.' successfully.');
		}

		if(isset($request->save_staff)) {
            return redirect('jobs/'.$job_id.'/staff')->with('success', 'Your job has been '.$msg.' successfully.');
        }

        return redirect('jobs')->with('success', 'Your job has been '.$msg.' successfully.');
    }
    
    public function AdminSendToZapier($id) {
        $require = env('COMPOSER_AUTOLOAD');
        require_once($require);
        $slack = new \Slack(env('SLACK_ADDRESS'));
        $slack->setDefaultUsername("TMS Logger");
        $slack->setDefaultChannel("#logging-tms");
    
        $data = DB::select(DB::raw('SELECT
            u.given_name, u.surname, u.external_id as employee_id
            FROM users u
            JOIN job_worklist jw on jw.user_id = u.id
            WHERE jw.id = '.$id));
        
        if(count($data) > 0) {
            $message = new \SlackMessage($slack);
            $message->setText($data[0]->given_name.' '.$data[0]->surname.' ('.$data[0]->employee_id.') was sent to Smartsheet by admin.');
            $message->send();
            $this->SendWorkerToZapier($id);
        }
    }
    
    public function SendWorkerToZapier($id) {
        $data = DB::select(DB::raw('SELECT
            u.given_name, u.middle_name, u.surname, u.external_id as employee_id, u.gender, u.title,
            u.address, u.city, u.state, u.postcode, u.date_of_birth, u.mobilephone, u.email, u.tfn,
            u.provider_id as super_fund_name, u.super_number as super_number, coalesce(su.usi_number, u.usi_id_other) as super_usi, 
            c.name AS client_name, u.created_at, u.updated_at, jw.startdate as start_date, jw.enddate as end_date,
            (select count(*) from job_worklist jj where jj.id <> jw.id and jj.startdate < jw.startdate and jj.user_id = u.id) + 1 as job_number, 
            u.tax_resident, case when u.tax_free_threshold = 1 then \'Yes\' else \'No\' end as tax_free_threshold, case when u.help_debt = 1 then \'Yes\' when u.fs_debt = 1 then \'Yes\' else \'No\' end as has_debt,
            case when visa_88_days = 1 then \'H\' else \'A\' end as residence_status, \'C\' as employment_status,
            u.previous_names, u.previous_given_name, u.previous_middle_name, u.previous_surname, case when u.seasonal_worker = 1 then \'Yes\' else \'No\' end as seasonal_worker
            FROM users u
            JOIN job_worklist jw on jw.user_id = u.id
            JOIN jobs j ON j.id = jw.jobs_id
            JOIN clients c ON c.id = j.client_id
            LEFT JOIN super_usi su ON su.usi_id = u.usi_id
            WHERE jw.id = '.$id));
    
        $slack = new \Slack(env('SLACK_ADDRESS'));
        $slack->setDefaultUsername("TMS Logger");
        $slack->setDefaultChannel("#logging-tms");
        $message = new \SlackMessage($slack);
        
        if(count($data) > 0) {
            /*
             * Payee ID                     users.id
             * Employee ID                  users.external_id
             * TFN                          users.tfn
             * Surname                      users.surname
             * First Name                   users.given_name
             * Second Given Name            users.middle_name *
             * Title                        users.title
             * Previous Surname
             * Previous First Name
             * Previous Second Given Name
             * DoB Day No.
             * DoB Month No.
             * DoB Year No.
             * Postal Address Line 1
             * Postal Address Line2
             * Suburb/City
             * PCode
             * State
             * Country
             * Email
             * Residency Status
             * Employment Basis
             * Claiming Tax Free Threshold
             * Has HELP VSL FS SSL or TSL Debt
             */
            $states = array('NSW', 'VIC', 'QLD', 'SA', 'WA', 'TAS', 'ACT', 'NT', 'OTH');
            if(!in_array($data[0]->state, $states)) {
                $data[0]->state = 'OTH';
            }
            
            if($data[0]->previous_names == 0) {
                $data[0]->previous_given_name = null;
                $data[0]->previous_middle_name = null;
                $data[0]->previous_surname = null;
            }
            
            switch($data[0]->gender) {
                case 1 :
                    $data[0]->gender = 'M';
                    break;
    
                case 2 :
                    $data[0]->gender = 'F';
                    break;
                    
                default:
                    $data[0]->gender = '';
                    break;
            }
            
            unset($data[0]->previous_names);
    
            //echo '<pre>'; var_dump($data[0]); echo '</pre>'; die();
    
            $ch = curl_init();
            if($_SERVER['HTTP_HOST'] == 'localhost') {
                $url = 'https://hooks.zapier.com/hooks/catch/4343612/ougcvg8/';
            } else {
                $url = 'https://hooks.zapier.com/hooks/catch/5546455/obzrna8/';
            }
            
            //echo $url;
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data[0]);
            //curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
    
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
            $server_output = curl_exec($ch);
    
            curl_close ($ch);
            
            //var_dump($server_output);
            $message_text = 'Sent '.$data[0]->given_name.' '.$data[0]->surname.' ('.$data[0]->employee_id.') to Zapier.'."\n".print_r($data[0], true)."\n".print_r($server_output, true);
        } else {
            $message_text = 'Tried to send jw_id '.$id.' to Zapier but no records were found';
        }
    
        $message->setText($message_text);
        $message->send();
        
        if(isset($_SERVER['HTTP_REFERER'])) {
            return redirect($_SERVER['HTTP_REFERER'])->with('success', 'Details sent to Smartsheet');
        }
    }

    public function deleteJob($id)
    {
        DB::table('jobs')
            ->where('id', $id)
            ->delete();
        //Display it as a list.
        return redirect('jobs')->with('success', 'The job has been deleted');
    }

    public function deleteShift($id)
    {
        DB::table('shifts')
            ->where('id', $id)
            ->delete();

        //Return a "success" status
        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function getJobList($client_id = null, $all = false) {
        //make up query based on whether or not we have a client id
        $jobsQ = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id');

        if(!$all) {
            $jobsQ->whereNull('jobs.end_date');
        }

        if($client_id != null) {
            $jobsQ->where('jobs.client_id', '=', $client_id);
        }

        $jobsQ->select('jobs.*', 'clients.name as client_name');

        return $jobsQ;
    }

    public function getFutureJobList() {
        //make up query based on whether or not we have a client id
        $jobsQ = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->select('jobs.*', 'clients.name as client_name')
            ->whereNull('jobs.end_date')
            ->orWhere('jobs.end_date', '>=', date('Y-m-d'));

        return $jobsQ;
    }

    public function getPayRatesByShift($shift_id) {
        $returnarray = DB::table('shift_tasks')
            ->join('tasks', 'tasks.task_id', '=', 'shift_tasks.task_id')
            ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
            ->where('shift_tasks.shift_id', '=', $shift_id)
            ->where('payrates.rate_type', '=', 1)
            ->select('tasks.task_name as task_name', 'shift_tasks.shta_rate', 'payrates.rate_type')
			->orderBy('tasks.task_name')
            ->get();

        return response()->json($returnarray);
    }

    public function workerShifts($id, $worker) {
        //Get every time_task record in this job, for this worker

        $shiftinfo = DB::table('users')
            ->join('timesheets', 'timesheets.user_id', '=', 'users.id')
            ->join('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
            ->join('shift_tasks', function($join) {
                $join->on('shift_tasks.shift_id', '=', 'shifts.id')
                    ->on('shift_tasks.task_id', '=', 'time_tasks.task_id');
            })
            ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->where('users.id', '=', $worker)
            ->where('shifts.jobs_id', '=', $id)
            ->whereNotNull('tita_start')->whereNotNull('tita_finish')
            ->select('users.given_name', 'users.surname', 'users.external_id', 'timesheets.processed', 'time_tasks.invoice_id', 'tita_start', 'tita_finish', 'tita_break_duration', 'tita_quantity', 'tasks.task_name', 'shift_tasks.shta_rate', 'jobs.name as jobs_name', 'clients.name as client_name')
            ->orderBy('time_tasks.tita_start')
            ->orderBy('tasks.task_name')
            ->get();

        $shiftarray = array();
        if(count($shiftinfo) > 0) {
            foreach ($shiftinfo as $shift) {
                $user = array(
                    'given_name' => $shift->given_name,
                    'surname' => $shift->surname,
                    'external_id' => $shift->external_id,
                    'jobs_name' => $shift->jobs_name,
                    'client_name' => $shift->client_name
                );
                //get the start of the week.
                $week_start = date('Y-m-d', strtotime('last Monday', strtotime($shift->tita_start)));

                $shiftarray[$week_start][$shift->task_name][date('N', strtotime($shift->tita_start))] = $shift;
            }

            return view('jobs.shifthistory', ['user' => $user, 'shifts' => $shiftarray]);
        } else {
            return view('errors.user', ['error_message' => 'There are no entered shifts for this worker in this job.']);
        }
    }

    public function getRoles($jobs_id) {
		$roles = DB::table('roles')
            ->leftJoin('payrates', 'payrates.id', '=', 'roles.rate_id')
            ->where('jobs_id', '=', $jobs_id)
			->orderBy('role_name')
			->get();

		$jobs = DB::table('jobs')
			->where('id', '=', $jobs_id)
			->get();

		$job = $jobs[0];

        $rates = DB::table('payrates')
            ->where('rate_archived', '=', 0)
            ->orderBy('name')
            ->select('id', 'name')
            ->get();

		return view('jobs.roleform', ['roles' => $roles, 'job' => $job, 'rates' => $rates]);
	}

	public function getRole($role_id) {
        $roles = DB::table('roles')
            ->where('role_id', '=', $role_id)
            ->get();

        $role = $roles[0];

        $rates = DB::table('payrates')
            ->where('rate_archived', '=', 0)
            ->orderBy('name')
            ->select('id', 'name')
            ->get();

        return view('jobs.editrole', ['role' => $role, 'rates' => $rates]);
	}

    public function saveRole(Request $request) {

		if(isset($request->role_default) && $request->role_default == 1) {
			DB::table('roles')
				->where('jobs_id', '=', $request->jobs_id)
				->update([
					'role_default' => 0
				]);
		}

    	if($request->role_id > 0) {
            DB::table('roles')
                ->where('role_id', '=', $request->role_id)
                ->update([
                    'role_name' => $request->role_name,
                    'rate_id' => $request->rate_id,
					'role_default' => (isset($request->role_default) && $request->role_default == 1) ? 1 : 0
                ]);
        } else {
            DB::table('roles')
                ->insertGetId([
                    'jobs_id' => $request->jobs_id,
                    'role_name' => $request->role_name,
                    'rate_id' => $request->rate_id,
					'role_default' => (isset($request->role_default) && $request->role_default == 1) ? 1 : 0
                ]);
        }

		return redirect('jobs/'.$request->jobs_id.'/roles')->with('success', 'The role has been added successfully.');
	}

    public function deleteRole($role_id) {
    	$roles = DB::table('roles')
            ->where('role_id', '=', $role_id)
            ->get();

    	$role = $roles[0];

        DB::table('roles')
			->where('role_id', '=', $role_id)
			->delete();

    	return redirect('jobs/'.$role->jobs_id.'/roles')->with('success', 'The role has been successfully deleted');
	}
}