<?php namespace App\Http\Controllers;

use App\Http\Controllers\Settings\MYOBController;
use App\Http\Controllers\Settings\SuperController;
use App\Http\Controllers\Settings\InductionsController;
use DB;
use Auth;
use Mail;
use Validator;
use App\User;
use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showProfile($id = 0)
    {
        $ud = Auth::user();

        if($id == 0){
            $user_id = $ud->id;
        } else {
            $user_id = $id;
        }

        //they exist, so check that I'm able to view them, e.g. if it's not me, I need to be an admin
        if($user_id <> $ud->id && $ud->level < 2) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'You do not have access to view the specified user, or they do not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $user = DB::table('users')
                    ->select('users.*', 'users.id as uid')
                    ->where('users.id', '=', $user_id)
                    ->get();

        if(count($user) == 0) {
            return view('errors.user', ['error_message' => 'You do not have access to view the specified user, or they do not exist.']);
        }

        $creds = DB::table('credentials')
                     ->leftjoin('staff_credentials', function($join) use($user_id) {
                         $join->on('credentials.id', '=', 'staff_credentials.credential_id')
                             ->where('staff_credentials.staff_id', '=', $user_id);
                     })
            ->orderBy('credentials.has_description')
            ->orderBy('credentials.name')
            ->select('credentials.id as cred_id', 'credentials.name', 'credentials.has_expiry', 'credentials.has_description', 'staff_credentials.*')
                     ->get();

        $payments = DB::table('payments')
                        ->leftjoin('payment_providers', 'payment_providers.provider_id', '=', 'payments.provider_id')
                        ->where('user_id', '=', $user_id)
                        ->get();

        $sc = new SuperController();
        $super = $sc->getSuperProviders();
        $usis = $sc->getSuperUSIs();

        $jobsQ = DB::table('jobs')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->join('job_worklist', 'job_worklist.jobs_id', '=', 'jobs.id')
            ->where('job_worklist.user_id', '=', $user_id)
            ->select('jobs.name', 'jobs.id', 'job_worklist.startdate as start_date', 'job_worklist.enddate as end_date', 'clients.name as client_name');

        $jobs = $jobsQ->get();

        $notes = DB::table('user_notes')
            ->leftJoin('users', 'users.id', '=', 'user_notes.note_user_id')
            ->where('user_id', '=', $user_id)
            ->orderBy('note_stamp', 'desc')
            ->select('user_notes.*', 'users.given_name', 'users.surname')
            ->get();
        
        $reportsQ = DB::table('reports')
            ->leftJoin('report_users', function($query) use ($user_id) {
                $query->on('report_users.report_id', '=', 'reports.report_id')
                    ->where('report_users.user_id', '=', $user_id);
            })
            ->select('report_users.user_id', 'reports.report_name', 'reports.report_id');
        $reports = $reportsQ->get();
        //echo $reportsQ->toSql();

        return view('user.profile', ['user' => $user[0], 'creds' => $creds, 'payments' => $payments, 'super' => $super, 'jobs' => $jobs, 'clie' => true, 'notes' => $notes, 'reports' => $reports, 'usis' => $usis]);
    }

    public function addUser() {
        $user = (object) array(
            'id' => 0,
            'given_name' => null,
            'surname' => null,
            'title' => null,
            'email' => null,
            'username' => null,
            'password' => null,
            'level' => null,
            'remember_token' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'confirmed' => null,
            'is_contact' => null,
            'is_supervisor' => null,
            'referral' => null,
            'myob_uid' => null,
            'preferred' => null,
            'accommodation' => null,
            'user_id' => null,
            'gender' => null,
            'date_of_birth' => null,
            'address' => null,
            'city' => null,
            'state' => null,
            'postcode' => null,
            'telephone' => null,
            'mobilephone' => null,
            'emergency_name' => null,
            'emergency_number' => null,
            'account_bsb' => null,
            'account_number' => null,
            'account_name' => null,
            'employed_as' => null,
            'tfn' => null,
            'aus_resident' => null,
            'tax_free_threshold' => null,
            'senior_tax_offset' => null,
            'help_debt' => null,
            'fs_debt' => null,
            'abn' => null,
            'gst' => null,
            'provider_id' => null,
            'super_number' => null,
            'external_id' => null,
            'previous_employer' => null,
            'previous_employer_years' => null,
            'previous_employer_months' => null,
            'workers_comp' => null,
            'workers_comp_details' => null,
            'passport_number' => null,
            'country' => null,
            'visa_expiry' => null,
            'visa_expiry_validated' => null,
            'passport_number_validated' => null,
            'vivo_validated' => null,
            'tax_resident' => null,
            'intl_student' => null,
            'current_location' => null,
            'date_arrive' => null,
            'loca_arrive' => null,
            'visa_88_days' => null,
            'stay_length' => null,
            'student_mode' => null,
            'uid' => 0,
            'holiday_start' => null,
            'holiday_end' => null,
            'provider_id_other' => null,
            'usi_id_other' => null,
            'job_network' => null,
            'job_network_tick' => null,
            'usi_id' => null,
			'seasonal_worker' => null
        );

        $sc = new SuperController();
        $super = $sc->getSuperProviders();
        $usis = $sc->getSuperUSIs();

        return view('user.profile', ['user' => $user, 'creds' => array(), 'payments' => array(), 'super' => $super, 'clie' => true, 'jobs' => null, 'notes' => null, 'reports' => null, 'usis' => $usis]);
    }

    public function printProfile($id)
    {
        //check that this user actually exists
        $doesUserExist = DB::table('users')
            ->where('id', '=', $id)
            ->get();
        if(count($doesUserExist) == 0) {
            //No, throw a nice error
            return view('errors.user', ['error_message' => 'The specified user does not exist.']);
        }

        //We made it.  Now get all the user's details and pass to view
        $user = DB::table('users')
            ->leftJoin('campaigns', 'campaigns.camp_id', '=', 'users.campaign_id')
            ->select('users.*', 'users.id as uid', 'campaigns.camp_name as campaign_name')
            ->where('users.id', '=', $id)
            ->get();

        $creds = DB::table('credentials')
            ->leftjoin('staff_credentials', function($join) use($id) {
                $join->on('credentials.id', '=', 'staff_credentials.credential_id')
                    ->where('staff_credentials.staff_id', '=', $id);
            })
            ->select('credentials.id as cred_id', 'credentials.name', 'credentials.has_expiry', 'credentials.has_description', 'staff_credentials.*')
            ->get();

        $jobs = DB::table('jobs')
            ->join('job_worklist', 'job_worklist.jobs_id', '=', 'jobs.id')
            ->join('clients', 'clients.id', '=', 'jobs.client_id')
            ->where('job_worklist.user_id', '=', $id)
            ->select('jobs.name as job_name', 'job_worklist.startdate', 'clients.name as client_name')
            ->get();

        $medical = DB::table('user_medical')
            ->where('user_id', '=', $id)
            ->get();

        $medical_questions = DB::table('user_medical')
            ->join('user_medical_questions', 'user_medical_questions.medical_id', '=', 'user_medical.medical_id')
            ->join('medical_questions', 'medical_questions.ques_id', '=', 'user_medical_questions.ques_id')
            ->where('user_id', '=', $id)
            ->where(function($where) {
                $where->whereNotNull('user_medical_questions.duration')
                    ->orWhereNotNull('user_medical_questions.status');
            })
            ->select('medical_questions.ques_text', 'user_medical_questions.*')
            ->get();

        $med = array(
            'medical' => $medical,
            'questions' => $medical_questions
        );

        $campaign_questions = DB::table('user_campaign_questions')
            ->join('campaign_questions', 'campaign_questions.caqu_id', '=', 'user_campaign_questions.caqu_id')
            ->leftJoin('campaign_question_options', function($join) {
                $join->on('campaign_question_options.cqop_id', '=', 'user_campaign_questions.response_integer')
                    ->on('campaign_question_options.caqu_id', '=', 'user_campaign_questions.caqu_id');
            })
            ->where('user_campaign_questions.user_id', '=', $id)
            ->get();

        return view('user.profileprint', ['user' => $user[0], 'creds' => $creds, 'jobs' => $jobs, 'med' => $med, 'campaign' => $campaign_questions]);
    }

    public function showResume($id) {
        $users = DB::table('user_resume')
            ->select('*')
            ->where('user_id', '=', $id)
            ->get();

        if(count($users) > 0) {
            $user = $users[0];
        }

        $data = base64_decode($user->resume_content);

        if(strpos($data, '%PDF') !== false) {
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment; filename="resume-'.$id.'.pdf"');
        } elseif(strpos($data, 'word/document.xml') !== false) {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="resume-'.$id.'.docx"');
        } else {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="resume-'.$id.'.doc"');
        }

        echo $data;
    }

    public function saveProfile(Request $request) {
        $uid = $request->user_id;
        $adding = false;
        $register = false;
        if($uid == 0) {
            $adding = true;
            //echo 'a';
        } else {
            $the_user_id = $uid;
            if(isset($request->register)) {
                $register = true;
                //echo 'b';
            }
        }

        if(Auth::user()->level < 2) {
            $rules = [
                'date_of_birth_year' => 'required',
                'date_of_birth_month' => 'required',
                'date_of_birth_day' => 'required',
                'gender' => 'required',
                'address' => 'required|max:100',
                'city' => 'required|max:50',
                'state' => 'required|max:3',
                'postcode' => 'required|max:4',
                'account_bsb' => 'required',
                'account_number' => 'required',
                'account_name' => 'required',
                'tfn' => 'required'
            ];
            
            if(!isset($request->no_super))
            {
                $rules['provider_id'] = 'required';
                $rules['super_number'] = 'required';
            }
    
            $validator = Validator::make($request->all(), $rules);

            if(strcmp($request->tfn, '000 000 000') != 0 && strcmp($request->tfn, '111 111 111') != 0 ) {
                //something other than ignore.
                if(!$this->ValidateTFN($request->tfn)) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add('tfn', 'Your Tax File Number does not match the required format');
                    });
                }
            }

            if ($validator->fails()) {
                if ($register) {
                    return redirect('dashboard')->withInput()->withErrors($validator);
                } else {
                    return redirect('profile')->withErrors($validator)->withInput();
                }
            }
            $redir = 'dashboard';
        } else {
            if(isset($the_user_id)) {
                $redir = 'profile/' . $the_user_id;
            } else {
                $redir = 'dashboard';
            }
        }

        /*if($adding) {
            //Make a user, then their staff details

            $sql = 'SELECT right(external_id, 5) as external_id FROM users WHERE right(external_id, 5) REGEXP(\'[0-9]{5}\') order by right(external_id, 5) desc';

            //echo $sql;

            $externalQ = DB::select($sql);

            //echo '<pre>'; var_dump($externalQ); echo '</pre>';

            $external = $externalQ[0];
            $temp_id = intval($external->external_id);
            $emp_id =  'LINX'.str_pad(($temp_id + 1), 5, '0', STR_PAD_LEFT);

            $the_user_id = DB::table('users')
                ->insertGetId([
                    'given_name' => $request->given_name,
                    'surname' => $request->surname,
                    'preferred' => $request->preferred,
                    'email' => $request->email,
                    'username' => $request->username,
                    'password' => bcrypt($request->password),
                    'level' => 4,
                    'confirmed' => 0,
                    'created_at' => date('Y-m-d'),
                    'external_id' => $emp_id
                ]);
        }*/

        $userdata = array(
            'date_of_birth' => $request->date_of_birth_year.'-'.$request->date_of_birth_month.'-'.$request->date_of_birth_day,
            'gender' => $request->gender,
            'title' => $request->title,
            
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'postcode' => $request->postcode,
            'no_address' => $request->no_address ?? 0,
            
            'telephone' => $request->telephone,
            'mobilephone' => $request->mobilephone,
            'emergency_name' => $request->emergency_name,
            'emergency_number' => $request->emergency_number,
            
            'account_bsb' => $request->account_bsb,
            'account_number' => $request->account_number,
            'account_name' => $request->account_name,
            'no_bank_account' => $request->no_bank_account ?? 0,
            
            'provider_id' => $request->provider_id,
            'super_number' => $request->super_number,
            'no_super' => $request->no_super ?? 0,
            
            'tfn' => $request->tfn,
            'tax_resident' => $request->tax_resident,
            'tax_free_threshold' => $request->tax_free_threshold,
            'senior_tax_offset' => $request->senior_tax_offset,
            'help_debt' => $request->help_debt,
            'fs_debt' => $request->fs_debt,
            'aus_resident' => $request->aus_resident ?? 0,
            'passport_number' => $request->passport_number,
            'country' => $request->country,
            'visa_expiry' => $request->visa_expiry,
            'visa_88_days' => $request->visa_88_days,
            'given_name' => $request->given_name,
            'surname' => $request->surname,
            'preferred' => $request->preferred,
            'email' => $request->email,
            'job_network' => $request->job_network,
            'job_network_tick' => $request->job_network_tick,
            'updated_at' => date('Y-m-d H:i:s'),
			'provider_id_other' => $request->provider_id_other,
			'usi_id_other' => $request->usi_id_other,
			'usi_id' => $request->usi_id,
			'seasonal_worker' => $request->seasonal_worker,
            
            'referral' => $request->referral,
            'accommodation' => $request->accommodation,
            'previous_employer' => $request->previous_employer ?? "",
            'previous_employer_years' => $request->previous_employer_years ?? 0,
            'previous_employer_months' => $request->previous_employer_months ?? 0,
            'workers_comp' => $request->workers_comp ?? 0,
            'workers_comp_details' => $request->workers_comp_details ?? "",
            'vivo_validated' => $request->vivo_validated ?? 0,
            'current_location' => $request->current_location ?? "",
            'date_arrive' => $request->date_arrive ?? "",
            'loca_arrive' => $request->loca_arrive ?? "",
            'stay_length' => $request->stay_length,
            'num_in_group' => $request->num_in_group,
            'has_transport' => $request->has_transport,
            'share_details' => $request->share_details,
            
            'middle_name' => $request->middle_name ?? "",
            'previous_given_name' => $request->previous_given_name ?? "",
            'previous_middle_name' => $request->previous_middle_name ?? "",
            'previous_surname' => $request->previous_surname ?? "",
            'previous_names' => $request->previous_names ?? ""
        );

        if(isset($request->myob_uid)) {
            $userdata['myob_uid'] = $request->myob_uid;
        }
        
        if(isset($request->profile_complete)) {
            $userdata['profile_complete'] = 1;
        }

        //echo '<pre>'; var_dump($userdata); echo '</pre>'; die();

        if($adding) {
            $the_user_id = DB::table('users')
                ->insert($userdata);
        } else {
            DB::table('users')
                ->where('id', $the_user_id)
                ->update($userdata);
        }

        if(strlen($request->password) > 0 && strcmp($request->password, $request->password_confirmation) == 0) {
            DB::table('users')
                ->where('id', $the_user_id)
                ->update([
                    'username' => $request->username,
                    'password' => bcrypt($request->password)
                ]);
        }

        //Only update admin if they have been sent
        if(Auth::user()->level > 1) {
            $data = array(
                'level' => $request->level,
                'confirmed' => $request->confirmed,
                'is_supervisor' => $request->is_supervisor,
                'vivo_validated' => $request->vivo_validated,
                'passport_number_validated' => $request->passport_number_validated,
                'visa_expiry_validated' => $request->visa_expiry_validated,
				'seasonal_worker' => $request->seasonal_worker
            );

            if(strlen($request->external_id) > 0) {
                $data['external_id'] = $request->external_id;
            }

            DB::table('users')
                ->where('id', $the_user_id)
                ->update($data);
        }

        //Credentials
        DB::table('staff_credentials')
            ->where('staff_id', '=', $the_user_id)
            ->delete();

        if(is_array($request->cred_id)) {
            foreach($request->cred_id as $key=>$value) {
                DB::table('staff_credentials')
                    ->insert([
                        'staff_id' => $the_user_id,
                        'credential_id' => $key,
                        'expiry_date' => $request->expiry[$key],
                        'description' => $request->description[$key]
                    ]);
            }
        }

        DB::table('report_users')
            ->where('user_id', '=', $the_user_id)
            ->delete();

        if(is_array($request->report_id)) {
            foreach($request->report_id as $key=>$value) {
                //echo $the_user_id.' '.$key.'<br />';
                DB::table('report_users')
                    ->insert([
                        'user_id' => $the_user_id,
                        'report_id' => $key
                    ]);
            }
            //die();
        }

        //Payment method
        if(is_array($request->payment_method)) {
            foreach($request->payment_method as $id=>$payment) {
                DB::table('payments')
                    ->where('payment_id', '=', $id)
                    ->update([
                        'provider_id' => (int)$payment,
                        'payment_made' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        if(is_array($request->payment_received)) {
            foreach($request->payment_received as $id=>$payment) {
                DB::table('payments')
                    ->where('payment_id', '=', $id)
                    ->update(['payment_received' => $payment]);
            }
        }

        //Send details to MYOB, if uid is not null
        if(strlen($request->myob_uid) > 0) {
            $myob = $this->createInMYOB($the_user_id);
            $auth = Auth::user();
            if($auth->level < 2) {
                return redirect($redir)->with('success', 'Your details have saved successfully.');
            }
            if(is_array($myob)) {
                $errorString = '';

                foreach($myob['errors'] as $error) {
                    $errorString.="\n".$error->Name.': '.($error->Message == null ? '' : $error->Message.' ').($error->AdditionalDetails == null ? '' : $error->AdditionalDetails.' ');
					if(is_object($error) || is_aray($error)) {
						$estring = json_encode($error);
					} else {
                        $estring = $error;
                    }
                    DB::table('myob_log')
						->insert([
							'url' => 'Save Client',
							'params' => '',
							'response' => $estring,
							'timestamp' => date('Y-m-d H:i:s')
						]);
                }

                return redirect($redir)->with('error', 'Staff member saved successfully, but there were MYOB issues: '.$errorString);
            } else {
                return redirect($redir)->with('success', 'Staff details have saved successfully.');
            }
        }

        return redirect($redir)->with('success', 'Staff details have saved successfully.');
    }

    public function deleteProfile($user_id)
    {
        DB::table('users')
            ->where('id', $user_id)
            ->delete();
        //Display it as a list.
        return redirect('staff')->with('success', 'The user has been deleted');
    }

    public function showList()
    {
        $perpage = 50;

        if(isset($_GET['page'])) {
            $pagenum = $_GET['page'];
            $skip = (((int)$pagenum) - 1) * $perpage;
        } else {
            $pagenum = 1;
            $skip = 0;
        }

        //ordering
        $order = (isset($_GET['order'])) ? $_GET['order'] : 'asc';

        //Get all the staff in the system
        $usersQ = DB::table('users')
            ->leftJoin('campaigns', 'campaigns.camp_id', '=', 'users.campaign_id');

        if(isset($_GET['type'])) {
            $usersQ->where('users.confirmed', '=', $_GET['type']);
        } else {
            $usersQ->where('users.confirmed', '=', 4);
        }
        $usersQ->orderBy('users.surname', $order)
            ->orderBy('users.given_name', $order)
            ->select('users.*', 'users.id as uid', 'campaigns.camp_name as campaign_name')
            ->take($perpage)
            ->skip($skip);

        $userlist = $usersQ->get();
        $usercount = $usersQ->count();

        $users = array();
        foreach($userlist as $thisuser) {
            $thisuser->notes = $this->getNotePart($thisuser->id);
            $users[] = $thisuser;
        }

        $jc = new JobsController();
        $jobsQ = $jc->getJobList();
        $jobs = $jobsQ->get();

        //Display it as a list.
        return view('user.list', ['users' => $users, 'jobs' => $jobs, 'usercount' => $usercount]);
    }

    public function jsonList() {
        $rawarr = array();

        $perpage = 50;

        if(isset($_GET['page'])) {
            $pagenum = $_GET['page'];
            $skip = (((int)$pagenum) - 1) * $perpage;
        } else {
            $pagenum = 1;
            $skip = 0;
        }

        //ordering
        $order = (isset($_GET['order'])) ? $_GET['order'] : 'asc';


        //each of the get vars
        if(isset($_GET['search-name']) && strlen($_GET['search-name']) > 0) {
            $rawarr[] = '(users.given_name like \'%'.str_replace('\'', '\'\'', $_GET['search-name']).'%\' or users.surname like \'%'.str_replace('\'', '\'\'', $_GET['search-name']).'%\')';
        }

        if(isset($_GET['search-external']) && strlen($_GET['search-external']) > 0) {
            $rawarr[] = '(users.external_id like \'%'.str_replace('\'', '\'\'', $_GET['search-external']).'%\')';
        }

        if(isset($_GET['search-phone']) && strlen($_GET['search-phone']) > 0) {
            $rawarr[] = '(replace(users.mobilephone, \' \', \'\') = \''.$_GET['search-phone'].'\' or replace(users.telephone, \' \', \'\') = \''.$_GET['search-phone'].'\')';
        }

        if(isset($_GET['search-email']) && strlen($_GET['search-email']) > 0) {
            $rawarr[] = '(users.email like \'%'.str_replace('\'', '\'\'', $_GET['search-email']).'%\')';
        }

        if(isset($_GET['search-job']) && is_array($_GET['search-job'])) {
            $jobarr = array();
            foreach($_GET['search-job'] as $job) {
                $jobarr[] = $job;
            }

            $rawarr[] = '(users.id in (select user_id from job_worklist where jobs_id in ('.implode(', ', $jobarr).')))';
        }

        if(isset($_GET['search-status']) && is_array($_GET['search-status'])) {
            $stusarr = array();
            foreach($_GET['search-status'] as $stus) {
                $stusarr[] = 'users.confirmed = '.$stus;
            }

            $rawarr[] = '('.implode(' or ', $stusarr).')';
        }

        $raw = implode(' and ', $rawarr);

        $userQ = DB::table('users')
            ->leftJoin('campaigns', 'campaigns.camp_id', '=', 'users.campaign_id')
            ->select('users.*', 'users.id as uid', 'campaigns.camp_name as campaign_name');

        if(strlen($raw) > 0) {
            $userQ->whereRaw($raw);
        }

        $usercount = $userQ->count();
        $pages = ceil($usercount / $perpage);

        if(isset($_GET['type']) && $_GET['type'] == 'date') {
            $userQ = $userQ->orderBy('users.created_at', $order);
        } else {
            $userQ = $userQ->orderBy('users.surname', $order)
                ->orderBy('users.given_name', $order);
        }

        $userQ = $userQ->skip($skip)
            ->take($perpage);

        $query = $userQ->toSql();

        $userlist = $userQ->get();

        $users = array();
        foreach($userlist as $thisuser) {
            $thisuser->notes = $this->getNotePart($thisuser->id);
            $users[] = $thisuser;
        }

        $returnarray = array('total' => $usercount, 'order' => $order, 'pages' => $pages, 'page' => $pagenum, 'perpage' => $perpage, 'users' => $users, 'query' => $query);
        return response()->json($returnarray);
    }

    public function simpleSearch() {
        $escapedInput = '%'.str_replace('%', '\\%', $_GET['q']).'%';
        $users = DB::table('users')
            ->where('given_name', 'like', $escapedInput)
            ->orWhere('surname', 'like', $escapedInput)
            ->orWhere('external_id', 'like', $escapedInput)
            ->select(DB::raw('CONCAT(users.surname, \', \', users.given_name, \' (\', users.external_id, \')\') as display'), 'id')
            ->get();
        return response()->json(array('items' => $users));
    }

    public function approveUser() {
        //First and foremost, does the employee ID already exist?
        $usercode = DB::table('users')
            ->where('external_id', '=', $_POST['external_id'])
            ->count();

        if($usercode > 0) {
            return redirect('profile/'.$_POST['user_id'].'/approve')->with('error', 'The entered employee number already exists.  We have generated a new one for you, but you will need to enter job information again.');
        }

        //Does not exist; continue
        if(isset($_POST['user_id']) && isset($_POST['approve_job'])) {
            $user = $_POST['user_id'];
            foreach($_POST['approve_job'] as $key => $job) {
                $jobworklistid = DB::table('job_worklist')
                    ->insert([
                        'jobs_id' => $job,
                        'user_id' => $user,
                        'startdate' => $_POST['approve_job_date'][$key],
                        'enddate' => null,
                        'needs_accepting' => 1
                    ]);
            }
            
            $jc = new JobsController();
            //$jc->SendWorkerToZapier($jobworklistid);

            DB::table('users')
                ->where('users.id', '=', $user)
                ->update([
                    'confirmed' => 1,
                    'external_id' => $_POST['external_id'],
                    'accommodation' => (isset($_POST['accommodation']) ? 1 : 0)
                ]);

            /*if(isset($_POST['payment-needed'])) {
                DB::table('payments')
                    ->insert([
                        'user_id' => $user,
                        'entered_user_id' => Auth::user()->id,
                        'payment_amount' => 50.00
                    ]);
            }*/

            //Add an induction for the user.
            DB::table('userinductions')
                ->insert([
                    'user_id' => $_POST['user_id'],
                    'ind_id' => $_POST['induction']
                ]);

            $userdata = DB::table('users')
                ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
                ->join('jobs', 'jobs.id', '=', 'job_worklist.jobs_id')
                ->where('users.id', '=', $user)
                ->get();

            if(count($userdata) > 0) {
                $userd = $userdata[0];

                try {
                    Mail::send('emails.approved', ['user' => $userd], function ($message) use ($userd) {
                        $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                        $message->to($userd->email);
                        $message->subject('Your sign-up with Linx Employment');
                    });
                } catch(Exception $ex) {
                    //Ignore
                }
            }
        }

        return redirect('staff/?type=0')->with('success', 'The worker has been approved, and an email sent to them');
    }

    public function getSupervisors($flag = 0) {
        $superQ = DB::table('users');
        if($flag != 0) {
            $superQ->where('users.is_supervisor', '=', $flag);
        }
        $superQ->select('users.given_name', 'users.surname', 'users.id');

        return $superQ->get();
    }
    
    public function getSuperAndAdmin() {
        return DB::table('users')
            ->where('level', '>', 1)
            ->orWhere('is_supervisor', '=', 1)
            ->select('users.given_name', 'users.surname', 'users.id', 'users.external_id', 'users.level')
            ->orderBy('users.level', 'desc')
            ->orderBy('users.surname')
            ->orderBy('users.given_name')
            ->get();
    }

    public function approveJob(Request $request) {
        $slack = new \Slack(env('SLACK_ADDRESS'));
        $slack->setDefaultUsername("TMS Logger");
        $slack->setDefaultChannel("#logging-tms");
        $message = new \SlackMessage($slack);
        $message_text = 'Attempting to approve jw_id '.print_r($request->jobs_id, true).' for user '.$request->user_id."\n".print_r($_POST, true);
        $message->setText($message_text);
        $message->send();
        
        $error = 0;

        if($request->jobs_id != null) {
            //send an email to Linx admin, and the user, to say that the job was accepted.
            $users = DB::table('users')
                ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
                ->join('jobs', 'jobs.id', '=', 'job_worklist.jobs_id')
                ->whereIn('job_worklist.id', $request->jobs_id)
                ->select('users.*', 'jobs.*', 'job_worklist.*', 'job_worklist.id as jw_id', 'users.id as uid')
                ->get();
    
            if (count($users) > 0) {
                $user = $users[0];
                $jc = new JobsController();
                $this->createInMYOB($user->uid);
                $jc->SendWorkerToZapier($user->jw_id);
                $message_text = "JW ".$user->jw_id." sent successfully";
        
                try {
                    Mail::send('emails.jobaccept', ['user' => $user], function ($message) use ($user) {
                        $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                        $message->to($user->email);
                        $message->subject('Job Offer Accepted');
                    });
            
                    Mail::send('emails.jobacceptadmin', ['user' => $user], function ($message) use ($user) {
                        $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                        $message->to('admin@linxemployment.com.au');
                        $message->subject('Job Offer Accepted');
                    });
                } catch (Exception $ex) {
                    //Ignore
                }
    
                DB::table('users')
                    ->where('id', '=', $request->user_id)
                    ->where('confirmed', '<', 3)
                    ->update([
                        'confirmed' => 3,
                        'level' => 1
                    ]);
    
                DB::table('job_worklist')
                    ->whereIn('id', $request->jobs_id)
                    ->update([
                        'needs_accepting' => 0
                    ]);
            } else {
                $message_text = "No users matched.";
                $error = 1;
            }
        } else {
            $message_text = "No jobs_id was provided.";
            $error = 1;
        }
        
        $message->setText($message_text);
        $message->send();
        if($error == 1) {
            return view('errors.user', ['error_message' => 'Something has gone wrong with accepting your position.  Please go back and try again.']);
        }
        return redirect('/dashboard');
    }

    public function rejectJob(Request $request) {
        $users = DB::table('users')
            ->join('job_worklist', 'job_worklist.user_id', '=', 'users.id')
            ->join('jobs', 'jobs.id', '=', 'job_worklist.jobs_id')
            ->where('users.id', '=', $request->user_id)
            ->get();

        $user = $users[0];
    
        if($request->jobs_id != null) {
            foreach($request->jobs_id as $job_id) {
                //echo $job_id.'<br>';
                DB::table('job_worklist')
                    ->where('id', '=', $job_id)
                    ->delete();
            }
        }
        //die();
        
        //Email linx to let them know.
        try {
            Mail::send('emails.jobreject', ['user' => $user], function ($message) use ($user) {
                $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                $message->to($user->email);
                $message->subject('Job Offer Rejected');
            });
        } catch(Exception $ex) {
            //Ignore
        }

        return redirect('/dashboard');
    }

    public function testCreate($user) {
        $this->createInMYOB($user, true);
    }

    public function createInMYOBWrapper($id)
    {
        //try {
            $myob = $this->createInMYOB($id);
            echo json_encode(array('success' => $myob));
        //} catch (\Exception $ex) {
        //    echo json_encode(array('errors' => 'There was an error sending to MYOB.  Please check that this workers has all their details (Name, Address, Bank, Tax, Super) filled out correctly and try again.'));
        //}
    }

    public function createInMYOB($user, $debug = false) {
        $require = env('COMPOSER_AUTOLOAD');
        require_once($require);
        $slack = new \Slack(env('SLACK_ADDRESS'));
        $slack->setDefaultUsername("TMS Logger");
        $slack->setDefaultChannel("#logging-tms");
        $myob_error = false;
    
        // Create a new message
        $message = new \SlackMessage($slack);
        //$message->setText("New public booking from $first_name $last_name in $course on $date. ($instance_url)");
        
        $request = DB::table('users')
            ->where('id', '=', $user)
            ->get()[0];

        //This is where we create them in MYOB.
        $data = array(
            'LastName' => $request->surname,
            'FirstName' => $request->given_name,
            'IsIndividual' => true,
            'DisplayID' => $request->external_id,
            'IsActive' => ($request->confirmed > -1) ? true : false
        );


        if(strlen($request->address) > 0) {
            $data['Addresses'][0]['Street'] = $request->address;
        }

        if(strlen($request->city) > 0) {
            $data['Addresses'][0]['City'] = $request->city;
        }

        if(strlen($request->state) > 0) {
            $data['Addresses'][0]['State'] = $request->state;
        }

        if(strlen($request->postcode) > 0) {
            $data['Addresses'][0]['PostCode'] = $request->postcode;
        }

        if(strlen($request->mobilephone) > 0) {
            $data['Addresses'][0]['Phone1'] = $request->mobilephone;
        }

        if(strlen($request->telephone) > 0) {
            $data['Addresses'][0]['Phone2'] = $request->telephone;
        }

        if(strlen($request->email) > 0) {
            $data['Addresses'][0]['Email'] = $request->email;
        }

        if(isset($data['Addresses'][0])) {
            $data['Addresses'][0]['Location'] = 1;
        } else {
            $data['Addresses'][0]['Street'] = 'PO Box 41';
            $data['Addresses'][0]['City'] = 'Legana';
            $data['Addresses'][0]['State'] = 'TAS';
            $data['Addresses'][0]['PostCode'] = '7277';
            $data['Addresses'][0]['Location'] = 1;
        }

        $myob = new MYOBController();
        if($myob->myobUID == null) {
            return;
        }

        //Log::info('MYOB data for user '.$user.': '.print_r($data, true));

        $supercategories = $myob->getSuperCategories();
        $taxcategories = $myob->getTaxCategories();
		$variation_rate = 0.0;
		if($request->seasonal_worker == 1) {
		    $taxtable_name = 'Withholding Variation';
		    $variation_rate = 15.0;
        } elseif($request->visa_88_days == 1) {
            $taxtable_name = 'Working Holiday Maker';
            //$variation_rate = 15.0;
        } else {
            if($request->tax_free_threshold == 1 && $request->help_debt == 1 && $request->fs_debt == 1) {
                $taxtable_name = 'Tax Free Threshold HELP + FS';
            }
            if($request->tax_free_threshold == 1 && $request->help_debt == 0 && $request->fs_debt == 1) {
                $taxtable_name = 'Tax Free Threshold + FS';
            }
            if($request->tax_free_threshold == 0 && $request->help_debt == 1 && $request->fs_debt == 1) {
                $taxtable_name = 'No Tax Free Threshold HELP + FS';
            }
            if($request->tax_free_threshold == 0 && $request->help_debt == 0 && $request->fs_debt == 1) {
                $taxtable_name = 'No Tax Free Threshold + FS';
            }
            if($request->tax_free_threshold == 1 && $request->help_debt == 1 && $request->fs_debt == 0) {
                $taxtable_name = 'Tax Free Threshold + HELP';
            }
            if($request->tax_free_threshold == 1 && $request->help_debt == 0 && $request->fs_debt == 0) {
                $taxtable_name = 'Tax Free Threshold';
            }
            if($request->tax_free_threshold == 0 && $request->help_debt == 1 && $request->fs_debt == 0) {
                $taxtable_name = 'No Tax Free Threshold + HELP';
            }
            if($request->tax_free_threshold == 0 && $request->help_debt == 0 && $request->fs_debt == 0) {
                $taxtable_name = 'No Tax Free Threshold';
            }
            $variation_rate = 0.0;
        }
        //$taxtable_name = 'PAYG Withholding';
        $taxtables = $myob->getTaxTables($taxtable_name);

        //echo '<pre>';
        //echo 'Super';
        //var_dump($supercategories);
        //echo 'Tax cat';
        //var_dump($taxcategories);
        //echo 'Tax table';
        //var_dump($taxtables);
        //echo '</pre>';

        if(isset($supercategories->Items) && count($supercategories->Items) > 0) {
            $supercats = array(
                array(
                    'UID' => $supercategories->Items[0]->UID
                )
            );
        } else {
            $supercats = null;
        }

        if(isset($taxcategories->Items) && count($taxcategories->Items) > 0) {
            $taxcats = array(
                'UID' => $taxcategories->Items[0]->UID
            );
        } else {
            $taxcats = null;
        }

        if(isset($taxtables->Items) && count($taxtables->Items) > 0) {
            $taxtable = array(
                'UID' => $taxtables->Items[0]->UID
            );
        } else {
            $taxtable = null;
        }

        $bsb_temp = preg_replace('/[^0-9+]/', '', $request->account_bsb);
        $bsb = substr($bsb_temp, 0, 3).'-'.substr($bsb_temp, 3, 3);

		$account_number = preg_replace('/[^0-9+]/', '', $request->account_number);

        $EmployeePaymentDetails = array(
            //'UID' => $this->GUID(),
            'PaymentMethod' => 'Electronic',
            'BankStatementText' => $request->external_id,
            'BankAccounts' =>array(
                array(
                    'BSBNumber' => $bsb,
                    'BankAccountNumber' => $account_number,
                    'BankAccountName' => substr($request->account_name, 0, 32),
                    'Value' => 100.00,
                    'Unit' => 'Percent'
                )
            )
        );

        $genderString = '';
        if($request->gender == 1) { $genderString = 'Male'; }
        if($request->gender == 2) { $genderString = 'Female'; }

        $expense = DB::table('myob')
            ->get();
        $wagesexpenseaccount = $expense[0]->wage_account;

        //super provider GUID
        $supers = DB::table('super_providers')
            ->where('super_name', '=', $request->provider_id)
            ->get();
        if(count($supers) > 0) {
            $superproviderGUID = $supers[0]->super_guid;
        } else {
            $superproviderGUID =  null;
        }

        //$data['EmployeePayrollDetails'] = $EmployeePayrollDetails;
        //$data['EmployeePaymentDetails'] = $EmployeePaymentDetails;

        try {
            $uiddata = $myob->updateEmployee($data, $debug);

            if (isset($uiddata['uid'])) {
                $uid = $uiddata['uid'];

                DB::table('users')
                    ->where('id', '=', $user)
                    ->update([
                        'myob_uid' => $uid,
                        'external_id' => $request->external_id
                    ]);
            }

            $EmployeePaymentDetails['Employee']['UID'] = $uid;
            $EmployeePayrollDetails['Employee']['UID'] = $uid;

            $empData = $myob->getEmployee($uid);

            //Log::info('Payment details for user '.$user.': '.print_r($EmployeePaymentDetails, true));
            $payment = $myob->updateEmployeePayment($EmployeePaymentDetails, $debug);
            if(isset($payroll['Errors'])) {
                $myob_error = true;
                $message->setText('There was a problem sending '.$request->given_name.' '.$request->surname.' ('.$request->external_id.') payment data to MYOB.'."\n".print_r($payment['Errors'], true)."\n".print_r($EmployeePaymentDetails, true));
                $message->send();
                return array('uid' => $uid, 'errors' => $payment['Errors']);
            } //else {
            //    return $uid;
            //}

            $payrollob = $myob->getEmployeePayroll($empData->EmployeePayrollDetails->UID);
            $payrollar = json_decode(json_encode($payrollob), true);


            //for each payrate in the system, get the guid and construct this
            $basehourly = DB::table('payrates')
                ->where('name', '=', 'Base Hourly - DO NOT DELETE OR USE')
                ->select('rate_guid')
                ->get();
            
            $categories = DB::table('payrate_rules')
                ->join('time_tasks', 'time_tasks.rate_id', '=', 'payrate_rules.rate_id')
                ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                ->where('timesheets.user_id', '=', $user)
                ->where('payrate_rules.paru_guid', '<>', '')
                ->select('paru_guid')
                ->distinct()
                ->get();

            $wagecategories = array();
            $wagecategories[] = array(
                'UID' => $basehourly[0]->rate_guid
            );
            
            foreach($categories as $category) {
                $wagecategories[] = array(
                    'UID' => $category->paru_guid
                );
            }

            $tfn1 = preg_replace('/[^0-9+]/', '', $request->tfn);
            $tfn = substr($tfn1, 0, 3).' '.substr($tfn1, 3, 3).' '.substr($tfn1, 6, 3);
            if(strlen($tfn) > 2 && strcasecmp($tfn, '000 000 000') != 0 && strcasecmp($tfn, '111 111 111') != 0) {
                $tfn = $tfn;
            } else {
                $tfn = null;
            }

            $EmployeePayrollDetails = $payrollar;
            $EmployeePayrollDetails['DateOfBirth'] = $request->date_of_birth;
            $EmployeePayrollDetails['EmploymentBasis'] = 'Individual';
            $EmployeePayrollDetails['EmploymentCategory'] = 'Temporary';
            $EmployeePayrollDetails['EmploymentStatus'] = 'Casual';
            $EmployeePayrollDetails['PaySlipDelivery'] = 'ToBeEmailed';
            $EmployeePayrollDetails['PaySlipEmail'] = $request->email;
            $EmployeePayrollDetails['Wage']['PayBasis'] = 'Hourly';
            $EmployeePayrollDetails['Wage']['AnnualSalary'] = 0.00;
            $EmployeePayrollDetails['Wage']['HourlyRate'] = 0.00;
            $EmployeePayrollDetails['Wage']['PayFrequency'] = 'Weekly';
            $EmployeePayrollDetails['Wage']['HoursInWeeklyPayPeriod'] = 1.00;
            $EmployeePayrollDetails['Wage']['WageCategories'] = $wagecategories;
            $EmployeePayrollDetails['Wage']['WagesExpenseAccount'] = array();
            $EmployeePayrollDetails['Wage']['WagesExpenseAccount']['UID'] = $wagesexpenseaccount;

            if(strlen($genderString) > 0) {
                $EmployeePayrollDetails['Gender'] = $genderString;
            }

            if($superproviderGUID != null) {
                $EmployeePayrollDetails['Superannuation']['SuperannuationFund'] = array(
                    'UID' => $superproviderGUID
                );
                $EmployeePayrollDetails['Superannuation']['EmployeeMembershipNumber'] = $request->super_number;
            }
            $EmployeePayrollDetails['Superannuation']['SuperannuationCategories'] = $supercats;

            $EmployeePayrollDetails['Tax'] = array(
                'TaxFileNumber' => $tfn,
                'TaxTable' => $taxtable,
                'TaxCategory' => $taxcats
            );

            if($variation_rate > 0) {
                $EmployeePayrollDetails['Tax']['WithholdingVariationRate'] = $variation_rate;
            }

            //echo 'Payroll details for user '.$user.': '.print_r($EmployeePayrollDetails, true);
            $payroll = $myob->updateEmployeePayroll($EmployeePayrollDetails, $debug);
            if(isset($payroll['Errors'])) {
                $myob_error = true;
                $message->setText('There was a problem sending '.$request->given_name.' '.$request->surname.' ('.$request->external_id.') payroll data to MYOB.'."\n".print_r($payroll['Errors'], true)."\n".print_r($EmployeePayrollDetails, true));
                $message->send();
                return array('uid' => $uid, 'errors' => $payroll['Errors']);
            }
            
            return $uid;
        } catch (\Exception $ex) {
            $myob_error = true;
            $message->setText('There was a problem sending '.$request->given_name.' '.$request->surname.' ('.$request->external_id.') to MYOB.'."\n".$ex->getMessage()."\n".print_r($data, true));
            $message->send();
        }
        
        if(!$myob_error) {
            $message->setText($request->given_name.' '.$request->surname.' ('.$request->external_id.') was sent to MYOB successfully.');
            $message->send();
        }
    }

    public function refreshProfile($id) {
        $users = DB::table('users')
            ->where('users.id', '=', $id)
            ->select('users.myob_uid')
            ->get();

        if(count($users) > 0) {
            $user = $users[0];
        }

        if($user->myob_uid == null) {
            $this->createInMYOB($id);
        } else {
            $mc = new MYOBController();
            $item = $mc->getEmployee($user->myob_uid);

            $fields = array();

            //get the rest of the data from MYOB
            $paymentDetails = $mc->getEmployeeStandard($item->EmployeePaymentDetails->UID);
            //echo '<pre>'; var_dump($paymentDetails); echo '</pre>';

            $payrollDetails = $mc->getEmployeePayroll($item->EmployeePayrollDetails->UID);
            //echo '<pre>'; var_dump($payrollDetails); echo '</pre>';

            $fields['myob_uid'] = $item->UID;
            if (isset($item->FirstName)) {
                $fields['given_name'] = $item->FirstName;
                $fields['surname'] = $item->LastName;
            } else {
                $fields['given_name'] = $item->CompanyName;
                $fields['surname'] = '';
            }
            $fields['external_id'] = $item->DisplayID;
            $fields['email'] = ($item->Addresses[0]->Email == null) ? '' : $item->Addresses[0]->Email;
            $fields['address'] = ($item->Addresses[0]->Street == null) ? '' : $item->Addresses[0]->Street;
            $fields['city'] = ($item->Addresses[0]->City == null) ? '' : $item->Addresses[0]->City;
            $fields['state'] = ($item->Addresses[0]->State == null) ? '' : $item->Addresses[0]->State;
            $fields['postcode'] = ($item->Addresses[0]->PostCode == null) ? '' : $item->Addresses[0]->PostCode;
            $fields['telephone'] = ($item->Addresses[0]->Phone2 == null) ? '' : $item->Addresses[0]->Phone2;
            $fields['mobilephone'] = ($item->Addresses[0]->Phone1 == null) ? '' : $item->Addresses[0]->Phone1;

            if (isset($paymentDetails->BankAccounts)) {
                $fields['account_bsb'] = $paymentDetails->BankAccounts[0]->BSBNumber;
                $fields['account_number'] = $paymentDetails->BankAccounts[0]->BankAccountNumber;
                $fields['account_name'] = $paymentDetails->BankAccounts[0]->BankAccountName;
            }

            if (isset($payrollDetails->Tax)) {
                $fields['tfn'] = ($payrollDetails->Tax->TaxFileNumber == null) ? '' : $payrollDetails->Tax->TaxFileNumber;
            }

            if (isset($payrollDetails->Superannuation) && isset($payrollDetails->Superannuation->SuperannuationFund)) {
                $providerDetails = DB::table('super_providers')
                    ->where('super_guid', '=', $payrollDetails->Superannuation->SuperannuationFund->UID)
                    ->get();
                $provider = '';
                if (count($providerDetails) > 0) {
                    $provider = $providerDetails[0]->super_name;
                }
                $fields['provider_id'] = $provider;
                $fields['super_number'] = ($payrollDetails->Superannuation->EmployeeMembershipNumber == null) ? '' : $payrollDetails->Superannuation->EmployeeMembershipNumber;
            }

            if (isset($payrollDetails->Gender)) {
                switch ($payrollDetails->Gender) {
                    case 'Male' :
                        $gender = 1;
                        break;
                    case 'Female' :
                        $gender = 2;
                        break;
                    default :
                        $gender = 0;
                        break;
                }
            } else {
                $gender = 0;
            }
            $fields['gender'] = $gender;
            $fields['date_of_birth'] = (!isset($payrollDetails->DateOfBirth) || $payrollDetails->DateOfBirth == null) ? '' : $payrollDetails->DateOfBirth;

            //echo '<pre>'; var_dump($fields); echo '</pre>';
            DB::table('users')
                ->where('users.id', '=', $id)
                ->update($fields);
        }
        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function addToJobs($id) {
        $jc = new JobsController();
        $futureQ = $jc->getFutureJobList();
        $fjobs = $futureQ->get();

        $ic = new InductionsController();
        $inds = $ic->getInductions();

        $user = DB::table('users')
            ->where('id', '=', $id)
            ->get()[0];

        $sql = 'SELECT right(external_id, 5) as external_id FROM users WHERE right(external_id, 5) REGEXP(\'[0-9]{5}\') order by right(external_id, 5) desc';

        //echo $sql;

        $externalQ = DB::select($sql);

        //echo '<pre>'; var_dump($externalQ); echo '</pre>';

        $external = $externalQ[0];
        $temp_id = intval($external->external_id);
        $emp_id =  'LINX'.str_pad(($temp_id + 1), 5, '0', STR_PAD_LEFT);

        return view('user.addtojob', ['modal_jobs' => $fjobs, 'user' => $user, 'inductions' => $inds, 'emp_id' => $emp_id]);
    }

    public function getNotePart($id) {
        $notes = DB::table('user_notes')
            ->where('user_id', '=', $id)
            ->orderBy('note_stamp', 'desc')
            ->take(1)
            ->get();

        if(count($notes) > 0) {
            $note = $notes[0];
            return substr($note->note_content, 0, 20).'...';
        } else {
            return '';
        }
    }

    public function getNotes($id) {
        $notes = DB::table('user_notes')
            ->leftJoin('users', 'users.id', '=', 'user_notes.note_user_id')
            ->where('user_id', '=', $id)
            ->orderBy('note_stamp', 'desc')
            ->select('user_notes.*', 'users.given_name', 'users.surname')
            ->get();

        return view('user.notes', ['notes' => $notes]);
    }

    public function saveNotes(Request $request) {
        DB::table('user_notes')
            ->insert([
                'user_id' => $request->user_id,
                'note_content' => $request->note_content,
                'note_stamp' => date('Y-m-d H:i:s'),
                'note_user_id' => Auth::user()->id
            ]);

        echo substr($request->note_content, 0, 20);
    }

    function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public function changePassword() {
        return view('user.changepassword');
    }

    public function savePassword(Request $request) {
        $user = Auth::user();
        //check old password
        $credentials = ['username' => $user->username, 'password' => $request->old_password ];
        if (Auth::validate($credentials)) {
            if(strcmp($request->password, $request->confirm_password) == 0) {
                DB::table('users')
                    ->where('id', '=', $user->id)
                    ->update(['password' => bcrypt($request->password)]);
            } else {
                return redirect('/changepw')->with('error', 'New passwords do not match.');
            }
        } else {
            return redirect('/changepw')->with('error', 'Old password is incorrect.');
        }

        return redirect('/dashboard')->with('success', 'Your password was updated successfully.');
    }

    public function ValidateTFN($tfn)
    {
        $weights = array(1, 4, 3, 7, 5, 8, 6, 9, 10);

        // strip anything other than digits
        $tfn = preg_replace("/[^\d]/","",$tfn);

        // check length is 9 digits
        if (strlen($tfn)==9) {
            $sum = 0;
            foreach ($weights as $position=>$weight) {
                $digit = $tfn[$position];
                $sum += $weight * $digit;
            }
            return ($sum % 11)==0;
        }
        return false;
    }

    public function impersonate($id)
    {
        $user = User::find($id);

        // Guard against administrator impersonate
        Auth::user()->setImpersonating($user->id);
        return redirect('dashboard')->with('success', 'You are now logged in as this user.');

    }

    public function impersonateStop()
    {
        Auth::user()->stopImpersonating();

        return redirect('dashboard')->with('success', 'You have returned to your profile');
    }

    public function retrieveSuper() {
    	$startdate = '2017-07-01';
    	$enddate = '2018-07-01';

    	//get all shifts that people have worked
		$shifts = DB::table('users')
			->join('timesheets', 'timesheets.user_id', '=', 'users.id')
			->join('time_tasks', 'time_tasks.time_id', '=', 'timesheets.time_id')
			->where('time_tasks.tita_start', '>=', $startdate)
			->where('time_tasks.tita_finish', '<', $enddate)
			->whereNotNull('users.myob_uid')
			->where('users.myob_uid', '<>', '')
			->select('users.given_name', 'users.surname', 'users.external_id', 'users.myob_uid', 'time_tasks.tita_quantity')
			->get();

		$hours = [];
		foreach($shifts as $shift) {
			if(isset($hours[$shift->myob_uid])) {
				$hours[$shift->myob_uid]['hours'] += $shift->tita_quantity;
			} else {
				$hours[$shift->myob_uid]['given'] = $shift->given_name;
				$hours[$shift->myob_uid]['surname'] = $shift->surname;
				$hours[$shift->myob_uid]['hours'] = $shift->tita_quantity;
				$hours[$shift->myob_uid]['number'] = $shift->external_id;
			}
		}

		//$myobController = new MYOBController();
		//$myobController->payrollAdvice('aa');

		echo '<table width="100%">';
		echo '<tr><td>Name</td><td>Employee Number</td><td>Hours</td>';
		foreach($hours as $user => $hour) {
			echo '<tr><td>'.$hour['given'].' '.$hour['surname'].'</td>';
			echo '<td>'.$hour['number'].'</td>';
			echo '<td>'.$hour['hours'].'</td></tr>';
		}
		echo '</table>';
	}
	
	public function updateFromZapier(Request $request) {
        $token = 'VkavqWuwEpFg7mIhfNbq';
        $reqToken = $request->header('token');
        
        if(strcmp($token, $reqToken) != 0) {
            header('HTTP/1.0 403 Forbidden', true, 403);
        }
        
        $tfn = $request->input('tfn');
        $super_number = $request->input('super_number');
        $super_provider = $request->input('super_provider');
        $super_usi = $request->input('super_usi');
        $external_id = $request->input('worker_id');
        
        //Super Provider should be a name.  This is what we save in the DB anyway.
        $superExists = (DB::table('super_providers')
            ->where('super_name', '=', $super_provider)
            ->count()) > 0;
        
        $userData = [
            'tfn' => $tfn,
            'super_number' => $super_number
        ];
        
        if($superExists) {
            $userData['provider_id'] = $super_provider;
            $userData['provider_id_other'] = '';
        } else {
            $userData['provider_id'] = '';
            $userData['provider_id_other'] = $super_provider;
        }
        
        //USI ID needs a lookup
        $usiExists = DB::table('super_usi')
                ->where('usi_number', '=', $super_usi)
                ->get();
        
        if(count($usiExists) > 0) {
            $userData['usi_id'] = $usiExists[0]->usi_id;
            $userData['usi_id_other'] = '';
        } else {
            $userData['usi_id'] = 0;
            $userData['usi_id_other'] = $super_usi;
        }
        
        DB::table('users')
            ->where('external_id', '=', $external_id)
            ->update($userData);
    }
}