<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return The view, and its data.
     */
    public function showForm()
    {
        //Get all the user's details and pass to view
        $user = DB::table('users')
            ->where('id', '=', Auth::user()->id)
            ->get();

        $roomtypes = DB::table('roomtypes')
            ->get();

        $rooms = DB::table('rooms')
            ->get();

        return view('bookings.form', ['user' => $user[0], 'roomtypes' => $roomtypes, 'rooms' => $rooms]);
    }

    /**
     * Save the booking that was submitted
     */
    public function saveForm(Request $request) {
        $users = array();

        foreach($request->usernames as $username) {
            //check each username to see that it exists.
            $user = DB::table('users')
                        ->where('username', '=', $username)
                        ->get();
            if(count($user) == 0) {
                return view('bookings.form')->with('error', 'One or more of your usernames do not exist in the system')->withInput();
            } else {
                $users[] = $user[0]->id;
            }
        }

        //Now enter into the booking table
        $book_id = DB::table('bookings')
                       ->insertGetId([
                           'user_id' => $request->user_id,
                           'roomtype_id' => $request->roomtypes,
                           'number_in_group' => $request->group_number,
                           'arrival_date' => $request->date_arrival,
                           'departure_date' => $request->date_departure
                        ]);

        //finally, who is staying there?
        foreach($users as $user) {
            DB::table('booking_users')
                ->insert([
                    'booking_id' => $book_id,
                    'user_id' => $user
                ]);
        }

        return view('bookings.complete');
    }

    /**
     * Show the list of bookings to an administrator
     */
    public function showList() {
        $bookings = DB::table('bookings')
            ->join('roomtypes', 'roomtypes.roomtype_id', '=', 'bookings.roomtype_id')
            ->join('users', 'users.id', '=', 'bookings.user_id')
            ->orderBy('bookings.arrival_date', 'DESC')
            ->get();

        return view('bookings.list', ['bookings' => $bookings]);
    }

    /**
     * Show the list of bookings to an administrator
     */
    public function showAdminForm($book_id) {
        $bookings = DB::table('bookings')
            ->join('users', 'users.id', '=', 'bookings.user_id')
            ->join('roomtypes', 'roomtypes.roomtype_id', '=', 'bookings.roomtype_id')
            ->where('bookings.booking_id', '=', $book_id)
            ->get();

        $roomtypes = DB::table('roomtypes')
            ->get();

        $rooms = DB::table('rooms')
            ->get();

        //TODO: exclude beds that are in use during this period.
        $beds = DB::table('beds')
            ->get();

        //Lastly, each user's details and the bed they're assigned to
        $users = DB::table('booking_users')
            ->join('users', 'users.id', '=', 'booking_users.user_id')
            ->where('booking_users.booking_id', '=', $book_id)
            ->get();

        return view('bookings.adminform', ['booking' => $bookings[0], 'roomtypes' => $roomtypes, 'rooms' => $rooms, 'beds' => $beds, 'users' => $users]);
    }

    /**
     * Show the list of bookings to an administrator
     */
    public function saveAdminForm(Request $request) {
        DB::table('bookings')
            ->where('bookings.booking_id', '=', $request->booking_id)
            ->update([
                'arrival_date' => $request->date_arrival,
                'departure_date' => $request->date_departure,
                'roomtype_id' => $request->room_type,
                'confirmed' => $request->confirmed
            ]);

        foreach($request->user_ids as $key=>$value) {
            DB::table('booking_users')
                ->where('booking_id', '=', $request->booking_id)
                ->where('user_id', '=', $value)
                ->update(['room_id' => $request->rooms[$key], 'beds_id' => $request->beds[$key]]);
        }

        return redirect('kingsley/bookings')->with('success', 'The booking\'s details have been updated');
    }

    /**
     * Delete a booking
     */
    public function deleteBooking($id) {
        $bookings = DB::table('bookings')
            ->where('booking_id', '=', $id)
            ->delete();

        return redirect('bookings')->with('success', 'The booking has been deleted');
    }

    /**
     * Show a nice big calendar of bookings
     */
    public function bookingCalendar() {
        $bookings = DB::table('beds')
            ->join('rooms', 'rooms.room_id', '=', 'beds.room_id')
            ->select('beds.beds_name', 'rooms.room_name', 'beds.beds_id')
            ->orderBy('rooms.room_name', 'asc')
            ->orderBy('beds.beds_name', 'asc')
            ->get();

        foreach($bookings as $booking) {
            $roominfo = DB::table('booking_users')
                ->join('bookings', 'bookings.booking_id', '=', 'booking_users.booking_id')
                ->join('users', 'users.id', '=', 'booking_users.user_id')
                ->where('booking_users.beds_id', '=', $booking->beds_id)
                ->select('bookings.arrival_date', 'bookings.departure_date', 'users.given_name', 'users.surname')
                ->get();

            $bookinfo = array();
            foreach($roominfo as $room) {
                $bookinfo[] = $room;
            }

            $booking->bookings = $bookinfo;
        }

        return view('bookings.calendar', ['bookings' => $bookings]);
    }
}