<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Response;
use App\Http\Controllers\Settings\MYOBController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;

class PayslipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = Auth::user();
        $jobs = DB::table('jobs')->get();

        return view('payslips.search', ['user' => $user, 'jobs' => $jobs]);
    }

    /**
     * Show a single payslip on the screen
     */
    public function show($id) {
        $generated = $this->generatePayslip($id);
        $outfile = $generated['outfile'];
        $outname = $generated['outname'];

        return Response::make(file_get_contents($outfile), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; '.$outname,
        ]);
    }

    /**
     * Retrieve the payslip information from MYOB
     */
    public function retrieve() {
        $myob = DB::table('myob')
        ->get()[0];

        $mc = new MYOBController();
        $payslip_data = $mc->getPayslips($myob->last_payslip);

        echo '<pre>'; var_dump($payslip_data); echo '</pre>';

        foreach($payslip_data->Items as $pay) {
            $workers = DB::table('users')
                ->where('users.external_id', '=', $pay->Employee->DisplayID)
                ->get();

            if(count($workers) > 0) {
                $worker = $workers[0];

                echo '<pre>'.$worker->id.'</pre>';

                $slip_id = DB::table('payslips')
                    ->insertGetId([
                        'user_id' => $worker->id,
                        'slip_number' => $pay->ChequeNumber,
                        'slip_date' => $pay->PaymentDate,
                        'slip_gross' => $pay->GrossPay,
                        'slip_net' => $pay->NetPay,
                        'slip_start' => $pay->PayPeriodStartDate,
                        'slip_end' => $pay->PayPeriodEndDate
                    ]);

                echo '<pre>'.$slip_id.'</pre>';

                foreach($pay->Lines as $payitem) {
                    //find a time_task for this worker that matches
                    if($payitem->PayrollCategory->Name == "Base Rate" || $payitem->PayrollCategory->Name == "Base Hourly" || $payitem->PayrollCategory->Name == "Base Salary") {
                        //Ignore it
                    } else {
                        switch ($payitem->PayrollCategory->Type) {
                            case 'Tax' : $slit_type = 1; break;
                            case 'Superannuation' : $slit_type = 2; break;
                            default : $slit_type = 0;
                        }

                        $slit_id = DB::table('payslip_item')->
                            insertGetId([
                                'slip_id' => $slip_id,
                                'slit_name' => $payitem->PayrollCategory->Name,
                                'slit_type' => $slit_type,
                                'slit_amount' => $payitem->Amount,
                                'slit_ytd' => $payitem->YearToDate
                            ]);

                        DB::table('time_tasks')
                            ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
                            ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
                            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
                            ->where('payrates.rate_guid', '=', $payitem->PayrollCategory->UID)
                            ->where('timesheets.user_id', '=', $worker->id)
                            ->where('shifts.start_time', '>=', $pay->PayPeriodStartDate)
                            ->where('shifts.start_time', '<=', date('Y-m-d', strtotime($pay->PayPeriodEndDate.' + 1 day')))
                            ->update(['slit_id' => $slit_id]);
                    }
                }
            }

            DB::table('myob')
                ->update(['last_payslip' => date('Y-m-d')]);
        }

        return redirect('/payslips');
    }

    public function email($id) {
        $this->send(array($id));

        return redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Send a payslip to a worker.  Likely to be called many times.
     */
    public function send($ids) {
        foreach($ids as $id) {
            $payslip = $this->generatePayslip($id);

            try {
                Mail::send('emails.payslip', ['user' => $payslip['worker']], function ($message) use ($payslip) {
                    $message->from('admin@linxemployment.com.au', 'Linx Employment Timesheet Management System');
                    $message->to($payslip['worker']->email);
                    $message->subject('Linx Employment Payslip');
                    $message->attach($payslip['outfile'], ['as' => $payslip['outname'], 'mime' => 'application/pdf']);
                });
            } catch(Exception $ex) {
                //Ignore
            }
        }
    }

    public function search() {
        $payslipQ = DB::table('payslips')
            ->join('users', 'users.id', '=', 'payslips.user_id');

        if(is_array($_GET['user'])) {
            //we are doing many users
            $payslipQ->whereIn('users.id', $_GET['user']);
        } elseif((int)$_GET['user'] > 0) {
            //we are doing one
            $payslipQ->where('users.id', '=', $_GET['user']);
        } else {
            //we are doing everyone
        }

        $payslipQ->join('payslip_item', 'payslip_item.slip_id', '=', 'payslips.slip_id')
            ->join('time_tasks', 'time_tasks.slit_id', '=', 'payslip_item.slit_id')
            ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id');
        if(is_array($_GET['jobs'])) {
            $payslipQ->whereIn('shifts.jobs_id', $_GET['jobs']);
        } elseif((int)$_GET['jobs'] > 0) {
            $payslipQ->where('shifts.jobs_id', '=', $_GET['jobs']);
        }

        $out = array();
        $payslipQ->select('users.given_name', 'users.surname', 'users.external_id', 'payslips.slip_id', 'payslips.slip_number', 'payslips.slip_date', 'payslips.slip_gross', 'payslips.slip_net', 'payslips.slip_start', 'payslips.slip_end')
            ->orderBy('slip_number', 'desc')
            ->distinct();
        $payslips = $payslipQ->get();
        if(count($payslips) > 0) {
            foreach($payslips as $payslip) {
                //do something.
                $out[] = array(
                    'given_name' => $payslip->given_name,
                    'surname' => $payslip->surname,
                    'external_id' => $payslip->external_id,
                    'slip_id' => $payslip->slip_id,
                    'slip_number' => $payslip->slip_number,
                    'slip_date' => $payslip->slip_date,
                    'slip_gross' => $payslip->slip_gross,
                    'slip_net' => $payslip->slip_net,
                    'slip_start' => $payslip->slip_start,
                    'slip_end' => $payslip->slip_end
                );
            }
        }

        return response()->json($out);
    }

    private function generatePayslip($id) {
        $payslips = DB::table('payslips')
            ->where('slip_id', '=', $id)
            ->get();

        if(count($payslips) > 0) {
            $payslip = $payslips[0];
        }

        $items = DB::table('payslip_item')
            ->where('slip_id', '=', $id)
            ->get();

        $linesArray = array();

        foreach ($items as $item) {
            $tasks = DB::table('time_tasks', 'time_tasks.slit_id', '=', 'payslip_item.slit_id')
                ->join('tasks', 'tasks.task_id', '=', 'time_tasks.task_id')
                ->join('roles', 'roles.id', '=', 'tasks.role_id')
                ->join('payrates', 'payrates.id', '=', 'tasks.rate_id')
                ->join('timesheets', 'timesheets.time_id', '=', 'time_tasks.time_id')
                ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
                ->join('shift_tasks', function ($join) {
                    $join->on('shift_tasks.shift_id', '=', 'shifts.id')
                        ->on('shift_tasks.task_id', '=', 'tasks.task_id');
                })
                ->join('jobs', 'jobs.id', '=', 'shifts.jobs_id')
                ->join('clients', 'clients.id', '=', 'jobs.client_id')
                ->where('time_tasks.slit_id', '=', $item->slit_id)
                ->select('jobs.name as jobs_name', 'tasks.task_name', 'shift_tasks.shta_rate', 'clients.name as client_name', DB::raw('sum(time_tasks.tita_quantity) as tita_quantity'))
                ->orderBy('payrates.name')
                ->orderBy('tasks.task_name')
                ->orderBy('shifts.start_time')
                ->get();

            $line = array();
            $line['slit_amount'] = $item->slit_amount;
            $line['slit_ytd'] = $item->slit_ytd;
            switch ($item->slit_type) {
                case 1 :
                    $line['slit_type'] = 'Tax';
                    $line['slit_name'] = $item->slit_name;
                    break;
                case 2 :
                    $line['slit_type'] = 'Superannuation';
                    $line['slit_name'] = $item->slit_name;
                    break;
                default:
                    $line['slit_type'] = 'Wage';
                    foreach ($tasks as $task) {
                        $line['slit_name'] = $task->jobs_name;
                        $taskitem = array(
                            'task_name' => $task->task_name,
                            'tita_quantity' => $task->tita_quantity,
                            'tita_amount' => round($task->tita_quantity * $task->shta_rate, 2, PHP_ROUND_HALF_EVEN), 2,
                            'shta_rate' => $task->shta_rate,
                            'client_name' => $task->client_name,
                            'jobs_name' => $task->jobs_name
                        );

                        $line['items'][] = $taskitem;
                    }
            }

            $linesArray[] = $line;
        }
        $clients = DB::table('users')
            ->leftJoin('super_providers', 'super_providers.super_name', '=', 'users.provider_id')
            ->where('id', '=', $payslip->user_id)
            ->get();
        if (count($clients) > 0) {
            $client = $clients[0];
        } else {
            $client = null;
        }

        $view = View::make('payslips.template', ['payslip' => $payslip, 'lines' => $linesArray, 'client' => $client]);

        try {
            $outname = $client->external_id.'_'.$payslip->slip_number.'.pdf';
            $outfile = storage_path('/'.$outname);

            $pdf = new \HTML2PDF('P', 'A4', 'en', true, 'UTF-8', array(10, 10, 10, 10));
            $pdf->pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($view->render());
            $pdf->Output($outfile, 'F');
        } catch (\HTML2PDF_exception $ex) {
            echo $ex.'<br />';
        }

        return array (
            'outfile' => $outfile,
            'outname' => $outname,
            'worker' => $client
        );
    }
}
