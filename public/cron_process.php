<?php
ini_set('max_execution_time', 300);
ini_set('display_errors', '1');

$timerstart = time();

include 'cron_config.php';

//The code from the Timesheet Controller, rewritten a bit.
/*$sql = 'SELECT DISTINCT
    users.myob_uid,
    timesheets.num_units, time_tasks.time_start, timesheets.time_id, timesheets.time_end, timesheets.payment_type,
    jobs.jobs_uid, clients.myob_uid as client_uid

    from `timesheets` 
    inner join `users` on `users`.`id` = `timesheets`.`user_id` 
    inner join `job_worklist` on `job_worklist`.`user_id` = `users`.`id`
    inner join `time_tasks` on `time_tasks`.`time_id` = `timesheets`.`time_id` 
    inner join `tasks` on `tasks`.`task_id` = `time_tasks`.`task_id`
    inner join `jobs` on `jobs`.`id` = `tasks`.`jobs_id` 
    inner join `clients` on `clients`.`id` = `jobs`.`client_id` 

    WHERE
    timesheets.processed = -1
    
    ORDER BY
    RIGHT(users.external_id, 5) asc,
    time_tasks.tita_start';*/

$sql = 'SELECT DISTINCT
users.myob_uid,
timesheets.num_units, timesheets.time_id,
jobs.jobs_uid, clients.myob_uid as client_uid, payrates.name as rate_name, payrates.rate_guid, payrates.rate_type,
time_tasks.tita_start, time_tasks.tita_finish, time_tasks.tita_break_duration, 
time_tasks.hours_ordinary, time_tasks.hours_overtime, time_tasks.hours_saturday, 
time_tasks.hours_sunday, time_tasks.hours_publicholiday,
payrates.id, time_tasks.task_id, time_tasks.role_id, y.year_id, RIGHT(users.external_id, 5)

from timesheets
join users on users.id = timesheets.user_id 
join job_worklist on job_worklist.user_id = users.id
join time_tasks on time_tasks.time_id = timesheets.time_id 
join tasks on tasks.task_id = time_tasks.task_id
join jobs on jobs.id = tasks.jobs_id 
join clients on clients.id = jobs.client_id 
join roles on roles.role_id = time_tasks.role_id
join payrates on payrates.id = time_tasks.rate_id
join financial_years y on y.year_start <= time_tasks.tita_start AND y.year_finish >= time_tasks.tita_start

WHERE
timesheets.processed = -1
AND
time_tasks.tita_quantity > 0
    
ORDER BY
RIGHT(users.external_id, 5) asc,
time_tasks.tita_start';

if(!$query = $db->query($sql)) {
    echo 'Error: '.$db->error;
}

while($payrate = $query->fetch_assoc()) {

    //echo '<pre>'.(__LINE__); var_dump($payrate); echo '</pre>';
    $rateh = array();
    $ratec = array();
    $payrates = array();

    /*$rateSql = 'SELECT DISTINCT
        payrates.name as rate_name, payrates.rate_guid, payrates.rate_type,
        time_tasks.tita_start, time_tasks.tita_finish, time_tasks.tita_break_duration, 
        time_tasks.hours_ordinary, time_tasks.hours_overtime, time_tasks.hours_saturday, 
        time_tasks.hours_sunday, time_tasks.hours_publicholiday,
        payrates.id, time_tasks.task_id, time_tasks.role_id

        FROM
        timesheets
        join time_tasks on time_tasks.time_id = timesheets.time_id
        join tasks on tasks.task_id = time_tasks.task_id
        join roles on roles.role_id = time_tasks.role_id
        join payrates on payrates.id = roles.rate_id

        WHERE
        time_tasks.tita_quantity > 0 AND 
        timesheets.time_id = '.$row['time_id'];

    //echo $rateSql;

    $rateQuery = $db->query($rateSql);
    while($rateRow = $rateQuery->fetch_assoc()) {
        //var_dump($rateRow);
        $payrates[] = $rateRow;
    }*/

    //foreach($payrates as $payrate) {

        $rate_note = $payrate['rate_name'];

        $start = $payrate['tita_start'];
        $end = $payrate['tita_finish'];

        $units = $payrate['hours_ordinary'];
        $rate_guid = $payrate['rate_guid'];
		$overSql = 'SELECT paru_guid FROM payrate_rules where rate_id = '.$payrate['id'].' AND paru_frequency = -1 AND year_id = '.$payrate['year_id'];
		//echo $overSql;
		$overQuery = $db->query($overSql);
		while($overRow = $overQuery->fetch_assoc()) {
			//echo '<pre>'.(__LINE__); var_dump($overRow); echo '</pre>';
		    $rate_guid = $overRow['paru_guid'];
		}
        //but is this a special case?
        if($payrate['hours_saturday'] > 0) {
            //get this rate's saturday
            $overSql = 'SELECT paru_guid FROM payrate_rules where rate_id = '.$payrate['id'].' AND paru_frequency = 4 AND year_id = '.$payrate['year_id'];
            //echo $overSql;
            $overQuery = $db->query($overSql);
            $units = $payrate['hours_saturday'];
            while($overRow = $overQuery->fetch_assoc()) {
                $rate_guid = $overRow['paru_guid'];
                $rate_note = $payrate['rate_name'].' (Saturday)';
            }
        }

        if($payrate['hours_sunday'] > 0) {
            //get this rate's saturday
            $overSql = 'SELECT paru_guid FROM payrate_rules where rate_id = '.$payrate['id'].' AND paru_frequency = 5 AND year_id = '.$payrate['year_id'];
            //echo $overSql;
            $overQuery = $db->query($overSql);
            $units = $payrate['hours_sunday'];
            while($overRow = $overQuery->fetch_assoc()) {
                $rate_guid = $overRow['paru_guid'];
                $rate_note = $payrate['rate_name'].' (Sunday)';
            }
        }

        if($payrate['hours_publicholiday'] > 0) {
            //get this rate's saturday
            $overSql = 'SELECT paru_guid FROM payrate_rules where rate_id = '.$payrate['id'].' AND paru_frequency = 6 AND year_id = '.$payrate['year_id'];
            //echo $overSql;
            $overQuery = $db->query($overSql);
            $units = $payrate['hours_publicholiday'];
            while($overRow = $overQuery->fetch_assoc()) {
                $rate_guid = $overRow['paru_guid'];
                $rate_note = $payrate['rate_name'].' (Public Holiday)';
            }
        }

        $time_array[$payrate['myob_uid']]['lines'][$rate_guid][] = array(
            'start' => $start,
            'end' => $end,
            'units' => $units,
            'notes' => $rate_note,
            'time_id' => $payrate['time_id']
        );

		if($payrate['hours_overtime'] > 0) {
			//get this rate's saturday
			$overSql = 'SELECT paru_guid FROM payrate_rules where rate_id = '.$payrate['id'].' AND paru_frequency < 4  AND year_id = '.$payrate['year_id'].' ORDER BY paru_frequency LIMIT 0,1';
			//echo $overSql;
			$overQuery = $db->query($overSql);
			$units = $payrate['hours_overtime'];
			while($overRow = $overQuery->fetch_assoc()) {
				$rate_guid = $overRow['paru_guid'];
				$rate_note = $payrate['rate_name'].' (Overtime)';
			}

			$time_array[$payrate['myob_uid']]['lines'][$rate_guid][] = array(
				'start' => $start,
				'end' => $end,
				'units' => $units,
				'notes' => $rate_note,
				'time_id' => $payrate['time_id']
			);
		}

        if (isset($time_array[$payrate['myob_uid']]['start'])) {
            if ($start > strtotime($time_array[$payrate['myob_uid']]['start'])) {
                $time_array[$payrate['myob_uid']]['start'] = $payrate['tita_start'];
            }
        } else {
            $time_array[$payrate['myob_uid']]['start'] = $payrate['tita_start'];
        }

        if (isset($time_array[$payrate['myob_uid']]['end'])) {
            if ($end < strtotime($time_array[$payrate['myob_uid']]['end'])) {
                $time_array[$payrate['myob_uid']]['end'] = $payrate['tita_finish'];
            }
        } else {
            $time_array[$payrate['myob_uid']]['end'] = $payrate['tita_finish'];
        }

        $time_array[$payrate['myob_uid']]['client_uid'] = $payrate['client_uid'];
        $time_array[$payrate['myob_uid']]['jobs_uid'] = $payrate['jobs_uid'];
    //}
}

/*echo '<pre>'.(__LINE__).'<br>'; var_dump($time_array); echo '</pre>';
foreach($time_array as $uid => $data) {
    foreach ($data['lines'] as $lineitem) {
        foreach ($lineitem as $dataitem) {
            echo '<pre>'; var_dump($dataitem); echo '</pre>';
        }
    }
}*/
//die();

//Build a MYOB array from the array we have put together
require_once(str_replace(array('/public', '\\public'), '', (__DIR__)).'/app/Http/Controllers/Settings/includes/AccountRightV2.php');

$myobQuery = $db->query('SELECT * FROM myob');
$myobDetails = $myobQuery->fetch_assoc();

//var_dump($myobDetails);

$myobConfig = array(
    'apiKey' => $myobDetails['api_key'],
    'apiSecret' => $myobDetails['api_secret'],
    'apiCallback' => $myobDetails['redirect_uri'],
    'username' => 'Administrator',
    'password' => ''
);

$ch = curl_init($myobDetails['redirect_uri']);
curl_setopt($ch, CURLOPT_HEADER, 0);
$settingsScreen = curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_exec($ch);
curl_close($ch);
echo '<h1>cURL done</h1>';

sleep(2);

$myob = new AccountRightV2($myobConfig, $db);

if(strlen($myobDetails['cf_guid']) > 0) {
    $myob->retriveAccessToken();
}

$continue = true;

foreach($time_array as $uid => $data) {
    if($continue) {
        ob_start();
    	//Find out if they have any new rates we don't know about
		$rateQ = "SELECT id, payrates FROM users where myob_uid = '$uid'";
		echo $rateQ;
		$refresh = true;
		$payrateQ = $db->query($rateQ);
		while($rateRow = $payrateQ->fetch_assoc()) {
			$rates = json_decode($rateRow['payrates']);
			if($rates == null) {
				$rates = [];
			}
			//echo '<pre>'.(__LINE__); var_dump($rates); echo '</pre>';
			$rateList = [];
			foreach ($data['lines'] as $rate_uid => $line) {
				$rateList[] = $rate_uid;
				//$refresh = (!in_array($rate_guid, $rates));
			}

			//echo '<pre>'.(__LINE__); var_dump($rateList); echo '</pre>';
		}

		if(!is_array($rates) || $refresh) {
			//Get existing payroll information
			$employee = $myob->ContactEmployee($uid);
			sleep(1);

			//Add these payroll categories
			$payrollob = $myob->ContactEmployeePayroll($employee->EmployeePayrollDetails->UID);
			$payrollar = (array)$payrollob;
			echo '<pre>'.(__LINE__); var_dump($payrollar); echo '</pre>';
			sleep(1);
			$payroll = json_decode(json_encode($payrollar), true);

			$i = 0;
			//Add these payroll categories
			foreach ($data['lines'] as $rate_uid => $line) {
				$inarray = false;
				foreach ($payroll['Wage']['WageCategories'] as $cats) {
					if (strcmp($cats['UID'], $rate_uid) == 0) {
						$inarray = true;
					}
				}

				if (!$inarray) {
					$payroll['Wage']['WageCategories'][] = array(
						'UID' => $rate_uid
					);
					//echo 'Adding '.$rate_uid.'<br />';
					$i++;
				}
			}

			if ($i > 0) {
				//Send it back
				$uid = $employee->EmployeePayrollDetails->UID;
				//echo '<pre>'.(__LINE__); var_dump($roll); echo '</pre>';
				$payroll['UID'] = $uid;

				$payroll['RowVersion'] = $payrollob->RowVersion;
				$payroll['Tax']['TaxCategory']['UID'] = $payrollob->Tax->TaxCategory->UID;
				$payroll['Tax']['TaxTable']['UID'] = $payrollob->Tax->TaxTable->UID;

				echo '<pre>'.(__LINE__); var_dump($payroll); echo '</pre>';
				$myob->ContactEmployeePayroll($payroll, 'PUT');
				sleep(1);
			}
		}

		$rate_encoded = json_encode($rateList);
		$db->query("UPDATE users SET payrates = '$rate_encoded' WHERE myob_uid = '$uid'");

		//die();


        echo '<pre>'.(__LINE__); var_dump($data); echo '</pre>';
        $myob_array = array(
            'UID' => $uid,
            'Employee' => array(
                'UID' => $uid
            ),
            'StartDate' => substr($data['start'], 0, 10) . ' 00:00:00',
            'EndDate' => substr($data['end'], 0, 10) . ' 23:59:59'
        );

        foreach ($data['lines'] as $rate_uid => $line) {
            $tempentries = array();
            foreach ($line as $entry) {
                $start = substr($entry['start'], 0, 10) . ' 00:00:00';
                if (isset($tempentries[$start])) {
                    $tempentries[$start] += $entry['units'];
                } else {
                    $tempentries[$start] = $entry['units'];
                }
                /*$entries[] = array(
                    'Date' => $entry['start'],
                    'Hours' => $entry['units']
                );*/
            }

            $entries = array();
            foreach ($tempentries as $date => $entry) {
                $entries[] = array(
                    'Date' => $date,
                    'Hours' => $entry
                );
            }

            $newarray = array(
                'PayrollCategory' => array(
                    'UID' => $rate_uid
                ),
                'Entries' => $entries
            );

            if (strlen($data['jobs_uid']) > 0) {
                $newarray['Job'] = array(
                    'UID' => $data['jobs_uid']
                );
            }

            $myob_array['Lines'][] = $newarray;
            $myob_array['time_id'] = $line[0]['time_id'];
        }

        //echo '<pre>'.(__LINE__); var_dump($myob_array); echo '</pre>';
        //die();
        $errors = false;

        $timesheet = $myob->Timesheet($myob_array);
        var_dump($timesheet);

        if (!$errors && isset($timesheet->Errors) && count($timesheet->Errors) > 0) {
            $errors = true;
        }
        sleep(1);

        if ($errors) {
            foreach ($data['lines'] as $lineitem) {
                foreach ($lineitem as $dataitem) {
                    //Poison the record
                    $sql = 'UPDATE timesheets SET processed = 99 WHERE time_id = ' . $dataitem['time_id'];
                    echo '<p>' . $sql . '</p>';
                    $db->query($sql);
                    foreach ($timesheet->Errors as $error) {
                        $sql = "INSERT INTO myob_log (url, params, response, timestamp, time_id) VALUES ('Timesheet', '" . print_r($myob_array, true) . "', '" . $error->Message . "', '" . date('Y-m-d H:i:s') . "', " . $dataitem['time_id'] . ")";
                        error_log($sql);
                        $db->query($sql);
                    }

                }
            }
        } else {
            foreach ($data['lines'] as $lineitem) {
                foreach ($lineitem as $dataitem) {
                    $sql = 'UPDATE timesheets SET processed = 1 WHERE time_id = ' . $dataitem['time_id'];
                    //echo '<p>' . $sql . '</p>';
                    $db->query($sql);
                }
            }
        }

        $s = time() - $timerstart;
        echo time().' - '.$timerstart.' = '.$s;
        if($s > 270) {
            $continue = false;
        }

        $returneddata = ob_get_clean();

        echo $returneddata;
        $params = print_r($myob_array, true);
        $sql = "INSERT INTO myob_log (url, params, response, timestamp, time_id) VALUES ('Timesheet', ?, ?, '" . date('Y-m-d H:i:s') . "', ".$myob_array['time_id'].")";
        echo '<p>'.$sql.'</p>';
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $params, $returneddata);
        $rc = $stmt->execute();
        if ( false===$rc ) {
            echo '<h1>log entry failed</h1><pre>'.htmlspecialchars($stmt->error).'</pre>';
        }
        $stmt->close();
    }
}

echo '<h1>Done</h1>';

?>