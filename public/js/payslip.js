$(document).ready(function() {
    searchPayslips();
});

$('.searchAjax').select2({
    ajax: {
        url: BASE_URL + '/staff/json',
        dataType: 'json',
        processResults: function (data) {
            return {
                results: $.map(data.items, function(obj) {
                    return {id: obj.id, text: obj.display};
                })
            }
        },
        delay: 250
    }
});

$('#payslipSearch').click(function(e) {
    searchPayslips();
});

function searchPayslips() {
    $('#slip_table tr:not(:first)').remove();
    $('#slip_table').append('<tr><td colspan="6" class="text-center"><i class="fa fa-spinner fa-pulse fa-fw fa-4x"></i></td></tr>');
    $.ajax({
        url: BASE_URL + '/payslips/json',
        dataType: 'json',
        data: {
            'start' : $('#slip_start').val(),
            'end' : $('#slip_end').val(),
            'user' : $('#user_id').val(),
            'jobs' : $('#jobs').val()
        },
        success: function(data) {
            $('#slip_table tr:not(:first)').remove();

            $.each(data, function(k,v) {
                $('#slip_table').append('<tr>' +
                    '<td>' + v.surname + ', ' + v.given_name + ' (' + v.external_id + ')</td>' +
                    '<td>' + v.slip_date + '</td>' +
                    '<td>' + v.slip_number + '</td>' +
                    '<td>' + v.slip_gross + '</td>' +
                    '<td>' + v.slip_net + '</td>' +
                    '<td>' +
                    '   <a class="btn btn-default" href="payslips/' + v.slip_id + '" target="_blank">' +
                    '       <i class="fa fa-eye"></i>' +
                    '   </a>' +
                    '   <a class="btn btn-primary" href="payslips/email/' + v.slip_id + '">' +
                    '       <i class="fa fa-envelope-o"></i>' +
                    '   </a>' +
                    '</td>' +
                '</tr>');
            });
        }
    });
}