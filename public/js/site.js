$(document).ready(function() {
    setTimeout(hideAlert, 3000);

    //setTimeout(hasNotifications(), 1000);

    $('#search-status').select2({
        width : '100%',
        minimumResultsForSearch: 1000000
    });

    /*small number so it literally always shows */
    $('select.search:not(.form-control)').select2({
        width : '100%',
        minimumResultsForSearch: 1,
        //dropdownCssClass : 'bigdrop'
    });

	$('select.search.form-control').select2({
		width : '100%',
		minimumResultsForSearch: 1
	});

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="dropdown"]').tooltip();

    if($('#external_id').val() != undefined) {
        if ($('#external_id').val().length == 0) {
            $('#client-myob').addClass('disabled');
        }
    }

    applyDates();
    doCurrentLocation();
    doVisa88Days();
    doTaxFileNumber();
    doBankAccount();
    doNoFixedAddress();
    doNoSuper();
    doSuperNotListed();
    doUSINotListed();
    doJobNetwork();

    checkSuperSignin();

    updateConditionalForm();
});

$(document).on('change', '.campaign input', function(e) {
    updateConditionalForm();
});

$(document).on('change', '.campaign select', function(e) {
    updateConditionalForm();
});

function updateConditionalForm() {
    $.each($('.form-group.conditional'), function(k,v) {
        var _field = $('#' + $(v).data('field')).find('option:selected').text();
        var _value = $(v).data('value');
        if(_field != _value) {
            $(v).hide();
        } else {
            $(v).show();
        }
    });
}

function applyDates() {
    if(!Modernizr.inputtypes.date) {
        $('input[type=date].week_start').datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShowDay: function(date){
                var day = date.getDay();
                return [day == 1,""];
            }
        });
        $('input[type=date]:not(.week_start)').datepicker({dateFormat: 'yy-mm-dd'});
    }
}

$(document).on('change', '.timepicker', function(e) {
    if($(this).hasClass('hour') || $(this).hasClass('minute')) {
        $parent = $(this).closest('.row');

        _hour = $parent.find('.hour').val();
        _minute = $parent.find('.minute').val();

        $parent.find('.time').val(_hour + ':' + _minute);
    }
});

/* Do this all the time */
$('button[type="submit"]').click(function(e) {
    $(this).addClass('disabled');
});

var previousUsers = 1;

/* register stuff */
$('#current_location').on('change', function(e) {
    doCurrentLocation();
});

$('#visa_88_days').on('change', function(e) {
    doVisa88Days();
});

$('#tfn_none').click(function(e) {
    doTaxFileNumber();
});

$('#tfn_applied').click(function(e) {
    doTaxFileNumber();
});

$('#bsb_none').click(function(e) {
    doBankAccount();
});

$('#no_fixed_address').click(function(e) {
    doNoFixedAddress();
});

$('#super_none').click(function(e) {
    doNoSuper();
});

$('#super_not_listed').click(function(e) {
    doSuperNotListed();
});

$('#usi_not_listed').click(function(e) {
	doUSINotListed();
});

$('#job_network_tick').change(function(e) {
    doJobNetwork();
});

function doCurrentLocation() {
    if($('#current_location').val() == 'Mainland Australia' || $('#current_location').val() == 'Outside Australia') {
        $('#outta_tassie').slideDown();
        $('#loca_arrive').val($('#loca_arrive').val());
        $('#stay_length').val($('#stay_length').val());
    } else {
        $('#outta_tassie').slideUp();
    }
}

function doVisa88Days() {
    if($('#visa_88_days option:selected').val() != 1) {
        $('#internationalDetails').slideUp();
        //Tax questions get cleared and answerable
        $('#tax_resident').removeAttr('readonly');
        $('#tax_free_threshold').removeAttr('readonly');
        $('#senior_tax_offset').removeAttr('readonly');
        $('#help_debt').removeAttr('readonly');
        $('#fs_debt').removeAttr('readonly');
    } else {
        $('#internationalDetails').slideDown();
        //Tax questions readonly and set to "no"
        $('#tax_resident').attr('readonly', 'readonly').val(0);
        $('#tax_free_threshold').attr('readonly', 'readonly').val(0);
        $('#senior_tax_offset').attr('readonly', 'readonly').val(0);
        $('#help_debt').attr('readonly', 'readonly').val(0);
        $('#fs_debt').attr('readonly', 'readonly').val(0);
    }
}

function doBankAccount() {
    if($('#bsb_none').is(':checked')) {
        $('#account_bsb').attr('readonly', 'readonly').val('xxxxxx');
        $('#account_number').attr('readonly', 'readonly').val('xxxxxx');
        $('#account_name').attr('readonly', 'readonly').val('xxxxxx');
    } else {
        $('#account_bsb').removeAttr('readonly');
        if($('#account_bsb').val() == 'xxxxxx') {
            $('#account_bsb').val('');
        }
        $('#account_number').removeAttr('readonly');
        if($('#account_number').val() == 'xxxxxx') {
            $('#account_number').val('');
        }
        $('#account_name').removeAttr('readonly');
        if($('#account_name').val() == 'xxxxxx') {
            $('#account_name').val('');
        }
    }
}

function doTaxFileNumber() {
    if($('#tfn_applied').is(':checked')) {
        $('#tfn').attr('readonly', 'readonly').val('111 111 111');
    } else {
        if ($('#tfn_none').is(':checked')) {
            $('#tfn').attr('readonly', 'readonly').val('000 000 000');
        } else {
            $('#tfn').removeAttr('readonly');
            if ($('#tfn').val() == '000 000 000') {
                $('#tfn').val('');
            }
        }
    }
}

function doNoFixedAddress() {
    if($('#no_fixed_address').is(':checked')) {
        $('#address').attr('readonly', 'readonly').val('PO Box 41');
        $('#city').attr('readonly', 'readonly').val('Legana');
        $('#state').attr('readonly', 'readonly').val('TAS');
        $('#postcode').attr('readonly', 'readonly').val('7277');
    } else {
        $('#address').removeAttr('readonly');
        /*if($('#address').val() == 'PO Box 41') {
            $('#address').val('');
        }*/
        $('#city').removeAttr('readonly');
        /*if($('#city').val() == 'Legana') {
            $('#city').val('');
        }*/
        $('#state').removeAttr('readonly');
        /*if($('#state').val() == 'TAS') {
            $('#state').val('');
        }*/
        $('#postcode').removeAttr('readonly');
        /*if($('#postcode').val() == '7277') {
            $('#postcode').val('');
        }*/
    }
}

function doNoSuper() {
    if($('#super_none').is(':checked')) {
        $('#super_number').attr('readonly', 'readonly').val('xxx');
        $('#provider_id').attr('readonly', 'readonly');
        $('#provider_id_other').attr('readonly', 'readonly');
        $('#super_not_listed').attr('readonly', 'readonly');

        $('#super_details').slideUp();
    } else {
        $('#super_number').removeAttr('readonly').val($('#super_number').val());
        $('#provider_id').removeAttr('readonly');
        $('#provider_id_other').removeAttr('readonly');
        $('#super_not_listed').removeAttr('readonly');

        $('#super_details').slideDown();
    }
}

function doSuperNotListed() {
    if($('#super_not_listed').is(':checked')) {
        $('#provider_id_other').removeAttr('readonly').parent().slideDown();
    } else {
        $('#provider_id_other').attr('readonly', 'readonly').val('').parent().slideUp();
    }
}

function doUSINotListed() {
	if($('#usi_not_listed').is(':checked')) {
		$('#usi_id_other').removeAttr('readonly').parent().slideDown();
	} else {
		$('#usi_id_other').attr('readonly', 'readonly').val('').parent().slideUp();
	}
}

function doJobNetwork() {
    if($('#job_network_tick').val() == 1) {
        $('#job_network').removeAttr('readonly').val($('#job_network').val()).parent().slideDown();
    } else {
        $('#job_network').attr('readonly', 'readonly').val('').parent().slideUp();
    }
}
/* end register */

$(document).on('change', '.task-list', function(e) {
    $this = $(this);

    $this.nextAll().remove();
    $.ajax({
        'url': BASE_URL + '/tasks/children/' + $this.val(),
        'type': 'GET',
        'success': function (data) {
            var _i = 0;
            //pop in another drop-down
            var _buildstring = '<select name="task_id" id="task_id_' + $this.val() + '" class="form-control task-list"> ' +
                '<option value="0">(none)</option>';
            $.each(data, function (k, v) {
                _buildstring += '<option value="' + v.task_id + '" data-rate-id="' + v.rate_id + '">' + v.task_name + '</option>';
                _i++;
            });
            _buildstring += '</select>';

            if(_i > 0) {
                $this.after(_buildstring);
            }
        }
    });

    checkSignInSave();
});

$('#date_departure').on('change', function(e) {
    var _arrive = new Date($('#date_arrival').val());
    var _depart = new Date($('#date_departure').val());

    var _days = (_depart - _arrive) / 1000 / 60 / 60 / 24;
    if(_days < 14) {
        $('button[type=submit]').attr('disabled', 'disabled');
        $(this).parent().append('<div class="alert alert-danger alert-inline">This must be a minimum of 2 weeks after the arrival date.</div>');
    } else {
        $('button[type=submit]').removeAttr('disabled');
    }
});

$('#date_departure').focus(function(e) {
    $('.alert-danger').remove();
});

$('#profileTabs a').click(function (e) {
    e.preventDefault();
    $('#profileTabs li').removeClass('active');
    $(this).parent().addClass('active');
    $('.tab-pane').slideUp();
    $($(this).attr('href')).slideDown();
});

$('#add_bed').click(function(e) {
    $('#bed_name_container').append('<input type="hidden" name="bed_ids[]" /> ' +
        '<input type="text" name="bed_names[]" id="bed_names" class="form-control">');
});

function hideAlert() {
    $('.alert-success').slideUp();
}

$('#search-user #search-btn-staff').on('click', function(e) {
    searchStaff();
});

$(document).on('click', '.archive-worker', function(e) {
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $(this).closest('tr').find('.enddate').val(today).show();
});


$(document).on('keydown', '#search-user #search-name', function(e) {
    if(e.keyCode == 13) {
        searchStaff();
    }
});

$(document).on('keydown', '#search-user #search-external', function(e) {
    if(e.keyCode == 13) {
        searchStaff();
    }
});

$(document).on('keydown', '#search-user #search-phone', function(e) {
    if(e.keyCode == 13) {
        searchStaff();
    }
});

$(document).on('keydown', '#search-user #search-email', function(e) {
    if(e.keyCode == 13) {
        searchStaff();
    }
});

$(document).on('keydown', '#external_id', function(e) {
    if($(this).val().length == 0) {
        $('#client-myob').addClass('disabled');
    } else {
        $('#client-myob').addClass('enabled');
    }
});

$('#client-myob').click(function(e) {
    e.preventDefault();

    $(this).hide();
    $('#myob-load').show();

    $.ajax({
        'method' : 'GET',
        'dataType' : 'JSON',
        'url' : BASE_URL + '/staff/myob/' + $(this).data('id'),
        'success' : function(data) {
            if(data.success) {
                $('#in_myob').html('This staff member is in MYOB').show();
                $('#myob_uid').val(data.success);
            }
            if(data.errors) {
                $('#in_myob').html('There was an error fully adding this staff member to MYOB: ' + data.errors + '<br />Please correct these issues and save their record to try again.').show();
                $('#myob_uid').val(data.uid);
            }
            $('#myob-load').hide();
        },
        'error' : function() {
            $('#myob-load').hide();
            $(this).show();
        }
    });
});

$('#payment-detail input').on('click', function(e) {
    $('.payment-info[data-id=' + $(this).data('id') + ']').hide();
    $('.payment-info[data-type=' + $(this).data('type') + '][data-id=' + $(this).data('id') + ']').show();
});

$('#paginate-forward').click(function(e) {
    searchStaff(parseInt($('#pagenum').val()) + 1, $('#ordering').val());
});

$('#paginate-back').click(function(e) {
    searchStaff(parseInt($('#pagenum').val()) - 1, $('#ordering').val());
});

$('#order-name-asc').click(function(e) {
    searchStaff($('#pagenum').val(), 'asc', 'name');
});

$('#order-name-desc').click(function(e) {
    searchStaff($('#pagenum').val(), 'desc', 'name');
});

$('#order-date-asc').click(function(e) {
    searchStaff($('#pagenum').val(), 'asc', 'date');
});

$('#order-date-desc').click(function(e) {
    searchStaff($('#pagenum').val(), 'desc', 'date');
});

function searchStaff(page, _order, _type) {
    $('.staff-table').append('<tr><td colspan="6" class="text-center staff-table-row"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></td></tr>');
    $('.staff-table-row').remove();

    if(page === undefined) {
        page = 1;
    }

    if(_order === undefined) {
        _order = 'asc';
    }

    if(_type === undefined) {
        _type = 'name';
    }

    $.ajax({
        'url' : BASE_URL + '/staff/filter',
        'data' : {
            'search-name' : $('#search-name').val(),
            'search-phone' : $('#search-phone').val(),
            'search-email' : $('#search-email').val(),
            'search-job' : $('#search-job').val(),
            'search-external' : $('#search-external').val(),
            'search-status' : $('#search-status').val(),
            'page' : page,
            'order' : _order,
            'type' : _type
        },
        'type' : 'GET',
        'success' : function(data) {
            $('.staff-table-row').remove();
            $.each(data.users, function(k,v) {
                switch(v.confirmed) {
                    case 0:
                        if(v.has_medical == 1) {
                            _active = 'Applicant';
                        } else if(v.has_medical == 2) {
                            _active = '<strong class="text-danger">Medical alert</strong>';
                        } else {
                            _active = '<strong class="text-danger">No Medical</strong>';
                        }
                        break;
                    case 1: _active = 'Offered';
                        break;
                    case 2: _active = 'Accepted';
                        break;
                    case 3: _active = 'Induction Pending';
                        break;
                    case 4: _active = 'Active';
                        break;
                    case -1: _active = 'Inactive';
                        break;
                }
                external_id = ((v.external_id !== null) ? v.external_id : '');
                email = ((v.email !== null) ? v.email : '');
                mobilephone = ((v.mobilephone !== null) ? v.mobilephone : '');
                notes = ((v.notes !== null) ? v.notes : '');

                if(v.created_at !== null) {
                    registered = v.created_at.substring(8,10) + '/' + v.created_at.substring(5,7) + '/' + v.created_at.substring(0,4);
                } else {
                    registered = '';
                }

                if(v.campaign_name !== null) {
                    registered += '<br />(' + v.campaign_name + ')';
                }

                _toAppend = '<tr class="staff-table-row">' +
                    '    <td>' +
                    '        <a href="profile/' + v.uid + '">' + v.surname + ', ' + v.given_name + (v.preferred == '' ? '' : ' (' + v.preferred + ')') + '</a>' +
                    '    </td>' +
                    '    <td>' + external_id + '</td>' +
                    '    <td>' + email + '</td>' +
                    '    <td>' + mobilephone + '</td>' +
                    '    <td>' + notes + '<a class="btn btn-primary pull-right user-notes" data-id="' + v.uid + '"><i class="fa fa-sticky-note-o" data-toggle="modal" data-target="#notesModal"></i></a></td>' +
                    '    <td>' + registered + '</td>' +
                    '    <td>' + _active + '</td>' +
                    '    <td class="text-right"> ';
                if(v.confirmed == 0 && v.has_medical > 0) {
                    _toAppend += '<a href="profile/' + v.uid + '/approve" class="btn btn-success approve-staff"><i class="fa fa-check"></i></a>';
                }
                _toAppend += '<a href="profile/' + v.uid + '" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Edit profile"><i class="fa fa-pencil"></i></a>' +
                    '<a href="profile/' + v.uid + '/print" target="_blank" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Print details"><i class="fa fa-print"></i></a>';
                if(v.has_resume > 0) {
                    _toAppend += '<a href="profile/' + v.uid + '/resume" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="View resume"><i class="fa fa-paperclip"></i></a>';
                }
                //if(v.myob_uid != "") {
                    _toAppend += '<a href="visa/' + v.uid + '" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Print Second Visa form"><i class="fa fa-file-text"></i></a>';
                    _toAppend += '<a href="profile/refresh/' + v.uid + '" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Sync from MYOB"><i class="fa fa-cloud-download"></i></a>';
                //}
                _toAppend += '<a href="profile/delete/' + v.uid + '" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Delete staff"><i class="fa fa-trash-o"></i></a>' +
                '<a href="impersonate/' + v.uid + '" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Log in as user"><i class="fa fa-user-secret"></i></a>' +
                '</td>' +
                '</tr>';
                $('.staff-table').append(_toAppend);
            });
            $('#pagenum').val(data.page);
            $('#ordering').val(data.order);
            $('#recordcount').html('Page ' + data.page + ' of ' + data.pages + '. (' + data.total + ' records)');

            if(data.page == 1) {
                $('#paginate-back').addClass('disabled');
            } else {
                $('#paginate-back').removeClass('disabled');
            }

            if((data.page == data.pages) || data.pages == 0) {
                $('#paginate-forward').addClass('disabled');
            } else {
                $('#paginate-forward').removeClass('disabled');
            }

            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}

$(document).on('keydown', '.modal #search-name', function(e) {
    if(e.keyCode == 13) {
        searchStaffForJob();
    }
});

$(document).on('keydown', '.modal #search-external', function(e) {
    if(e.keyCode == 13) {
        searchStaffForJob();
    }
});

$('.modal #search-btn-staff').on('click', function(e) {
    searchStaffForJob();
});

function searchStaffForJob() {
    $.ajax({
        'url' : BASE_URL + '/staff/filter',
        'data' : {
            'search-name' : $('#search-name').val(),
            'search-phone' : $('#search-phone').val(),
            'search-email' : $('#search-email').val(),
            'search-job' : $('#search-job').val(),
            'search-external' : $('#search-external').val(),
            'search-status' : $('#search-status').val()
        },
        'type' : 'GET',
        'success' : function(data) {
            $('.staff-table-row').remove();
            $.each(data.users, function(k,v) {

                switch(v.confirmed) {
                    case 0:
                        if(v.has_medical == 1) {
                            _active = 'Applicant';
                        } else if(v.has_medical == 2) {
                            _active = '<strong class="text-danger">Medical alert</strong>';
                        } else {
                            _active = '<strong class="text-danger">No Medical</strong>';
                        }
                        break;
                    case 1: _active = 'Offered';
                        break;
                    case 2: _active = 'Accepted';
                        break;
                    case 3: _active = 'Induction Pending';
                        break;
                    case 4: _active = 'Active';
                        break;
                    case -1: _active = 'Inactive';
                        break;
                }

                $('.staff-table').append(
                    '<tr class="staff-table-row">' +
                    '    <td>' +
                    '        <a href="profile/' + v.uid + '">' + v.given_name + ' ' + v.surname + (v.preferred == '' ? '' : ' (' + v.preferred + ')') + '</a>' +
                    '    </td>' +
                    '    <td>' + v.external_id + '</td>' +
                    '    <td>' + _active + '</td>' +
                    '    <td><button class="btn btn-success job-staff-btn" data-id="' + v.uid + '"><i class="fa fa-plus"></i></button></td>' +
                    '</tr>'
                );
            });
        }
    });
}

$('.shift-info .expand').on('click', function(e) {
    $('.shift-worklist[data-id=' + $(this).parent().data('id') + ']').slideToggle();
    if($(this).children('i').hasClass('fa-caret-down')) {
        $(this).children('i').removeClass('fa-caret-down').addClass('fa-caret-up');
    } else {
        $(this).children('i').removeClass('fa-caret-up').addClass('fa-caret-down');
    }
});

$(document).on('click', '#user-accept', function(e) {
    e.preventDefault();
    $(this).parents('form').attr('action', BASE_URL + '/timesheets/accept').submit();
});

$(document).on('click', '#user-dispute', function(e) {
    e.preventDefault();
    $(this).parents('form').attr('action', BASE_URL + '/timesheets/dispute').submit();
});

$('#btn-add-shift').on('click', function(e) {
    $new = $('#shift-list-template').clone().removeAttr('id');
    $new.appendTo('#shift-list');

    $pt = $('#payrate-template').clone().removeAttr('id');
    $pt.appendTo('#shift-list');
});

$('#add_role_btn').on('click', function(e) {
    $new = $('#new_role_template').clone().removeAttr('id');
    $new.appendTo('#role_table');
    $.each($new.find('select[name="tasks[]"]'), function(k,v) {
        $(v).select2();
    });
});

$(document).on('click', '.delete_shift_role', function(e) {
    $(this).parents('tr').remove();
});

$(document).on('click', '.remove-task', function(e) {
    e.preventDefault();
    if(confirm('Are you sure?')) {
        window.location = $(this).attr('href');
    }
});

$(document).on('click', '.delete-copy-shift', function(e) {
    $(this).parents('tr').remove();
});

$(document).on('click', '.delete_role_task', function(e) {
    $(this).parents('tr').remove();
});

$('#superSignOut').submit(function(e) {
    btn_count = $('.time_id').length + $('.signin').length;
    tick_count = $('.time_id.btn-success').length + $('.signin.btn-success').length;
    if(btn_count > 0 && tick_count == 0) {
        alert('You have not selected any timesheets to save');
        $(this).find('button[type=submit]').removeClass('disabled');
        return false;
    } else {
        $('.dashboard-tile:not(:last-child)').hide();
        $('.dashboard-tile:last-child').prepend('<span><i class="fa fa-circle-o-notch fa-spin"></i> Saving</span>');
        return true;
    }
});

$(document).on('click', '.delete_timesheet', function(e) {
    $this = $(this);

    if(confirm('Are you sure you want to delete this timesheet?')) {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/timesheets/delete/' + $this.data('id'),
            success: function(data) {
                $this.parents('tr').remove();
            }
        })
    }
});

$('#add-task-btn').on('click', function(e) {
    $('#task-template').clone().removeAttr('id').appendTo($('#task-table'));
});

$('#add-ques-btn').on('click', function(e) {
    $('#add_question').html($('#addQuestionModalContent').html());
    $('#addQuestionModal #ques_id').val($(this).data('id'));
    $('#addQuestionModal #ind_id').val($(this).data('ind'));
    CKEDITOR.replace('ques_text');
});

$('.add-opt-btn').on('click', function(e) {
    $('#add_answer').html($('#addAnswerModalContent').html());
    $('#addAnswerModal #opt_id').val($(this).data('id'));
    $('#addAnswerModal #ques_id').val($(this).data('ques'));
});

$('.edit-ques-btn').on('click', function(e) {
    $('#add_question').html($('#addQuestionModalContent').html());
    $('#addQuestionModal #ques_id').val($(this).data('id'));
    $('#addQuestionModal #ind_id').val($(this).data('ind'));
    $('#addQuestionModal #ques_text').val($(this).parent().prev().prev().html());
    CKEDITOR.replace('ques_text');
});

$('.edit-opt-btn').on('click', function(e) {
    $('#add_answer').html($('#addAnswerModalContent').html());
    $('#addAnswerModal #opt_id').val($(this).data('id'));
    $('#addAnswerModal #ques_id').val($(this).data('ques'));
    $('#addAnswerModal #opt_text').val($(this).parent().prev().prev().text());
    $('#addAnswerModal #opt_correct').val($(this).data('ques'));
});

$('#addQuestionModal .save-modal').on('click', function () {
    for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }

    $.ajax({
        'method' : 'POST',
        'url' : BASE_URL + '/settings/inductions/questions',
        'data' : $('#add_question').serialize(),
        'success' : function(data) {
            window.location.reload();
        }
    });
});

$('#addAnswerModal .save-modal').on('click', function () {
    $.ajax({
        'method' : 'POST',
        'url' : BASE_URL + '/settings/inductions/answers',
        'data' : $('#add_answer').serialize(),
        'success' : function(data) {
            window.location.reload();
        }
    });
});

$('#addPayrateModal .save-modal').on('click', function () {
    $.ajax({
        'method' : 'POST',
        'url' : BASE_URL + '/settings/payrates',
        'data' : $('#payrate_add').serialize(),
        'success' : function(data) {
            window.location.reload();
        }
    });
});

$('.addPayrateBtn').on('click', function(e) {
    $parent = $(this).parents('table');
    $new = $parent.parent().find('.payrateTemplate').clone().removeClass('payrateTemplate');
    $new.appendTo($parent);
});

$(document).on('click', '.removePayrate', function(e) {
    $parent = $(this).parents('tr');
    $('form').append('<input type="hidden" name="toDelete[]" value="' + $parent.find('.shpa_id').val() + '">');
    $parent.remove();
});

$(document).on('click', '.copy-shift', function(e) {
    var $new = $(this).parents('tr').clone().data('id', '');
    $new.children('input[type=hidden]').val(0);
    $new.children('select').val(0);
    $new.appendTo($(this).parents('table'));
    applyDates();
});

$(document).on('click', '#add_shift', function(e) {
    $table = $(this).parents('table');
    $toCopy = $table.find('tr:last-child');

    $toCopy.clone().appendTo($table);
});

$(document).on('click', '.checkbox', function(e) {
    $(this).removeClass('btn-default').addClass('btn-success');
    $(this).children('i').removeClass('fa-square-o').addClass('fa-check-square-o');
    $(this).parents('table').addClass('tofix');
});

$(document).on('click', '.select-all', function(e) {
    if($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-danger').html('<i class="fa fa-times"></i> Select none');
        $.each($('input[type=checkbox]'), function(k, v) {
            $(v).prop('checked', true);
        });
    } else {
        $(this).removeClass('btn-danger').addClass('btn-success').html('<i class="fa fa-check"></i> Select all');
        $.each($('input[type=checkbox]'), function(k, v) {
            $(v).prop('checked', false);
        });
    }
});

/* #region SIGN OUT OF SHIFT */
$(document).on('click', '#add_time_task', function(e) {
    $('#time_task_template').clone().removeAttr('id').appendTo('#time_tasks');
});

$(document).on('click', '.add_time_task', function(e) {
    $this = $(this);

    var _id = $(this).data('id');
    $row = $('#task_template').clone().removeAttr('id');
    $row.find('[name="tita_id[][]"]').attr('name', 'tita_id[' + _id + '][]');
    $row.find('[name="task_id[][]"]').attr('name', 'task_id[' + _id + '][]');
    $row.find('[name="tita_start[][]"]').attr('name', 'tita_start[' + _id + '][]');
    $row.find('[name="tita_user_start[][]"]').attr('name', 'tita_user_start[' + _id + '][]');
    $row.find('[name="tita_end[][]"]').attr('name', 'tita_end[' + _id + '][]');
    $row.find('[name="tita_user_end[][]"]').attr('name', 'tita_user_end[' + _id + '][]');
    $row.find('[name="tita_user_break_duration[][]"]').attr('name', 'tita_user_break_duration[' + _id + '][]');
    $row.find('[name="tita_break_duration[][]"]').attr('name', 'tita_break_duration[' + _id + '][]');
    $row.find('[name="tita_quantity[][]"]').attr('name', 'tita_quantity[' + _id + '][]');
    $row.find('[name="tita_user_quantity[][]"]').attr('name', 'tita_user_quantity[' + _id + '][]');
    $row.appendTo($this.closest('tr').find('.tita_table'));
});

$(document).on('click', '.delete_time_task', function(e) {
    $(this).closest('tr').remove();
    validateTimes();
});

$(document).on('change', '.task_box', function(e) {
    $selected = $(this).find('option:selected');
    $row = $(this).closest('tr');

    if($selected.data('type') == 0) {
        $row.find('.shta_contract').hide();
    } else {
        $row.find('.shta_contract').show();
    }
});

$(document).on('keyup', '#time_tasks input', function(e) {
    validateTimes();
});

$(document).on('change', '#time_tasks select', function(e) {
    validateTimes();
});

$('#add_time_task').click(function(e) {
    $this = $(this);
    $taskTemplate = $('#task_template').clone().removeAttr('id');
    $.each($taskTemplate.find('input, select'), function(k,v) {
        $(v).attr('name', $(v).attr('name').replace('[', '[' + $this.data('id')));
    });
    $(this).closest('td').prev('td').find('table').append($taskTemplate);
});

function validateTimes() {
    /* Assume the best */
    var _validNone = true;
    var _validValid = true;
    $('.invalid').removeClass('invalid');

    if(!_validNone) {
        $('#signoutErrorNone').show();
    } else {
        $('#signoutErrorNone').hide();
    }

    if(!_validValid) {
        $('#signoutErrorInvalid').show();
    } else {
        $('#signoutErrorInvalid').hide();
    }

    if(_validValid && _validNone) {
        $('#userSignOut button[type="submit"]').removeClass('disabled');
    } else {
        $('#userSignOut button[type="submit"]').addClass('disabled');
    }
}
/* #endregion SIGN OUT OF SHIFT */

$('.signin').on('click', function(e) {
    if($(this).hasClass('btn-default')) {
        $(this).removeClass('btn-default').addClass('btn-success');
        $(this).parent().children('.signinflag').val(1);
    } else {
        $(this).removeClass('btn-success').addClass('btn-default');
        $(this).parent().children('.signinflag').val(0);
    }
});

$('.view-payrates').on('click', function(e) {
    $('#user_id').val($(this).data('user'));
    $('#shift_id').val($(this).data('shift'));

    $.ajax({
        'url' : BASE_URL + '/timesheets/shift/' + $(this).data('shift') + '/rates',
        'method' : 'get',
        'dataType' : 'json',
        'success' : function(data) {
            $('#payrate-table tr:not(.header)').remove();
            if(data.length > 0) {
                $.each(data, function(k,v) {
                    $('#payrate-table').append('<tr>' +
                        '<td>' + v.task_name + '</td>' +
                        '<td>' + (v.rate_type == 0 ? 'Hourly' : 'Contract') + '</td>' +
                        '<td class="text-right">$' + v.shta_rate + '</td>' +
                    '</tr>');
                });

                $('#payrate-table').show();
            } else {
                $('#payrate-table').hide();
            }
        }
    });
});

$('.timesheetViewBtn').on('click', function(e) {
    $this = $(this);
    $.ajax({
        'url' : BASE_URL + '/timesheets/' + $(this).data('id'),
        'method' : 'get',
        'dataType' : 'html',
        'success' : function(data) {
            $('#tsView').html(data);
            if($this.hasClass('btn-danger')) {
                $('#tsView').append('<input type="hidden" name="resolve" value="1">');
            }
        }
    })
});

$('.timesheetIncludeBtn').on('click', function(e) {
    if($(this).hasClass('btn-default')) {
        $(this).removeClass('btn-default').addClass('btn-success');
        $(this).parent().children('input[name="time_id[]"]').prop('checked', true);
    } else {
        $(this).removeClass('btn-success').addClass('btn-default');
        $(this).parent().children('input[name="time_id[]"]').prop('checked', false);
    }
});

$('.timesheetDoneBtn').on('click', function(e) {
	if($(this).hasClass('btn-default')) {
		$(this).removeClass('btn-default').addClass('btn-danger');
		$(this).parent().children('input[name="done_id[]"]').prop('checked', true);
	} else {
		$(this).removeClass('btn-danger').addClass('btn-default');
		$(this).parent().children('input[name="done_id[]"]').prop('checked', false);
	}
});

$('.time_id').on('click', function(e) {
    if($(this).hasClass('btn-default')) {
        $(this).removeClass('btn-default').addClass('btn-success');
        $(this).parent().children('input').prop('checked', true);
    } else {
        $(this).removeClass('btn-success').addClass('btn-default');
        $(this).parent().children('input').prop('checked', false);
    }
});

$('#selectAllBtn').on('click', function(e) {
    if($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-danger').html('<i class="fa fa-times"></i> Select none');
        $.each($('.timesheetIncludeBtn'), function(k, v) {
        	var p = $(v).parent().children('input[name="done_id[]"]').prop('checked');
        	if(!p) {
				$(v).removeClass('btn-default').addClass('btn-success');
				$(v).parent().children('input[name="time_id[]"]').prop('checked', true);
			}
        });
    } else {
        $(this).removeClass('btn-danger').addClass('btn-success').html('<i class="fa fa-check"></i> Select all');
        $.each($('.timesheetIncludeBtn'), function(k, v) {
            $(v).removeClass('btn-success').addClass('btn-default');
            $(v).parent().children('input[name="time_id[]"]').prop('checked', false);
        });
    }
});

$('#shiftworkers-all').on('click', function(e) {
    if($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-danger').html('<i class="fa fa-times"></i> Select none');
        $.each($('.time_id'), function(k, v) {
            $(v).removeClass('btn-default').addClass('btn-success');
            $(v).parent().children('input').prop('checked', true);
        });
    } else {
        $(this).removeClass('btn-danger').addClass('btn-success').html('<i class="fa fa-check"></i> Select all');
        $.each($('.time_id'), function(k, v) {
            $(v).removeClass('btn-success').addClass('btn-default');
            $(v).parent().children('input').prop('checked', false);
        });
    }
});

$('#shiftworkers-signin').on('click', function(e) {
    if($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-danger').html('<i class="fa fa-times"></i> Select none');
        $.each($('.signin'), function(k, v) {
            $(v).removeClass('btn-default').addClass('btn-success');
            $(v).parent().children('.signinflag').val(1);
        });
    } else {
        $(this).removeClass('btn-danger').addClass('btn-success').html('<i class="fa fa-check"></i> Select all');
        $.each($('.signin'), function(k, v) {
            $(v).removeClass('btn-success').addClass('btn-default');
            $(v).parent().children('.signinflag').val(0);
        });
    }
});

$('#processUsr').on('click', function(e) {
    /*$.ajax({
        'method' : 'POST',
        'url' : BASE_URL + '/timesheets/process/user',
        'datatype' : 'json',
        'data' : $(this).parents('form').serialize()
    })*/

    $(this).parents('form').attr('action', 'process/user').submit().removeAttr('action');
});

$(document).on('change', '.supervised input, .supervised select', function(e) {
    $('#notes').addClass('invalid');
    $('#notes_desc').addClass('invalid').text('Notes - please leave a comment about your changes')
    $('#staffmodalclose').hide();
});

$(document).on('keypress', '#notes', function(e) {
    $('#notes').removeClass('invalid');
    $('#notes_desc').removeClass('invalid').text('Notes');
    $('#staffmodalclose').show();
});

$(document).on('click', '.remove-worker', function(e) {
    $(this).parents('tr').remove();
});

$('#job_end').on('click', function(e) {
    if($(this).is(':checked')) {
        $('#job_end_date').show();
    } else {
        $('#job_end_date').hide();
    }
});

$(document).on('change', '.tasks-list', function(e) {
    $elem = $(this);
    $selected = $elem.find('option:selected');
    //$selected.data('rate'); $selected.data('type');
    $row = $elem.closest('tr');

    if($selected.data('type') == 0) {
        $row.find('.task-payment-type').text('Hourly');
        $row.find('.task-payment-amount').val($selected.data('rate')).attr('readonly', 'readonly');
    } else {
        $row.find('.task-payment-type').text('Contract');
        $row.find('.task-payment-amount').val($selected.data('rate')).removeAttr('readonly');
    }
});

$(document).on('click', '.job-staff-btn', function(e) {
    $new = $('#job_worklist_template').clone().removeAttr('id');
    $firsttd = $($new.children('td')[0]);
    $firsttd.append($($(this).parents('tr').children()[0])[0].textContent.trim() + ' (' + $($(this).parents('tr').children()[1])[0].textContent.trim() + ')');
    $firsttd.children('input').val($(this).attr('data-id'));
    $new.insertBefore('#lastWorkerRow');
    $(this).parents('tr').remove();

    $('#job_worklist select').select2({
        width: '100%',
        minimumResultsForSearch: 1000000 /*arbitrarily large number, so it never shows */
    });

    applyDates();
});

$(document).on('click', 'button[type=submit]', function(e) {
    $(this).addClass('disabled');
    $('#updateForm').removeClass('disabled');
});

$(document).on('click', '#sendform', function(e) {
    $(this).addClass('disabled');
    $.ajax({
        'url' : 'send/' + $('#emp_id').val(),
        'type': 'POST',
        'data' : {
            'empl_id': $('#emp_id').val(),
            'contactName': $('#emp_name').val(),
            'contactEmail': $('#emp_mail').val()
        },
        'dataType' : 'json',
        'success' : function(data) {
            if(data.success) {
                alert('Email sent successfully');
            } else {
                alert(data.error);
            }
            $('#sendform').removeClass('disabled');
        }
    })
});

$(document).on('click', '.job-staff-del', function(e) {
    $(this).parents('tr').remove();
});

$(document).on('click', '.delete-shift', function(e) {
    e.preventDefault();
    var $this = $(this);
    $.ajax({
        'url' : $this.attr('href'),
        'type' : 'GET',
        'success' : function(data) {
            $this.closest('tr').remove();
        }
    });
});

$('#roomtypes').on('change', function(e) {
    //Populate the rooms drop-down with relevant rooms
    $.each($('#roomlist').children('[data-roomtype=' + $(this).val() + ']'), function(k,v) {
        $('#room-image img#fg').attr('src', $(v).attr('data-image'));
    });

    //Update the image
    var option = $('option:selected', this).attr('data-image');
    $('#roomtype-image img').attr('src', option);
});

$('.spinner .btn:first-of-type').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1).change();
});

$('.spinner .btn:last-of-type').on('click', function() {
    //Don't allow negative numbers
    if($('.spinner input').val() > 1) {
        $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1).change();
    }
});

$('#date_arrival, #date_departure').on('change', function(e) {
    $.ajax({
        'url' : '/linx/staff/public/kingsley/rooms/available/' + $('#date_arrival').val() + '/' + $('#date_departure').val(),
        'type' : 'GET',
        'success' : function(data) {
            $('.select-rooms').html('').append('<option value="0"></option>');
            $.each(data, function(k,v) {
                $('.select-rooms').append('<option value="' + v.value + '">' + v.display + '</option>');
            });
        }
    });
});

$('.select-rooms').on('change', function(e) {
    elem = $(this).parents('tr').find('.select-beds');

    $.ajax({
        'url' : BASE_URL + '/kingsley/beds/available/' + $(this).val() + '/' + $('#date_arrival').val() + '/' + $('#date_departure').val(),
        'type' : 'GET',
        'success' : function(data) {
            $(elem).html('');
            $(elem).append('<option value="0"></option>');
            $.each(data, function(k,v) {
                if(v.active) {
                    $(elem).append('<option value="' + v.value + '">' + v.display + '</option>');
                }
            });
        }
    });
});

$('.spinner input').on('change', function(e) {
    //difference
    var _difference = $(this).val() - previousUsers;
    if(_difference > 0) {
        for(i = 0; i < _difference; i++) {
            addUsername();
        }
    } else {
        //removing
        for(i = 0; i > _difference; i--) {
            removeUsername();
        }
    }
});

$(document).on('click', '#update-supervisors', function(e) {
    //update all of the supervisors to match this one.
    $('select.supervisor option:selected').removeAttr('selected');

    $.each($('select.supervisor option'), function(k,v) {
        if($(v).attr('value') == $('#supervisor_master').val()) {
            $(v).attr('selected', 'selected');
        }
    })
});

function addUsername() {
    $('.usernames').append('<input type="text" name="usernames[]" id="username" class="form-control">');
    previousUsers++;
}

function removeUsername() {
    $('.usernames input:last-child').remove();
    previousUsers--;
}

$(document).on('click', '.user-notes', function(e) {
    $this = $(this);
    $('#user-notes-div').hide();
    $('#user-notes-loading').show();
    $.ajax({
        'url' : BASE_URL + '/profile/' + $this.data('id') + '/notes',
        'method' : 'get',
        'dataType' : 'html',
        cache: false,
        'success' : function(data) {
            $('#user-notes-loading').hide();
            $('#user-notes-div').html(data).show();
            $('#user-notes-div form').append('<input type="hidden" name="user_id" value="' + $this.data('id') + '">');
        }
    })
});

$(document).on('submit', '#user-note-form', function(e) {
    e.preventDefault();
    _data_id = $('input[name=user_id]').val();

    $.ajax({
        'url' : BASE_URL + '/profile/notes',
        'method' : 'post',
        'data' : $(this).serialize(),
        'cache' : false,
        'success' : function(data) {
            //Put the notes into the list
            $('a[data-id=' + _data_id + ']').closest('td').html(data + '...' + '<a class="btn btn-primary pull-right user-notes" data-id="' + _data_id + '"><i class="fa fa-sticky-note-o" data-toggle="modal" data-target="#notesModal"></i></a>');

            //TODO: put the notes into the user manager - prepend a panel.

            $('#notesModal').modal('hide');
        }
    });
});

$('#copy_destination').click(function(e) {
    _startdate = new Date($('input[type=date]:last').val());
    _enddate = new Date($('input[type=date]:first').val());

    var newDate = _startdate.setDate(_startdate.getDate() + 1);
    _startdate = new Date(newDate);

    newDate = _enddate.setDate(_enddate.getDate() + 1);
    _enddate = new Date(newDate);

    while(_startdate < _enddate) {
        $table = $(this).parents('table');
        $toCopy = $table.find('tr:last-child');

        $new = $toCopy.clone().appendTo($table);

        _dateval = _startdate.getFullYear() + '-' + ('0'+(_startdate.getMonth()+1)).slice(-2) + '-' + ('0' + _startdate.getDate()).slice(-2);
        $new.find('input[type=date]').val(_dateval);

        newDate = _startdate.setDate(_startdate.getDate() + 1);
        _startdate = new Date(newDate);
    }
});

var _avail = false;
$('#checkAllAvail').click(function (e) {
    $this = $(this);
    _avail = !_avail;
    $.each($this.closest('.col-sm-3').find('.btn-group input[type="checkbox"]'), function(k,v) {
        if($(v).is(':visible')) {
            $(v).prop('checked', _avail);
        }
    });
    checkSignInSave();
});

var _work = false;
$('#checkAllWorking').click(function (e) {
    $this = $(this);
    _work = !_work;
    $.each($this.closest('.col-sm-3').find('.btn-group input[type="checkbox"]'), function(k,v) {
        if($(v).is(':visible')) {
            $(v).prop('checked', _work);
        }
    });
    checkSignInSave();
});

$('#workerAvailable .btn-group input[type="checkbox"]').click(function(e) {
    checkSignInSave();
});

$('#workerWorking .btn-group input[type="checkbox"]').click(function(e) {
    checkSignInSave();
});

function checkSignInSave() {
    var _enabled = false;
    var _payrate = 0;

    $.each($('select[name="task_id"] option:selected'), function(k,v) {
        if($(v).val() > 0) {
            _payrate = $(v).val();
        }
    });

    if($('#workerAvailable .btn-group input[type="checkbox"]:checked').length > 0 && _payrate > 0) {
        _enabled = true;
    }

    if($('#workerWorking .btn-group input[type="checkbox"]:checked').length > 0) {
        _enabled = true;
    }

    if(_enabled) {
        $('#saveWorkers').removeClass('disabled');
    } else {
        $('#saveWorkers').addClass('disabled');
    }
}

$('#select-all-workers').click(function(e) {
    $('td input[type="checkbox"]').prop('checked', $('#select-all-workers').prop('checked'));
});

$('#toggle-workers').click(function(e) {
    if($(this).hasClass('btn-success')) {
        $(this).removeClass('btn-success').addClass('btn-danger').text('Show my workers');
        $('#super-form .btn-group').show();
    } else {
        $(this).removeClass('btn-danger').addClass('btn-success').text('Show all workers');
        checkSuperSignin();
    }
});

$('#job-all-workers').click(function(e) {
    $('tr.archived').toggle();
    $this = $(this);
    if($this.children('i').hasClass('fa-eye')) {
        $this.html('<i class="fa fa-eye-slash"></i> Hide old workers');
    } else {
        $this.html('<i class="fa fa-eye"></i> Show old workers');
    }
});

$('.copyTimeTask').click(function(e) {
    e.preventDefault();
    $parent = $(this).parents('tr');
    $parent.find('.select2').remove();
    $newrow = $parent.clone();
    $.each($newrow.find('input[name="tita_id[' + $parent.data('id') + ']"]'), function(k, v) {
        $(v).val('');
    });

    $.each($parent.find('select.search'), function(k,v) {
        $(v).select2();
    });
    $.each($newrow.find('select.search'), function(k,v) {
        $(v).select2();
    });
    $newrow.html($newrow.html().replaceAll('[' + $parent.data('id') + ']', '[' + $('#maxid').val() + ']'));
    $newrow.insertAfter($parent);
    //add one to max id
    $('#maxid').val(parseInt($('#maxid').val()) + 1);
});

String.prototype.replaceAll = function(search, replace) {
    if (replace === undefined) {
        return this.toString();
    }
    return this.split(search).join(replace);
}

$('#super-form').submit(function(e) {
    _data = $('#super-form').serialize();
    _payrate = 0;
    $.each($('select[name="task_id"] option:selected'), function(k,v) {
        if($(v).val() > 0) {
            _payrate = $(v).val();
        }
    });

    $('#super-form').append('<input type="hidden" name="task_id" value="' + _payrate + '">');
});

function checkSuperSignin() {
    $.each($('#super-form .btn-group'), function(k,v) {
        if($(v).data('supervisor') != $('#super_id').val()) {
            $(v).hide();
        } else {
            $(v).show();
        }
    });
}

$(document).on('click', '.btn-danger', function(e) {
    return confirm('Are you sure?');
});

$('#editAllWorkers').click(function(e) {
	$('[disabled]').removeAttr('disabled');
	$('.absentrow').removeClass('absentrow').addClass('presentrow');
});

$(document).on('click', '.deleteRateRuleRow', function(e) {
    $(this).parents('tr').remove();
});

$(document).on('click', '#addRateRuleRow', function(e) {
	$id = $(this).data('id');
    $new = $('#rateRuleTemplate tr').clone().appendTo('.rateRuleTable[data-id=' + $id + ']');
    $.each($new.find('select.rate_guid'), function(k,v) {
        $(v).select2();
    });
    $.each($new.find('input[name="year_id[]"]'), function(k,v) {
    	$(v).val($id);
	})
});

$(document).on('click', '#teViewAll', function(e) {
    $('#teWorkers tr.hidden').removeClass('hidden');
});

$(document).on('click', '#contactModal .job-staff-btn', function(e) {
	e.preventDefault();
	e.stopPropagation();

	$.ajax({
		'type' : 'POST',
		'url' : BASE_URL + '/client/' + $('#client_id').val() + '/contact/' + $(this).data('id'),
		'success' : function(data) {
			$(this).parents('tr').remove();
		}
	});
});

$(document).on('click', '#contactModal .btn[data-dismiss="modal"]', function(e) {
	e.preventDefault();
	e.stopPropagation();

	window.location.reload();
});