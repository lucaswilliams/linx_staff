$('#medical-form input[type=radio]').click(function(e) {
    //console.log($(this).data('id'));
    $(this).closest('tr').removeClass('danger');

    if($(this).val() == 2) {
        $('.old_smoker, .current_smoker').removeAttr('disabled');
    } else {
        $('.old_smoker').attr('disabled', 'disabled');
        if ($(this).val() == 0) {
            //No, so we disable the textarea
            $('.' + $(this).data('id')).attr('disabled', 'disabled');
        } else {
            //Yes, so we enable the textarea
            $('.' + $(this).data('id')).removeAttr('disabled');
        }
    }
});

$('input[name=current_smoker]').click(function(e) {
    $('input[name=smokes_daily_cigarettes]').parent().removeClass('danger');
    $('input[name=smoker_quit_year]').parent().removeClass('danger');
    $('input[name=smoker_duration]').parent().removeClass('danger');
});
$('input[name=drink_alcohol]').click(function(e) {
    $('input[name=alcohol_per_week]').parent().removeClass('danger');
    $('input[name=alcohol_type]').parent().removeClass('danger');
});


$('#medical-form textarea').blur(function(e) {
    $(this).parent().removeClass('danger');
});

$('#medical-form [name=skip]').click(function(e) {
    //console.log('a');
    //e.preventDefault();
    //e.stopPropagation();
});

$('#medical-form [name=submit]').click(function(e) {
    //A long and complicated validation.  We start by assuming it is true
    var _valid = true;

    var _value = $('input[name=lost_time_radio]:checked').val();
    if(_value == undefined) {
        $('#lost_time').addClass('danger');
        _valid = false;
        //console.log('time_lost undefined');
    } else if (_value == 1 && $('textarea[name=lost_time]').val() == '') {
        $('textarea[name=lost_time]').parent().addClass('danger');
        _valid = false;
        //console.log('time_lost');
    }

    _value = $('input[name=medication_radio]:checked').val();
    if(_value == undefined) {
        $('#medication').addClass('danger');
        _valid = false;
        //console.log('medication undefined');
    } else if (_value == 1 && $('textarea[name=medication]').val() == '') {
        $('textarea[name=medication]').parent().addClass('danger');
        _valid = false;
        //console.log('medication');
    }

    _value = $('input[name=allergies_radio]:checked').val();
    if(_value == undefined) {
        $('#allergies').addClass('danger');
        _valid = false;
        //console.log('allergies undefined');
    } else if (_value == 1 && $('textarea[name=allergies]').val() == '') {
        $('textarea[name=allergies]').parent().addClass('danger');
        _valid = false;
        //console.log('allergies');
    }

    _value = $('input[name=allergies_medication_radio]:checked').val();
    if(_value == undefined) {
        $('#allergies_medication').addClass('danger');
        _valid = false;
        //console.log('allergies med undefined');
    } else if (_value == 1 && $('textarea[name=allergies_medication]').val() == '') {
        $('textarea[name=allergies_medication]').parent().addClass('danger');
        _valid = false;
        //console.log('allergies med');
    }

    _value = $('input[name=allergies_additional_radio]:checked').val();
    if(_value == undefined) {
        $('#allergies_additional').addClass('danger');
        _valid = false;
        //console.log('allergies additional undefined');
    } else if (_value == 1 && $('textarea[name=allergies_additional]').val() == '') {
        $('textarea[name=allergies_additional]').parent().addClass('danger');
        _valid = false;
    }

    _value = $('input[name=sport_hobbies_radio]:checked').val();
    if(_value == undefined) {
        $('#sport_hobbies').addClass('danger');
        _valid = false;
    } else if (_value == 1 && $('textarea[name=sport_hobbies]').val() == '') {
        $('textarea[name=sport_hobbies]').parent().addClass('danger');
        _valid = false;
    }

    _value = $('input[name=existing_condition_radio]:checked').val();
    if(_value == undefined) {
        $('#existing_condition').addClass('danger');
        _valid = false;
    } else if (_value == 1 && $('textarea[name=existing_condition]').val() == '') {
        $('textarea[name=existing_condition]').parent().addClass('danger');
        _valid = false;
    }

    _value = $('input[name=stand_radio] option:selected').val();
    if(_value == 0) {
        $('#stand').addClass('danger');
        _valid = false;
    }

    $.each($('#med-questions tr:not(:first-child)'), function(k,v) {
        $kids = $(v).find(':checked');
        //console.log($kids);
        if($kids.length == 0) {
            $(v).addClass('danger');
            _valid = false;
        } else if($($kids[0]).val() == 1) {
            //check the duration and dates, and current status
            $.each($(v).find('textarea'), function(l,w) {
                if($(w).val() == '') {
                    $(w).parent().addClass('danger');
                    _valid = false;
                }
            })
        }
    });

    _value = $('input[name=current_smoker]:checked').val();
    if(_value == undefined) {
        $('#current_smoker').addClass('danger');
        //console.log('current smoke undefined');
        _valid = false;
    } else {
        if(_value == 1) {
            if ($('input[name=smokes_daily_cigarettes]').val() == '') {
                $('input[name=smokes_daily_cigarettes]').parent().addClass('danger');
                _valid = false;
                //console.log('Yes: daily cigarettes');
            }
        } else if(_value == 2) {
            if ($('input[name=smokes_daily_cigarettes]').val() == '') {
                $('input[name=smokes_daily_cigarettes]').parent().addClass('danger');
                _valid = false;
                //console.log('Yes: daily cigarettes');
            }
            if ($('input[name=smoker_quit_year]').val() == '') {
                $('input[name=smoker_quit_year]').parent().addClass('danger');
                _valid = false;
                //console.log('Yes: quit year');
            }
            if ($('input[name=smoker_duration]').val() == '') {
                $('input[name=smoker_duration]').parent().addClass('danger');
                _valid = false;
                //console.log('Yes: duration');
            }
        }
    }

    _value = $('input[name=drink_alcohol]:checked').val();
    if(_value == undefined) {
        $('#drink_alcohol').addClass('danger');
        _valid = false;
        //console.log('alcohol undefined');
    } else if(_value == 1) {
        if ($('input[name=alcohol_per_week]').val() == '') {
            $('input[name=alcohol_per_week]').parent().addClass('danger');
            _valid = false;
            //console.log('Yes alcohol: per week');
        }
        if ($('input[name=alcohol_type]').val() == '') {
            $('input[name=alcohol_type]').parent().addClass('danger');
            _valid = false;
            //console.log('Yes alcohol: type');
        }
    }

    _value = $('input[name=vaccinate_hep_a]:checked').val();
    if(_value == undefined) {
        $('#vaccinate_hep_a').addClass('danger');
        _valid = false;
    }

    _value = $('input[name=vaccinate_hep_b]:checked').val();
    if(_value == undefined) {
        $('#vaccinate_hep_b').addClass('danger');
        _valid = false;
    }

    _value = $('input[name=vaccinate_tetanus]:checked').val();
    if(_value == undefined) {
        $('#vaccinate_tetanus').addClass('danger');
        _valid = false;
    }

    if(!_valid) {
        $("html, body").animate({scrollTop: $('#questions').position().top + "px"});
        $('#questions-message').text('There were some errors with your submission.  These have been highlighted below in red.  Please fix these issues, then re-submit your form');
    }

    setTimeout(function() {
        $('#medical-form [type="submit"]').removeClass('disabled');
    }, 2000);

    return _valid;
});