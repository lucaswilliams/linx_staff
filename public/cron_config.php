<?php
$find = array(
    ' ',
    ')',
    'DB_HOST\',',
    'env(\'DB_DATABASE\',',
    'env(\'DB_USERNAME\',',
    'env(\'DB_PASSWORD\',',
    "\n",
    "\r",
    '\''
);

$database = file(str_replace('\\public', '', str_replace('/public', '', (__DIR__))).'/.env');
$parts = array();
foreach($database as $line) {
    if(strcmp(substr($line, 0, 3), 'DB_') == 0) {
        //These are database lines
        $parts[] = str_replace(array("\r", "\n"), '', $line);
    }
}

$config = array();
foreach($parts as $part) {
    $bits = explode('=', $part);
    if(count($bits) > 1) {
        $config[$bits[0]] = $bits[1];
    }
}

//echo '<pre>'; var_dump($config); echo '</pre>';

$time_array = array();

$db = new mysqli($config['DB_HOST'], $config['DB_USERNAME'], $config['DB_PASSWORD'], $config['DB_DATABASE']);