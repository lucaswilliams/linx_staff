<?php
ini_set('max_execution_time', 0);

include 'cron_config.php';

//Get all open jobs
$sql = 'SELECT 
  jobs.id 
  FROM 
  jobs 
  WHERE 
  jobs.end_date IS NULL
  AND NOT EXISTS (SELECT * FROM shifts WHERE shifts.jobs_id = jobs.id AND shifts.start_time >= \''.date('Y-m-d', strtotime('tomorrow')).'\')';

$query = $db->query($sql);
while($row = $query->fetch_assoc()) {
    $shiftSql = 'SELECT * FROM shifts WHERE jobs_id = '.$row['id'].' ORDER BY start_time DESC LIMIT 1';

    $shiftQuery = $db->query($shiftSql);
    while($shiftRow = $shiftQuery->fetch_assoc()) {
        for($i = 1; $i <= 7; $i++) {
            $insShiftSql = "INSERT INTO shifts(jobs_id, start_time, end_time, break_duration) 
              VALUES (".$shiftRow['jobs_id'].", '".date('Y-m-d', strtotime($shiftRow['start_time'].' +'.$i.' days'))."', '".date('Y-m-d', strtotime($shiftRow['end_time'].' +'.$i.' days'))."', ".$shiftRow['break_duration'].")";
            $db->query($insShiftSql);

            $insRateSql = "INSERT INTO shift_tasks(shift_id, task_id, shta_rate)
              SELECT (SELECT max(id) FROM shifts), task_id, shta_rate FROM shift_tasks WHERE shift_id = ".$shiftRow['id'];
            $db->query($insRateSql);
        }
    }
}