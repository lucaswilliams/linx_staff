<?php

/*
 Website Baker Project <http://www.websitebaker.org/>
 Copyright (C) 2004-2006, Ryan Djurovich

 Website Baker is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Website Baker is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Website Baker; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

ini_set('display_errors', 'off');

require('../../config.php');

// Include WB admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require_once(WB_PATH.'/modules/admin.php');
// Load Language file
if(LANGUAGE_LOADED) {
    require_once(WB_PATH.'/modules/massmail/languages/EN.php');
    if(file_exists(WB_PATH.'/modules/massmail/languages/'.LANGUAGE.'.php')) {
        require_once(WB_PATH.'/modules/massmail/languages/'.LANGUAGE.'.php');
    }
}

//echo '<pre>'; var_dump($_POST); echo '</pre>';

$sel_group_id = $_POST['sel_group_id'];
$my_email = $_POST['my_email'];
$cc = $_POST['cc'];
$emails = $_POST['emails'];
$subject = stripslashes($_POST['subject']);
$mail_header = $_POST['mail_header'];
$mail_header = stripslashes($mail_header);
$mail_header = str_replace('[[Date]]', date('j-M-Y'), $mail_header);

$content = $_POST['content'];
//$content = htmlspecialchars($content);
$content = stripslashes($content);

$_SESSION['content'] = $content;
$_SESSION['subject'] = addslashes($subject);
$_SESSION['emails'] = $emails;

$mail_footer = $_POST['mail_footer'];
$mail_footer = stripslashes($mail_footer);
$mail_footer = str_replace('[[Unsubscribe]]', 'Want to stop receiving these emails? <a href="'.WB_URL.'/pages/newsletter.php?u={email}">Unsubscribe here.</a>', $mail_footer);
$mail_footer = str_replace('[[Year]]', date('Y'), $mail_footer);
$mail_footer = str_replace('Â', '', $mail_footer);

//$content = nl2br($content);

//echo htmlspecialchars($content);

// check subject text exsist
if ($subject == ''){
	$admin->print_error('<font color="red"><b>Error sending your newsletter! There was no subject.</b></font>', ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// Check contents and send mail
if(isset($content) and $content != '') {
	$tags = array('<?php', '?>', '<?');
	$blanks = array('','','');

    $pcontent = '<div role="banner">
        <div class="preheader">
            <div class="preheader__inner--inline">
                <div class="snippet">
                    <p>Upcoming Fortnightly Auction</p>
                </div>
    
                <div class="webversion">
                    <p>No Images? <a href="'.WB_URL.'/pages/newsletter.php?id='.$thisemail.'">Click here</a></p>
                </div>
            </div>
        </div>
    </div>'.$mail_header.'<div style="text-align: center;">'.$content.'</div>'.$mail_footer;
	
	//select group id
	$wb_group_id = $database->query("SELECT wb_group_id FROM ".TABLE_PREFIX."mod_massmail_groups WHERE group_id = '$sel_group_id' LIMIT 1");
	if($wb_group_id->numRows() > 0) {
		while($id_group = $wb_group_id->fetchRow()) {
			$group_id = $id_group['wb_group_id'];
		}
	}
	// Insert email into mod_massmail table with the contents and wb group
	$sql = "INSERT INTO ".TABLE_PREFIX."mod_massmail"
		."( `page_id` , `section_id` , `subject` , `from` , `to` , `cc` , `bcc` , `content`, `date`, `group_id` ) "
		. "VALUES ('$page_id','$section_id','".addslashes($subject)."','".$my_email."','"
		.	$my_email."','$cc','".implode(', ',$emails)."','".addslashes($pcontent)."','".time()."','".$group_id."')";
	$database->query($sql) or die('Query issue in "'.htmlspecialchars($sql).'"');
	// print group mail send with success
	
	$max_query = $database->query("SELECT max(id) as the_id from ".TABLE_PREFIX."mod_massmail");
	if($max_query->numRows() > 0)
	{
		while($row = $max_query->fetchRow())
		{
			$thisemail = $row['the_id'];
		}
	}

	$add_mail_headers = '';
	if (OPERATING_SYSTEM=='windows') {
		str_replace("\n","\r\n",$add_mail_headers);
		str_replace("\n","\r\n",$pcontent);
	}

	echo '<h1>Newsletter content</h1>';
    echo $pcontent;
	
	echo '<hr />
	<h3>Subscribers email list</h3>';
	
	foreach($emails as $the_email)
	{	
		$the_email = str_replace(' ', '', $the_email);
		//Look up the first and last names
		$sql = "SELECT first_name, surname from ".TABLE_PREFIX."mod_massmail_addresses where mail_to = '".$the_email."'";
		/*echo $sql;*/
		$firstname = "";
		$surname = "";
		$details_q = $database->query($sql);
		if($details_q->numRows() > 0)
		{
			while($row = $details_q->fetchRow())
			{
				$firstname = $row['first_name'];
				$surname = $row['surname'];
			}

            echo $the_email.'<br />';
            //echo 'smail('.$my_email.', '.$the_email.', '.$subject.', '.$pcontent.', '.$add_mail_headers.', '.$firstname.', '.$surname.');';
            $res=smail($my_email, $the_email, $subject, $pcontent, $add_mail_headers, $firstname, $surname);
            //$res = true;
		}
	}
	//$res=true;
	// Check if there are a send mail error, otherwise say successful
	echo '<hr />';
	if(!$res) {
		$admin->print_error('<font color="red"><b>'.$MMTEXT['ERROR_USER_MAIL'].' </b></font>', ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
	}
	else
	{
		$database->query('DELETE FROM '.TABLE_PREFIX.'mod_massmail_temptable');
		$_SESSION['subject'] = '';
		$_SESSION['content'] = '';
		$admin->print_error('<span class="mailsuccess">Newsletter sent successfully</span>', ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
	}
} else {
	// Error send mail, no content
	$admin->print_error('<font color="red"><b>'.$MMTEXT['ERROR_NO_CONTENT'].'</b></font>', ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}
// Print admin footer
$admin->print_footer();

function smail($fromaddress, $toaddress, $subject, $message, $addheader, $firstname = '', $surname = '') 	{
	global $smartmail;
	$fromaddress = preg_replace('/[\r\n]/', '', $fromaddress);
	$toaddress = preg_replace('/[\r\n]/', '', $toaddress);
	$subject = preg_replace('/[\r\n]/', '', $subject);
	if ($fromaddress=='') {
		$fromaddress = SERVER_EMAIL;
	}
	if(defined('DEFAULT_CHARSET')) {
		$charset = DEFAULT_CHARSET;
	} else {
		$charset='utf-8';
	}
	$headers  = "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=".$charset."\n";
	$headers .= "X-Priority: 3\n";
	$headers .= "X-MSMail-Priority: Normal\n";
	$headers .= "X-Mailer: Website Baker\n";
	$headers .= "From: ".$fromaddress."\n";
	$headers .= "Return-Path: ".$fromaddress."\n";
	$headers .= "Reply-To: ".$fromaddress."\n";
	//$headers .= $addheader."\n";
	$headers .= "\n"; /*extra empty line needed??*/

	/*$message = 'Dear '.$firstname.' '.$surname.",\nHaving trouble reading this email?  <a href=\"'.WB_URL.'/pages/newsletter.php?id=".$thisemail."\">View it online</a><br />\n".$message;*/

	$message = str_replace('{First}', $firstname, $message);
	$message = str_replace('{Last}', $surname, $message);
	$message = str_replace('{to_addr}', $toaddress, $message);
	$message = str_replace('{email}', $toaddress, $message);

	if (OPERATING_SYSTEM=='windows') {
		str_replace("\n","\r\n",$headers);
		str_replace("\n","\r\n",$message);
	}

	//echo $toaddress.', '.$subject.', '.$message.', '.$headers;

	$smartmail->sendMail($toaddress, $subject, $message);
}
?>