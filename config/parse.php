<?php

/*
 * This file is part of Laravel Parse.
 *
 * (c) Graham Campbell <graham@alt-three.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Parse App Id
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse app id.
    |
    */

    'app_id' => 'Ep3GDOB5S6XG62mjyt0jx4EfP56YB3aFIh502Jli',

    /*
    |--------------------------------------------------------------------------
    | Parse Rest Key
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse rest key.
    |
    */

    'rest_key' => 'Ih1IpoVnkDKh5BbQ9rEaYSwPJSDNt8jXZIoMBTN4',

    /*
    |--------------------------------------------------------------------------
    | Parse Master Key
    |--------------------------------------------------------------------------
    |
    | Here you may specify your parse master key.
    |
    */

    'master_key' => 'KfmgcRbwU7sxjZAlKS7efpGv0QcQLa6UPq1yAlLk',

];
