<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffReferral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('referral', 50);
            $table->string('myob_uid', 100);
            $table->string('preferred', 50);
        });

        Schema::table('staff_credentials', function($table) {
            $table->integer('sighted');
            $table->integer('sighted_user_id');
            $table->datetime('sighted_timestamp');
        });

        Schema::table('staff_details', function($table) {
            $table->string('current_location');
            $table->date('date_arrive');
            $table->string('loca_arrive');
            $table->integer('visa_88_days');
            $table->integer('stay_length');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('referral');
            $table->dropColumn('myob_uid');
            $table->dropColumn('preferred');
        });

        Schema::table('staff_credentials', function($table) {
            $table->dropColumn('sighted');
            $table->dropColumn('sighted_user_id');
            $table->dropColumn('sighted_timestamp');
        });

        Schema::table('staff_details', function($table) {
            $table->dropColumn('current_location');
            $table->dropColumn('date_arrive');
            $table->dropColumn('loca_arrive');
            $table->dropColumn('visa_88_days');
            $table->dropColumn('stay_length');
        });
    }
}
