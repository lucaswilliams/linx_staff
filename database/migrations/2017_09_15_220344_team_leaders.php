<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeamLeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::drop('job_roles');
		Schema::drop('roles');

		Schema::create('roles', function(Blueprint $table) {
			$table->increments('role_id');
			$table->integer('jobs_id');
			$table->string('role_name');
			$table->integer('rate_id');
		});

		Schema::create('task_roles', function(Blueprint $table) {
			$table->increments('taro_id');
			$table->integer('task_id');
			$table->integer('role_id');
			$table->integer('rate_id');
		});

		Schema::table('time_tasks', function($table) {
			$table->integer('taro_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
