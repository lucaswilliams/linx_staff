<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OtherStaffDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Add a "confirmed" flag to the users table.
        Schema::table('users', function($table) {
            $table->integer('confirmed')->default(0);
        });

        //Add in some fields about Internationals
        Schema::table('staff_details', function($table) {
            //MYOB number
            $table->string('external_id', 20);

            //Previous Employer and Worker's Compensation
            $table->string('previous_employer', 100);
            $table->integer('previous_employer_years');
            $table->integer('previous_employer_months');
            $table->integer('workers_comp');
            $table->string('workers_comp_details', 500);

            //Internationals
            $table->string('passport_number', 30);
            $table->string('country', 100);
            $table->date('visa_expiry');
            $table->date('visa_expiry_validated');
            $table->string('passport_number_validated', 30);
            $table->integer('vivo_validated');
            //aus_resident will be used for the international page, tax_resident on the tax page.
            $table->integer('tax_resident');
            $table->integer('intl_student');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
