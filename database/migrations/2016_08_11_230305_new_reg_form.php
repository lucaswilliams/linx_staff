<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewRegForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::update('update users set visa_88_days = 2 where intl_student = 1');

        Schema::table('users', function($table) {
            $table->dropColumn('intl_student');
            $table->dropColumn('student_mode');
            $table->dropColumn('holiday_start');
            $table->dropColumn('holiday_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
