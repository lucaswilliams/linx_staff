<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimesheetPayhistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('time_payratehistory', function(Blueprint $table) {
            $table->increments('tpah_id');
            $table->integer('tipa_id');
            $table->integer('shpa_id');
            $table->integer('time_id');
            $table->decimal('tipa_quantity');
        });

        Schema::table('time_history', function($table) {
            $table->integer('user_id');
            $table->integer('shift_id');
            $table->integer('entered_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('time_payratehistory');
    }
}
