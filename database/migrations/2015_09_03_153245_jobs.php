<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Make job, worklist and shift tables
        Schema::create('jobs', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('confirmed');
            $table->integer('supervisor_id');
            $table->integer('role_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('required_staff');
        });

        Schema::create('job_uniforms', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('jobs_id');
            $table->integer('uniform_id');
        });

        Schema::create('job_worklist', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('jobs_id');
            $table->integer('user_id');
        });

        Schema::create('shifts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('jobs_id');
            $table->datetime('start_time');
            $table->datetime('end_time');
            $table->integer('confirmed');
        });

        Schema::create('shift_worklist', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('shift_id');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Quite a few tables to drop this time...
        Schema::drop('jobs');
        Schema::drop('job_uniforms');
        Schema::drop('job_worklist');
        Schema::drop('shifts');
        Schema::drop('shift_worklist');
    }
}
