<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeploymentAddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_worklist', function($table) {
            $table->date('startdate');
        });

        Schema::table('users', function($table) {
            $table->integer('has_medical');
        });

        Schema::create('user_medical', function(Blueprint $table) {
            $table->increments('medical_id');
            $table->integer('user_id');

            $table->text('lost_time')->nullable();
            $table->text('medication')->nullable();
            $table->text('allergies')->nullable();
            $table->text('allergies_medication')->nullable();
            $table->text('allergies_additional')->nullable();
            $table->integer('stand')->default(0);

            $table->integer('vaccinate_hep_a')->default(0);
            $table->integer('vaccinate_hep_b')->default(0);
            $table->integer('vaccinate_tetanus')->default(0);

            $table->integer('current_smoker')->default(0);
            $table->integer('smokes_daily_cigarettes')->default(0);
            $table->integer('smokes_daily_cigars')->default(0);
            $table->integer('smokes_daily_pipes')->default(0);
            $table->integer('ever_smoker')->default(0);
            $table->integer('smoker_quit_year')->default(0);
            $table->integer('smoker_duration')->default(0);

            $table->integer('drink_alcohol')->default(0);
            $table->integer('alcohol_per_week')->default(0);
            $table->string('alcohol_type')->nullable();
            $table->text('sport_hobbies')->nullable();
            $table->text('existing_condition')->nullable();

            $table->string('doctor_1_name')->nullable();
            $table->string('doctor_1_address')->nullable();
            $table->string('doctor_1_phone')->nullable();

            $table->string('doctor_2_name')->nullable();
            $table->string('doctor_2_address')->nullable();
            $table->string('doctor_2_phone')->nullable();
        });

        Schema::create('user_medical_questions', function(Blueprint $table) {
            $table->increments('medques_id');
            $table->integer('medical_id');
            $table->integer('ques_id');
            $table->text('duration');
            $table->text('status');
        });

        Schema::create('medical_questions', function(Blueprint $table) {
            $table->increments('ques_id');
            $table->string('ques_text');
        });

        $questions = array(
            'Asthma, Bronchitis, Pleurisy, Coughing, Breathlessness, Tuberculosis (TB) or other lung complaints',
            'Coughing up blood',
            'Hay fever, sinusitis, severe headaches',
            'Heart disease, heart attack, heat complaints',
            'Chest pain',
            'Blood pressure, heart irregularities, rheumatic fever',
            'Anaemia, Bleeding disorders, other disorders of the blood',
            'Vascular or other blood vessel disorders',
            'Arthritis',
            'Bone or joint problems',
            'Broken bones, factures or dislocations',
            'Any joint pain or injury',
            'Muscle, tendon or ligament problems',
            'Pains, aches, numbness or weakness in the neck, shoulders, arms, hands or fingers',
            'Feet, ankle, knee problems',
            'Strains or sprains',
            'Back complaint / back injury',
            'Gout',
            'Hernia, rupture',
            'Stomach or duodenal ulcers',
            'Chronic indigestion',
            'Intestinal or bowel trouble',
            'Chronic constipation',
            'Haemorrhoids',
            'Vomiting blood',
            'Passing blood in motions',
            'Hepatitis',
            'Diabetes, thyroid problems, gallbladder problems or other gland problems',
            'Any skin conditions',
            'Head injury',
            'Epilepsy, fainting, fits, blackouts or dizzy spells',
            'Chronic headaches or migraines',
            'Vision problems that cannot be corrected by prescription glasses',
            'Ear conditions, hearing loss, deafness or tinnitus (ringing in ears)',
            'Nervous disorders, mental or psychiatric problems, depression',
            'Any sporting, vehicle or work-related illness or injury',
            'Have you worked in any noisy conditions',
            'Have you worked in any dusty conditions',
            'Do you have a fear of heights or confined spaces',
            'Any other illness, injury, health condition or operation not mentioned'
        );

        foreach($questions as $question) {
            DB::table('medical_questions')
                ->insert(['ques_text' => $question]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
