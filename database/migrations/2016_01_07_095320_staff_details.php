<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffDetailsMove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add all the staff_details fields to the users table
        /*Schema::table('users', function($table) {
            $table->integer('gender');
            $table->date('date_of_birth');
            $table->string('title', 4);

            //Addresses
            $table->string('address', 100);
            $table->string('city', 50);
            $table->string('state', 3);
            $table->string('postcode', 4);

            //Phone
            $table->string('telephone', 20);
            $table->string('mobilephone', 20);
            $table->string('emergency_name', 50);
            $table->string('emergency_number', 20);

            //Financial details
            $table->string('account_bsb', 6);
            $table->string('account_number', 10);
            $table->string('account_name', 50);

            $table->string('tfn', 50);
            $table->integer('aus_resident');
            $table->integer('tax_free_threshold');
            $table->integer('senior_tax_offset');
            $table->integer('help_debt');

            //Super Details
            $table->string('provider_id', 36);
            $table->string('super_number', 20);

            //MYOB number
            $table->string('external_id', 20);

            //Previous Employer and Worker's Compensation
            $table->string('previous_employer', 100);
            $table->integer('previous_employer_years');
            $table->integer('previous_employer_months');
            $table->integer('workers_comp');
            $table->string('workers_comp_details', 500);

            //Internationals
            $table->string('passport_number', 30);
            $table->string('country', 100);
            $table->date('visa_expiry');
            $table->date('visa_expiry_validated');
            $table->string('passport_number_validated', 30);
            $table->integer('vivo_validated');
            //aus_resident will be used for the international page, tax_resident on the tax page.
            $table->integer('tax_resident');
            $table->integer('intl_student');

            $table->string('current_location');
            $table->date('date_arrive');
            $table->string('loca_arrive');
            $table->integer('visa_88_days');
            $table->integer('stay_length');

            $table->integer('student_mode');
        });

        //move the data
        $details = DB::table('staff_details')
            ->get();

        foreach($details as $user) {
            DB::table('users')
                ->where('id', '=', $user->id)
                ->update([
                    'gender' => $user->gender,
                    'date_of_birth' => $user->date_of_birth,
                    'title' => $user->title,
                    'address' => $user->address,
                    'city' => $user->city,
                    'state' => $user->state,
                    'postcode' => $user->postcode,
                    'telephone' => $user->telephone,
                    'mobilephone' => $user->mobilephone,
                    'emergency_name' => $user->emergency_name,
                    'emergency_number' => $user->emergency_number,
                    'account_bsb' => $user->account_bsb,
                    'account_number' => $user->account_number,
                    'account_name' => $user->account_name,
                    'tfn' => $user->tfn,
                    'aus_resident' => $user->aus_resident,
                    'tax_free_threshold' => $user->tax_free_threshold,
                    'senior_tax_offset' => $user->senior_tax_offset,
                    'help_debt' => $user->help_debt,
                    'provider_id' => $user->provider_id,
                    'super_number' => $user->super_number,
                    'external_id' => $user->external_id,
                    'previous_employer' => $user->previous_employer,
                    'previous_employer_years' => $user->previous_employer_years,
                    'previous_employer_months' => $user->previous_employer_months,
                    'workers_comp' => $user->workers_comp,
                    'workers_comp_details' => $user->workers_comp_details,
                    'passport_number' => $user->passport_number,
                    'country' => $user->country,
                    'visa_expiry' => $user->visa_expiry,
                    'visa_expiry_validated' => $user->visa_expiry_validated,
                    'passport_number_validated' => $user->passport_number_validated,
                    'vivo_validated' => $user->vivo_validated,
                    'tax_resident' => $user->tax_resident,
                    'intl_student' => $user->intl_student,
                    'current_location' => $user->current_location,
                    'date_arrive' => $user->date_arrive,
                    'loca_arrive' => $user->loca_arrive,
                    'visa_88_days' => $user->visa_88_days,
                    'stay_length' => $user->stay_length,
                    'student_mode' => $user->student_mode
                ]);
        }

        //drop staff_details
        Schema::drop('staff_details');*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
