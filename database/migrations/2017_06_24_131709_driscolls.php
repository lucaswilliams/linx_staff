<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Driscolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Add a flag for whether a worker is signed in
        Schema::table('users', function($table) {
            $table->integer('current_signin');
            $table->integer('current_working');
        });

        Schema::table('job_worklist', function($table) {
            $table->dropColumn('supervisor_id');
        });

        Schema::table('tasks', function($table) {
            $table->integer('task_parent_id');
        });

        Schema::create('job_superlist', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('jobs_id');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
