<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Make up a table to store the notification information.
        Schema::create('notifications', function(Blueprint $table) {
            $table->bigIncrements('noti_id');
            $table->integer('user_to_id');
            $table->integer('user_from_id');
            $table->integer('noti_type'); //0 - on-screen, 1 - email
            $table->string('noti_text');
            $table->integer('noti_read');
            $table->dateTime('noti_stamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Delete the notification tables as we don't need no tables.  Yes we do, we just used a double negative.
        Schema::dropTable('notifications');
    }
}
