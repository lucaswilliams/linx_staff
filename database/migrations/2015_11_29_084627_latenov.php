<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Latenov extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add a "worker mode" to staff details, to show if someone is "at school" or "on holidays"
        Schema::table('staff_details', function($table) {
            $table->integer('student_mode');
        });

        Schema::table('shifts', function($table) {
            $table->integer('payment_type');
            $table->decimal('unit_rate', 6, 2);
        });

        Schema::table('timesheets', function($table) {
            $table->integer('num_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
