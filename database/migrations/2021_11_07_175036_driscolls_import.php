<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DriscollsImport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spreadsheet_import', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('batch_id');
            $table->date('date');
            $table->string('worker_id', 20);
            $table->string('worker', 100);
            $table->decimal('hours');
            $table->string('payband', 50);
            $table->string('location', 100);
            $table->string('task', 100);
            $table->string('user', 100);
            $table->integer('public_holiday')->default(0);
        });

        Schema::table('timesheets', function($table) {
            $table->string('user_name', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('timesheets', function($table) {
            $table->dropColumn('user_name');
        });

        Schema::drop('spreadsheet_import');
    }
}
