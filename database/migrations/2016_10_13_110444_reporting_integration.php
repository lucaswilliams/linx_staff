<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportingIntegration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function(Blueprint $table) {
            $table->increments('report_id');
            $table->string('report_name', 50);
            $table->longText('report_description');
            $table->string('report_filename', 255);
        });

        Schema::create('report_users', function(Blueprint $table) {
            $table->integer('report_id');
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_users');
        Schema::drop('reports');
    }
}
