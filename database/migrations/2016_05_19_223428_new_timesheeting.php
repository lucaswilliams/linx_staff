<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewTimesheeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_tasks', function(Blueprint $table) {
            $table->integer('shift_id');
            $table->integer('task_id');
        });

        Schema::table('roles', function($table) {
            $table->integer('rate_id');
        });

        Schema::create('time_tasks', function(Blueprint $table) {
            $table->increments('tita_id');
            $table->integer('time_id');
            $table->integer('task_id');
            $table->datetime('tita_start')->nullable();
            $table->datetime('tita_finish')->nullable();
            $table->integer('tita_break_duration')->nullable();
            $table->decimal('tita_quantity', 10, 2);

            $table->datetime('tita_user_start')->nullable();
            $table->datetime('tita_user_finish')->nullable();
            $table->integer('tita_user_break_duration')->nullable();
            $table->decimal('tita_user_quantity', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('time_tasks');
        Schema::drop('shift_tasks');
        Schema::table('roles', function($table) {
            $table->dropColumn('rate_id');
        });
    }
}
