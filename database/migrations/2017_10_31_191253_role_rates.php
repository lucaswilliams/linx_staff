<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoleRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('time_tasks', function($table) {
            $table->integer('role_id');
            $table->integer('public_holiday');
            $table->integer('hours_ordinary');
            $table->integer('hours_overtime');
            $table->integer('hours_saturday');
            $table->integer('hours_sunday');
            $table->integer('hours_publicholiday');
        });

        Schema::table('roles', function($table) {
            $table->integer('rate_id');
        });

        DB::statement('update time_tasks set role_id = (select role_id from task_roles where task_roles.taro_id = time_tasks.taro_id)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_tasks', function($table) {
            $table->dropColumn('role_id');
            $table->dropColumn('public_holiday');
            $table->dropColumn('hours_ordinary');
            $table->dropColumn('hours_overtime');
            $table->dropColumn('hours_saturday');
            $table->dropColumn('hours_sunday');
            $table->dropColumn('hours_publicholiday');
        });

        Schema::table('roles', function($table) {
            $table->dropColumn('rate_id');
        });

        DB::statement('update time_tasks set role_id = 0');
    }
}
