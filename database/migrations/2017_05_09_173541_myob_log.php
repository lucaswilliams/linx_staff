<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MyobLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('myob_log', function(Blueprint $table) {
			$table->increments('id');
			$table->string('url', 255);
			$table->longText('params');
			$table->longText('response');
			$table->dateTime('timestamp');
		});

	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('myob_log');
    }
}
