<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChargeRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrates', function($table) {
            $table->dropColumn('client_rate');
        });

        Schema::create('charge_rates', function($table) {
            $table->increments('charge_id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->string('charge_name', 50);
            $table->decimal('charge_rate', 8, 2);
        });

        //Foreign keys.  Let's start preventing things from being deleted unnecessarily.
        Schema::table('charge_rates', function($table) {
            $table->foreign('client_id')->references('id')->on('clients');
        });

        //Make a new migration process later for foreign keys
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //keeping the foreign keys.  kinda irreversible.
    }
}
