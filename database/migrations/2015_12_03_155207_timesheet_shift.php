<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimesheetShift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('timesheets');

        Schema::table('shift_worklist', function($table) {
            $table->longtext('time_notes');
            $table->datetime('start_time');
            $table->datetime('end_time');
            $table->integer('break_duration');
            $table->integer('user_approved');
            $table->integer('super_approved');
            $table->integer('client_approved');
            $table->integer('admin_approved');
            $table->integer('processed');
            $table->decimal('num_units', 6, 2);
            $table->renameColumn('id', 'shwo_id');
        });

        Schema::table('timesheet_history', function($table) {
            $table->renameColumn('time_id', 'shwo_id');
            $table->renameColumn('time_start', 'start_time');
            $table->renameColumn('time_finish', 'end_time');
            $table->integer('user_approved');
            $table->integer('super_approved');
            $table->integer('client_approved');
            $table->integer('admin_approved');
            $table->integer('processed');
            $table->decimal('num_units', 6, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //There's no coming back from this...
    }
}
