<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoomsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('rooms', function($table) {
            $table->dropColumn('room_image');
        });

        Schema::table('roomtypes', function($table) {
            $table->string('roomtype_position_image');
        });

        Schema::create('beds', function(Blueprint $table) {
            $table->increments('beds_id');
            $table->integer('room_id');
            $table->string('beds_name', 30);
        });

        Schema::table('bookings', function($table) {
            $table->dropColumn('room_id');
            $table->integer('confirmed');
            $table->integer('paid_flag');
            $table->datetime('paid_date');
            $table->string('paid_rrn', 50);
        });

        Schema::table('booking_users', function($table) {
            $table->integer('room_id');
            $table->integer('beds_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
