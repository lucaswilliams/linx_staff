<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuestionConditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_questions', function($table) {
            $table->boolean('caqu_conditional')->default(0);
            $table->integer('caqu_conditional_question')->nullable();
            $table->string('caqu_conditional_value');
        });

        Schema::table('users', function($table) {
        	$table->boolean('seasonal_worker')->default(0);
		});

        Schema::table('payrate_rules', function($table) {
        	$table->decimal('paru_client_seasonal', 10, 2)->default(0.0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('campaign_questions', function($table) {
			$table->dropColumn('caqu_conditional');
			$table->dropColumn('caqu_conditional_question');
			$table->dropColumn('caqu_conditional_value');
		});

		Schema::table('users', function($table) {
			$table->dropColumn('seasonal_worker');
		});

		Schema::table('payrate_rules', function($table) {
			$table->dropColumn('paru_client_seasonal');
		});
    }
}
