<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimesheetPayrates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('time_payrates', function(Blueprint $table) {
            $table->increments('tipa_id');
            $table->integer('shpa_id');
            $table->integer('time_id');
            $table->decimal('tipa_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('time_payrates');
    }
}
