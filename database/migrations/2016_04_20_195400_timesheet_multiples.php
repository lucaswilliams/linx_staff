<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimesheetMultiples extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('timesheets', function($table) {
            $table->integer('payment_type');
        });

        Schema::table('time_history', function($table) {
            $table->integer('payment_type');
        });

        $times = DB::table('timesheets')
            ->join('shifts', 'shifts.id', '=', 'timesheets.shift_id')
            ->select('timesheets.time_id', 'shifts.payment_type')
            ->get();

        foreach($times as $time) {
            DB::table('timesheets')
                ->where('time_id', '=', $time->time_id)
                ->update([ 'payment_type' => $time->payment_type]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
