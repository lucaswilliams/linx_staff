<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TitaHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tita_history', function(Blueprint $table) {
        	$table->increments('tihi_id');
        	$table->integer('time_id');
        	$table->integer('tita_id');
        	$table->integer('task_id');
        	$table->integer('role_id');
        	$table->integer('supervisor_id');
        	$table->integer('signed_in_by');
			$table->datetime('tita_start')->nullable();
			$table->datetime('tita_finish')->nullable();
			$table->integer('tita_break_duration')->nullable();
			$table->decimal('tita_quantity', 10, 2);
			$table->integer('public_holiday');
			$table->integer('hours_ordinary');
			$table->integer('hours_overtime');
			$table->integer('hours_saturday');
			$table->integer('hours_sunday');
			$table->integer('hours_publicholiday');

		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tita_history');
    }
}
