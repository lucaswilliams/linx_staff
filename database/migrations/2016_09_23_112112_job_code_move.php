<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobCodeMove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Move the jobs uid from clients to jobs
        Schema::table('jobs', function($table) {
            $table->string('jobs_uid', 50);
        });

        $clients = DB::table('clients')
            ->where('job_uid', '<>', '')
            ->get();

        foreach($clients as $client) {
            DB::table('jobs')
                ->where('client_id', '=', $client->id)
                ->update(['jobs_uid' => $client->job_uid]);
        }

        Schema::table('clients', function($table) {
            $table->dropColumn('job_uid');
        });

        Schema::create('help_articles', function($table) {
            $table->increments('article_id');
            $table->longText('article_content');
        });

        Schema::table('help_system', function($table) {
            $table->integer('article_id')->unsigned();
            $table->dropColumn('help_content');
        });

        Schema::table('help_system', function($table) {
            $table->foreign('article_id')->references('article_id')->on('help_articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
