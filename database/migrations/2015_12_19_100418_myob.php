<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Myob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('myob', function(Blueprint $table) {
            $table->string('cf_guid', 255);
            $table->string('refresh_token', 2047);
            $table->string('access_token', 2047);
            $table->dateTime('token_expire');
            $table->dateTime('last_client_pull');
            $table->string('api_key');
            $table->string('api_secret');
            $table->string('redirect_uri');
            $table->string('myob_uri');
        });

        DB::table('myob')
            ->insert([
                'redirect_uri' => 'http://prototype.ddns.info/linx/staff/public/settings/myob',
                'api_key' => 'fu9y8g4mmq2gf6psa97jnkd8',
                'api_secret' => 'yKupgyMXpxaNYjnMkfhxT45r',
                'myob_uri' => 'https://api.myob.com/accountright/'
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('myob');
    }
}
