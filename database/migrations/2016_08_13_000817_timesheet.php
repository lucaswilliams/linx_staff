<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timesheet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function($table) {
            $table->integer('rate_id');
        });

        DB::update('update tasks set rate_id = (select rate_id from roles where id = tasks.role_id)');

        Schema::table('roles', function($table) {
            $table->dropColumn('rate_id');
        });


        //changes for height and weight in the medical form
        Schema::table('user_medical', function($table) {
            $table->integer('height');
            $table->integer('weight');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
