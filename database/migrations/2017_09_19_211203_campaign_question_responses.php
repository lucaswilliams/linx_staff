<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampaignQuestionResponses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_campaign_questions', function(Blueprint $table) {
			$table->increments('uscq_id');
			$table->integer('user_id');
			$table->integer('caqu_id');
			$table->integer('response_integer');
			$table->integer('response_string');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_campaign_questions');
    }
}
