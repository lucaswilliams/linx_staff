<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetUpTaskRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('insert into roles(role_name, jobs_id) select \'Worker\', id from jobs');
		DB::statement('insert into task_roles(task_id, role_id, rate_id) 
						select ta.task_id, ro.role_id, ta.rate_id
						from tasks ta 
						join roles ro on ro.jobs_id = ta.jobs_id');
		DB::statement('update time_tasks set taro_id = (
			select tr.taro_id 
			from task_roles tr 
			join roles ro on ro.role_id = tr.role_id 
			where tr.task_id = time_tasks.task_id and ro.role_name = \'Worker\'
		)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('TRUNCATE TABLE task_roles');
        DB::statement('TRUNCATE TABLE roles');
    }
}
