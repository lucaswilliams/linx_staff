<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayrateRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('payrate_rules', function($table) {
			$table->integer('paru_staff');
			$table->integer('paru_client');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('payrate_rules', function($table) {
			$table->dropColumn('paru_staff');
			$table->dropColumn('paru_client');
		});
    }
}
