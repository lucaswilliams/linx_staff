<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VariablePayrates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('time_tasks', function($table) {
            $table->integer('rate_id')->unsigned()->nullable();
        });

        Schema::table('payrates', function($table) {
            $table->integer('rate_parent_id')->unsigned()->nullable();
            $table->integer('rate_days')->unsigned()->nullable();
        });

        DB::statement('update time_tasks set rate_id = (select rate_id from roles where roles.role_id = time_tasks.role_id)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_tasks', function($table) {
            $table->dropColumn('rate_id');
        });

        Schema::table('payrates', function($table) {
            $table->dropColumn('rate_parent_id');
            $table->dropColumn('rate_days');
        });
    }
}
