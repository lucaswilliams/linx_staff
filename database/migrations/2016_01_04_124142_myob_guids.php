<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MyobGuids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('super_providers', function(Blueprint $table) {
            $table->increments('super_id');
            $table->string('super_name', 100);
            $table->string('super_guid', 36)->null();
        });

        Schema::table('payrates', function($table) {
            $table->string('rate_guid', 36)->null();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
