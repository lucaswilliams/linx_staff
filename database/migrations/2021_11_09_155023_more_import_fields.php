<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoreImportFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spreadsheet_import', function($table) {
            $table->string('start', 10);
            $table->string('finish', 10);
            $table->string('break', 10);
        });

        Schema::table('timesheets', function($table) {
            $table->integer('import_id')->nullable();
        });

        Schema::table('time_tasks', function($table) {
            $table->integer('import_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spreadsheet_import', function($table) {
            $table->dropColumn('start');
            $table->dropColumn('finish');
            $table->dropColumn('break');
        });
    }
}
