<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function(Blueprint $table) {
            $table->increments('invoice_id');
            $table->integer('client_id');
            $table->date('invoice_date');
            $table->string('invoice_number', 20);
            $table->string('invoice_order', 20);
            $table->decimal('invoice_amount', 15, 2);
            $table->decimal('invoice_tax', 15, 2);
        });

        Schema::table('time_tasks', function($table) {
            $table->integer('invoice_id')->nullable();
        });

        Schema::table('myob', function($table) {
            $table->string('invoice_tax_code', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('time_tasks', function($table) {
            $table->dropColumn('invoice_id');
        });

        Schema::drop('invoices');
    }
}
