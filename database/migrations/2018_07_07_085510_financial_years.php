<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FinancialYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_years', function(Blueprint $table) {
        	$table->increments('year_id');
        	$table->string('year_name', 20);
        	$table->date('year_start');
        	$table->date('year_finish');
		});

        Schema::table('payrate_rules', function($table) {
        	$table->integer('year_id')->unsigned()->nullable();
		});

        DB::table('financial_years')
			->insert([
				'year_name' => '2017/2018',
				'year_start' => '2017-07-01',
				'year_finish' => '2018-07-01'
			]);

		DB::table('financial_years')
			->insert([
				'year_name' => '2018/2019',
				'year_start' => '2018-07-01',
				'year_finish' => '2019-07-01'
			]);

		DB::statement("INSERT INTO payrate_rules(rate_id, paru_frequency, paru_hours, paru_guid, paru_staff, paru_client)
			SELECT id, -1, 0, rate_guid, staff_rate, client_rate FROM payrates");
		DB::statement("UPDATE payrate_rules SET year_id = (SELECT year_id from financial_years WHERE year_name = '2017/2018')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('financial_years');
        Schema::table('payrate_rules', function($table) {
        	$table->dropColumn('year_id');
		});
    }
}
