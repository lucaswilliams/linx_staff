<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Credentials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Make the credentials table
        Schema::create('credentials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->integer('has_expiry');
            $table->integer('has_description');
        });

        //Make the staff_credentials table
        Schema::create('staff_credentials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id'); //to staff details
            $table->integer('credential_id'); //to credentials
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('staff_credentials');
        Schema::drop('credentials');
    }
}
