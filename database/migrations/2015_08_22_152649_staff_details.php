<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create a staff_details table for staff members' details
        Schema::table('users', function ($table) {
            $table->integer('gender');
            $table->date('date_of_birth');
            $table->string('title', 4);

            //Addresses
            $table->string('address', 100);
            $table->string('city', 50);
            $table->string('state', 3);
            $table->string('postcode', 4);

            //Phone
            $table->string('telephone', 20);
            $table->string('mobilephone', 20);
            $table->string('emergency_name', 50);
            $table->string('emergency_number', 20);

            //Financial details
            $table->string('account_bsb', 6);
            $table->string('account_number', 10);
            $table->string('account_name', 50);

            $table->string('tfn', 50);
            $table->integer('aus_resident');
            $table->integer('tax_free_threshold');
            $table->integer('senior_tax_offset');
            $table->integer('help_debt');

            //Super Details
            $table->string('provider_id', 36);
            $table->string('super_number', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
