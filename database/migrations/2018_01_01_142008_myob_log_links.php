<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MyobLogLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('myob_log', function($table) {
            $table->integer('user_id');
            $table->integer('time_id');
            $table->integer('resolved')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('myob_log', function($table) {
            $table->dropColumn('user_id');
            $table->dropColumn('time_id');
            $table->dropColumn('resolved');
        });
    }
}
