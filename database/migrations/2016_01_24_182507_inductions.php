<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //First, some modifications to the user table
        Schema::table('users', function($table) {
            $table->integer('has_transport');
            $table->integer('num_in_group');
            $table->integer('share_details');
        });

        //Make the inductions things up
        Schema::create('inductions', function(Blueprint $table) {
            $table->increments('ind_id');
            $table->string('ind_name', 50);
            $table->text('ind_content');
        });

        Schema::create('questions', function(Blueprint $table) {
            $table->increments('ques_id');
            $table->integer('ind_id');
            $table->text('ques_text');
        });

        Schema::create('options', function(Blueprint $table) {
            $table->increments('opt_id');
            $table->integer('ques_id');
            $table->string('opt_text', 100);
            $table->integer('opt_correct');
        });

        Schema::create('userinductions', function(Blueprint $table) {
            $table->increments('usin_id');
            $table->integer('user_id');
            $table->integer('ind_id');
            $table->timestamp('usin_created');
            $table->timestamp('usin_completed');
        });

        Schema::create('usinques', function(Blueprint $table) {
            $table->increments('uiqu_id');
            $table->integer('usin_id');
            $table->integer('ques_id');
            $table->integer('opti_id');
            $table->integer('correct');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
