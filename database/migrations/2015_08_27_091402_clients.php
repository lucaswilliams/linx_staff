<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //make the clients table
        Schema::create('clients', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('abn', 20);
            $table->string('telephone', 20);
            $table->string('email');
            $table->string('address', 100);
            $table->string('city', 50);
            $table->string('state', 3);
            $table->string('postcode', 4);
            $table->integer('status');
            $table->string('external_id', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('clients');
    }
}
