<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payslips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslips', function(Blueprint $table) {
            $table->increments('slip_id');
            $table->integer('user_id');
            $table->string('slip_number', 20);
            $table->date('slip_date');
            $table->decimal('slip_gross', 15, 2);
            $table->decimal('slip_net', 15, 2);
            $table->date('slip_start');
            $table->date('slip_end');
        });

        Schema::create('payslip_item', function(Blueprint $table) {
            $table->increments('slit_id');
            $table->integer('slip_id');
            $table->string('slit_name', 50);
            $table->integer('slit_type');
            $table->decimal('slit_amount', 15, 2);
            $table->decimal('slit_ytd', 15, 2);
        });

        Schema::table('time_tasks', function($table) {
            $table->integer('slit_id');
        });

        Schema::table('myob', function($table) {
            $table->date('last_payslip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('myob', function($table) {
            $table->dropColumn('last_payslip');
        });

        Schema::table('time_tasks', function($table) {
            $table->dropColumn('slit_id');
        });

        Schema::drop('payslip_items');
        Schema::drop('payslips');
    }
}
