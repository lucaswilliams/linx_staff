<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientMyobData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_emails', function(Blueprint $table) {
            $table->increments('emai_id');
            $table->integer('client_id')->unsigned();
            $table->string('emai_address', 255);
            $table->boolean('emai_primary');
        });

        Schema::table('client_emails', function($table) {
            $table->foreign('client_id')->references('id')->on('clients');
        });

        Schema::table('clients', function($table) {
            $table->string('income_account', 100);
        });

        $clients = DB::table('clients')
            ->select('id', 'email')
            ->get();
        foreach($clients as $client) {
            DB::table('client_emails')
                ->insert([
                    'client_id' => $client->id,
                    'emai_address' => $client->email,
                    'emai_primary' => true
                ]);
        }

        Schema::table('clients', function($table) {
            $table->dropColumn('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_emails');
        Schema::table('clients', function($table) {
            $table->dropColumn('income_account');
        });
    }
}
