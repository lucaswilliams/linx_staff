<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Make a table to store payment providers (ID and name until I have details)
        Schema::create('payment_providers', function(Blueprint $table) {
            $table->increments('provider_id');
            $table->string('provider_name');
        });

        //Make a table representing payments that may or may not have been made
        Schema::create('payments', function(Blueprint $table) {
            $table->increments('payment_id');
            $table->integer('user_id');
            $table->integer('entered_user_id');
            $table->integer('provider_id');
            $table->dateTime('payment_made');
            $table->dateTime('payment_received');
            $table->decimal('payment_amount');
            $table->string('payment_rrn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
        Schema::drop('payment_providers');
    }
}
