<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobsExtras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function($table) {
            $table->integer('client_id');
        });

        Schema::table('shifts', function($table) {
            $table->integer('role_id');
            $table->integer('break_duration');
        });

        Schema::table('shift_worklist', function($table) {
            $table->integer('task_id');
        });

        Schema::create('tasks', function(Blueprint $table) {
            $table->increments('task_id');
            $table->string('task_name');
            $table->string('task_desc');
        });

        Schema::create('role_tasks', function(Blueprint $table) {
            $table->integer('role_id');
            $table->integer('task_id');
        });

        Schema::create('timesheets', function(Blueprint $table) {
            $table->increments('time_id');
            $table->integer('staff_id');
            $table->integer('task_id');
            $table->integer('user_id');
            $table->dateTime('time_start');
            $table->dateTime('time_finish');
            $table->string('time_notes');
            $table->timestamps();
        });

        Schema::create('timesheet_history', function(Blueprint $table) {
            $table->increments('tihi_id');
            $table->integer('time_id');
            $table->integer('task_id');
            $table->integer('user_id');
            $table->dateTime('time_start');
            $table->dateTime('time_finish');
            $table->string('time_notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function($table) {
            $table->dropColumn('client_id');
        });

        Schema::table('shifts', function($table) {
            $table->dropColumn('role_id');
            $table->dropColumn('break_duration');
        });

        Schema::table('shift_worklist', function($table) {
            $table->dropColumn('task_id');
        });

        Schema::drop('tasks');

        Schema::drop('role_tasks');

        Schema::drop('timesheets');

        Schema::drop('timesheet_history');
    }
}
