<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TasksChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::drop('role_tasks');

        Schema::table('tasks', function($table) {
            $table->integer('role_id');
        });

        Schema::table('payrates', function($table) {
            $table->integer('rate_type')->default(0);
        });

        Schema::table('shifts', function($table) {
            $table->integer('rate_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
