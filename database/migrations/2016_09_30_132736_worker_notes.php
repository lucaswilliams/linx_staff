<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkerNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notes', function(Blueprint $table) {
            $table->increments('note_id');
            $table->longText('note_content');
            $table->dateTime('note_stamp');
            $table->integer('note_user_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });

        Schema::table('user_notes', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('note_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_notes');
    }
}
