<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reid2019 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('reid-1')->default(0);
            $table->string('reid-1-1', 100);
            $table->string('reid-1-2', 100);
            $table->string('reid-1-3', 100);
            $table->integer('reid-2')->default(0);
            $table->string('reid-2-1', 100);
            $table->string('reid-2-2', 100);
            $table->string('reid-2-3', 100);
            $table->integer('reid-3')->default(0);
            $table->string('reid-3-1', 100);
            $table->string('reid-3-2', 100);
            $table->integer('reid-4')->default(0);
            $table->string('reid-4-1', 100);
            $table->string('reid-5', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
