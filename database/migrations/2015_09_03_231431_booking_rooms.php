<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roomtypes', function(Blueprint $table) {
            $table->increments('roomtype_id');
            $table->string('roomtype_name');
            $table->string('roomtype_image');
        });

        Schema::create('rooms', function(Blueprint $table) {
            $table->increments('room_id');
            $table->integer('roomtype_id');
            $table->string('room_name');
            $table->integer('room_capacity');
            $table->string('room_image');
        });

        Schema::create('bookings', function(Blueprint $table) {
            $table->increments('booking_id');
            $table->integer('user_id');
            $table->integer('room_id');
            $table->integer('roomtype_id');
            $table->integer('number_in_group');
            $table->integer('separate_rooms');
            $table->integer('bunk_room');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
        Schema::drop('rooms');
        Schema::drop('roomtypes');
    }
}
