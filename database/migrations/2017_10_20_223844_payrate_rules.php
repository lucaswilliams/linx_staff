<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PayrateRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('payrate_rules', function(Blueprint $table) {
            $table->increments('paru_id');
            $table->integer('rate_id');
            $table->integer('paru_frequency');
            $table->integer('paru_hours');
            $table->string('paru_guid', 36);
        });

        Schema::create('public_holidays', function(Blueprint $table) {
            $table->increments('holi_id');
            $table->date('holi_date');
            $table->string('holi_name');
            $table->longText('holi_desc')->nullable();
            $table->string('holi_state', 10)->nullable();
        });

        $holidays = array(
            array(
                'holi_date' => '2017-12-25',
                'holi_name' => 'Christmas Day'
            ),
            array(
                'holi_date' => '2017-12-26',
                'holi_name' => 'Boxing Day'
            ),
            array(
                'holi_date' => '2018-01-01',
                'holi_name' => 'New Years Day'
            ),
        );

        DB::table('public_holidays')
            ->insert($holidays);

        DB::table('public_holidays')
            ->insert(
                array(
                    'holi_date' => '2017-11-06',
                    'holi_name' => 'Recreation Day',
                    'holi_state' => 'TAS'
                )
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
