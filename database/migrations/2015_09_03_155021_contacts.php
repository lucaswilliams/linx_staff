<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //make a link table for contacts.
        Schema::create('contacts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('client_id');
        });

        Schema::table('users', function($table) {
            $table->integer('is_contact')->default(0);
            $table->integer('is_supervisor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('contacts');
    }
}
