<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SigninSignout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function($table) {
            //Kill some old fields
            $table->dropColumn('supervisor_id');
            $table->dropColumn('client_supervisor_id');
        });

        Schema::table('job_worklist', function($table) {
            $table->integer('supervisor_id');
        });

        Schema::create('shift_payrates', function(Blueprint $table){
            //these are the ones to be set up by a supervisor
            $table->increments('shpa_id');
            $table->integer('shift_id');
            $table->integer('rate_id');
            $table->decimal('shpa_rate', 6, 2);
        });

        Schema::drop('timesheet_history');
        Schema::drop('shift_worklist');

        Schema::create('timesheets', function(Blueprint $table) {
            //
            $table->increments('time_id');
            $table->integer('user_id');
            $table->integer('shift_id');
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->integer('break_duration');
            $table->longText('time_notes');
            $table->integer('rate_id');
            $table->decimal('num_units', 4, 2);
            $table->integer('approved_user');
            $table->integer('approved_super');
            $table->integer('processed');
            $table->datetime('signed_in');
            $table->datetime('signed_out');
        });

        Schema::create('time_history', function(Blueprint $table) {
            //
            $table->integer('time_id');
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->integer('break_duration');
            $table->longText('time_notes');
            $table->integer('rate_id');
            $table->decimal('num_units', 4, 2);
            $table->integer('approved_user');
            $table->integer('approved_super');
            $table->integer('processed');
            $table->datetime('signed_in');
            $table->datetime('signed_out');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
