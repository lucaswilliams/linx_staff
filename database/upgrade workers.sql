insert into client_users(client_id, user_id, level)
select cu.client_id, us.id, cu.level
from client_users cu, users us
where cu.user_id = 8531 and us.id in (5014, 6932);

insert into report_users(report_id, user_id)
select cu.report_id, us.id
from report_users cu, users us
where cu.user_id = 8531 and us.id in (5014, 6932);

update users set is_supervisor = 1 where id in (5014, 6932);