<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminUserSeeder extends Seeder
{
    /**
     * Insert the default administation user
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'given_name' => 'Lucas',
            'surname' => 'Williams',
            'email' => 'lucas@lucas-williams.com',
            'username' => 'lucas.williams',
            'password' => bcrypt('Pr0m3th3u5'),
            'level' => 3,
            'confirmed' => 1
        ]);
    }
}
