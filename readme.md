## Linx Employment Timesheet Management System

A system that is loosely based off the ideas in the Staffbooks system, but set out in a way that works better for the agriculture industry and the flexible changes that need to happen when recording timesheets.
To best serve the growing needs of Linx Employment, a new staff and timesheet management system are required, to replace the ill-fitting Staffbooks system.  This new system will grant additional flexibility to the business, and push a lot of the responsibility of correct data entry back to the staff members and their supervisors, leaving the administration team with more time to focus on the important things.

### License

Steal it, and we'll come after you.